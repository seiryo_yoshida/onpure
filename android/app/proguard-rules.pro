# Add project specific ProGuard rules here.

# liblinphone
-keep class org.linphone.** { *; }

# glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# Google Play Service
#-keep class com.google.android.gms.** { *; }
#-keep class * extends java.util.ListResourceBundle {
#    protected Object[][] getContents();
#}
# Keep SafeParcelable value, needed for reflection. This is required to support backwards
# compatibility of some classes.
#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#    public static final *** NULL;
#}
# Keep the names of classes/members we need for client functionality.
#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#    @com.google.android.gms.common.annotation.KeepName *;
#}
# Needed for Parcelable/SafeParcelable Creators to not get stripped
#-keepnames class * implements android.os.Parcelable {
#    public static final ** CREATOR;
#}
# -dontwarn com.google.android.gms.**

-dontwarn okio.**
-dontwarn org.apache.**
-dontwarn org.linphone.**
-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder

-keep class org.eclipse.paho.client.mqttv3.** { *; }
