package jp.co.seiryodenki.professionalptt.model;

import android.location.Location;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyMqttMessage {

  @SerializedName("cmd")
  private String cmd;

  @SerializedName("call_type")
  private String call_type;

  @SerializedName("call_id")
  private String call_id;

  @SerializedName("call_list")
  private List<String> call_list;

  @SerializedName("call_name")
  private String call_name;

  @SerializedName("area")
  private List<String> area;

  @SerializedName("priority")
  private Integer priority;

  @SerializedName("from")
  private String from;

  @SerializedName("latitude")
  private Double latitude;

  @SerializedName("longitude")
  private Double longitude;

  @SerializedName("radius")
  private Double radius;

  /**
   *
   *
   * <pre>
   * 状態に応じて以下のメッセージを設定する
   * "START":通話開始時
   * "BUSY":通話拒否
   * "OUT":通話抜け
   * "BYE":通話終了
   * "PARK":通話保留
   * </pre>
   */
  @SerializedName("status")
  private String status;

  @SerializedName("report")
  private String report;

  @SerializedName("body")
  private String body;

  @SerializedName("talk")
  private String talk; // [端末A-ID]

  /**
   *
   *
   * <pre>
   * "ZONE" or "AREA" or "BLOCK"
   * "ZONE"は、ActivityZoen以外の利用制限
   * "AREA"は、作業場所以外の利用制限
   * "BLOCK"は、サーバからの利用制限
   * </pre>
   */
  @SerializedName("type")
  private String type;

  /** "PTTALL" or GROUP-ID or 相手端末-ID */
  @SerializedName("id")
  private String id;

  /**
   *
   *
   * <pre>
   * "ON" or "OFF"
   * "ON"の場合には、端末利用制限開始
   * "OFF"の場合には、端末利用制限解除
   * </pre>
   */
  @SerializedName("ctl")
  private String ctl;

  @SerializedName("flag")
  private String flag;

  @SerializedName("att_file_id")
  private String att_file_id;

  @SerializedName("dummy")
  private String dummy;

  @SerializedName("gps")
  private String gps;

  public MyMqttMessage() {}

  public String getCmd() {
    return cmd;
  }

  public void setCmd(String cmd) {
    this.cmd = cmd;
  }

  public String getCallType() {
    return call_type;
  }

  public void setCallType(String call_type) {
    this.call_type = call_type;
  }

  public String getCallId() {
    return call_id;
  }

  public void setCallId(String call_id) {
    this.call_id = call_id;
  }

  public List<String> getCallList() {
    return call_list;
  }

  public void setCallList(List<String> call_list) {
    this.call_list = call_list;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  // 2018/5/28 V0.4対応　近隣通話時の半径設定
  public void setRadius(Double radius) { this.radius = radius; }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getTalk() {
    return talk;
  }

  public void setTalk(String talk) {
    this.talk = talk;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCtl() {
    return ctl;
  }

  public void setCtl(String ctl) {
    this.ctl = ctl;
  }

  public String getReport() {
    return report;
  }

  public void setReport(String report) {
    this.report = report;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public String getAttFileId() {
    return att_file_id;
  }

  public void setAttFileId(String att_file_id) {
    this.att_file_id = att_file_id;
  }

  public String getDummy() {
    return dummy;
  }

  public void setDummy(String dummy) {
    this.dummy = dummy;
  }

  public String getGps() {
    return gps;
  }

  public void setGps(String gps) {
    this.gps = gps;
  }

  @Override
  public String toString() {
    return "cmd:"
        + cmd
        + ", call_id:"
        + call_id
        + ", call_list:"
        + ((call_list == null) ? 0 : call_list.toString())
        + " ... ";
  }

  public static String getMessageString(
          @NonNull String message, String myTerminalId, Location location) {
    return String.format(
      "%1$s\n terminal_id = %2$s\n, %3$s",
      message, myTerminalId, RemoteNotificationInfo.getLocationStringLite(location));
  }

  public static String getMessageString(
    @NonNull String message, @NonNull String myTerminalId) {
    return String.format("%1$s\n terminal_id = %2$s", message, myTerminalId);
  }

  public String getCall_name() {
    return call_name;
  }

  public void setCall_name(String call_name) {
    this.call_name = call_name;
  }

  public List<String> getArea() {
    return area;
  }

  public void setArea(ArrayList<String> areaList) {
    this.area = areaList;
  }
}
