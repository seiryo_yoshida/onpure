package jp.co.seiryodenki.professionalptt.ui;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.List;
import java.util.Set;
import jp.co.seiryodenki.professionalptt.Constants;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.HomeActivity;
import jp.co.seiryodenki.professionalptt.MessageActivity;
import jp.co.seiryodenki.professionalptt.PhoneBookActivity;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfoEntity;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.view.GroupItemRecyclerViewAdapter;
import jp.co.seiryodenki.professionalptt.view.PromptDialog;
import jp.co.seiryodenki.professionalptt.view.UserItemRecyclerViewAdapter;

public class AddressListFragment extends Fragment {

  private static final String TAG = AddressListFragment.class.getSimpleName();

  private static final String ARG_ADDRESS_TYPE = "address_type";
  private static final String ARG_USER_PRIORITY = "arg_user_priority";

  private Realm realm;

  private Switch switchMultipleSelect;

  private RecyclerView recyclerView;
  private UserItemRecyclerViewAdapter userListAdapter;
  private GroupItemRecyclerViewAdapter groupListAdapter;

  //マルチグループ通話ボタン
  private Button fabMulti = null;

  private int userPriority = 0;

  public interface OnSelectPromptListener {
    void onSelectPrompt(CallType callType);
  }

  public interface OnClickUserListener {
    void onClickUser(UserInfoEntity user, boolean isMessage);
  }

  public interface OnCheckGroupChangedListener {
    void onCheckGroup(GroupInfoEntity group, boolean isChecked);
  }

  public interface OnClickGroupListener {
    void onClickGroup(GroupInfoEntity group, boolean isMessage);
  }

  public interface OnLongClickGroupListener {
    void onLongClickGroup(boolean isMultiSelected);
  }

  public AddressListFragment() {}

  /** Returns a new instance of this fragment for the given section number. */
  public static AddressListFragment newInstance(
      PhoneBookActivity.ADDRESS_TYPE type, int userPriority) {
    AddressListFragment fragment = new AddressListFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_ADDRESS_TYPE, type.ordinal());
    args.putInt(ARG_USER_PRIORITY, userPriority);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_phonebook, container, false);

    // get views
    recyclerView = view.findViewById(R.id.recycler_list);
    //FloatingActionButton fabMulti = view.findViewById(R.id.fab);
    fabMulti = getActivity().findViewById(R.id.fab);
    switchMultipleSelect = view.findViewById(R.id.switch_multiple_group);

    // set recycler view
    LinearLayoutManager llm = new LinearLayoutManager(getContext());
    llm.setOrientation(LinearLayoutManager.VERTICAL);
    recyclerView.setLayoutManager(llm);
    recyclerView.setOnTouchListener(
        (v, e) -> {
          CallService.broadcastTouchEvent(getContext(), e);
          return false;
        });

    // create adapter
    int type = -1;
    if (getArguments() != null) {
      type = getArguments().getInt(ARG_ADDRESS_TYPE);
      userPriority = getArguments().getInt(ARG_USER_PRIORITY);
    }
    switch (PhoneBookActivity.ADDRESS_TYPE.getType(type)) {
      case USER:
        userListAdapter =
            new UserItemRecyclerViewAdapter(
                (user, isMessage) -> {
                  if (isMessage) {
                    startActivity(
                        MessageActivity.getMessageIntent(
                            getContext(), user.getTerminalId(), user.getUserName(), userPriority));
                  } else {
                    startActivity(HomeActivity.getCallIntent(
                            getContext(), CallType.CALL, new String[]{user.getTerminalId()}, false));
                    getActivity().finish();
                  }
                });
        recyclerView.setAdapter(userListAdapter);
        userListAdapter.setSelectedPosition(0);
        break;
      case GROUP:
        groupListAdapter =
            new GroupItemRecyclerViewAdapter(
                isMultiSelected -> {
                  switchMultipleSelect.setChecked(isMultiSelected);
                  if (isMultiSelected) {
                    fabMulti.setVisibility(View.VISIBLE);
                    fabMulti.setClickable(true);
                    //マルチグループ通話ボタンの使用可否を切り替える
                    chgMultiButton();
                  } else {
                    fabMulti.setVisibility(View.INVISIBLE);
                    fabMulti.setClickable(false);
                  }
                  //fabMulti.setVisibility(isMultiSelected ? View.VISIBLE : View.INVISIBLE);
                },
                (group, isMessage) -> {
                  if (!isMessage) {
                    dialogGroupCall(group);
                    return;
                  }
                  startActivity(
                      MessageActivity.getMessageIntent(
                          getContext(), group.getGroupId(), group.getGroupName(), userPriority));
                  getActivity().finish();
                },
                (group, isChecked) -> {
                  if (realm != null) {
                    realm.executeTransaction(
                        transaction -> group.setSelected(isChecked));
                    //マルチグループ通話ボタンの使用可否を切り替える
                    chgMultiButton();
                  }
                });
        recyclerView.setAdapter(groupListAdapter);
        switchMultipleSelect.setVisibility(View.VISIBLE);
        break;
    }

    fabMulti.setOnClickListener(
        viewFab -> dialogMultiGroupCall(groupListAdapter.getSelectedGroups()));

    switchMultipleSelect.setOnCheckedChangeListener(
        (buttonView, isChecked) -> {
          groupListAdapter.changeModeMultipleGroup(isChecked);
          fabMulti.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
          if (isChecked) {
            chgMultiButton();
          }
        });

    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    realm = Realm.getDefaultInstance();
    if (userListAdapter != null) {
      RealmResults<UserInfoEntity> resUser = realm.where(UserInfoEntity.class).findAll();
      userListAdapter.setUserList(resUser);
    }
    if (groupListAdapter != null) {
      RealmResults<GroupInfoEntity> resGroup = realm.where(GroupInfoEntity.class).findAll();
      groupListAdapter.setGroupList(resGroup);
    }
  }

  @Override
  public void onStop() {
    if (realm != null) {
      realm.close();
    }
    super.onStop();
  }

  public void notifyKeyEvent(PhoneBookActivity.ADDRESS_TYPE addressType, int keyCode) {
    if (keyCode == Constants.KEY_ARROW_DOWN) {
      switch (addressType) {
        case USER:
          moveUserSelectedItem(1);
          break;
        case GROUP:
          moveGroupSelectedItem(1);
          break;
      }
    } else if (keyCode == Constants.KEY_ARROW_UP) {
      switch (addressType) {
        case USER:
          moveUserSelectedItem(-1);
          break;
        case GROUP:
          moveGroupSelectedItem(-1);
          break;
      }
    } else if ((keyCode == Constants.KEY_PTT) || (keyCode == Constants.KEY_PTT_KYOCERA)  || (keyCode == Constants.KEY_PTT_ATOM)   || (keyCode == Constants.KEY_PTT_CAT) || (keyCode == Constants.KEY_HEADSETHOOK)){
      switch (addressType) {
        case USER:
          UserInfoEntity user = userListAdapter.getCurrentInfo();
          if (user != null) {
            startActivity(HomeActivity.getCallIntent(
                    getContext(), CallType.CALL, new String[]{user.getTerminalId()}, true));
          }
          break;
        case GROUP:
          if (switchMultipleSelect.isChecked()) {
            List<GroupInfoEntity> groups = groupListAdapter.getSelectedGroups();
            if (groups != null && groups.size() > 0) {
              performMultiGroupCall(groups, CallType.MULTI_GROUP_CALL, true);
            }
          } else {
            GroupInfoEntity group = groupListAdapter.getCurrentInfo();
            if (group != null) {
              performGroupCall(group, CallType.GROUP_CALL, true);
            }
          }
          break;
      }
    }
  }

  private void moveUserSelectedItem(int diffPosition) {
    if (userListAdapter != null) {
      int position = userListAdapter.getSelectedPosition();
      int length = userListAdapter.getItemCount();
      position += diffPosition;
      if (position < 0) {
        position = 0;
      } else if (position >= length) {
        position = length - 1;
      }
      userListAdapter.setSelectedPosition(position);
      recyclerView.scrollToPosition(position);
    }
  }

  private void moveGroupSelectedItem(int diffPosition) {
    if (groupListAdapter != null) {
      int position = groupListAdapter.getSelectedPosition();
      int length = groupListAdapter.getItemCount();
      position += diffPosition;
      if (position < 0) {
        position = 0;
      } else if (position >= length) {
        position = length - 1;
      }
      groupListAdapter.setSelectedPosition(position);
      recyclerView.scrollToPosition(position);
    }
  }

  private void dialogMultiGroupCall(final List<GroupInfoEntity> groups) {
    // 2018/5/28 V0.4対応 マルチグループ未選択で起動時に強制終了発生
    if(groups.size() > 0) {
      if (isSatisfiedPriority(CallType.MULTI_GROUP_CALL_ENFORCEMENT)) {
        PromptDialog dialog =
                new PromptDialog(
                        getActivity(),
                        callType -> performMultiGroupCall(groups, callType, false));
        dialog.showDialog(CallType.MULTI_GROUP_CALL, CallType.MULTI_GROUP_CALL_ENFORCEMENT);
      } else {
        performMultiGroupCall(groups, CallType.MULTI_GROUP_CALL, false);
      }
    }
  }

  private void performMultiGroupCall(
      final List<GroupInfoEntity> groups, CallType callType, boolean isImmediate) {
    String[] ids = new String[groups.size()];
    for (int i = 0; i < groups.size(); i++) {
      GroupInfoEntity group = groups.get(i);
      ids[i] = group.getGroupId();
    }
    startActivity(HomeActivity.getCallIntent(getContext(), callType, ids, isImmediate));
    getActivity().finish();
  }

  private void dialogGroupCall(final GroupInfoEntity group) {
    if (isSatisfiedPriority(CallType.GROUP_CALL_ENFORCEMENT)) {
      PromptDialog dialog =
          new PromptDialog(
              getActivity(),
              callType -> performGroupCall(group, callType, false));
      dialog.showDialog(CallType.GROUP_CALL, CallType.GROUP_CALL_ENFORCEMENT);
    } else {
      performGroupCall(group, CallType.GROUP_CALL, false);
    }
  }

  private void performGroupCall(
      final GroupInfoEntity group, CallType callType, boolean isImmediate) {
    startActivity(HomeActivity.getCallIntent(getContext(), callType, new String[]{group.getGroupId()}, isImmediate));
    getActivity().finish();
  }

  private boolean isSatisfiedPriority(CallType callType) {
    Set<String> typeSet =
        TerminalInfo.getCallType(
            getContext(), PreferenceManager.getDefaultSharedPreferences(getContext()));
    for (String type : typeSet) {
      if (type.equals(callType.toString())) {
        return true;
      }
    }
    return false;
  }

  /**
   * マルチグループ通話ボタンの使用可能／不可を切り替える
   */
  public void chgMultiButton() {
    //チェックされているグループがあるか確認
    List<GroupInfoEntity> groups = groupListAdapter.getSelectedGroups();
    boolean isSelect = false;
    if (groups != null && groups.size() > 0) {
      isSelect = true;
    }
    if (fabMulti != null) {
      fabMulti.setClickable(isSelect);
      fabMulti.setEnabled(isSelect);
    }
  }

}
