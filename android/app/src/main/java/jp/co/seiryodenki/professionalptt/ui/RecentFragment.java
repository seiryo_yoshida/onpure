package jp.co.seiryodenki.professionalptt.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.HomeActivity;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.RecentActivity;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.RecentInfo;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.view.RecentItemRecyclerViewAdapter;

public class RecentFragment extends Fragment {

  public interface OnClickListener {
    void onClick(RecentInfo recentInfo);
  }

  private static final String ARG_SECTION_NUMBER = "section_number";
  private static final String ARG_ADDRESS_TYPE = "address_type";

  public RecentFragment() {}

  /** Returns a new instance of this fragment for the given section number. */
  public static RecentFragment newInstance(RecentActivity.CALL_TYPE type, int sectionNumber) {
    RecentFragment fragment = new RecentFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    args.putInt(ARG_ADDRESS_TYPE, type.ordinal());
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup recent_container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_recent, recent_container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    Realm realm = Realm.getDefaultInstance();

    LinearLayoutManager llm = new LinearLayoutManager(getContext());
    llm.setOrientation(LinearLayoutManager.VERTICAL);

    RecyclerView recyclerView = view.findViewById(R.id.recent_recycler_list);
    recyclerView.setOnTouchListener(
        (v, e) -> {
          CallService.broadcastTouchEvent(getContext(), e);
          return false;
        });
    recyclerView.setLayoutManager(llm);

    RealmResults<RecentInfo> recentInfos =
        RecentInfo.findAllAsync(realm, getArguments().getInt(ARG_ADDRESS_TYPE));
    RecentItemRecyclerViewAdapter adapter =
        new RecentItemRecyclerViewAdapter(
            recentInfos,
            recentInfo -> {
              if (recentInfo.getCallType() == CallType.MULTI_GROUP_CALL_ENFORCEMENT || recentInfo.getCallType() == CallType.MULTI_GROUP_CALL) {
                startActivity(HomeActivity.getCallIntent(getContext(),
                  recentInfo.getCallType(),
                  recentInfo.getCallList(), false));
              } else {
                startActivity(HomeActivity.getCallIntent(getContext(),
                  recentInfo.getCallType(),
                  new String[]{recentInfo.getCallId()}, false));
              }
              getActivity().finish();
            });
    recyclerView.setAdapter(adapter);
    RealmChangeListener<RealmResults<RecentInfo>> changeListener =
      results -> {
        // Realm.waitForChange();
        adapter.notifyDataSetChanged();
      };
    recentInfos.addChangeListener(changeListener);
  }
}
