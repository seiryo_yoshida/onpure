package jp.co.seiryodenki.professionalptt.core;

import com.google.gson.annotations.SerializedName;

public class WebApiResponse extends JsonObject {
  @SerializedName("result")
  private String result;

  @SerializedName("message")
  private String message;

  public String getResult() {
    return result;
  }

  public String getMessage() {
    return message;
  }
}
