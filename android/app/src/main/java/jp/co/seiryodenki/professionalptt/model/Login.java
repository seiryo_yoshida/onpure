package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import jp.co.seiryodenki.professionalptt.core.PttException;

import jp.co.seiryodenki.professionalptt.core.WebApiResponse;

public class Login extends WebApiResponse {

  @SerializedName("usr_uuid")
  private String usr_uuid;

  // 2019/4/17 新サーバ移行対応
  @SerializedName("sip_addr")
  private String sip_addr; 

  @SerializedName("sip_port")
  private String sip_port;    
  // 2019/4/17 新サーバ移行対応

  @SerializedName("mqtt_info")
  private List<MqttInfo> mqtt_info;

  @SerializedName("user_info")
  private List<UserInfo> user_info;

  @SerializedName("group_info")
  private List<GroupInfo> group_info;

  @SerializedName("terminal_info")
  private TerminalInfo terminal_info;

  // 2018/7/24 サーバからの録音有無設定
  @SerializedName("record_info")
  private String record_info;

  public String getUsrUuid() { 
    return usr_uuid; 
  }

 // 2019/4/17 新サーバ移行対応
  public String getSipAddr(){
    return sip_addr;
  } 
  
  public String getSipPort(){
    return sip_port;
  }
 // 2019/4/17 新サーバ移行対応

  public List<MqttInfo> getMqttInfo() {
    return mqtt_info;
  }

  public List<UserInfo> getUserInfo() {
    return user_info;
  }

  public List<GroupInfo> getGroupInfo() {
    return group_info;
  }

  public TerminalInfo getTerminalInfo() {
    return terminal_info;
  }
  // 2018/7/24 サーバからの録音有無設定
  public String getRecordInfo() { return record_info; }
}
