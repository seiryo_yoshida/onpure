package jp.co.seiryodenki.professionalptt.model;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import java.util.Date;
import java.util.ListIterator;

public class MessageInfo extends RealmObject {
  private int inOutBox;
  private Date date;
  private String callId;
  private String name;
  private String message;
  private String attFileId;

  public Date getDate() {
    return date;
  }

  public String getCallId() {
    return callId;
  }

  public String getName() {
    return name;
  }

  public String getMessage() {
    return message;
  }

  public String getAttFileId() {
    return attFileId;
  }

  public int getInOutBox() {
    return inOutBox;
  }

  public static final int INBOX = 0;
  public static final int OUTBOX = 1;

  public static void receive(Realm realm, String callId, String name, String message, String attFileId) {
    insert(realm, INBOX, callId, name, message, attFileId);
  }

  public static void send(Realm realm, String callId, String name, String message, String attFileId) {
    insert(realm, OUTBOX, callId, name, message, attFileId);
  }

  public static void insert(Realm realm, int inOutBox, String callId, String name, String message, String attFileId) {
    realm.executeTransactionAsync(
        new Realm.Transaction() {
          private final int MAX_MESSAGE_SIZE = 100;

          private void deleteFrom(Realm realm, int location) {
            RealmResults<MessageInfo> messages =
                realm
                    .where(MessageInfo.class)
                    .equalTo("inOutBox", inOutBox)
                    .sort("date", Sort.DESCENDING)
                    .findAll();
            if (messages.size() < location) {
              return;
            }
            ListIterator<MessageInfo> it = messages.listIterator(location);
            while (it.hasNext()) {
              it.next().deleteFromRealm();
            }
          }

          @Override
          public void execute(Realm realm) {
            deleteFrom(realm, MAX_MESSAGE_SIZE - 1);
            MessageInfo messageInfo = realm.createObject(MessageInfo.class);
            messageInfo.inOutBox = inOutBox;
            messageInfo.date = new Date();
            messageInfo.callId = callId;
            messageInfo.name = name;
            messageInfo.message = message;
            messageInfo.attFileId = attFileId;
          }
        });
  }

  public static RealmResults<MessageInfo> findAllAsync(Realm realm, int inOutBox) {
    return realm
        .where(MessageInfo.class)
        .equalTo("inOutBox", inOutBox)
        .sort("date", Sort.DESCENDING)
        .findAllAsync();
  }
}
