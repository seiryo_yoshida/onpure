package jp.co.seiryodenki.professionalptt.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.app.DialogFragment;

import jp.co.seiryodenki.professionalptt.R;

public class HomeDialogFragment extends DialogFragment {

  public static HomeDialogFragment newInstance() {
    return new HomeDialogFragment();
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final Activity activity = getActivity();
    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    final View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_error_network, null);

    builder.setView(dialogView)
      .setPositiveButton("OK", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          setDialogOKClick();
        }
      });

    return builder.create();
  }

  /**
   * ダイアログOKボタンクリック時の動作
   */
  public void setDialogOKClick() {
    //
    return;
  }
}
