package jp.co.seiryodenki.professionalptt.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;

import jp.co.seiryodenki.professionalptt.Constants;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.util.logger;

public class ServiceConnectionHelper {

    private static final String TAG = "ServiceConnectionHelper";

    private ServiceConnection serviceConnection;
    private CallService callService;
    private boolean isBound = false;
    private boolean isBounding = false;

    public ServiceConnectionHelper(@NonNull Context ctx) {
    }

    public synchronized void bindDataService(@NonNull Context ctx, @NonNull IRequestBindServiceListener completeListener) {
        if (isBound) {
            completeListener.onCompleteRequest(true, callService);
            return;
        }
        if (isBounding) {
            new Handler(Looper.getMainLooper()).postDelayed(() ->
                    bindDataService(ctx, completeListener), 100);
            return;
        }
        isBounding = true;
        Intent intent = new Intent(ctx, CallService.class);
        ctx.startService(intent);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                CallService.CallServiceBinder binder = (CallService.CallServiceBinder) service;
                callService = binder.getService();
                isBounding = false;
                isBound = true;
                completeListener.onCompleteRequest(true, callService);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBounding = false;
                isBound = false;
                completeListener.onCompleteRequest(false, null);
            }
        };
        ctx.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public synchronized void unbindDataService(@NonNull Context ctx) {
        if (serviceConnection != null && isBound) {
            try {
                ctx.unbindService(serviceConnection);
            } catch (IllegalArgumentException e) {
                logger.e(TAG, "unbindDataService:", e);
            }
        }
        isBound = false;
    }

    public void destory(@NonNull Context ctx) {
        unbindDataService(ctx);
        serviceConnection = null;
    }

    public boolean isBound() {
        return isBound;
    }

    public void onPressPttEvent(@NonNull Context ctx, boolean actionDown) {
        if (!isBound) {
            bindDataService(ctx, (isSuccess, service) -> {
                service.onPressPttEvent(actionDown);
            });
        } else {
            callService.onPressPttEvent(actionDown);
        }
    }

    public void publishSos(@NonNull Context ctx) {
        if (!isBound) {
            bindDataService(ctx, (isSuccess, service) -> {
                service.publishSos();
            });
        } else {
            callService.publishSos();
        }
    }

    public void stopCalling(@NonNull Context ctx) {
        if (!isBound) {
            bindDataService(ctx, (isSuccess, service) -> {
                service.stopCalling(Constants.MQTTSTATUS.BYE, true);
            });
        } else {
            callService.stopCalling(Constants.MQTTSTATUS.BYE, true);
        }
    }

    public boolean isEmergencyMode() {
        return isBound && callService.isEmergencyMode();
    }

    public boolean isMonitoring() {
        return isBound && callService.isMonitoring();
    }

    public CallInfo getDefaultCallInfo() {
        if (isBound) {
            return callService.getDefaultCallInfo();
        }
        return null;
    }

    public synchronized void setDefaultCallInfo(@NonNull Context ctx, CallType callType, String[] callIDs) {
        if (!isBound) {
            bindDataService(ctx, (isSuccess, service) -> {
                callService.setDefaultCallInfo(callType, callIDs);
            });
        } else {
            callService.setDefaultCallInfo(callType, callIDs);
        }
    }
}
