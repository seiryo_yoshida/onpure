package jp.co.seiryodenki.professionalptt.ui;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.view.MenuItemRecyclerViewAdapter;

public class MenuFragment extends Fragment {

  private static final int GRID_COLUMN_NUM = 3;
  private List<MenuItem> mMenus = new ArrayList<>();
  private List<Integer> mTitlePositions = new ArrayList<>();

  private void addItem(MenuItem item, boolean isTitle) {
    if (isTitle) {
      mTitlePositions.add(mMenus.size());
    }
    mMenus.add(item);
  }

  private OnClickMenuListener listener;

  public MenuFragment() {}

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    if (mMenus.size() == 0) {
      //addItem(new MenuItem(getString(R.string.menu_subtitle_ptt_apps)), true);
      String[] titles = getResources().getStringArray(R.array.list_menu);
      final TypedArray resources = getResources().obtainTypedArray(R.array.list_menu_res_id);
      for (int i = 0; i < titles.length; i++) {
        // 2018/7/24 録音情報の有無を制御する
        if(titles[i].equals(getString(R.string.title_activity_voice_recoding))){
          if(TerminalUtils.getRecordFlag().equals("0")){
            continue;
          }
        }
          addItem(new MenuItem(resources.getResourceId(i, 0), titles[i]), false);
      }
      resources.recycle();
    }

    //通常メニューを取得
    getAllPackages(inflater.getContext());

    View view = inflater.inflate(R.layout.fragment_menu, container, false);
    setMenuView(view);

    //TextView textDate = view.findViewById(R.id.text_date);
    //textDate.setText(Util.getCurrentDateStr());

    return view;
  }

  private void setMenuView(View rootView) {
    RecyclerView recyclerView = rootView.findViewById(R.id.recycler_list);
    recyclerView.setAdapter(new MenuItemRecyclerViewAdapter(mMenus, listener));
    //GridLayoutManager manager = new GridLayoutManager(getContext(), GRID_COLUMN_NUM);
    LinearLayoutManager manager = new LinearLayoutManager(getContext());
    //manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
    //manager.setSpanSizeLookup(new LinearLayoutManager.SpanSizeLookup() {
    //    @Override
    //  public int getSpanSize(int position) {
    //    return mTitlePositions.contains(position) ? GRID_COLUMN_NUM : 1;
    //  }
    //});
    recyclerView.setLayoutManager(manager);

    recyclerView.setOnTouchListener(
            (v, e) -> {
              CallService.broadcastTouchEvent(getContext(), e);
              return false;
            });
  }

  private void getAllPackages(@NonNull Context ctx) {
    //addItem(new MenuItem(getString(R.string.menu_subtitle_default_apps)), true);
    PackageManager pm = ctx.getPackageManager();
    final String myPackage = ctx.getPackageName();
    for (PackageInfo info : pm.getInstalledPackages(PackageManager.GET_META_DATA)) {
      if (myPackage.equals(info.packageName)) {
        continue; // my application
      }
      if (pm.getLaunchIntentForPackage(info.packageName) == null) {
        continue; // not launchable application
      }
      try {
        String title = info.applicationInfo.loadLabel(pm).toString();
        Drawable icon = pm.getApplicationIcon(info.packageName);
        addItem(new MenuItem(info.packageName, title, icon), false);
      } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnClickMenuListener) {
      listener = (OnClickMenuListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnClickMenuListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    listener = null;
  }

  public interface OnClickMenuListener {
    void onClickMenu(MenuItem item);
  }

  public static class MenuItem {
    public boolean isTitle;
    public boolean isPackage;
    public int idRes;
    public String title;
    public String packageName;
    public Drawable icon;

    //public MenuItem(String title) {
    //  this.title = title;
    //  this.isTitle = true;
    //}

    public MenuItem(int idRes, String title) {
      this.isPackage = false;
      this.packageName = null;
      this.idRes = idRes;
      this.title = title;
    }

    public MenuItem(String packageName, String title, Drawable icon) {
      this.isPackage = true;
      this.packageName = packageName;
      this.title = title;
      this.icon = icon;
    }

    @Override
    public String toString() {
      return title;
    }
  }

}
