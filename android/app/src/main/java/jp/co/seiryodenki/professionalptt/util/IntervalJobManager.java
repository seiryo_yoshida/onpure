package jp.co.seiryodenki.professionalptt.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.os.PowerManager;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.co.seiryodenki.professionalptt.BuildConfig;
import jp.co.seiryodenki.professionalptt.Constants;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.model.MyMqttMessage;
import jp.co.seiryodenki.professionalptt.model.RemoteNotificationInfo;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import static jp.co.seiryodenki.professionalptt.Constants.DEVICE_STATUS_INTERVAL_RATE_MIN;
import static jp.co.seiryodenki.professionalptt.Constants.MAX_ACCURACY;

public class IntervalJobManager {

  private static final String TAG = IntervalJobManager.class.getSimpleName();
  public static final String ACTION_INTERVAL_JOB_GPS =
          BuildConfig.APPLICATION_ID + ".ACTION_INTERVAL_JOB_GPS";
  private static final int JOB_WORKING_PERIOD_SEC = 30;

  private static IntervalJobManager instance;

  private Boolean mGpsListening = false;
  private FusedLocationProviderClient mFusedLocationClient;
  private SettingsClient mSettingsClient;
  private LocationCallback mLocationCallback;
  private Geocoder mGeocoder;

  private OnIntervalJobResultListener mResultListener;
  private RemoteNotificationInfo notificationInfo;

  public interface OnIntervalJobResultListener {
    void onIntervalJobResult(RemoteNotificationInfo notificationInfo);
  }

  private IntervalJobManager(Context context) {
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    mSettingsClient = LocationServices.getSettingsClient(context);
    mGeocoder = new Geocoder(context, Locale.ENGLISH);
  }

  public static IntervalJobManager getInstance(Context context) {
    if (instance == null) {
      instance = new IntervalJobManager(context);
    }
    return instance;
  }

  public static void setGpsInterval(Context context) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MINUTE, DEVICE_STATUS_INTERVAL_RATE_MIN);
    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    if (am != null) {
      am.setWindow(
          AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000,
              getGpsIntervalPendingIntent(context));
    }
  }

  private static void cancelGpsIntervalJob(Context context) {
    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    if (am != null) {
      am.cancel(getGpsIntervalPendingIntent(context));
    }
  }

  private static PendingIntent getGpsIntervalPendingIntent(Context context) {
    Intent intent = new Intent();
    intent.setAction(ACTION_INTERVAL_JOB_GPS);
    return PendingIntent.getBroadcast(context, 101, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }

  public synchronized void startLocationUpdates(
      Context context, OnIntervalJobResultListener callback) {
    if (instance == null) {
      return;
    }

    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    if (pm != null) {
      PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
      wl.acquire(JOB_WORKING_PERIOD_SEC * 1000);
    }

    mResultListener = callback;
    notificationInfo = new RemoteNotificationInfo();
    notificationInfo.setCellIdentity(TelephonyUtils.getCellIdentity(context));
    notificationInfo.setBatteryInfo(BatteryUtils.getBatteryInfo(context));

    if (!checkGpsAvailable(context)) {
      notificationInfo.setLocation(false, null);
      if (mResultListener != null) {
        mResultListener.onIntervalJobResult(notificationInfo);
      }
    } else if (mGpsListening) {
      stopGpsLocationUpdates();
    } else {
      listenLocation(context, getLocationRequest());
    }
  }

  private void listenLocation(
          Context context, LocationRequest locationRequest){
    mGpsListening = true;

    mLocationCallback = createLocationCallback();
    mSettingsClient
        .checkLocationSettings(getLocationSettingsRequest(locationRequest))
        .addOnSuccessListener(locationSettingsResponse -> {
          mFusedLocationClient.requestLocationUpdates(
                  locationRequest, mLocationCallback, Looper.myLooper());
        })
        .addOnFailureListener(e -> {
          mGpsListening = false;
        });

    Observable.just(true)
        .delay(JOB_WORKING_PERIOD_SEC, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnNext(flag -> stopGpsLocationUpdates());
  }

  private boolean checkGpsAvailable(Context context) {
    LocationManager locationManager =
            (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    return !(locationManager == null
            || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
  }

  public void finish(Context context) {
    if (mLocationCallback != null) {
      mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
    cancelGpsIntervalJob(context);
    instance = null;
  }

  private LocationCallback createLocationCallback() {
    return new LocationCallback() {
      private long lastTimestamp;
      private Location lastLocation;

      private void setSpeedAndBearing(Location location) {
        long timestamp = System.currentTimeMillis();
        if (lastLocation != null) {
          float interval = (timestamp - lastTimestamp) / 1000f;
          if (interval > 1f) {
            location.setSpeed(location.distanceTo(lastLocation) / interval);
          }
          if (location.getSpeed() * 3.6f < 1f) {
            location.setBearing(lastLocation.getBearing());
          } else {
            location.setBearing(lastLocation.bearingTo(location));
          }
        }
        lastTimestamp = timestamp;
        lastLocation = location;
      }

      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        mGpsListening = false;
        Location location = locationResult.getLastLocation();
        if (location.getAccuracy() > MAX_ACCURACY) {
          return;
        }
        setSpeedAndBearing(location);
        notificationInfo.setLocation(true, location);
        // 2019/08/08 オンプレ対応
        // Google APIを利用した逆ジオロケーション機能は利用不可
        //notificationInfo.setAddress(getGeoLocations(location));
        if (mResultListener != null) {
          mResultListener.onIntervalJobResult(notificationInfo);
        }
      }
    };
  }

  private static LocationRequest getLocationRequest() {
    return new LocationRequest()
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        .setNumUpdates(1)
        .setInterval(10 * 1000)
        .setFastestInterval(5 * 1000);
  }

  private static LocationSettingsRequest getLocationSettingsRequest(
      LocationRequest locationRequest) {
    return new LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build();
  }

  private void stopGpsLocationUpdates() {
    if (!mGpsListening) {
      return;
    }

    mFusedLocationClient
        .removeLocationUpdates(mLocationCallback)
        .addOnCompleteListener(
            task -> {
              mGpsListening = false;
              if (mResultListener != null) {
                mResultListener.onIntervalJobResult(notificationInfo);
              }
            });
  }

  private Address getGeoLocations(Location location) {
    if (!Geocoder.isPresent()) return null;

    try {
      List<Address> addresses = mGeocoder.getFromLocation(
              location.getLatitude(), location.getLongitude(), 1);
      if (addresses.size() > 0) {
        return addresses.get(0);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void setLocationSettings(Activity activity) {
    SettingsClient settingsClient = LocationServices.getSettingsClient(activity);
    settingsClient
        .checkLocationSettings(getLocationSettingsRequest(getLocationRequest()))
        .addOnFailureListener(
            e -> {
              if (e instanceof ResolvableApiException) {
                ResolvableApiException resolvable = (ResolvableApiException) e;
                if (resolvable.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                  try {
                    resolvable.startResolutionForResult(activity, 1);
                  } catch (Exception error) {
                    logger.e(TAG, "[IntervalJob]startResolutionForResult", e);
                  }
                }
              }
            });
  }

  public void setActivityZone(Context ctx, SharedPreferences preference, RemoteNotificationInfo notificationInfo) {

    if (TerminalInfo.getActiveSetUse(ctx, preference)) {
      Location currentLocation = notificationInfo.getLocation();
      Location baseLocation = new Location(currentLocation);
      try {
        baseLocation.setLatitude(
                Double.parseDouble(TerminalInfo.getActiveSetLatitude(ctx, preference)));
        baseLocation.setLongitude(
                Double.parseDouble(TerminalInfo.getActiveSetLongitude(ctx, preference)));
      } catch (Throwable e) {
        logger.e(TAG, "parse exception", e);
      }
      int maxDistance = TerminalInfo.getActiveSetSize(ctx, preference);
      String prefActivityZone = ctx.getString(R.string.key_pref_activity_zone_release_message_flag);
      boolean releaseMessage = preference.getBoolean(prefActivityZone, false);
      if (baseLocation.distanceTo(currentLocation) > maxDistance) {
        if (!releaseMessage) {
          if (!MqttManager.getInstance()
                  .notifyMessageWithGps(
                          TerminalInfo.getActiveSetMesgto(ctx, preference),
                          Constants.MqttMessageType.ACT_ST.toString(),
                          MyMqttMessage.getMessageString(
                                  ctx.getString(R.string.message_activity_zone_start),
                                  StateManager.getMyTerminalId(),
                                  currentLocation),
                          RemoteNotificationInfo.getLatitudeLongitude(currentLocation))) {
            logger.e(TAG, "Failed to start activity zone.");
          }
          preference.edit().putBoolean(prefActivityZone, true).apply();
          MyNotificationManager.notifiedActivityZoneWarning(
                  ctx, ctx.getString(R.string.notification_left_activity_zone));
        }
      } else if (releaseMessage) {
        if (!MqttManager.getInstance()
                .notifyMessageWithGps(
                        TerminalInfo.getActiveSetMesgto(ctx, preference),
                        Constants.MqttMessageType.ACT_ED.toString(),
                        MyMqttMessage.getMessageString(
                                ctx.getString(R.string.message_activity_zone_end),
                                StateManager.getMyTerminalId(),
                                currentLocation),
                        RemoteNotificationInfo.getLatitudeLongitude(currentLocation))) {
          logger.e(TAG, "Failed to enter activity zone.");
        } else {
          MyNotificationManager.notifiedActivityZoneWarning(
                  ctx, ctx.getString(R.string.notification_enter_activity_zone));
          preference.edit().putBoolean(prefActivityZone, false).apply();
        }
      }
    }
  }
}
