package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.media.MediaPlayer;

public class SoundPlayer {
  private static final String TAG = SoundPlayer.class.getSimpleName();
  private static MediaPlayer mediaPlayer = null;

  public static void start(Context c, int resid, MediaPlayer.OnCompletionListener onCompletion) {
      try {
          if (mediaPlayer != null) {
              mediaPlayer.reset();
              mediaPlayer.release();
              mediaPlayer = null;
          }
          mediaPlayer = MediaPlayer.create(c, resid);
          mediaPlayer.setOnErrorListener(
                  (mp, what, ext) -> {
                      logger.e(TAG, "start:" + what);
                      return true;
                  });
          mediaPlayer.setOnCompletionListener(onCompletion);
          mediaPlayer.start();
          try {
              Thread.sleep(500);
          } catch (InterruptedException e) {

              // Do nothing.
          }
      } catch (Exception e) {
          logger.e("SoundPlayer::", "SoundPlayer Error");
      }
  }
}
