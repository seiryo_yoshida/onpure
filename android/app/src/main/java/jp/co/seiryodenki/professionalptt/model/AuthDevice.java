package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

import jp.co.seiryodenki.professionalptt.core.WebApiResponse;

public class AuthDevice extends WebApiResponse {

  @SerializedName("company_id")
  private String company_id;

  @SerializedName("terminal_id")
  private String terminal_id;

  public String getCompany_id() {
    return company_id;
  }

  public void setCompany_id(String company_id) {
    this.company_id = company_id;
  }

  public String getTerminal_id() {
    return terminal_id;
  }

  public void setTerminal_id(String terminal_id) {
    this.terminal_id = terminal_id;
  }
}
