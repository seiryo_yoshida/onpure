package jp.co.seiryodenki.professionalptt;

import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_ID;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_NAME;
import static jp.co.seiryodenki.professionalptt.Constants.MqttMessageType;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;
import io.realm.Realm;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.AttFile;
import jp.co.seiryodenki.professionalptt.model.GroupInfo;
import jp.co.seiryodenki.professionalptt.model.MessageInfo;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.util.logger;
import android.provider.DocumentsContract;
import android.os.Environment;
import 	android.content.ContentUris;

/** メッセージ一覧制御クラス */
public class MessageActivity extends BaseActivity {

  private static final String TAG = MessageActivity.class.getSimpleName();

  // ユーザPriorigy Intent Key
  private static final String INTENT_USER_PRIORITY = "intent_user_priority";

  // 一斉通話用ID
  private static final String ADDRESS_PTTALL = "PTTALL";

  // 画像選択用ギャラリー表示要求コード
  private static final int REQUEST_CODE_CHOOSER = 1000;

  // ギャラリー表示画像種別
  private static final List<String> types = Collections.unmodifiableList(new LinkedList<String>() {
    {
      add("image/jpeg");
      add("image/jpg");
      add("image/png");
    }
  });

  // ユーザ操作状態保持フラグ
  private boolean isSending = false;

  // 画像付帯データ
  private String attFileId;

  // Realm DBハンドラ
  private Realm realm;

  // 添付画像View
  private LinearLayout mImageOnOff;
  private ImageView mImageView;
  private ImageView mImageCheck;
  private Uri mImageURI;
  private String mImagePath;

  //ボタン
  private Button attachButton;
  private Button sendButton;

  //送信先
  private static final int RECEIVER_MAX = 1;
  private EditText receiver[] = new EditText[RECEIVER_MAX];

    /**
     * Activity起動時処理
     *
     * @param savedInstanceState 状態保存データ
     */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    realm = Realm.getDefaultInstance();
    setContentView(R.layout.activity_message);

    Intent intent = getIntent();
    String callId = intent.getStringExtra(INTENT_CALL_ID);
    String callName = intent.getStringExtra(INTENT_CALL_NAME);
    //int userPriority = intent.getIntExtra(INTENT_USER_PRIORITY, 0);

    ProgressBar progressBar = findViewById(R.id.progressBar_Send);
    progressBar.setVisibility(View.INVISIBLE);

    //添付画像ビュー
    mImageOnOff = findViewById(R.id.file_image_view);
    mImageView =  findViewById(R.id.file_image);
    //mImageCheck = findViewById(R.id.file_check);

    //ボタン表示
    attachButton = findViewById(R.id.btn_attach);
    sendButton = findViewById(R.id.btn_Send);

    //EditText receiver = (EditText) findViewById(R.id.txt_To);
    receiver[0] = findViewById(R.id.txt_To);
    if (callId != null && callName != null) {
      receiver[0].setText(callId);
    }
    //宛先設定後の処理
    receiver[0].addTextChangedListener(new TextWatcher() {
      //テキスト変更前
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }
      //テキスト変更中
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }
      //テキスト変更後
      @Override
      public void afterTextChanged(Editable s) {
        if (receiver[0].getText() != null && s.length() >= 1) {
          sendButton.setBackgroundResource(R.drawable.btn_sendmessagebt);
          sendButton.setClickable(true);
        } else {
          sendButton.setBackgroundResource(R.drawable.ic_sendmessagebtoff);
          sendButton.setClickable(false);
        }
      }
    });

    Switch switchAll = (Switch) findViewById(R.id.switch_sendAll);
    if (isSatisfiedPriority()) {
      switchAll.setVisibility(View.VISIBLE);
      switchAll.setOnCheckedChangeListener(
          (buttonView, isChecked) -> {
            for (int i = 0; i < RECEIVER_MAX; i++) {
              receiver[i].setEnabled(!isChecked);
            }
            //一斉送信の切り替えで送信ボタンON/OFFを切り替える
            if (isChecked) {
              sendButton.setBackgroundResource(R.drawable.btn_sendmessagebt);
              sendButton.setClickable(true);
            } else {
              if (receiver[0].getText() == null || receiver[0].getText().length() <= 0) {
                sendButton.setBackgroundResource(R.drawable.ic_sendmessagebtoff);
                sendButton.setClickable(false);
              }
            }
          });
    } else {
      switchAll.setVisibility(View.GONE);
    }

    //宛先が入力されていない場合は、送信ボタンをOFFにする
    if ((receiver[0].getText() == null || receiver[0].getText().length() <= 0) && !switchAll.isChecked()) {
      sendButton.setBackgroundResource(R.drawable.ic_sendmessagebtoff);
      sendButton.setClickable(false);
    }

    EditText editMessage = (EditText) findViewById(R.id.txt_Message);
    //EditText attachText = findViewById(R.id.file_path);

    //Button sendButton = (Button) findViewById(R.id.btn_Send);
    sendButton.setOnClickListener(
        v -> {
          if (isSending) {
            return;
          }
          isSending = true;
          sendButton.setVisibility(View.INVISIBLE);
          progressBar.setVisibility(View.VISIBLE);

          //送信先を求める
          String terminalId[] = new String[RECEIVER_MAX];
          String terminalName[] = new String[RECEIVER_MAX];
          int terminalId_n = 0;
          for (int i = 0; i < RECEIVER_MAX; i++) {
            terminalName[i] = null;
          }

          for (int i = 0; i < RECEIVER_MAX; i++) {

            //一斉送信
            if (switchAll.isChecked()) {
              terminalId[0] = ADDRESS_PTTALL;
              terminalName[0] = getString(R.string.label_send_all);
              terminalId_n = 1;
              break;
            }

            //一斉送信以外
            terminalId[terminalId_n] = receiver[i].getText().toString();
            terminalName[terminalId_n] = getTerminalName(terminalId[terminalId_n]);
            if (terminalName[terminalId_n] == null && i == 0) {
              terminalName[terminalId_n] = callName;
            }
            if (terminalName[terminalId_n] != null && terminalName[terminalId_n].length() >= 1) {
              terminalId_n += 1;
            }
          }

          //宛先名が取得できない場合は処理終了
          if (terminalId_n <= 0) {
            String err = getString(R.string.err_not_found_user_message, terminalId[0]);
            Toast.makeText(this, err, Toast.LENGTH_LONG).show();
            sendButton.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            isSending = false;
            return;
          }

          //String name = terminalName;

          //String attachFilePath = attachText.getText().toString();

          //添付画像が設定されている場合は画像ファイルを保存する
          String attachFilePath = null;
          Uri uri = null;
          if (mImageOnOff.getVisibility() == View.VISIBLE) {
            attachFilePath = mImagePath;
          }

          attFileId = null;

          //宛先数だけ繰り返す
          for (int i = 0; i < terminalId_n; i++) {
            String terminal = terminalId[i];
            String name = terminalName[i];
            new WebApiRequest<AttFile>(AttFile.class)
              .setOnResponse(
                response -> {
                  attFileId = response.getAttFileId();
                  if (attFileId == null || attFileId.isEmpty()) {
                    attFileId = null;
                  }
                  String message = editMessage.getText().toString();
                  if (!MqttManager.getInstance()
                    .notifyMessage(
                      terminal, MqttMessageType.MSG.toString(), message, attFileId)) {
                    runOnUiThread(
                      () -> {
                        Toast.makeText(this, R.string.err_send_message, Toast.LENGTH_LONG)
                          .show();
                        sendButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        isSending = false;
                      });
                    return;
                  }
                  runOnUiThread(
                    () -> {
                      try (Realm realm = Realm.getDefaultInstance()) {
                        MessageInfo.send(realm, terminal, name, message, attFileId);
                      }
                      startActivity(new Intent(this, HomeActivity.class));
                      finish();
                    });
                })
              .setOnFailure(
                e -> {
                  String message = editMessage.getText().toString();
                  if (!MqttManager.getInstance()
                    .notifyMessage(
                      terminal, MqttMessageType.MSG.toString(), message, null)) {
                    runOnUiThread(
                      () -> {
                        Toast.makeText(this, R.string.err_send_message, Toast.LENGTH_LONG)
                          .show();
                        sendButton.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        isSending = false;
                      });
                    return;
                  }
                  runOnUiThread(
                    () -> {
                      try (Realm realm = Realm.getDefaultInstance()) {
                        MessageInfo.send(realm, terminal, name, message, null);
                      }
                      startActivity(new Intent(this, HomeActivity.class));
                      finish();
                    });
                })
              .uploadAttachFile(
                this,
                PreferenceManager.getDefaultSharedPreferences(this),
                MqttManager.getInstance().getMyCompanyId(),
                isAttachedID(MqttManager.getInstance().getMyTerminalId(), terminal),
                attachFilePath);
          }
        });

    //Button attachButton = (Button) findViewById(R.id.btn_attach);
    attachButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //メッセージ送信中
        if (isSending) {
          return;
        }
        //画像が選択されていない場合
        if (mImageOnOff.getVisibility() == View.GONE) {
          //画像ギャラリーを開く
          Intent intentGallery;
          intentGallery = new Intent(Intent.ACTION_GET_CONTENT);
          intentGallery.addCategory(Intent.CATEGORY_OPENABLE);
          intentGallery.setType("image/*");

          if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            intentGallery.putExtra(Intent.EXTRA_MIME_TYPES, types.toArray());
          }

          Intent intent = Intent.createChooser(intentGallery, null);
          startActivityForResult(intent, REQUEST_CODE_CHOOSER);

        //画像が表示されている場合
        } else {
          //画像領域を非表示にする
          mImageOnOff.setVisibility(View.GONE);
          mImageURI = null;
          mImagePath = null;
          //画像選択ボタンを入れ替える
          attachButton.setBackgroundResource(R.drawable.btn_addimagebt);
        }
      }
    });

    //アルバム展開ボタン設定
    Button btnAlbumOpen = findViewById(R.id.file_button);
    btnAlbumOpen.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //画像ギャラリーを開く
        Intent intentGallery;
        intentGallery = new Intent(Intent.ACTION_GET_CONTENT);
        intentGallery.addCategory(Intent.CATEGORY_OPENABLE);
        intentGallery.setType("image/*");

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
          intentGallery.putExtra(Intent.EXTRA_MIME_TYPES, types.toArray());
        }

        Intent intent = Intent.createChooser(intentGallery, null);
        startActivityForResult(intent, REQUEST_CODE_CHOOSER);
      }
    });
  }

  /* 送信先種別判定処理
   *
   * @return 添付ファイルID
   */
  private String isAttachedID(String userID, String terminalID){
      String attachFileID = null;
      String callType = terminalID.substring(0,1);
      if(callType.compareTo("4") == 0){
          attachFileID = userID;
      } else{
          attachFileID = terminalID;
      }
      return attachFileID;
  }


  private String getTerminalName(String terminalId) {
    UserInfo user = UserInfo.getUserInfo(realm, terminalId);
    if (user != null) {
      return user.getUserName();
    }
    GroupInfo group = GroupInfo.getGroupInfo(realm, terminalId);
    if (group != null) {
      return group.getGroupName();
    }
    return null;
  }

  /**
   * 通話種別判定処理
   *
   * @return 判定結果
   */
  private boolean isSatisfiedPriority() {
    Set<String> typeSet =
        TerminalInfo.getCallType(this, PreferenceManager.getDefaultSharedPreferences(this));
    for (String type : typeSet) {
      if (type.equals(CallType.OPERATING_STATION.toString())
          || type.equals(CallType.ALL_CALL_SYSTEM_WIDE.toString())
          || type.equals(CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT.toString())) {
        return true;
      }
    }
    return false;
  }

    /** Activity終了時処理 */
  @Override
  protected void onDestroy() {
    super.onDestroy();
    realm.close();
  }

    /**
     * Activity起動Intent作成処理
     *
     * @param context Context
     * @param userPriority ユーザPriority
     * @return Activity起動Intent
     */
  public static Intent getIntent(Context context, int userPriority) {
    Intent intent = new Intent(context, MessageActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_USER_PRIORITY, userPriority);
    return intent;
  }

    /**
     * Activity起動Intent作成処理
     *
     * @param context Context
     * @param id ユーザID
     * @param name ユーザ名
     * @param userPriority ユーザPriority
     * @return Activity起動Intent
     */
  public static Intent getMessageIntent(Context context, String id, String name, int userPriority) {
    Intent intent = new Intent(context, MessageActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_CALL_ID, id);
    intent.putExtra(INTENT_CALL_NAME, name);
    intent.putExtra(INTENT_USER_PRIORITY, userPriority);
    return intent;
  }

    /**
     * Activity処理結果受信処理
     *
     * @param requestCode Activity起動コード
     * @param resultCode 処理結果コード
     * @param data 処理結果データ
     */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case REQUEST_CODE_CHOOSER:
        if (resultCode != RESULT_OK) {
          Toast.makeText(this, "failed to select image.", Toast.LENGTH_LONG).show();
        }
        ContentResolver resolver = getContentResolver();
        String[] columns = {MediaStore.Images.Media.DATA};
        if (data == null || data.getData() == null) {
          return;
        }
        
        Uri uri = data.getData();
        String path = getPathFromUri(this, uri);

        //EditText attachText = findViewById(R.id.file_path);
        //attachText.setText(path);

        //画像データを表示
        mImageOnOff.setVisibility(View.VISIBLE);
        mImageView.setImageURI(null);
        mImageView.setImageURI(uri);
        mImageView.setVisibility(View.VISIBLE);
        //mImageCheck.setVisibility(View.VISIBLE);
        mImageURI = uri;
        mImagePath = path;
        attachButton.setBackgroundResource(R.drawable.btn_closeimageareabt);

        break;
      default:
        super.onActivityResult(requestCode, resultCode, data);
        break;
    }
  }

  //
  public String getPathFromUri(final Context context, final Uri uri) {
      boolean isAfterKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
      // DocumentProvider
      logger.e(TAG,"uri:" + uri.getAuthority());
      if (isAfterKitKat && DocumentsContract.isDocumentUri(context, uri)) {
          if ("com.android.externalstorage.documents".equals(
                  uri.getAuthority())) {// ExternalStorageProvider
              final String docId = DocumentsContract.getDocumentId(uri);
              final String[] split = docId.split(":");
              final String type = split[0];
              if ("primary".equalsIgnoreCase(type)) {
                  return Environment.getExternalStorageDirectory() + "/" + split[1];
              }else {
                  return "/stroage/" + type +  "/" + split[1];
              }
          }else if ("com.android.providers.downloads.documents".equals(
                  uri.getAuthority())) {// DownloadsProvider
              final String id = DocumentsContract.getDocumentId(uri);
              final Uri contentUri = ContentUris.withAppendedId(
                      Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
              return getDataColumn(context, contentUri, null, null);
          }else if ("com.android.providers.media.documents".equals(
                  uri.getAuthority())) {// MediaProvider
              final String docId = DocumentsContract.getDocumentId(uri);
              final String[] split = docId.split(":");
              final String type = split[0];
              Uri contentUri = null;
              contentUri = MediaStore.Files.getContentUri("external");
              final String selection = "_id=?";
              final String[] selectionArgs = new String[] {
                      split[1]
              };
              return getDataColumn(context, contentUri, selection, selectionArgs);
          }
      }else if ("content".equalsIgnoreCase(uri.getScheme())) {//MediaStore
          return getDataColumn(context, uri, null, null);
      }else if ("file".equalsIgnoreCase(uri.getScheme())) {// File
          return uri.getPath();
      }
      return null;
  }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        Cursor cursor = null;
        final String[] projection = {
                MediaStore.Files.FileColumns.DATA
        };
        try {
          cursor = context.getContentResolver().query(
            uri, projection, selection, selectionArgs, null);
          if (cursor != null && cursor.moveToFirst()) {
            final int cindex = cursor.getColumnIndexOrThrow(projection[0]);
            return cursor.getString(cindex);
          }
        } catch (Exception e) {
          logger.e(TAG, "Messege Image attachment Error");
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

  /**
   * 宛先欄を増やす
   * @param v
   */
  public void setSendTo(View v) {
    for (int i = 0; i < RECEIVER_MAX; i++) {
      if (receiver[i].getText().length() <= 0) {
        if (receiver[i].getVisibility() == View.GONE) {
          receiver[i].setVisibility(View.VISIBLE);
        }
        receiver[i].setFocusable(true);
        receiver[i].setFocusableInTouchMode(true);
        receiver[i].requestFocus();
        break;
      }
    }
  }

  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    startActivity(HomeActivity.getCallIntent(this,true));
    onBackPressed();
  }
}
