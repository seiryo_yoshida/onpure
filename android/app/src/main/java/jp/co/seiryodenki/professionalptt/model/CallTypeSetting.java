package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class CallTypeSetting {

  @SerializedName("ACS")
  private int ACS;

  @SerializedName("FMG")
  private int FMG;

  @SerializedName("CLL")
  private int CLL;

  @SerializedName("FGC")
  private int FGC;

  @SerializedName("MGP")
  private int MGP;

  @SerializedName("NAL")
  private int NAL;

  @SerializedName("HHH")
  private int HHH;

  @SerializedName("EMC")
  private int EMC;

  @SerializedName("RTM")
  private int RTM;

  @SerializedName("GPC")
  private int GPC;

  @SerializedName("CMD")
  private int CMD;

  @SerializedName("SAL")
  private int SAL;

  public CallTypeSetting(
    int acs,
    int fmg,
    int cll,
    int fgc,
    int mgp,
    int nal,
    int hhh,
    int emc,
    int rtm,
    int gpc,
    int cmd,
    int sal) {
    ACS = acs;
    FMG = fmg;
    CLL = cll;
    FGC = fgc;
    MGP = mgp;
    NAL = nal;
    HHH = hhh;
    EMC = emc;
    RTM = rtm;
    GPC = gpc;
    CMD = cmd;
    SAL = sal;
  }

  public int getACS() {
    return ACS;
  }

  public void setACS(int ACS) {
    this.ACS = ACS;
  }

  public String getACSKey() {
    return "ACS";
  }

  public int getFMG() {
    return FMG;
  }

  public void setFMG(int FMG) {
    this.FMG = FMG;
  }

  public String getFMGKey() {
    return "FMG";
  }

  public int getCLL() {
    return CLL;
  }

  public void setCLL(int CLL) {
    this.CLL = CLL;
  }

  public String getCLLKey() {
    return "CLL";
  }

  public int getFGC() {
    return FGC;
  }

  public void setFGC(int FGC) {
    this.FGC = FGC;
  }

  public String getFGCKey() {
    return "FGC";
  }

  public int getMGP() {
    return MGP;
  }

  public void setMGP(int MGP) {
    this.MGP = MGP;
  }

  public String getMGPKey() {
    return "MGP";
  }

  public int getNAL() {
    return NAL;
  }

  public void setNAL(int NAL) {
    this.NAL = NAL;
  }

  public String getNALKey() {
    return "NAL";
  }

  public int getHHH() {
    return HHH;
  }

  public void setHHH(int HHH) {
    this.HHH = HHH;
  }

  public String getHHHKey() {
    return "HHH";
  }

  public int getEMC() {
    return EMC;
  }

  public void setEMC(int EMC) {
    this.EMC = EMC;
  }

  public String getEMCKey() {
    return "EMC";
  }

  public int getRTM() {
    return RTM;
  }

  public void setRTM(int RTM) {
    this.RTM = RTM;
  }

  public String getRTMKey() {
    return "RTM";
  }

  public int getGPC() {
    return GPC;
  }

  public void setGPC(int GPC) {
    this.GPC = GPC;
  }

  public String getGPCKey() {
    return "GPC";
  }

  public int getCMD() {
    return CMD;
  }

  public void setCMD(int CMD) {
    this.CMD = CMD;
  }

  public String getCMDKey() {
    return "CMD";
  }

  public int getSAL() {
    return SAL;
  }

  public void setSAL(int SAL) {
    this.SAL = SAL;
  }

  public String getSALKey() {
    return "SAL";
  }
}
