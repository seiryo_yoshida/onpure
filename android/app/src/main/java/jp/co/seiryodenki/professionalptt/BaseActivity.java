package jp.co.seiryodenki.professionalptt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import jp.co.seiryodenki.professionalptt.service.CallService;

/**
 * ログイン時共通処理基底クラス
 *
 * <pre>
 *     ログアウト、画面タッチイベントについて処理を記載
 * </pre>
 */
public class BaseActivity extends AppCompatActivity {

  public static final String BROADCAST_PTT_LOGOUT =
      "jp.co.seiryodenki.professionalptt.action.BROADCAST_PTT_LOGOUT";

  /** ログアウトBroadcast受信時処理 */
  private BroadcastReceiver mBroadcastReceiver =
      new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          if (intent == null) {
            return;
          }
          String action = intent.getAction();
          if (action == null) {
            return;
          }
          if (BROADCAST_PTT_LOGOUT.equals(action)) {
            finish();
          }
        }
      };

  /**
   * ログアウト状態をアプリ全体に通知する
   *
   * @param context Context
   */
  public static void broadcastLogout(Context context) {
    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BROADCAST_PTT_LOGOUT));
  }

  /**
   * 画面タッチ時処理
   *
   * @param event タッチイベント
   * @return 処理結果
   */
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    CallService.broadcastTouchEvent(this, event);
    return super.onTouchEvent(event);
  }

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    final IntentFilter filter = new IntentFilter();
    filter.addAction(BROADCAST_PTT_LOGOUT);
    LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, filter);
    super.onCreate(savedInstanceState);
  }

  /** Activity終了時処理 */
  @Override
  protected void onDestroy() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    super.onDestroy();
  }
}
