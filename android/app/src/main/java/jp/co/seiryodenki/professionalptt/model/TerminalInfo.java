package jp.co.seiryodenki.professionalptt.model;

import static java.lang.Integer.parseInt;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_ENABLE;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.util.Base64Decoder;

public class TerminalInfo {

  @SerializedName("messageset")
  private int messageset;

  @SerializedName("endcall")
  private EndCall endcall;

  @SerializedName("interruptset")
  private InterruptSet interruptset;

  @SerializedName("appset")
  private AppSet appset;

  @SerializedName("radioset")
  private RadioSet radioset;

  @SerializedName("keepalive")
  private String keepalive;

  @SerializedName("mandownset")
  private MandownSet mandownset;

  @SerializedName("saftyset")
  private SafetySettings safetySettings;

  @SerializedName("rtmset")
  private RtmSet rtmset;

  @SerializedName("sites")
  private List<Site> sites;

  @SerializedName("calldefult")
  private String calldefult;

  @SerializedName("emcset")
  private EmcSet emcset;

  @SerializedName("terminalstatus")
  private TerminalStatus terminalstatus;

  @SerializedName("calltype")
  private CallTypeSetting calltype;

  @SerializedName("keyset")
  private KeySet keyset;

  @SerializedName("gpsinfo")
  private GpsInfo gpsinfo;

  @SerializedName("message")
  private String message;

  @SerializedName("talklimit")
  private String talklimit;

  @SerializedName("nocall")
  private String nocall;

  @SerializedName("activeset")
  private ActiveSet activeSet;

  // 2018/7/24 近隣距離設定 
  @SerializedName("neighborset")
  private int neighborSet;

  public TerminalInfo(
      int messageset,
      EndCall endcall,
      InterruptSet interruptset,
      AppSet appset,
      RadioSet radioset,
      String keepalive,
      MandownSet mandownset,
      SafetySettings safetySettings,
      RtmSet rtmset,
      String calldefult,
      EmcSet emcset,
      TerminalStatus terminalstatus,
      CallTypeSetting calltype,
      KeySet keyset,
      GpsInfo gpsinfo,
      String message,
      String talklimit,
      String nocall,
      ActiveSet activeSet,
      // 2018/7/24 近隣距離設定 
      int neighborSet) {
    this.messageset = messageset;
    this.endcall = endcall;
    this.interruptset = interruptset;
    this.appset = appset;
    this.radioset = radioset;
    this.keepalive = keepalive;
    this.mandownset = mandownset;
    this.safetySettings = safetySettings;
    this.rtmset = rtmset;
    this.calldefult = calldefult;
    this.emcset = emcset;
    this.terminalstatus = terminalstatus;
    this.calltype = calltype;
    this.keyset = keyset;
    this.gpsinfo = gpsinfo;
    this.message = message;
    this.talklimit = talklimit;
    this.nocall = nocall;
    this.activeSet = activeSet;
    // 2018/7/24 近隣距離設定 
    this.neighborSet = neighborSet;
  }

  public int getMessageset() {
    return messageset;
  }

  public void setMessageset(int messageset) {
    this.messageset = messageset;
  }

  public static void saveMessageset(
      Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      int messageset = terminalInfo.getMessageset();
      pref.edit().putInt(ctx.getString(R.string.key_pref_messageset), messageset).apply();
    }
  }

  public static int getMessageset(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_messageset), getDefaultMessageset(ctx));
  }

  public static int getDefaultMessageset(Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_messageset));
  }

  public EndCall getEndcall() {
    return endcall;
  }

  public void setEndcall(EndCall endcall) {
    this.endcall = endcall;
  }

  public static void saveEndcall(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save end call
    if (terminalInfo != null) {
      EndCall endCall = terminalInfo.getEndcall();
      if (endCall != null) {
        pref.edit()
          .putInt(ctx.getString(R.string.key_pref_endcall_endkey), endCall.getEndKey())
          .apply();
        pref.edit()
          .putInt(ctx.getString(R.string.key_pref_endcall_outtalk), endCall.getOutTalk())
          .apply();
      }
    }
  }

  public static int getEndcallEndkey(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_endcall_endkey), getDefaultEndcallEndkey(ctx));
  }

  public static int getDefaultEndcallEndkey(Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_endcall_endkey));
  }

  public static int getEndcallOuttalk(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_endcall_outtalk), getDefaultEndcallOuttalk(ctx));
  }

  public static int getDefaultEndcallOuttalk(Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_endcall_outtalk));
  }

  public InterruptSet getInterruptSet() {
    return interruptset;
  }

  public void setInterruptSet(InterruptSet interruptset) {
    this.interruptset = interruptset;
  }

  public static void saveInterruptSet(
      Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      InterruptSet interruptSet = terminalInfo.getInterruptSet();
      if (interruptSet != null) {
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_interruptset_use), interruptSet.getUse())
            .apply();
        pref.edit()
            .putBoolean(
                ctx.getString(R.string.key_pref_interruptset_parking),
                interruptSet.getParking() != 0)
            .apply();
        pref.edit()
            .putBoolean(
                ctx.getString(R.string.key_pref_interruptset_notice), interruptSet.getNotice() != 0)
            .apply();
      }
    }
  }

  public static int getInterruptSetUse(Context ctx, SharedPreferences pref) {
    return  Integer.parseInt(pref.getString(ctx.getString(R.string.key_pref_interruptset_use), getDefaultInterruptSetUse(ctx)));
  }

  public static String getDefaultInterruptSetUse(Context ctx) {
    return ctx.getString(R.string.const_default_interruptset_use);
  }

  public static boolean getInterruptSetParking(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_interruptset_parking), getDefaultInterruptSetParking(ctx));
  }

  public static boolean getDefaultInterruptSetParking(Context ctx) {
    return ctx.getString(R.string.const_default_interruptset_parking).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public static boolean getInterruptSetNotice(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_interruptset_notice), getDefaultInterruptSetNotice(ctx));
  }

  public static boolean getDefaultInterruptSetNotice(Context ctx) {
    return ctx.getString(R.string.const_default_interruptset_notice).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public AppSet getAppSet() {
    return appset;
  }

  public void setAppSet(AppSet appset) {
    this.appset = appset;
  }

  public static void saveAppSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      AppSet appSet = terminalInfo.getAppSet();
      if (appSet != null) {
        pref.edit()
          .putString(ctx.getString(R.string.key_pref_appset_use), appSet.getUse())
          .apply();
      }
    }
  }

  public static String getAppSetUse(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_appset_use), getDefaultAppSetUse(ctx));
  }

  public static String getDefaultAppSetUse(Context ctx) {
    return ctx.getString(R.string.const_default_appset_use);
  }

  public RadioSet getRadioSet() {
    return radioset;
  }

  public void setRadioSet(RadioSet radioset) {
    this.radioset = radioset;
  }

  public static void saveRadioSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      RadioSet radioSet = terminalInfo.getRadioSet();
      if (radioSet != null) {
        pref.edit()
          .putBoolean(ctx.getString(R.string.key_pref_radioset_no),
            radioSet.getNo() == SETTINGS_ENABLE)
          .apply();
        pref.edit()
          .putBoolean(ctx.getString(R.string.key_pref_radioset_zero),
            radioSet.getZero() == SETTINGS_ENABLE)
          .apply();
        pref.edit()
          .putBoolean(ctx.getString(R.string.key_pref_radioset_one),
            radioSet.getOne() == SETTINGS_ENABLE)
          .apply();
      }
    }
  }

  public static boolean getRadioSetNo(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_radioset_no), getDefaultRadioSetNo(ctx));
  }

  public static boolean getDefaultRadioSetNo(Context ctx) {
    return ctx.getString(R.string.const_default_radioset_no).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public static boolean getRadioSetZero(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_radioset_zero), getDefaultRadioSetZero(ctx));
  }

  public static boolean getDefaultRadioSetZero(Context ctx) {
    return ctx.getString(R.string.const_default_radioset_zero).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public static boolean getRadioSetOne(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_radioset_one), getDefaultRadioSetOne(ctx));
  }

  public static boolean getDefaultRadioSetOne(Context ctx) {
    return ctx.getString(R.string.const_default_radioset_one).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public String getKeepAlive() {
    return keepalive;
  }

  public void setKeepAlive(String keepalive) {
    this.keepalive = keepalive;
  }

  public static void saveKeepAlive(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      String keepalive = terminalInfo.getKeepAlive();
      if (keepalive != null) {
        pref.edit().putString(ctx.getString(R.string.key_pref_keepalive), keepalive).apply();
      }
    }
  }

  public static long getKeepAliveMillis(Context ctx) {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
    return parseInt(
            pref.getString(ctx.getString(R.string.key_pref_keepalive), getDefaultKeepAlive(ctx)))
        * 60000L;
  }

  public static String getKeepAlive(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_keepalive), getDefaultKeepAlive(ctx));
  }

  public static String getDefaultKeepAlive(Context ctx) {
    return ctx.getString(R.string.const_default_keepalive);
  }

  public MandownSet getMandownSet() {
    if (mandownset != null && mandownset.getMesgto() != null) {
      mandownset.setMesgto(mandownset.getMesgto().toUpperCase());
    }
    return mandownset;
  }

  public void setMandownSet(MandownSet mandownset) {
    this.mandownset = mandownset;
  }

  public static void saveMandownset(
      Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      MandownSet mandownSet = terminalInfo.getMandownSet();
      if (mandownSet != null) {
        pref.edit()
            .putBoolean(
                ctx.getString(R.string.key_pref_mandownset_use),
                mandownSet.getUse() == SETTINGS_ENABLE)
            .apply();
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_mandownset_mesgto), mandownSet.getMesgto().toUpperCase())
            .apply();
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_mandownset_angle), mandownSet.getAngle())
            .apply();
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_mandownset_time), mandownSet.getTime())
            .apply();
      }
    }
  }

  public static boolean getMandownSetUse(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(
        ctx.getString(R.string.key_pref_mandownset_use), getDefaultMandownSetUse(ctx));
  }

  public static boolean getDefaultMandownSetUse(Context ctx) {
    return ctx.getString(R.string.const_default_mandownset_use)
        .equals(Integer.toString(SETTINGS_ENABLE));
  }

  public static String getMandownSetMesgto(Context ctx, SharedPreferences pref) {
    return pref.getString(
        ctx.getString(R.string.key_pref_mandownset_mesgto), getDefaultMandownSetMesgto(ctx));
  }

  public static String getDefaultMandownSetMesgto(Context ctx) {
    return ctx.getString(R.string.const_default_mandownset_mesgto);
  }

  public static String getMandownSetAngle(Context ctx, SharedPreferences pref) {
    return pref.getString(
        ctx.getString(R.string.key_pref_mandownset_angle), getDefaultMandownSetAngle(ctx));
  }

  public static String getDefaultMandownSetAngle(Context ctx) {
    return ctx.getString(R.string.const_default_mandownset_angle);
  }

  public static String getMandownSetTime(Context ctx, SharedPreferences pref) {
    return pref.getString(
        ctx.getString(R.string.key_pref_mandownset_time), getDefaultMandownSetTime(ctx));
  }

  public static String getDefaultMandownSetTime(Context ctx) {
    return ctx.getString(R.string.const_default_mandownset_time);
  }

  public SafetySettings getSafetySettings() {
    if (safetySettings != null && safetySettings.getMessageTo() != null) {
      safetySettings.setMessageTo(safetySettings.getMessageTo().toUpperCase());
    }
    return safetySettings;
  }

  public void setSafetySettings(SafetySettings safetySettings) {
    this.safetySettings = safetySettings;
  }

  public RtmSet getRtmSet() {
    return rtmset;
  }

  public void setRtmSet(RtmSet rtmset) {
    this.rtmset = rtmset;
  }

  public static void saveRtmSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      RtmSet rtmSet = terminalInfo.getRtmSet();
      if (rtmSet != null) {
        pref.edit()
          .putBoolean(ctx.getString(R.string.key_pref_rtmset_use),
            rtmSet.getUse() == SETTINGS_ENABLE)
          .apply();
        String keyNotice = ctx.getString(R.string.key_pref_rtmset_notice);
        if (rtmSet.getNotice() > 0) {
          pref.edit()
              .putInt(keyNotice, rtmSet.getNotice())
              .apply();
        } else {
          pref.edit().remove(keyNotice).apply();
        }
        String keyCallto = ctx.getString(R.string.key_pref_rtmset_callto);
        if (rtmSet.getCallto() != null) {
          pref.edit()
                  .putString(keyCallto, rtmSet.getCallto().toUpperCase())
                  .apply();
        } else {
          pref.edit().remove(keyCallto).apply();
        }
      }
    }
  }

  public static boolean getRtmSetUse(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(ctx.getString(R.string.key_pref_rtmset_use), getDefaultRtmSetUse(ctx));
  }

  public static boolean getDefaultRtmSetUse(Context ctx) {
    return ctx.getString(R.string.const_default_rtmset_use).equals(Integer.toString(SETTINGS_ENABLE));
  }

  public static int getRtmSetNotice(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_rtmset_notice), getDefaultRtmSetNotice(ctx));
  }

  public static int getDefaultRtmSetNotice(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_rtmset_notice);
  }

  public static String getRtmSetCallto(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_rtmset_callto), getDefaultRtmSetCallto(ctx)).toUpperCase();
  }

  public static String getDefaultRtmSetCallto(Context ctx) {
    return ctx.getString(R.string.const_default_rtmset_callto).toUpperCase();
  }

  public String getCallDefault() {
    return calldefult;
  }

  public void setCallDefault(String calldefult) {
    this.calldefult = calldefult;
  }

  public static void saveCallDefault(
      Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      String calldefault = terminalInfo.getCallDefault();
      if (calldefault != null) {
        pref.edit().putString(ctx.getString(R.string.key_pref_calldefault), calldefault).apply();
      }
    }
  }

  public static String getCallDefault(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_calldefault), getDefaultCallDefault(ctx));
  }

  public static String getDefaultCallDefault(Context ctx) {
    return ctx.getString(R.string.const_default_calldefault);
  }

  public EmcSet getEmcSet() {
    if (emcset != null && emcset.getMesgto() != null) {
      emcset.setMesgto(emcset.getMesgto().toUpperCase());
    }
    return emcset;
  }

  public void setEmcSet(EmcSet emcset) {
    this.emcset = emcset;
  }

  public static void saveEmcSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      EmcSet emcset = terminalInfo.getEmcSet();
      if (emcset != null) {
        pref.edit().putInt(ctx.getString(R.string.key_pref_emcset_use), emcset.getUse()).apply();
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_emcset_mesgto), emcset.getMesgto().toUpperCase())
            .apply();
        pref.edit()
            .putString(ctx.getString(R.string.key_pref_emcset_notice), emcset.getNotice())
            .apply();
      }
    }
  }

  public static int getEmcSetUse(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_emcset_use), getDefaultEmcSetUse(ctx));
  }

  public static int getDefaultEmcSetUse(Context ctx) {
    return Integer.parseInt(ctx.getString(R.string.const_default_emcset_use));
  }

  public static String getEmcSetMesgto(Context ctx, SharedPreferences pref) {
    return pref.getString(
        ctx.getString(R.string.key_pref_emcset_mesgto), getDefaultEmcSetMesgto(ctx));
  }

  public static String getDefaultEmcSetMesgto(Context ctx) {
    return ctx.getString(R.string.const_default_emcset_mesgto);
  }

  public static String getEmcSetNotice(Context ctx, SharedPreferences pref) {
    return pref.getString(
        ctx.getString(R.string.key_pref_emcset_notice), getDefaultEmcSetNotice(ctx));
  }

  public static String getDefaultEmcSetNotice(Context ctx) {
    return ctx.getString(R.string.const_default_emcset_notice);
  }

  public TerminalStatus getTerminalStatus() {
    return terminalstatus;
  }

  public void setTerminalStatus(TerminalStatus terminalstatus) {
    this.terminalstatus = terminalstatus;
  }

  public static void saveTerminalStatusSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    Set<String> values = new HashSet<>();
    if (terminalInfo != null) {
      TerminalStatus status = terminalInfo.getTerminalStatus();
      if (status != null) {
        if (status.getS0() != null && status.getS0().length() > 0) {
          values.add(Base64Decoder.decode(status.getS0()));
        }
        if (status.getS1() != null && status.getS1().length() > 0) {
          values.add(Base64Decoder.decode(status.getS1()));
        }
        if (status.getS2() != null && status.getS2().length() > 0) {
          values.add(Base64Decoder.decode(status.getS2()));
        }
        if (status.getS3() != null && status.getS3().length() > 0) {
          values.add(Base64Decoder.decode(status.getS3()));
        }
        if (status.getS4() != null && status.getS4().length() > 0) {
          values.add(Base64Decoder.decode(status.getS4()));
        }
        if (status.getS5() != null && status.getS5().length() > 0) {
          values.add(Base64Decoder.decode(status.getS5()));
        }
        if (status.getS6() != null && status.getS6().length() > 0) {
          values.add(Base64Decoder.decode(status.getS6()));
        }
        if (status.getS7() != null && status.getS7().length() > 0) {
          values.add(Base64Decoder.decode(status.getS7()));
        }
        if (status.getS8() != null && status.getS8().length() > 0) {
          values.add(Base64Decoder.decode(status.getS8()));
        }
        if (status.getS9() != null && status.getS9().length() > 0) {
          values.add(Base64Decoder.decode(status.getS9()));
        }
      }
      // 2018/4/23
      // "業態設定が表示されない"に対応
      // 本API 1回目呼び出し時には問題ない。2回目呼び出し時には、terminalInfoがNULLとなり。
      // 情報が消される。
      pref.edit().putStringSet(ctx.getString(R.string.key_pref_terminal_status_set), values).apply();

      if (values.size() == 0) {
        setTerminalStatus(ctx, pref, null);
      } else {
        String current = getTerminalStatus(ctx, pref);
//        if (current != null && !values.contains(current)) {
          setTerminalStatus(ctx, pref, null);
//        }
      }
    }
  }

  public static Set<String> getTerminalStatusSet(Context ctx, SharedPreferences pref) {
    return pref.getStringSet(ctx.getString(R.string.key_pref_terminal_status_set), new HashSet<>());
  }

  public static void setTerminalStatus(Context ctx, SharedPreferences pref, String status) {
    String key = ctx.getString(R.string.key_pref_terminal_status);
    if (status == null) {
      pref.edit().remove(key).apply();
    } else {
      pref.edit().putString(key, status).apply();
    }
  }

  public static String getTerminalStatus(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_terminal_status), null);
  }

  public CallTypeSetting getCallType() {
    return calltype;
  }

  public void setCallType(CallTypeSetting calltype) {
    this.calltype = calltype;
  }

  public static void saveCallType(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    Set<String> callTypes = new HashSet<>();
    if (terminalInfo != null) {
      CallTypeSetting callType = terminalInfo.getCallType();
      if (callType != null) {
        if (callType.getACS() == 1) {
          callTypes.add(callType.getACSKey());
        }
        if (callType.getFMG() == 1) {
          callTypes.add(callType.getFMGKey());
        }
        if (callType.getCLL() == 1) {
          callTypes.add(callType.getCLLKey());
        }
        if (callType.getFGC() == 1) {
          callTypes.add(callType.getFGCKey());
        }
        if (callType.getMGP() == 1) {
          callTypes.add(callType.getMGPKey());
        }
        if (callType.getNAL() == 1) {
          callTypes.add(callType.getNALKey());
        }
        if (callType.getHHH() == 1) {
          callTypes.add(callType.getHHHKey());
        }
        if (callType.getEMC() == 1) {
          callTypes.add(callType.getEMCKey());
        }
        if (callType.getRTM() == 1) {
          callTypes.add(callType.getRTMKey());
        }
        if (callType.getGPC() == 1) {
          callTypes.add(callType.getGPCKey());
        }
        if (callType.getCMD() == 1) {
          callTypes.add(callType.getCMDKey());
        }
        if (callType.getSAL() == 1) {
          callTypes.add(callType.getSALKey());
        }
        pref.edit().putStringSet(ctx.getString(R.string.key_pref_calltype), callTypes).apply();
      }
    }
  }

  public static Set<String> getCallType(Context ctx, SharedPreferences pref) {
    return pref.getStringSet(ctx.getString(R.string.key_pref_calltype), new HashSet<>());
  }

  public KeySet getKeySet() {
    return keyset;
  }

  public void setKeySet(KeySet keyset) {
    this.keyset = keyset;
  }

  public static void saveKeySet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      KeySet keySet = terminalInfo.getKeySet();
      if (keySet != null) {
        pref.edit()
          .putInt(ctx.getString(R.string.key_pref_keyset_key), keySet.getKey())
          .apply();
        pref.edit()
          .putInt(ctx.getString(R.string.key_pref_keyset_ptt), keySet.getPtt())
          .apply();
      }
    }
  }

  public static int getKeySetKey(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_keyset_key), getDefaultKeySetKey(ctx));
  }

  public static int getDefaultKeySetKey(Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_keyset_key));
  }

  public static int getKeySetPtt(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_keyset_ptt), getDefaultKeySetPtt(ctx));
  }

  public static int getDefaultKeySetPtt(Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_keyset_ptt));
  }

  public GpsInfo getGpsInfo() {
    return gpsinfo;
  }

  public void setGpsInfo(GpsInfo gpsinfo) {
    this.gpsinfo = gpsinfo;
  }

  public static void saveGpsInfo(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      GpsInfo gpsinfo = terminalInfo.getGpsInfo();
      if (gpsinfo != null) {
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_use), gpsinfo.getUse())
                .apply();
        // 2019/01/22 SEC
        // トランシーバー位置情報通知制限対応
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_limit), gpsinfo.getLimit())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_type), gpsinfo.getType())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_mon), gpsinfo.getMon())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_tue), gpsinfo.getTue())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_wed), gpsinfo.getWed())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_thu), gpsinfo.getThu())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_fri), gpsinfo.getFri())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_sat), gpsinfo.getSat())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_sun), gpsinfo.getSun())
                .apply();
        pref.edit()
                .putString(ctx.getString(R.string.key_pref_gpsinfo_start), gpsinfo.getStart())
                .apply();
        pref.edit()
                .putString(ctx.getString(R.string.key_pref_gpsinfo_end), gpsinfo.getEnd())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_time), gpsinfo.getTime())
                .apply();
        pref.edit()
                .putInt(ctx.getString(R.string.key_pref_gpsinfo_dist), gpsinfo.getDist())
                .apply();
      }
    }
  }

  public static boolean getGpsInfoUse(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_use), getDefaultGpsInfoUse(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoUse(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_use);
  }
  // 2019/01/22 SEC
  // トランシーバー位置情報通知制限対応
  public static boolean getGpsInfoLimit(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_limit), getDefaultGpsInfoLimit(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoLimit(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_limit);
  }

  public static boolean getGpsInfoType(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_type), getDefaultGpsInfoType(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoType(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_type);
  }

  public static boolean getGpsInfoMon(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_mon), getDefaultGpsInfoMon(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoMon(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_mon);
  }

  public static boolean getGpsInfoTue(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_tue), getDefaultGpsInfoTue(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoTue(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_tue);
  }

  public static boolean getGpsInfoWed(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_wed), getDefaultGpsInfoWed(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoWed(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_wed);
  }

  public static boolean getGpsInfoThu(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_thu), getDefaultGpsInfoThu(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoThu(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_thu);
  }

  public static boolean getGpsInfoFri(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_fri), getDefaultGpsInfoFri(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoFri(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_fri);
  }

  public static boolean getGpsInfoSat(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_sat), getDefaultGpsInfoSat(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoSat(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_sat);
  }

  public static boolean getGpsInfoSun(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_sun), getDefaultGpsInfoSun(ctx)) == SETTINGS_ENABLE;
  }

  public static int getDefaultGpsInfoSun(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_sun);
  }

  public static String getGpsInfoStart(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_gpsinfo_start), getDefaultGpsInfoStart(ctx));
  }

  public static String getDefaultGpsInfoStart(Context ctx) {
    return ctx.getResources().getString(R.string.const_default_gpsinfo_start);
  }

  public static String getGpsInfoEnd(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_gpsinfo_end), getDefaultGpsInfoEnd(ctx));
  }

  public static String getDefaultGpsInfoEnd(Context ctx) {
    return ctx.getResources().getString(R.string.const_default_gpsinfo_end);
  }

  // トランシーバー位置情報通知制限対応
  // 2019/01/22 SEC

  public static int getGpsInfoTime(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_time), getDefaultGpsInfoTime(ctx));
  }

  public static int getDefaultGpsInfoTime(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_time);
  }

  public static int getGpsInfoDist(Context ctx, SharedPreferences pref) {
    return pref.getInt(ctx.getString(R.string.key_pref_gpsinfo_dist), getDefaultGpsInfoDist(ctx));
  }

  public static int getDefaultGpsInfoDist(Context ctx) {
    return ctx.getResources().getInteger(R.integer.const_default_gpsinfo_dist);
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getTalklimit() {
    return talklimit;
  }

  public void setTalklimit(String talklimit) {
    this.talklimit = talklimit;
  }

  public static void saveTalklimit(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      String talklimit = terminalInfo.getTalklimit();
      if (talklimit != null) {
        pref.edit().putString(ctx.getString(R.string.key_pref_talklimit), talklimit).apply();
      }
    }
  }

  public static String getTalklimit(Context ctx, SharedPreferences pref) {
    return pref.getString(ctx.getString(R.string.key_pref_talklimit), getDefaultTalklimit(ctx));
  }

  public static String getDefaultTalklimit(Context ctx) {
    return ctx.getString(R.string.const_default_talklimit);
  }

  public String getNocall() {
    return nocall;
  }

  public void setNocall(String nocall) {
    this.nocall = nocall;
  }

  public static void saveNocall(
      @NonNull final Context ctx,
      @NonNull final SharedPreferences pref,
      TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      String nocall = terminalInfo.getNocall();
      if (nocall != null) {
        pref.edit().putString(ctx.getString(R.string.key_pref_nocall), nocall).apply();
      }
    }
  }

  public static int getNocall(@NonNull final Context ctx, SharedPreferences pref) {
    return Integer.valueOf(
        pref.getString(ctx.getString(R.string.key_pref_nocall), getDefaultNocall(ctx)));
  }

  public static String getDefaultNocall(@NonNull final Context ctx) {
    return ctx.getString(R.string.const_default_nocall);
  }

  public ActiveSet getActiveSet() {
    return activeSet;
  }

  public void setActiveSet(ActiveSet activeSet) {
    this.activeSet = activeSet;
  }

  // 2018/7/24 近隣距離設定
  public int getNeighborSet() { return neighborSet;}

  public static void saveActiveSet(Context ctx, SharedPreferences pref, TerminalInfo terminalInfo) {
    // save terminal info
    if (terminalInfo != null) {
      ActiveSet activeSet = terminalInfo.getActiveSet();
      if (activeSet != null) {
        pref.edit()
          .putBoolean(ctx.getString(R.string.key_pref_activeset_use), activeSet.getUse() == SETTINGS_ENABLE)
          .apply();
        pref.edit()
          .putString(ctx.getString(R.string.key_pref_activeset_mesgto), activeSet.getMesgto().toUpperCase())
          .apply();
        pref.edit()
          .putString(ctx.getString(R.string.key_pref_activeset_size), activeSet.getSize())
          .apply();
        pref.edit()
          .putString(ctx.getString(R.string.key_pref_activeset_lat), activeSet.getLatitude())
          .apply();
        pref.edit()
          .putString(ctx.getString(R.string.key_pref_activeset_long), activeSet.getLongitude())
          .apply();
      }
    }
  }

  public static boolean getActiveSetUse(Context ctx, SharedPreferences pref) {
    return pref.getBoolean(
      ctx.getString(R.string.key_pref_activeset_use), getDefaultActiveSetUse(ctx));
  }

  private static boolean getDefaultActiveSetUse(@NonNull Context ctx) {
    return parseInt(ctx.getString(R.string.const_default_activeset_use)) == SETTINGS_ENABLE;
  }

  public static String getActiveSetMesgto(Context ctx, SharedPreferences pref) {
    return pref.getString(
      ctx.getString(R.string.key_pref_activeset_mesgto), getDefaultActiveSetMesgto(ctx));
  }

  public static String getDefaultActiveSetMesgto(Context ctx) {
    return ctx.getString(R.string.const_default_activeset_mesgto);
  }

  public static int getActiveSetSize(Context ctx, SharedPreferences pref) {
    return parseInt(pref.getString(
      ctx.getString(R.string.key_pref_activeset_size), getDefaultActiveSetSize(ctx)));
  }

  public static String getDefaultActiveSetSize(Context ctx) {
    return ctx.getString(R.string.const_default_activeset_size);
  }

  public static String getActiveSetLatitude(Context ctx, SharedPreferences pref) {
    return pref.getString(
      ctx.getString(R.string.key_pref_activeset_lat), getDefaultActiveSetLatitude(ctx));
  }

  public static String getDefaultActiveSetLatitude(Context ctx) {
    return ctx.getString(R.string.const_default_activeset_lat);
  }

  public static String getActiveSetLongitude(Context ctx, SharedPreferences pref) {
    return pref.getString(
      ctx.getString(R.string.key_pref_activeset_long), getDefaultActiveSetLongitude(ctx));
  }

  public static String getDefaultActiveSetLongitude(Context ctx) {
    return ctx.getString(R.string.const_default_activeset_long);
  }

  public List<Site> getSites() {
    return sites;
  }

  public void setSites(List<Site> sites) {
    this.sites = sites;
  }

}
