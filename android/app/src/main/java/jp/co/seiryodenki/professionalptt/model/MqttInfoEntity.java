package jp.co.seiryodenki.professionalptt.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MqttInfoEntity extends RealmObject {

  @PrimaryKey private String mqtt_topic;

  public MqttInfoEntity(String mqtt_topic) {
    this.mqtt_topic = mqtt_topic;
  }

  public MqttInfoEntity() {}

  public String getMqttTopic() {
    return mqtt_topic;
  }

  public void setMqttTopic(String mqtt_topic) {
    this.mqtt_topic = mqtt_topic;
  }
}
