package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class SafetySettings {
  private static final int DISABLED = 0;

  @SerializedName("use")
  private int use;

  @SerializedName("mesgto")
  private String mesgto;

  @SerializedName("time")
  private String time;

  public SafetySettings(int use, String mesgto, String time) {
    this.use = use;
    this.mesgto = mesgto;
    this.time = time;
  }

  public boolean isDisabled() {
    return use == DISABLED;
  }

  public String getMessageTo() {
    return mesgto;
  }

  public void setMessageTo(String mesgto) {
    this.mesgto = mesgto;
  }

  public long getTimeMills() {
    return Long.valueOf(time) * 60000L;
  }
}
