package jp.co.seiryodenki.professionalptt.core;

public class AuthException extends PttException {

  public AuthException() {}

  public AuthException(String message) {
    super(message);
  }

  public AuthException(Throwable throwable) {
    super(throwable);
  }

  public AuthException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
