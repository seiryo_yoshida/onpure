package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Button;
import java.util.ArrayList;
import java.util.List;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment.OnCheckGroupChangedListener;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment.OnClickGroupListener;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment.OnLongClickGroupListener;

public class GroupItemRecyclerViewAdapter
    extends RecyclerView.Adapter<GroupItemRecyclerViewAdapter.ViewHolder> {

  private static final String TAG = GroupItemRecyclerViewAdapter.class.getSimpleName();
  private List<GroupInfoEntity> groupList;
  private int focusPosition = 0;
  private boolean isSelectable = false;
  private final OnLongClickGroupListener onLongClickGroupListener;
  private final OnClickGroupListener onClickGroupListener;
  private final OnCheckGroupChangedListener onCheckGroupChangedListener;
  private int paddingGroup = 0;
  private int paddingMultiGroup = 0;

  public GroupItemRecyclerViewAdapter(
      OnLongClickGroupListener onLongClickGroupListener,
      OnClickGroupListener onClickGroupListener,
      OnCheckGroupChangedListener onCheckGroupChangedListener) {
    this.onLongClickGroupListener = onLongClickGroupListener;
    this.onClickGroupListener = onClickGroupListener;
    this.onCheckGroupChangedListener = onCheckGroupChangedListener;
  }

  public void setGroupList(List<GroupInfoEntity> groupList) {
    this.groupList = groupList;
    notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    return new ViewHolder(
        context, LayoutInflater.from(context).inflate(R.layout.item_group, parent, false));
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bindView(groupList.get(position), position == focusPosition);
  }

  public List<GroupInfoEntity> getSelectedGroups() {
    List<GroupInfoEntity> res = new ArrayList<>();
    for (int i = 0; i < groupList.size(); i++) {
      if (groupList.get(i).isSelected()) {
        res.add(groupList.get(i));
      }
    }
    return res;
  }

  public void changeModeMultipleGroup(boolean isSelectable) {
    this.isSelectable = isSelectable;
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    if (groupList != null) {
      return groupList.size();
    }
    return 0;
  }

  public int getSelectedPosition() {
    return focusPosition;
  }

  public void setSelectedPosition(int position) {
    int oldPosition = focusPosition;
    focusPosition = position;
    notifyItemChanged(oldPosition);
    notifyItemChanged(focusPosition);
  }

  public GroupInfoEntity getCurrentInfo() {
    if (groupList == null) {
      return null;
    }
    return groupList.get(focusPosition);
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    private final Context context;
//    private final TextView textId;
    private final TextView textName;
    private final CheckBox selectCheckbox;
    private final Button btnMessage;
    private final View view;
    //private final Drawable[] icons;
    private boolean isMessage;

    ViewHolder(Context context, View view) {
      super(view);
      this.context = context;
      this.view = view;
//      textId = view.findViewById(R.id.group_id);
      this.textName = view.findViewById(R.id.group_name);
      this.selectCheckbox = view.findViewById(R.id.check_item_group);
      this.btnMessage = view.findViewById(R.id.group_message);
      //icons = textName.getCompoundDrawables();
      //左側のパディング幅をセット
      if (paddingGroup <= 0 || paddingMultiGroup <= 0) {
        float scale = this.context.getResources().getDisplayMetrics().density;
        paddingGroup = (int)(15 * scale + 0.5f);
        paddingMultiGroup = (int)(60 * scale + 0.5f);
      }
      this.textName.setPadding(paddingGroup, 0, 0, 0);
    }

    void bindView(GroupInfoEntity group, boolean isSelected) {
      //if (isSelectable) {
      //  textName.setCompoundDrawables(null, null, null, null);
      //} else {
      //  textName.setCompoundDrawables(icons[0], icons[1], icons[2], icons[3]);
      //}
      //textId.setText(group.getGroupId());

      textName.setText(group.getGroupName());
      if (isSelectable) {
        textName.setPadding(paddingMultiGroup, 0, 0, 0);
      } else {
        textName.setPadding(paddingGroup, 0, 0, 0);
      }
      btnMessage.setVisibility((isSelectable) ? View.GONE : View.VISIBLE);
      selectCheckbox.setVisibility((isSelectable) ? View.VISIBLE : View.GONE);
      selectCheckbox.setChecked(group.isSelected());
      selectCheckbox.setOnCheckedChangeListener(
          (buttonView, isChecked) -> {
            if (onCheckGroupChangedListener != null) {
              onCheckGroupChangedListener.onCheckGroup(group, isChecked);
            }
          });
      /*
      view.setOnTouchListener(
          (view, event) -> {
            if (!isSelectable) {
              isMessage = event.getX() > 0.8 * view.getWidth();
            }
            CallService.broadcastTouchEvent(context, event);
            return false;
          });
          */
      /*
      view.setOnClickListener(
          v -> {
            if (isSelectable) {
              selectCheckbox.setChecked(!selectCheckbox.isChecked());
            } else {
              if (onClickGroupListener != null) {
                onClickGroupListener.onClickGroup(group, isMessage);
              }
            }
          });
          */
      //グループ名をクリック→グループ通話画面に遷移
      textName.setOnClickListener(
        v -> {
          if  (isSelectable) {
            selectCheckbox.setChecked(!selectCheckbox.isChecked());
          } else {
            if (onClickGroupListener != null) {
              onClickGroupListener.onClickGroup(group, false);
            }
          }
        });
      //メッセージボタンをクリック→メッセージ作成画面に遷移
      btnMessage.setOnClickListener(
        v -> {
          if  (isSelectable) {
            selectCheckbox.setChecked(!selectCheckbox.isChecked());
          } else {
            if (onClickGroupListener != null) {
              onClickGroupListener.onClickGroup(group, true);
            }
          }
        });
      //長押し→マルチグループ設定ON/OFF
      view.setOnLongClickListener(
          v -> {
            isSelectable = !isSelectable;
            if (onLongClickGroupListener != null) {
              onLongClickGroupListener.onLongClickGroup(isSelectable);
            }
            notifyDataSetChanged();
            return false;
          });
      view.setSelected(isSelected);
    }
  }
}
