package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
  /**
   * リソースをコピーしたファイルを作成する。
   *
   * @param c コンテキスト
   * @param resourceId リソースID
   * @param filename ファイル名
   * @return 作成したファイルの絶対パス
   * @exception IOException 失敗
   */
  public static String create(@NonNull Context c, int resourceId, @NonNull String filename)
      throws IOException {
    File file = new File(c.getFilesDir().getAbsolutePath(), filename);
    if (file.exists()) {
      //return file.getAbsolutePath();
    }
    FileOutputStream output = c.openFileOutput(filename, Context.MODE_PRIVATE);
    InputStream input = c.getResources().openRawResource(resourceId);
    int readByte;
    byte[] buffer = new byte[8048];
    while ((readByte = input.read(buffer)) != -1) {
      output.write(buffer, 0, readByte);
    }
    output.flush();
    output.close();
    input.close();
    return file.getAbsolutePath();
  }
}
