package jp.co.seiryodenki.professionalptt.model;

import android.location.Address;
import android.location.Location;

import com.google.gson.annotations.SerializedName;

import jp.co.seiryodenki.professionalptt.util.BatteryUtils;
import jp.co.seiryodenki.professionalptt.util.TelephonyUtils;

public class DeviceStatusMessage {

  @SerializedName("cmd")
  private String cmd;

  @SerializedName("from")
  private String from;

  @SerializedName("ptt_info")
  private PttInfo ptt_info;

  public DeviceStatusMessage() {}

  public String getCmd() {
    return cmd;
  }

  public void setCmd(String cmd) {
    this.cmd = cmd;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public PttInfo getPtt_info() {
    return ptt_info;
  }

  public void setPtt_info(PttInfo ptt_info) {
    this.ptt_info = ptt_info;
  }

  public void setReport(String report) {
    if (ptt_info == null) {
      ptt_info = new PttInfo();
    }
    ptt_info.setReport(report);
  }

  public static class PttInfo {
    @SerializedName("report")
    private String report;

    @SerializedName("gpsuse")
    private boolean gpsuse; // gps設定有無情報.

    @SerializedName("gpsinfo")
    private GpsInfo gpsinfo;

    @SerializedName("terminalinfo")
    private TerminalInfo terminalinfo;

    @SerializedName("address")
    private String address;

    public String getReport() {
      return report;
    }

    public void setReport(String report) {
      this.report = report;
    }

    public boolean getGpsuse() {
      return gpsuse;
    }

    public void setGpsuse(boolean gpsuse) {
      this.gpsuse = gpsuse;
    }

    public GpsInfo getGpsinfo() {
      return gpsinfo;
    }

    public void setGpsinfo(GpsInfo gpsinfo) {
      this.gpsinfo = gpsinfo;
    }

    public TerminalInfo getTerminalinfo() {
      return terminalinfo;
    }

    public void setTerminalinfo(TerminalInfo terminalinfo) {
      this.terminalinfo = terminalinfo;
    }

    public String getAddress() {
      return address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

  }

  public static class GpsInfo {
    @SerializedName("lat")
    private double lat;

    @SerializedName("lon")
    private double lon;

    @SerializedName("speed")
    private float speed;

    @SerializedName("direct")
    private float direct;

    @SerializedName("ope_mcc")
    private String ope_mcc; // 基地局情報.

    @SerializedName("ope_mnc")
    private String ope_mnc; // 基地局情報.

    @SerializedName("cell_id")
    private String cell_id; // 基地局情報.

    @SerializedName("mobile_area")
    private String mobile_area; // 基地局情報.

    public double getLat() {
      return lat;
    }

    public void setLat(double lat) {
      this.lat = lat;
    }

    public double getLong() {
      return lon;
    }

    public void setLong(double lon) {
      this.lon = lon;
    }

    public float getSpeed() {
      return speed;
    }

    public void setSpeed(float speed) {
      this.speed = speed;
    }

    public float getDirect() {
      return direct;
    }

    public void setDirect(float direct) {
      this.direct = direct;
    }

    public String getOpe_mcc() {
      return ope_mcc;
    }

    public void setOpe_mcc(String ope_mcc) {
      this.ope_mcc = ope_mcc;
    }

    public String getOpe_mnc() {
      return ope_mnc;
    }

    public void setOpe_mnc(String ope_mnc) {
      this.ope_mnc = ope_mnc;
    }

    public String getCell_id() {
      return cell_id;
    }

    public void setCell_id(String cell_id) {
      this.cell_id = cell_id;
    }

    public String getMobile_area() {
      return mobile_area;
    }

    public void setMobile_area(String mobile_area) {
      this.mobile_area = mobile_area;
    }

  }

  public static class TerminalInfo {

    @SerializedName("rest")
    private int rest; // バッテリー残量

    @SerializedName("temp")
    private float temp; // バッテリー温度

    @SerializedName("status")
    private String status; // バッテリー状態

    public int getRest() {
      return rest;
    }

    public void setRest(int rest) {
      this.rest = rest;
    }

    public float getTemp() {
      return temp;
    }

    public void setTemp(float temp) {
      this.temp = temp;
    }

    public String getStatus() {
      return status;
    }

    public void setStatus(String status) {
      this.status = status;
    }

  }

  public static DeviceStatusMessage setCellIdentity(DeviceStatusMessage deviceStatus, TelephonyUtils.CellIdentity cellIdentity) {
    GpsInfo gpsInfo = new GpsInfo();;
    if (cellIdentity != null) {
      gpsInfo.setCell_id(String.valueOf(cellIdentity.getCi()));
      gpsInfo.setOpe_mcc(String.valueOf(cellIdentity.getMcc()));
      gpsInfo.setOpe_mnc(String.valueOf(cellIdentity.getMnc()));
      gpsInfo.setMobile_area(String.valueOf(cellIdentity.getAreaCode()));
    }
    PttInfo pttInfo;
    if (deviceStatus.getPtt_info() == null) {
      pttInfo = new PttInfo();
    } else {
      pttInfo = deviceStatus.getPtt_info();
    }
    pttInfo.setGpsinfo(gpsInfo);
    deviceStatus.setPtt_info(pttInfo);
    return deviceStatus;
  }

  public static DeviceStatusMessage setBatteryInfo(DeviceStatusMessage deviceStatus, BatteryUtils.BatteryInfo batteryInfo) {
    TerminalInfo terminalInfo = new TerminalInfo();
    if (batteryInfo != null) {
      terminalInfo.setRest(batteryInfo.getRest());
      terminalInfo.setTemp(batteryInfo.getTemp());
      terminalInfo.setStatus(batteryInfo.getStatus());
    }
    PttInfo pttInfo;
    if (deviceStatus.getPtt_info() == null) {
      pttInfo = new PttInfo();
    } else {
      pttInfo = deviceStatus.getPtt_info();
    }
    pttInfo.setTerminalinfo(terminalInfo);
    deviceStatus.setPtt_info(pttInfo);
    return deviceStatus;
  }

  public static DeviceStatusMessage setLocation(DeviceStatusMessage deviceStatus, boolean enable, Location location) {
    GpsInfo gpsInfo = new GpsInfo();
    if (location != null) {
      gpsInfo.setLat(location.getLatitude());
      gpsInfo.setLong(location.getLongitude());
      gpsInfo.setSpeed(location.getSpeed());
      gpsInfo.setDirect(location.getBearing());
    }
    PttInfo pttInfo;
    if (deviceStatus.getPtt_info() == null) {
      pttInfo = new PttInfo();
    } else {
      pttInfo = deviceStatus.getPtt_info();
    }
    pttInfo.setGpsuse(enable);
    pttInfo.setGpsinfo(gpsInfo);
    deviceStatus.setPtt_info(pttInfo);
    return deviceStatus;
  }

  public static DeviceStatusMessage setAddress(DeviceStatusMessage deviceStatus, Address address) {
    String addressInfo = null;
    if (address != null) {
      addressInfo = address.getAddressLine(0);
    }
    PttInfo pttInfo;
    if (deviceStatus.getPtt_info() == null) {
      pttInfo = new PttInfo();
    } else {
      pttInfo = deviceStatus.getPtt_info();
    }
    pttInfo.setAddress(addressInfo);
    deviceStatus.setPtt_info(pttInfo);
    return deviceStatus;
  }

}
