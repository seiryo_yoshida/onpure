package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import jp.co.seiryodenki.professionalptt.CallTypeListActivity.CallTypeItem;
import jp.co.seiryodenki.professionalptt.CallTypeListActivity.OnClickCallTypeListener;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.service.CallService;

/**
 * ショートカットリスト表示アダプタ
 * <pre>
 *     サーバから受信した通話種別情報をリスト表示する
 * </pre>
 * */
public class CallTypeItemRecyclerViewAdapter
    extends RecyclerView.Adapter<CallTypeItemRecyclerViewAdapter.ViewHolder> {

  // 表示に使用するlayout xmlのリソースID
  private final int resourceID;

  // リストデータ
  private final List<CallTypeItem> values;

  // リスト選択時コールバックリスナ
  private final OnClickCallTypeListener listener;

  /**
   * コンストラクタ
   *
   * @param resourceID layout xmlのリソースID
   * @param items リストデータ
   * @param listener リスト選択時コールバックリスナ
   */
  public CallTypeItemRecyclerViewAdapter(
      int resourceID, List<CallTypeItem> items, OnClickCallTypeListener listener) {
    this.resourceID = resourceID;
    values = items;
    this.listener = listener;
  }

  /**
   * 画面表示時処理
   *
   * @param parent Parent View
   * @param viewType View Type
   * @return 画面
   */
  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    View view = LayoutInflater.from(context).inflate(resourceID, parent, false);
    return new ViewHolder(context, view);
  }

  /**
   * リスト項目を表示する処理
   *
   * <pre>
   *     データから画面を構成する
   * </pre>
   *
   * @param holder 画面
   * @param position 表示位置
   */
  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bindView(values.get(position));
  }

  /**
   * リストサイズ取得
   *
   * @return リストサイズ
   */
  @Override
  public int getItemCount() {
    return values.size();
  }

  /** 画面データ保持クラス */
  public class ViewHolder extends RecyclerView.ViewHolder {

    // Context
    private final Context context;

    // 画面要素
    private final View view;
    private final TextView contentView;
    private CallTypeItem item;

    /**
     * コンストラクタ
     *
     * @param context Context
     * @param view Inflated View
     */
    public ViewHolder(Context context, View view) {
      super(view);
      this.context = context;
      this.view = view;
      contentView = view.findViewById(R.id.content);
    }

    /**
     * 項目データ設定処理
     *
     * @param callTypeItem 項目データ
     */
    public void bindView(CallTypeItem callTypeItem) {
      item = callTypeItem;
      contentView.setText(callTypeItem.getTitle());
      view.setOnTouchListener(
          (view, event) -> {
            CallService.broadcastTouchEvent(context, event);
            return false;
          });
      view.setOnClickListener(
          v -> {
            if (null != listener) {
              listener.onClickCallType(item);
            }
          });
    }

    @Override
    public String toString() {
      return super.toString() + " '" + contentView.getText() + "'";
    }
  }
}
