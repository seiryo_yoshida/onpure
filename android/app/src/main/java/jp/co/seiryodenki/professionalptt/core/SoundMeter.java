package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import java.util.Calendar;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import static jp.co.seiryodenki.professionalptt.Constants.NOISE_EXCEED_MIN_PERIOD;
import static jp.co.seiryodenki.professionalptt.Constants.NOISE_LEVEL_BASE;

public class SoundMeter {
  private static final String TAG = SoundMeter.class.getSimpleName();
  private static final int SAMPLE_RATE = 8000;

  private static SoundMeter instance;
  private Thread recodeThread;

  private final boolean isEnabled;
  private final int borderLevel;

  private long lastNotification;
  private boolean isNotified = false;
  private boolean isRecoding = false;

  public interface IExceedSoundLevelListener {
    void onExceedSoundLevel(int meter);
  }

  private SoundMeter(final Context ctx) {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
    isEnabled = TerminalInfo.getRtmSetUse(ctx, pref);
    borderLevel = TerminalInfo.getRtmSetNotice(ctx, pref) * NOISE_LEVEL_BASE;
  }

  public static SoundMeter getInstance(@NonNull final Context ctx) {
    if (instance == null) {
      instance = new SoundMeter(ctx);
    }
    return instance;
  }

  public static synchronized void destroy() {
    if (instance == null) {
      return;
    }
    instance.onDestroy();
    instance = null;
  }

  public synchronized void beginRecoding(IExceedSoundLevelListener exceedSoundLevelListener) {
    if (!isEnabled) {
      return;
    }

    if (recodeThread == null) {

      recodeThread =
          new Thread(
              () -> {
                android.os.Process.setThreadPriority(
                    android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

                int bufferSize =
                    AudioRecord.getMinBufferSize(
                        SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                AudioRecord audioRecord =
                    new AudioRecord(
                        MediaRecorder.AudioSource.MIC,
                        SAMPLE_RATE,
                        AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_PCM_16BIT,
                        bufferSize);
                TerminalUtils.setAudioRecordInstance(audioRecord);
                short[] buffer = new short[bufferSize];

                try {
                  audioRecord.startRecording();
                  isRecoding = true;

                  int read = 0;
                  while (isRecoding && !recodeThread.isInterrupted()) {
                    read = audioRecord.read(buffer, 0, buffer.length);
                    if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                      long lastSensingTime = 0;
                      short level = 0;
                      for (int i = 0; i < bufferSize; i++) {
                        synchronized (this) {
                          level = (short) Math.max(level, buffer[i]); // 最大音量を計算
                          if (level <= borderLevel) { // 最大音量がボーダーを超えてない
                            isNotified = false;
                            continue;
                          }

                          long currentTime = Calendar.getInstance().getTimeInMillis(); // 連続検出無視(1秒以内も連続)
                          if (currentTime - lastSensingTime < 1000 || isNotified) {
                            continue;
                          }
                          lastSensingTime = currentTime;

                          // 通知最低間隔の検出
                          int diffP = Long.valueOf((currentTime - lastNotification) / 1000).intValue();
                          if (diffP < NOISE_EXCEED_MIN_PERIOD) {
                            continue;
                          }

                          // 通話中
                          if (StateManager.isCalling()) {
                            continue;
                          }

                          // 条件を満たす
                          lastNotification = currentTime;
                          isNotified = true;
                          if (exceedSoundLevelListener != null) {
                            exceedSoundLevelListener.onExceedSoundLevel(level);
                          }
                        }
                      }
                    } else {
                      isRecoding = false;
                    }
                  }
                } finally {
                  TerminalUtils.releaseRecord();
                  //audioRecord.stop();
                  //audioRecord.release();
                  isRecoding = false;
                  isNotified = true;
                }
              });
      recodeThread.start();
    }
  }

  public void onDestroy() {
    if (recodeThread != null) {
      recodeThread.interrupt();
      try {
        recodeThread.join(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      recodeThread = null;
    }
  }
}
