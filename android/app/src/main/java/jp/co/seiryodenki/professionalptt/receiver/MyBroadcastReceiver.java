package jp.co.seiryodenki.professionalptt.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import jp.co.seiryodenki.professionalptt.BuildConfig;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_PTT_DOWN;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_NETWORK_ONOFF;

/**
 * ブロードキャスト制御クラス
 *
 * <pre>
 *     端末起動とPTTボタン押下のBroadcastを受信して適切な処理を行う
 * </pre>
 */
public class MyBroadcastReceiver extends BroadcastReceiver {

  private static final String TAG = MyBroadcastReceiver.class.getSimpleName();

  // パッケージ名
  private static final String PACKAGE_NAME = "jp.co.seiryodenki.professionalptt";

  /** コンストラクタ */
  public MyBroadcastReceiver() {}

  /**
   * Broadcast受信処理
   *
   * @param context Context
   * @param intent 受信データ
   */
  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    if (action == null) {
      return;
    }
    if (action.equals(Intent.ACTION_BOOT_COMPLETED) || action.equals(Intent.ACTION_REBOOT)) {
      // 端末起動時のBroadcastを受信した時
      Intent intentLogin = new Intent(Intent.ACTION_MAIN);
      intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
      intentLogin.addCategory(Intent.CATEGORY_HOME);
      intentLogin.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME + ".LoginActivity");
      context.startActivity(intentLogin);
    } else if (action.equals(BROADCAST_PTT_DOWN)) {
      // PTTボタン押下
      PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
      if (pm != null && pm.isInteractive()) {
        // 画面点灯時は以降の処理は不要
        return;
      }
      Intent intentLogin = new Intent(Intent.ACTION_MAIN);
      intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intentLogin.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME + ".CallNotificationActivity");
      context.startActivity(intentLogin);
    } else if (action.equals("android.net.wifi.WIFI_STATE_CHANGED") || action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
      Intent intent_network = new Intent(BROADCAST_NETWORK_ONOFF);
      LocalBroadcastManager.getInstance(context).sendBroadcast(intent_network);
    }
  }
}
