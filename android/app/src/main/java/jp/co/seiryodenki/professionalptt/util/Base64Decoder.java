package jp.co.seiryodenki.professionalptt.util;

import android.util.Base64;

public class Base64Decoder {
  public static String decode(String src) {
    if (src == null) {
      return null;
    }
    byte[] result = Base64.decode(src, Base64.DEFAULT);
    if (result == null) {
      return null;
    }
    return new String(result);
  }
}
