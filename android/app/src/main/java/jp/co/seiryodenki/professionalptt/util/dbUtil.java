package jp.co.seiryodenki.professionalptt.util;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.model.UserInfoEntity;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.model.SiteEntity;

import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_GROUP;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_USER;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_AREA;

public class dbUtil {
    static Realm getRealm(){
        return Realm.getDefaultInstance();
    }

    public static String getUserName(String callId){
        Realm realm = getRealm();
        RealmResults<UserInfoEntity> users =
                realm.where(UserInfoEntity.class).sort(DB_KEY_CALL_ID_USER).findAll();
        UserInfoEntity user = null;
        if (users != null && users.size() > 0) {
            if (callId != null && callId.length() > 0) {
                user = realm.where(UserInfoEntity.class)
                        .equalTo(DB_KEY_CALL_ID_USER, callId).findFirst();
            }
        }
        realm.close();
        if(user == null){
            return callId;
        }
        return user.getUserName();
    }

    public static String getGroupName(String groupId){
        Realm realm = getRealm();
        RealmResults<GroupInfoEntity> groups =
                realm.where(GroupInfoEntity.class).sort(DB_KEY_CALL_ID_GROUP).findAll();
        GroupInfoEntity groupName = null;
        if (groups != null && groups.size() > 0) {
            if (groupId != null && groupId.length() > 0) {
                groupName = realm.where(GroupInfoEntity.class)
                        .equalTo(DB_KEY_CALL_ID_GROUP, groupId).findFirst();
            }
        }
        realm.close();;
        if(groupName == null){
            return groupId;
        }
        return groupName.getGroupName();
    }

    public static String getAreaName(String areaId) {
        Realm realm = getRealm();
        RealmResults<SiteEntity> sites = realm.where(SiteEntity.class).findAll();
        SiteEntity site = null;
        if (sites != null && sites.size() > 0) {
            if (areaId != null && areaId.length() > 0) {
                site = realm.where(SiteEntity.class)
                        .equalTo(DB_KEY_CALL_ID_AREA, areaId).findFirst();
            }
        }
        realm.close();
        if(site == null){
            return areaId;
        }
        return site.getArea_name();
    }
}
