package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

import io.realm.Realm;

public class MqttInfo {

  @SerializedName("topic")
  private String topic;

  public MqttInfo(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String toString() {
    return "topic:" + topic;
  }

  public static void saveMqttInfo(Realm realm, List<MqttInfo> mqttInfo) {
    realm.delete(MqttInfoEntity.class);
    Iterator<MqttInfo> mi = mqttInfo.iterator();
    while (mi.hasNext()) {
      MqttInfo mqtt = mi.next();
      realm.copyToRealmOrUpdate(new MqttInfoEntity((mqtt.getTopic())));
    }
  }

}
