package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class EmcSet {

  @SerializedName("use")
  private int use;

  @SerializedName("mesgto")
  private String mesgto;

  @SerializedName("notice")
  private String notice;

  public EmcSet(int use, String mesgto, String notice) {
    this.use = use;
    this.mesgto = mesgto;
    this.notice = notice;
  }

  public int getUse() {
    return use;
  }

  public void setUse(int use) {
    this.use = use;
  }

  public String getMesgto() {
    return mesgto;
  }

  public void setMesgto(String mesgto) {
    this.mesgto = mesgto;
  }

  public String getNotice() {
    return notice;
  }

  public void setNotice(String notice) {
    this.notice = notice;
  }
}
