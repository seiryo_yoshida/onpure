package jp.co.seiryodenki.professionalptt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.service.ServiceConnectionHelper;

import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_CALL_IN_PROGRESS;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_CALL_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_PTT_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.CALLSTATUS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_MIC_MUTED;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_PTT_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_KYOCERA;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_CAT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_ATOM;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_HEADSETHOOK;

/**
 * PTTボタン押下時発呼クラス
 *
 * <pre>
 *     MyBroadcastReceiverでPTT押下のBroadcastを受信したら呼ばれるクラス
 * </pre>
 */
public class CallNotificationActivity extends BaseActivity {
  private static final String TAG = CallNotificationActivity.class.getSimpleName();

  // 画面表示要素
  private ImageButton btnPtt;
  private Button btnDisconnect;

  // PTTボタンの押下状態フラグ
  private boolean mPttKeyPushed = false;

  // CallService接続ヘルパ
  private ServiceConnectionHelper mHelper;

  /** Windowに画面がアタッチされた時の処理 画面点灯設定 */
  @Override
  public void onAttachedToWindow() {
    Window window = getWindow();
    window.addFlags(
        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_call_notification);

    btnPtt = findViewById(R.id.image_volume_activity);
    btnPtt.setOnTouchListener((v, event) -> {
      onClickPtt(v, event);
      CallService.broadcastTouchEvent(this, event);
      return true;
    });
    btnDisconnect = findViewById(R.id.btn_disconnect);
    btnDisconnect.setOnClickListener(v -> {
      mHelper.stopCalling(this);
      finish();
    });
    LocalBroadcastManager.getInstance(this)
            .registerReceiver(mBroadcastReceiver, new IntentFilter(BROADCAST_PTT_STATUS));

    mHelper = new ServiceConnectionHelper(this);
    // Bind to LocalService
    if (!mHelper.isBound()) {
      mHelper.bindDataService(
          this,
          (isSuccess, service) -> {
            if (isSuccess) {
              onPttPressEvent(true);
            }
          });
    }
  }

  /** Activity終了時処理 */
  @Override
  protected void onDestroy() {
    if (mHelper != null) {
      mHelper.destory(this);
    }
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    super.onDestroy();
  }

  /** Activity停止時処理 */
  @Override
  protected void onPause() {
    super.onPause();
    if (mHelper.isBound()) {
      mHelper.unbindDataService(this);
    }
  }

  /**
   * キータッチのUP時処理
   *
   * @param keyCode キーコード
   * @param event 処理イベント
   * @return 処理結果
   */
  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if ((keyCode == KEY_PTT) || (keyCode == KEY_PTT_KYOCERA) || (keyCode == KEY_PTT_ATOM) || (keyCode == KEY_PTT_CAT)) {
      mPttKeyPushed = false;
      onPttPressEvent(false);
    }
    return super.onKeyUp(keyCode, event);
  }

  /**
   * キータッチのPUSH時処理
   *
   * @param keyCode キーコード
   * @param event 処理イベント
   * @return 処理結果
   */
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if ((keyCode == KEY_PTT) || (keyCode == KEY_PTT_KYOCERA) || (keyCode == KEY_PTT_ATOM) || (keyCode == KEY_PTT_CAT)) {
      if (!mPttKeyPushed) {
        mPttKeyPushed = true;
        onPttPressEvent(true);
      }
      CallService.broadcastTouchEvent(this, null);
    }
    else if(keyCode == KEY_HEADSETHOOK){
      if (!mPttKeyPushed) {
        mPttKeyPushed = true;
        onPttPressEvent(true);
      } else {
        mPttKeyPushed = false;
        onPttPressEvent(false);       
      } 
      CallService.broadcastTouchEvent(this, null);
    }
    return super.onKeyDown(keyCode, event);
  }

  /** Broadcast受信クラス */
  private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

    /**
     * Broadcast受信時処理
     *
     * @param context Context
     * @param intent Intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent == null || intent.getAction() == null) {
        return;
      }
      switch (intent.getAction()) {
        case BROADCAST_CALL_STATUS:
        case BROADCAST_CALL_IN_PROGRESS:
          CALLSTATUS callStatus = (CALLSTATUS) intent.getSerializableExtra(INTENT_CALL_STATUS);
          boolean micMuted = intent.getBooleanExtra(INTENT_CALL_MIC_MUTED, true);
          onReceiveCallStatus(callStatus, micMuted);
          break;
        case BROADCAST_PTT_STATUS:
          onReceivePttStatus(intent);
          break;
      }
    }

    /**
     * PTTボタン押下Broadcast受信時処理
     *
     * @param intent Intent
     */
    private void onReceivePttStatus(Intent intent) {
      Integer isPushed = intent.getIntExtra(INTENT_PTT_STATUS, 0);
      onPttPressEvent(isPushed == 0);
    }
  };

  /**
   * 通話状況受信時処理
   *
   * <pre>
   *     IDLE時にActivityを破棄
   * </pre>
   *
   * @param callStatus 通話状況
   * @param micMuted マイク状況
   */
  private void onReceiveCallStatus(CALLSTATUS callStatus, boolean micMuted) {
    switch (callStatus) {
      case IDLE:
        finish();
        break;
    }
  }

  /**
   * PTTボタン押下時処理
   *
   * @param v ボタンView
   * @param event 押下イベント
   */
  public void onClickPtt(View v, MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      onPttPressEvent(true);
    } else if (event.getAction() == MotionEvent.ACTION_UP) {
      onPttPressEvent(false);
    }
  }

  /**
   * PTTボタン押下時処理
   *
   * <pre>PTTボタン押下状態をサービスに通知する</pre>
   *
   * @param actionDown 押下状態
   */
  private synchronized void onPttPressEvent(boolean actionDown) {
    mHelper.onPressPttEvent(this, actionDown);
  }

}
