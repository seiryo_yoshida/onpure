package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;
import jp.co.seiryodenki.professionalptt.core.JsonObject;

public class AppSet extends JsonObject {

  @SerializedName("use")
  private String use;

  public AppSet(String use) {
    this.use = use;
  }

  public String getUse() {
    return use;
  }

  public void setUse(String use) {
    this.use = use;
  }
}
