package jp.co.seiryodenki.professionalptt.service;

public interface IRequestBindServiceListener {
    void onCompleteRequest(boolean isSuccess, CallService service);
}
