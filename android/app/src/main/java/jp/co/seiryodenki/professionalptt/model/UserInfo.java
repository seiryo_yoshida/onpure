package jp.co.seiryodenki.professionalptt.model;

import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_USER;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.util.Base64Decoder;

public class UserInfo {
  @SerializedName("trm_terminal_id")
  private String trm_terminal_id;

  @SerializedName("usr_login_id")
  private String usr_login_id;

  @SerializedName("usr_name")
  private String usr_name;

  @SerializedName("usr_priority")
  private int usr_priority;

  public UserInfo(String trm_terminal_id, String usr_login_id, String usr_name, int usr_priority) {
    this.trm_terminal_id = trm_terminal_id;
    this.usr_login_id = usr_login_id;
    this.usr_name = usr_name;
    this.usr_priority = usr_priority;
  }

  public String getTerminalId() {
    return trm_terminal_id;
  }

  public void setTerminalId(String trm_terminal_id) {
    this.trm_terminal_id = trm_terminal_id;
  }

  public String getUserId() {
    return usr_login_id;
  }

  public void setUserId(String usr_login_id) {
    this.usr_login_id = usr_login_id;
  }

  public String getUserName() {
    return usr_name;
  }

  public void setUserName(String usr_name) {
    this.usr_name = usr_name;
  }

  public int getPriority() {
    return usr_priority;
  }

  public void setPriority(int usr_priority) {
    this.usr_priority = usr_priority;
  }

  public String toString() {
    return "trm_terminal_id:"
        + trm_terminal_id
        + ", usr_login_id:"
        + usr_login_id
        + ", usr_name:"
        + usr_name
        + ", usr_priority:"
        + usr_priority;
  }

  public static void saveUserInfo(Realm realm, String myUserId, List<UserInfo> userInfo) {
    realm.delete(UserInfoEntity.class);
    Iterator<UserInfo> ui = userInfo.iterator();
    while (ui.hasNext()) {
      UserInfo user = ui.next();
      if (myUserId.equals(user.getUserId())) {
        StateManager.setMyUserInfo(user);
      } else {
        realm.copyToRealmOrUpdate(
                new UserInfoEntity(
                        user.getTerminalId(),
                        user.getUserId(),
                        Base64Decoder.decode(user.getUserName()),
                        user.getPriority()));
      }
    }
  }

  public static UserInfo getUserInfo(Realm realm, String callId) {
    UserInfoEntity user =
        realm.where(UserInfoEntity.class).equalTo(DB_KEY_CALL_ID_USER, callId).findFirst();
    if (user != null) {
      return new UserInfo(
          user.getTerminalId(), user.getUserId(), user.getUserName(), user.getPriority());
    }
    return null;
  }

  public static UserInfo getFirstUser(Realm realm) {
    UserInfoEntity user =
            realm.where(UserInfoEntity.class).sort(DB_KEY_CALL_ID_USER).findFirst();
    if (user != null) {
      return new UserInfo(
              user.getTerminalId(), user.getUserId(), user.getUserName(), user.getPriority());
    }
    return null;
  }
}
