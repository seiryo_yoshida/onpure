package jp.co.seiryodenki.professionalptt.core;

import com.google.gson.Gson;

import jp.co.seiryodenki.professionalptt.util.GsonUtils;

public class JsonObject {
  public static <T> T fromJson(String json, Class<T> classOfT) {
    return GsonUtils.fromJson(json, classOfT);
  }

  public String toString() {
    return GsonUtils.toJson(this);
  }
}
