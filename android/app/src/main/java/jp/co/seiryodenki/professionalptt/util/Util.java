package jp.co.seiryodenki.professionalptt.util;

import android.content.res.Resources;
import android.text.format.DateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import jp.co.seiryodenki.professionalptt.R;

public class Util {

  /**
   * 時間を"時:分:秒"の形式にフォーマットする。
   *
   * @param duration 時間(ミリ秒単位)
   * @return フォーマット文字列
   */
  public static String getTimerFormat(long duration) {
    return String.format(
        "%02d:%02d:%02d",
        TimeUnit.MILLISECONDS.toHours(duration),
        TimeUnit.MILLISECONDS.toMinutes(duration) % 60,
        TimeUnit.MILLISECONDS.toSeconds(duration) % 60);
  }

  public static String getCurrentDateStr() {
    return DateFormat.format("dd/MM/yyyy E", Calendar.getInstance()).toString();
  }

  public static String getSimpleDateStr(long time) {
    return DateFormat.format("yyyy/MM/dd HH:mm", time).toString();
  }

  public static String getCallName(Resources resources, String callTypeStr) {
    String[] titles = resources.getStringArray(R.array.list_call_type);
    String[] types = resources.getStringArray(R.array.list_call_type_values);
    if (titles.length == types.length) {
      for (int i = 0; i < types.length; i++) {
        if (types[i].equals(callTypeStr)) {
          return titles[i];
        }
      }
    }
    return null;
  }

  public static String getCallNameShort(Resources resources, String callTypeStr) {
    String[] titles = resources.getStringArray(R.array.list_call_type_label_short);
    String[] types = resources.getStringArray(R.array.list_call_type_values);
    if (titles.length == types.length) {
      for (int i = 0; i < types.length; i++) {
        if (types[i].equals(callTypeStr)) {
          return titles[i];
        }
      }
    }
    return null;
  }
}
