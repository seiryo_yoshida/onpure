package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import java.util.List;

public class MySensorManager implements SensorEventListener {

  private static final String TAG = MySensorManager.class.getSimpleName();

  private static MySensorManager instance;

  private SensorManager mSensorManager;
  private boolean isNotified = false;
  private boolean isReleased = true;
  private OnSensingListener mOnSensingListener;

  private int mandownAngle;
  private int mandownPeriodSec;

  private Float lastAngle;
  private long lastTimestamp;
  private int regularNotificationCounter = 0;
  private int notificationIntervalSec = 30; // 30s 仮.

  @Override
  public void onSensorChanged(SensorEvent event) {
    getSensorOrientation(event);
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {

  }

  public interface OnSensingListener {
    void onSensingAccelerometer(float angle, boolean isNotified);
    void onReleaseAccelerometer();
  }

  private MySensorManager(@NonNull Context c) {
    mSensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
  }

  public static MySensorManager getInstance(@NonNull Context context) {
    if (instance == null) {
      instance = new MySensorManager(context);
    }
    return instance;
  }

  public void startSensing(@NonNull String mandownSetAngle,@NonNull String mandownSetTime,@NonNull OnSensingListener listener) {

    mandownAngle = Integer.valueOf(mandownSetAngle);
    mandownPeriodSec = Integer.valueOf(mandownSetTime) * 60;

    mOnSensingListener = listener;
    mSensorManager.registerListener(
            this,
            mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_GAME);
    List<Sensor> sensorsAcc = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
    if(sensorsAcc.size() != 0) {
      for(Sensor s : sensorsAcc){
        showSensingInfo(s);
      }
    }
  }

  public void stopSensing() {
    mOnSensingListener = null;
    if (mSensorManager != null) {
      mSensorManager.unregisterListener(this);
      mSensorManager = null;
    }
    if (instance != null) {
      instance = null;
    }
  }

  private void getSensorOrientation(SensorEvent event) {

    float[] acc = null;
    long timestamp = 0;
    switch(event.sensor.getType()){
      case Sensor.TYPE_ACCELEROMETER:
        acc = event.values.clone();
        timestamp = event.timestamp;
        break;
    }

    if (acc != null){
      Integer angle = applyLowPassFilter(acc2deg(acc[0], acc[1], acc[2]));
      if (angle != null) {
        sensingLog(acc[0], acc[1], acc[2], angle);
        setAngle(angle, timestamp);
      }
    }
  }

  private void setAngle(int angle, long timestamp) {

    if (Math.abs(angle) < mandownAngle || lastTimestamp == 0) {
      lastTimestamp = timestamp;
      if (!isReleased) {
        isReleased = true;
        regularNotificationCounter = 0;
        mOnSensingListener.onReleaseAccelerometer();
      }
    } else {
      int diffSec = Long.valueOf((timestamp - lastTimestamp) / 1000000000).intValue();
      if (diffSec > mandownPeriodSec) {
        if (mOnSensingListener != null && !isNotified) {
          mOnSensingListener.onSensingAccelerometer(angle, isNotified);
          isNotified = true;
          isReleased = false;
          regularNotificationCounter = 0;
        } else if (regularNotificationCounter < (diffSec - mandownPeriodSec) / notificationIntervalSec) {
          regularNotificationCounter++;
          mOnSensingListener.onSensingAccelerometer(angle, isNotified);
        }
      } else {
        isNotified = false;
        mandownLog(diffSec);
      }
    }
  }

  private int acc2deg(float x, float y, float z) {
    // 0 degree if Y is points to top
    return Long.valueOf(Math.round(
            ((Math.atan(y/(Math.sqrt(Math.pow(x,2)+Math.pow(z,2)))))*180)/Math.PI
    )).intValue() - 90;
  }

  private Integer applyLowPassFilter(int angle) {
    if (lastAngle == null) {
      lastAngle = (float) angle; // this is for omit first time sensing data
      return null;
    }

    final float alpha = 0.9f;
    lastAngle = alpha * lastAngle + (1f - alpha) * (float) angle;
    return Math.round(lastAngle);
  }

  private void showSensingInfo(@NonNull Sensor sensor) {
    StringBuilder builder = new StringBuilder("[SensorManager] --- \nName: ");
    builder.append(sensor.getName());
    builder.append("\n");

    builder.append("Vendor: ");
    builder.append(sensor.getVendor());
    builder.append("\n");

    builder.append("Type: ");
    builder.append(sensor.getType());
    builder.append("\n");

    int data = sensor.getMinDelay();
    builder.append("Mindelay: ");
    builder.append(String.valueOf(data));
    builder.append(" usec\n");

    data = sensor.getMaxDelay();
    builder.append("Maxdelay: ");
    builder.append(String.valueOf(data));
    builder.append(" usec\n");

    data = sensor.getReportingMode();
    String stinfo = "unknown";
    if(data == 0){
      stinfo = "REPORTING_MODE_CONTINUOUS";
    }else if(data == 1){
      stinfo = "REPORTING_MODE_ON_CHANGE";
    }else if(data == 2){
      stinfo = "REPORTING_MODE_ONE_SHOT";
    }
    builder.append("ReportingMode: ");
    builder.append(stinfo);
    builder.append("\n");

    builder.append("MaxRange: ");
    float fData = sensor.getMaximumRange();
    builder.append(String.valueOf(fData));
    builder.append("\n");

    builder.append("Resolution: ");
    fData = sensor.getResolution();
    builder.append(String.valueOf(fData));
    builder.append(" m/s^2\n");

    builder.append("Power: ");
    fData = sensor.getPower();
    builder.append(String.valueOf(fData));
    builder.append(" mA\n ---");
  }

  private int counter;
  private int averageAngle;
  private void sensingLog(float x, float y, float z, int angle) {
    if (counter++ < 100) {
      averageAngle += angle;
    } else {
      counter = 0;
      averageAngle = 0;
    }
  }

  private int lastDownTimeSec;
  private void mandownLog(int sec) {
    if (lastDownTimeSec != sec) {
      lastDownTimeSec = sec;
    }
  }

}
