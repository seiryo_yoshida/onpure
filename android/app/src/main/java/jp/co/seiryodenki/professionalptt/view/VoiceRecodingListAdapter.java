package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jp.co.seiryodenki.professionalptt.util.dbUtil;

import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.VoiceInfo;

public class VoiceRecodingListAdapter
    extends RecyclerView.Adapter<VoiceRecodingListAdapter.ViewHolder> {

  private List<VoiceInfo.VoiceFile> recoding = new ArrayList<>();
  private OnSelectVoiceFileListener onSelectVoiceFileListener;

  public interface OnSelectVoiceFileListener {
    void onSelectVoiceFile(VoiceInfo.VoiceFile file);
  }

  public VoiceRecodingListAdapter(OnSelectVoiceFileListener listener) {
    onSelectVoiceFileListener = listener;
  }

  public void notifyDataSetChanged(List<VoiceInfo.VoiceFile> files) {
    recoding = files;
    notifyDataSetChanged();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new ViewHolder(
            LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_voice_recoding, parent, false));
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bindView(recoding.get(position));
  }

  @Override
  public int getItemCount() {
    if (recoding == null) {
      return 0;
    }
    return recoding.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView fileDate;
    private final TextView fileMeeting;
    private View view;

    public ViewHolder(View view) {
      super(view);
      fileDate  = view.findViewById(R.id.text_voice_recoding_date);
      fileMeeting = view.findViewById(R.id.text_voice_recoding_meeting);
      this.view = view;
    }

    public void bindView(VoiceInfo.VoiceFile file) {
      final Context ctx = view.getContext();
      String diffTimeStr = null;
      // 開始日時及び経過時間を設定
      try {
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormatter = new SimpleDateFormat ("mm:ss");
        Date dateTo               = null;
        Date dateFrom             = null;
        long diffTime             = 0;
        dateTo      = dateTimeFormat.parse(file.getBegin());
        dateFrom    = dateTimeFormat.parse(file.getEnd());
        diffTime    = dateFrom.getTime() - dateTo.getTime();
        diffTimeStr = timeFormatter.format(new Date(diffTime));
      } catch (ParseException e) {
        e.printStackTrace();
      }
      fileDate.setText(file.getBegin() + "(" + diffTimeStr + ")");

      // 通話種別を設定
      // 2018/5/31 V0.4対応　一斉通話ログ取得時にアプリが強制終了する問題に対応
      //String callType     = file.getMeeting().substring(6,7);
      //String conferenceID = file.getMeeting().substring(6,12);
      //if(callType.compareTo("4") == 0){
      //  if(callType.compareTo("40000") == 0) {
      //    fileMeeting.setText("ALL");
      //  } else {
      //    fileMeeting.setText("PERSONAL(" + conferenceID + ")");
      //  }
      //}
      //else if(callType.compareTo("2") == 0){
      //  fileMeeting.setText("GROUP(" + conferenceID + ")");
      //}
//      String callType     = file.getMeeting().substring(6,7);
//      if(callType.compareTo("1") == 0){
//        //fileMeeting.setText("ALL");
//        fileMeeting.setText(R.string.label_switch_send_all);
//      } else if(callType.compareTo("2") == 0 ){
//        String conferenceID = file.getMeeting().substring(6,12);
//        // 2018/06/08 V0.4対応　CallID表示名
//        if(conferenceID != null){
//          String groupName = dbUtil.getGroupName(conferenceID);
//          if(groupName != null){
//            fileMeeting.setText(groupName);
//          }
//        }
//      } else if(callType.compareTo("4") == 0 ){
//        String conferenceID = file.getMeeting().substring(6,12);
//        // 2018/06/08 V0.4対応　CallID表示名
////        fileMeeting.setText("GROUP(" + conferenceID + ")");
////        fileMeeting.setText(dbUtil.getGroupName(conferenceID));
////        fileMeeting.setText(dbUtil.getUserName(conferenceID));
//        // 2018/06/08 V0.4対応　CallID表示名
//        if(conferenceID != null){
//          String userName = dbUtil.getUserName(conferenceID);
//          if(userName != null){
//            fileMeeting.setText(userName);
//          }
//        }
//      }
      fileMeeting.setText(file.getId());
//      //fileMeeting.setText(conferenceID);
      view.setOnClickListener(v -> {
        if (onSelectVoiceFileListener != null) {
          onSelectVoiceFileListener.onSelectVoiceFile(file);
        }
      });
    }

    @Override
    public String toString() {
      return super.toString() + " '" + fileMeeting.getText() + "'";
    }
  }
}
