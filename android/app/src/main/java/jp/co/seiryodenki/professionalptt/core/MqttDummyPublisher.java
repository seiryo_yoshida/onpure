package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import java.util.Timer;
import java.util.TimerTask;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.util.logger;

import static jp.co.seiryodenki.professionalptt.Constants.MQTT_KEEP_ALIVE_PERIOD;

public class MqttDummyPublisher {
  private static final String TAG = MqttDummyPublisher.class.getSimpleName();

  private Handler looper;
  private Timer timer;
  private Handler callLooper;
  private Timer callTimer;
  private long periodMilli = MQTT_KEEP_ALIVE_PERIOD;

  public interface OnFireKeepAliveListener {
    void onFireKeepAlive();
  }

  public MqttDummyPublisher(Context context) {
    try {
      long period = TerminalInfo.getKeepAliveMillis(context);
      periodMilli = period;
    } catch (Throwable ignore) {
      logger.e(TAG, "keep alive except: ", ignore);
    }
  }

  public synchronized void startKeepAlive(@NonNull OnFireKeepAliveListener onFireKeepAliveListener) {
    if (timer != null) {
      stopKeepAlive();
    }
    try {
      looper = new Handler(Looper.getMainLooper());
      TimerTask task =
          new TimerTask() {
            @Override
            public void run() {
              looper.post(
                  () -> {
                    try {
                      MqttManager.getInstance().publishDummyPacket(false);
                      onFireKeepAliveListener.onFireKeepAlive();
                    } catch (Exception ignored) {
                    }
                  });
            }
          };
      timer = new Timer("MQTT keep alive scheduler");
      timer.schedule(task, 0, periodMilli);
    } catch (Exception e) {
      logger.e(TAG, "Cannot start Mqtt", e);
    }
  }

  public synchronized void stopKeepAlive() {
    try {
      if (timer != null) {
        timer.cancel();
        timer = null;
      }
    } catch (Exception e) {
      logger.e(TAG, "cancel dummy timer", e);
    }
  }

  public synchronized void startCallKeepAlive() {
    if (callTimer != null) {
      stopCallKeepAlive();
    }
    try {
      callLooper = new Handler(Looper.getMainLooper());
      TimerTask task =
          new TimerTask() {
            @Override
            public void run() {
              callLooper.post(
                  () -> {
                    try {
                      MqttManager.getInstance().publishDummyPacket(true);
                    } catch (Exception ignored) {
                    }
                  });
            }
          };
      callTimer = new Timer("MQTT keep alive scheduler during call");
      callTimer.schedule(task, 0, periodMilli);
    } catch (Exception e) {
      logger.e(TAG, "Cannot start Mqtt", e);
    }
  }

  public synchronized void stopCallKeepAlive() {
    try {
      if (callTimer != null) {
        callTimer.cancel();
        callTimer = null;
      }
    } catch (Exception e) {
      logger.e(TAG, "cancel dummy timer", e);
    }
  }

  public synchronized void destroy() {
    stopKeepAlive();
    stopCallKeepAlive();
  }
}
