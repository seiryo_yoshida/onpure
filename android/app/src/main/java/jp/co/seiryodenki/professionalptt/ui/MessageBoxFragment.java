package jp.co.seiryodenki.professionalptt.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.MessageInfo;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.view.MessageBoxItemRecyclerViewAdapter;
import jp.co.seiryodenki.professionalptt.model.CallType;

public class MessageBoxFragment extends Fragment {
  private static final String TAG = MessageBoxFragment.class.getSimpleName();
  private static final String ARG_SECTION_NUMBER = "section_number";
  private static final String ARG_ADDRESS_TYPE = "address_type";
  private static final String ARG_MY_PRIORITY = "arg_my_priority";

  private Realm realm;

  public interface OnSelectPromptListener {
    void onSelectPrompt(CallType callType);
  }

  public interface OnClickMessageBoxListener {
    void onClickMessage(MessageInfo messageInfo);
  }

  public MessageBoxFragment() {}

  /** Returns a new instance of this fragment for the given section number. */
  public static MessageBoxFragment newInstance(int type, int sectionNumber, int myPriority) {
    MessageBoxFragment fragment = new MessageBoxFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    args.putInt(ARG_ADDRESS_TYPE, type);
    args.putInt(ARG_MY_PRIORITY, myPriority);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup message_container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_message_box, message_container, false);

    //Switch switchMultipleSelect = view.findViewById(R.id.switch_multiple_message);
    //switchMultipleSelect.setOnCheckedChangeListener(
    //  (buttonView, isChecked) -> {
    //    // mGroupListAdapter.changeModeMultipleGroup(isChecked);
    //    // mFabMulti.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
    //  });

    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    realm = Realm.getDefaultInstance();
    LinearLayoutManager llm = new LinearLayoutManager(getContext());
    llm.setOrientation(LinearLayoutManager.VERTICAL);
    RecyclerView recyclerView = view.findViewById(R.id.message_recycler_list);
    recyclerView.setLayoutManager(llm);
    recyclerView.setOnTouchListener(
        (v, e) -> {
          CallService.broadcastTouchEvent(getContext(), e);
          return false;
        });

    int addressType = 0;
    int myPriority = 0;
    if (getArguments() != null) {
      addressType = getArguments().getInt(ARG_ADDRESS_TYPE);
      myPriority = getArguments().getInt(ARG_MY_PRIORITY);
    }

    RealmResults<MessageInfo> messages = MessageInfo.findAllAsync(realm, addressType);
    MessageBoxItemRecyclerViewAdapter adapter =
        new MessageBoxItemRecyclerViewAdapter(messages, myPriority);
    recyclerView.setAdapter(adapter);
    RealmChangeListener<RealmResults<MessageInfo>> changeListener =
      results -> {
        // Realm.waitForChange();
        adapter.notifyDataSetChanged();
      };
    messages.addChangeListener(changeListener);
  }

  @Override
  public void onStart() {
    super.onStart();
    if (realm == null) {
      realm = Realm.getDefaultInstance();
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    realm.close();
    realm = null;
  }
}
