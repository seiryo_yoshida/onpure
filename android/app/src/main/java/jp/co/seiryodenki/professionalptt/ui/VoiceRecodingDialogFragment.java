package jp.co.seiryodenki.professionalptt.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;
import jp.co.seiryodenki.professionalptt.R;

public class VoiceRecodingDialogFragment extends DialogFragment {

  /**
   * ダイアログのインスタンスを作成
   *
   * @return
   */
  /*
  public static VoiceRecodingDialogFragment newInstance() {
    VoiceRecodingDialogFragment fragment = new VoiceRecodingDialogFragment();
    return fragment;
  }
  */

  /**
   * ダイアログを作成
   */
  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    //ダイアログを作成
    /*
    Activity activity = getActivity();
    AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.layout.dialog_voice_recoding);
    LayoutInflater inflater = getActivity().getLayoutInflater();
    builder.setView(inflater.inflate(R.layout.dialog_voice_recoding, null));
    return builder.create();
    */

    Dialog dialog = new Dialog(getActivity());
    // タイトル非表示
    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    // フルスクリーン
    dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
    dialog.setContentView(R.layout.dialog_voice_recoding);
    // 背景を透明にする
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    return dialog;
  }

}
