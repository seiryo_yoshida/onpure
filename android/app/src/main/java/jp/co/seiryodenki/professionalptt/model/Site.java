package jp.co.seiryodenki.professionalptt.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import io.realm.Realm;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.util.Base64Decoder;

import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_AREA;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_USER;

public class Site {

  @SerializedName("area_name")
  private String area_name;

  @SerializedName("postal_code")
  private String postal_code;

  @SerializedName("admin")
  private String admin;

  @SerializedName("subadmin")
  private String subadmin;

  @SerializedName("locality")
  private String locality;

  public Site() {}

  public Site(String area_name, String postal_code, String admin, String subadmin, String locality) {
    this.area_name = area_name;
    this.postal_code = postal_code;
    this.admin = admin;
    this.subadmin = subadmin;
    this.locality = locality;
  }

  public String getArea_name() {
    return area_name;
  }

  public void setArea_name(String area_name) {
    this.area_name = area_name;
  }

  public String getPostal_code() {
    return postal_code;
  }

  public void setPostal_code(String postal_code) {
    this.postal_code = postal_code;
  }

  public String getAdmin() {
    return admin;
  }

  public void setAdmin(String admin) {
    this.admin = admin;
  }

  public String getSubadmin() {
    return subadmin;
  }

  public void setSubadmin(String subadmin) {
    this.subadmin = subadmin;
  }

  public String getLocality() {
    return locality;
  }

  public void setLocality(String locality) {
    this.locality = locality;
  }

  public static void saveSites(Realm realm, TerminalInfo terminalInfo) {
    realm.delete(SiteEntity.class);
    if (terminalInfo == null || terminalInfo.getSites() == null) {
      return;
    }
    Iterator<Site> itr = terminalInfo.getSites().iterator();
    while (itr.hasNext()) {
      Site site = itr.next();
      realm.copyToRealm(
              new SiteEntity(
                      Base64Decoder.decode(site.getArea_name()),
                      Base64Decoder.decode(site.getPostal_code()),
                      Base64Decoder.decode(site.getAdmin()),
                      Base64Decoder.decode(site.getSubadmin()),
                      Base64Decoder.decode(site.getLocality())));
    }
  }

  public static Site getFirstSite(Realm realm) {
    SiteEntity site =
            realm.where(SiteEntity.class).findFirst();
    if (site != null) {
      return new Site(site.getArea_name(), site.getPostal_code(), site.getAdmin(),
              site.getSubadmin(), site.getLocality());
    }
    return null;
  }

  public static Site getSite(Realm realm, @NonNull String[] areaNames) {
    SiteEntity site =
            realm.where(SiteEntity.class).in(DB_KEY_CALL_ID_AREA, areaNames).findFirst();
    if (site != null) {
      return new Site(site.getArea_name(), site.getPostal_code(), site.getAdmin(),
              site.getSubadmin(), site.getLocality());
    }
    return null;
  }

  public static String getAreaStr(Site site) {
    if (site == null) {
      return null;
    }
    return String.format(Locale.getDefault(), "%1$s %2$s %3$s %4$s",
            site.getPostal_code(), site.getAdmin(), site.getSubadmin(), site.getLocality());
  }

  public static ArrayList<String> getArrayList(Site site) {
    ArrayList<String> list = new ArrayList<>();
    if (!TextUtils.isEmpty(site.postal_code)) {
      list.add(site.postal_code);
    }
    if (!TextUtils.isEmpty(site.admin)) {
      list.add(site.admin);
    }
    if (!TextUtils.isEmpty(site.subadmin)) {
      list.add(site.subadmin);
    }
    if (!TextUtils.isEmpty(site.locality)) {
      list.add(site.locality);
    }
    return list;
  }

}
