package jp.co.seiryodenki.professionalptt.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.Toast;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimeZone;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import android.telephony.TelephonyManager;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import jp.co.seiryodenki.professionalptt.BuildConfig;
import jp.co.seiryodenki.professionalptt.Constants;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.core.CallManager;
import jp.co.seiryodenki.professionalptt.core.HeadsetManager;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import jp.co.seiryodenki.professionalptt.core.PttException;
import jp.co.seiryodenki.professionalptt.core.SipManager;
import jp.co.seiryodenki.professionalptt.core.SoundMeter;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.GroupInfo;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.model.Login;
import jp.co.seiryodenki.professionalptt.model.MessageInfo;
import jp.co.seiryodenki.professionalptt.model.MqttInfo;
import jp.co.seiryodenki.professionalptt.model.MyMqttMessage;
import jp.co.seiryodenki.professionalptt.model.RecentInfo;
import jp.co.seiryodenki.professionalptt.model.RemoteNotificationInfo;
import jp.co.seiryodenki.professionalptt.model.SafetySettings;
import jp.co.seiryodenki.professionalptt.model.Site;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfo;
import jp.co.seiryodenki.professionalptt.util.Base64Decoder;
import jp.co.seiryodenki.professionalptt.util.BatteryUtils;
import jp.co.seiryodenki.professionalptt.util.IntervalJobManager;
import jp.co.seiryodenki.professionalptt.util.MyNotificationManager;
import jp.co.seiryodenki.professionalptt.util.MySensorManager;
import jp.co.seiryodenki.professionalptt.util.SoundPlayer;
import jp.co.seiryodenki.professionalptt.util.TelephonyUtils;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;

import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;

// 2019/03/25 バックグラウンドでのキーイベント対応
import android.os.Build;
import android.view.KeyEvent;
import android.media.MediaPlayer;
import android.app.PendingIntent;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.app.NotificationCompat;
import android.graphics.BitmapFactory;
// 2019/03/25 バックグラウンドでのキーイベント対応

import static jp.co.seiryodenki.professionalptt.BaseActivity.BROADCAST_PTT_LOGOUT;
import static jp.co.seiryodenki.professionalptt.Constants.CALLSTATUS;
import static jp.co.seiryodenki.professionalptt.Constants.CALL_PTTALL_ID;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_GROUP;
import static jp.co.seiryodenki.professionalptt.Constants.EMCSET_USE_DISABLE;
import static jp.co.seiryodenki.professionalptt.Constants.ENABLE_PRIORITY_CALL;
import static jp.co.seiryodenki.professionalptt.Constants.ENABLE_PRIORITY_CALL_MANUAL;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_MIC_MUTED;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_PERIOD;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_STATUS_NOCALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_STATUS_PARKING;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_PTTALL;
import static jp.co.seiryodenki.professionalptt.Constants.MqttMessageType;
import static jp.co.seiryodenki.professionalptt.Constants.REFRESH_RATE_MSEC_ON_CALL;
import static jp.co.seiryodenki.professionalptt.util.IntervalJobManager.ACTION_INTERVAL_JOB_GPS;
import static jp.co.seiryodenki.professionalptt.util.MyNotificationManager.ACTION_CALL_CONFIRMATION;
import static jp.co.seiryodenki.professionalptt.util.MyNotificationManager.ACTION_PHONE_CALL;

public class CallService extends Service
        implements MqttManager.OnReceiveMqttMessageListener, SipManager.OnCallStateErrorListener {

  private static final String TAG = CallService.class.getSimpleName();
  public static final String ACTION_TOUCH = BuildConfig.APPLICATION_ID + ".ACTION_TOUCH";

  // a binder to this service
  private final IBinder mBinder = new CallServiceBinder();

  public boolean isEmergencyMode() {
    return StateManager.isEmergencyMode();
  }

  public boolean isMonitoring() {
    return StateManager.isMonitorMode() && !StateManager.isMonitored();
  }

  public boolean isMonitored() {
    return StateManager.isMonitored();
  }

  public boolean telephonyState = false;

  public CallInfo getDefaultCallInfo() {
    return StateManager.getDefaultCallInfo();
  }

  public void setDefaultCallInfo(CallType callType, String[] callIDs) {
    StateManager.setDefaultCallInfo(callType, callIDs);
  }

  public class CallServiceBinder extends Binder {
    public CallService getService() {
      return CallService.this;
    }
  }

  // parameters for database and preferences
  private SharedPreferences mPreference;

  // parameters for Power manager
  PowerManager.WakeLock wakeLock;

  // background process handler for calling
  private Disposable mDisposableCall;

  private MySensorManager mySensorManager;

  // parameters for calling status
  private long timerNocall;
  public boolean isPressedPtt = false; // while calling

  private Timer loneTimer;
  private boolean hasReleasedLoneTimer = true;

  // 2019/03/25 バックグラウンドでのキーイベント対応
  private MediaPlayer mMediaPlayer;
  private MediaSessionCompat mSession;
  private NotificationManager mNotification;
  private static final String CHANNEL_ID = "default_channel_id";
  private boolean mPttKeyPushed = false;
  // 2019/03/25 バックグラウンドでのキーイベント対応

  public CallService() {}

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    // 2019/03/25 バックグラウンドでのキーイベント対応
    mNotification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    mMediaPlayer = MediaPlayer.create(this, R.raw.alarm);
    activateMediaSession();
    mMediaPlayer.start();

    // 2019/03/25 バックグラウンドでのキーイベント対応
    StateManager.deleteCallInfo();

    TimeZone zone = TimeZone.getDefault();
    int timeOffset = zone.getRawOffset();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    // 2019/03/25 バックグラウンドでのキーイベント対応
    startService();
    // 2019/03/25 バックグラウンドでのキーイベント対応
    // set database and preference instances
    mPreference = PreferenceManager.getDefaultSharedPreferences(this);
    registerReciever();
    restartLoneTimer();

    return START_STICKY;
  }
  // 2019/03/25 バックグラウンドでのキーイベント対応
  private void startService() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      Intent intent = new Intent();
      PendingIntent pendingIntent = PendingIntent.getActivity(
              this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


      if (mNotification != null) {

        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

        mNotification.createNotificationChannel(channel);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.raw.ic_ptt_notification)
                .setLargeIcon(BitmapFactory.decodeResource(
                        getResources(), R.raw.ic_ptt_notification))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notif_msg_content))
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(pendingIntent, true)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)
                .build();

        startForeground(0, notification);
      } else {
        Toast.makeText(this, "Failed to acquire NotificationManager.", Toast.LENGTH_LONG).show();
      }
    }
  }

  private void activateMediaSession() {
    mSession = new MediaSessionCompat(this, TAG);
    mSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
            MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
    mSession.setCallback(new MediaSessionCompat.Callback() {
      @Override
      public boolean onMediaButtonEvent(Intent intent) {
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
          final KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
          if (event == null) {
            return super.onMediaButtonEvent(intent);
          }
          handleMediaEvent(event);
        }
        return super.onMediaButtonEvent(intent);
      }
    });
    mSession.setActive(true);
  }

  private void handleMediaEvent(KeyEvent event) {
    if (event.getAction() == KeyEvent.ACTION_DOWN) {
      switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_HEADSETHOOK:
          if (!mPttKeyPushed) {
            mPttKeyPushed = true;
            onPressPttEvent(true);
          } else {
            mPttKeyPushed = false;
            onPressPttEvent(false);
          }
          break;
        default:
          break;
      }
    }
  }
  // 2019/03/25 バックグラウンドでのキーイベント対応

  private void registerReciever() {
    final IntentFilter filter = new IntentFilter();
    filter.addAction(ACTION_CALL_CONFIRMATION);
    filter.addAction(ACTION_INTERVAL_JOB_GPS);
    filter.addAction(ACTION_PHONE_CALL);
    registerReceiver(mBroadcastReceiver, filter);

    final IntentFilter localFilter = new IntentFilter();
    localFilter.addAction(ACTION_TOUCH);
    localFilter.addAction(BROADCAST_PTT_LOGOUT);
    LocalBroadcastManager.getInstance(this).registerReceiver(mLocalReceiver, localFilter);
  }

  private BroadcastReceiver mLocalReceiver =
          new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
              if (ACTION_TOUCH.equals(intent.getAction())) {
                restartLoneTimer();
              } else if (BROADCAST_PTT_LOGOUT.equals(intent.getAction())) {
                logoutRequest();
              }
            }
          };

  private BroadcastReceiver mBroadcastReceiver =
          new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
              if (intent != null && intent.getAction() != null) {
                if (ACTION_CALL_CONFIRMATION.equals(intent.getAction())) {
                  confirmSwitchCall(intent);
                } else if (ACTION_INTERVAL_JOB_GPS.equals(intent.getAction())) {
                  intervalJobUpdate();
                  IntervalJobManager.setGpsInterval(context);
                } else if (ACTION_PHONE_CALL.equals(intent.getAction())) {
                  // 2019/01/22 SEC
                  // Transceiverアプリ起動中に電話着信でSIP切断処理が実行される問題に関して対処する
                  telephonyState = false;
                  String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                  if(TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state)) {
                    telephonyState = true;
                    if (StateManager.isCalling()) {
                      // 2019/02/25 速度改善のため、構成見直し
                      stopCalling(Constants.MQTTSTATUS.BYE, false);
                      stopTalking();
                    }
                  }
                }
              }
            }
          };

  private void confirmSwitchCall(@NonNull Intent intent) {
    MyNotificationManager.removeNotification(getApplicationContext());
    CallInfo callInfo = CallInfo.getCallInfo(intent);
    boolean isAccept = MyNotificationManager.isAccept(intent);
    String sender = MyNotificationManager.getSenderID(intent);
    CallType callType = MyNotificationManager.getCallType(intent);
    CallType currentCallType = StateManager.getCallType();
    String currentCallId = StateManager.getCallId();
    String[] currentCallList = StateManager.getCallList();
    if (isAccept) {
      Constants.MQTTSTATUS status = Constants.MQTTSTATUS.BYE;
      switch (callType) {
        case CALL:
          int priority = MyNotificationManager.getPriority(intent);
          if (StateManager.isCalling() && currentCallType != null && currentCallId != null) {
            if (StateManager.getMyPriority() < priority) {
              if (TerminalInfo.getInterruptSetParking(this, mPreference)) {
                status = Constants.MQTTSTATUS.PARK;
              } else if (TerminalInfo.getInterruptSetNotice(this, mPreference)
                      && (currentCallType == CallType.CALL || currentCallType == CallType.GROUP_CALL)) {
                String sendTo = "";
                if (currentCallType == CallType.CALL && currentCallList != null) {
                  sendTo = currentCallList[0];
                } else if (currentCallType == CallType.GROUP_CALL) {
                  sendTo = currentCallId;
                }
                MqttManager.getInstance()
                        .notifyMessage(
                                sendTo,
                                MqttMessageType.MSG.toString(),
                                getString(R.string.message_interruptset_notice));
              }
            }
            switch (currentCallType) {
              // 2019/02/25 速度改善のため、構成見直し
              case CALL:
                stopCalling(status, false);
              default:
                /* 他の通話種別では保留は発生しない */
                stopCalling(Constants.MQTTSTATUS.BYE, false);
                break;
            }
          }
          StateManager.setIsParking(status == Constants.MQTTSTATUS.PARK);
          handleIncomingCallAction(callInfo, true);
          break;
        default:
          if (StateManager.isCalling()) {
            // 2019/02/25 速度改善のため、構成見直し
            stopCalling(Constants.MQTTSTATUS.BYE, false);
          }
          handleIncomingCallAction(callInfo, true);
          break;
      }
    } else if (callType == CallType.CALL || callType == CallType.EMERGENCY_CALL || callType == CallType.REMOTE_MONITOR) {
      // 2019/02/25 速度改善のため、構成見直し
      MqttManager.getInstance().notifyMqttStatus(callType, Constants.MQTTSTATUS.BUSY, sender,false);
    }
  }

  private static RemoteNotificationInfo mNotificationInfo;

  static synchronized void setRemoteNotificationInfo(RemoteNotificationInfo notificationInfo) {
    mNotificationInfo = notificationInfo;
  }

  static synchronized Location getLocation() {
    if (mNotificationInfo != null) {
      return mNotificationInfo.getLocation();
    }
    return null;
  }

  private void intervalJobUpdate() {
    IntervalJobManager intervalJobManager = IntervalJobManager.getInstance(this);
    intervalJobManager.startLocationUpdates(this,
            notificationInfo -> {
              if (notificationInfo != null) {
                if (notificationInfo.getLocation() != null) {
                  setRemoteNotificationInfo(notificationInfo);
                  // アクティビティーゾーン.
                  intervalJobManager.setActivityZone(this, mPreference, notificationInfo);
                }

                if (mNotificationInfo == null) {
                  return;
                }
                if (mNotificationInfo.isSatisfiedInterval(this, mPreference)) {
                  mNotificationInfo.setLastInformation();
                  mNotificationInfo.setReport(TerminalInfo.getTerminalStatus(this, mPreference));
                  if (MqttManager.isInstantiated()) {
                    MqttManager.getInstance().notifyTerminalInfo(mNotificationInfo.getMessage());
                  }
                }
              } else {
                logger.e(TAG, "[IntervalJob]Interval job for remote information is null.");
              }
            });
  }

  private SafetySettings safetySettings;

  private synchronized void cancelLoneTimer() {
    if (loneTimer != null) {
      loneTimer.cancel();
      loneTimer = null;
    }
  }

  private synchronized void restartLoneTimer() {
    cancelLoneTimer();
    if (safetySettings == null) {
      logger.e(TAG, "[LoneWorker] Failed to start lone worker safety support.");
      return;
    }
    if (safetySettings.isDisabled()) {
      return;
    }

    String sendTo = safetySettings.getMessageTo();
    if (!hasReleasedLoneTimer) {
      if (!MqttManager.getInstance()
              .notifyMessage(
                      sendTo,
                      MqttMessageType.SAF_ED.toString(),
                      MyMqttMessage.getMessageString(
                              getString(R.string.message_lone_worker_end),
                              MqttManager.getInstance().getMyTerminalId()))) {
      }
      if (wakeLock != null) {
        wakeLock.release();
        wakeLock = null;
      }
      hasReleasedLoneTimer = true;
    }

    long delay = safetySettings.getTimeMills();
    long interval = 30000; // 30s 仮.
    try {
      Handler looper = new Handler(Looper.getMainLooper());
      TimerTask task = new TimerTask() {
        @Override
        public void run() {
          looper.post(
                  () -> {
                    if (hasReleasedLoneTimer) {
                      PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                      if (pm != null && wakeLock == null) {
                        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
                        wakeLock.acquire();
                      }
                      if (!MqttManager.getInstance()
                              .notifyMessageWithGps(
                                      sendTo,
                                      MqttMessageType.SAF_ST.toString(),
                                      MyMqttMessage.getMessageString(
                                              getString(R.string.message_lone_worker_start),
                                              MqttManager.getInstance().getMyTerminalId(),
                                              getLocation()),
                                      RemoteNotificationInfo.getLatitudeLongitude(getLocation()))) {
                      }
                      hasReleasedLoneTimer = false;
                    }
                    MyNotificationManager.notifyEmergencyAlarm(CallService.this);
                  });
        }
      };
      loneTimer = new Timer("Lone timer");
      loneTimer.schedule(task, delay, interval);
    } catch (Exception e) {
      logger.e(TAG, "[LoneWorker] Cannot start loneTimer", e);
    }
  }

  @Override
  public void onDestroy() {
    // 2019/03/25 バックグラウンドでのキーイベント対応
    try {
      if (mMediaPlayer != null) {
        mMediaPlayer.release();
        mMediaPlayer = null;
      }
      mNotification.cancelAll();
      mSession.release();
      // 2019/03/25 バックグラウンドでのキーイベント対応
      LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocalReceiver);
      unregisterReceiver(mBroadcastReceiver);
      stopCallProcess();
      // 2019/03/25 バックグラウンドでのキーイベント対応
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        stopForeground(true);
      }
      // 2019/03/25 バックグラウンドでのキーイベント対応
    }catch (Exception e) {
      logger.e(TAG, "onDestroy", e);
    }
  }

  public void stopCallProcess() {
    IntervalJobManager.getInstance(this).finish(this);
    // 2019/02/25 速度改善のため、構成見直し
    stopCalling(Constants.MQTTSTATUS.BYE, false);
    StateManager.deleteCallInfo();
    MqttManager.destroy();
    SipManager.destroy();
    SoundMeter.destroy();
    if (mySensorManager != null) {
      mySensorManager.stopSensing();
    }
    TelephonyUtils.destroy();
    BatteryUtils.destroy();
    cancelLoneTimer();
  }

  public void startBootRequest(
          String companyId, String terminalId, String userId, String password, Login response) {
    if (SipManager.isInstantiated() && MqttManager.isInstantiated()) {
      MqttManager.getInstance().reconnect();
      CallInfo callInfo = StateManager.getCallInfo();
      if (callInfo != null) {
        localBroadcastCallStatus(callInfo, false);
      }
      return;
    }
    StateManager.create(companyId, terminalId);
    saveLoginData(userId, response);

    // 2019/4/17 新サーバ移行対応
    //// PTT01とPTT02切り替えのためにしよう
    ////SipManager.create(this, companyId, terminalId, password).setOnCallStateErrorListener(this);
    //SipManager.create(this, companyId, terminalId, response.getUsrUuid()).setOnCallStateErrorListener(this);
    SipManager.create(this, companyId, terminalId, response.getUsrUuid(), response.getSipAddr(), response.getSipPort()).setOnCallStateErrorListener(this);

    MqttManager.create(this, companyId, terminalId)
            .setOnReceiveMqttMessageListener(this)
            .connect(companyId + "/" + terminalId, companyId + terminalId, password);
    IntervalJobManager.setGpsInterval(this);
    if (TerminalInfo.getMandownSetUse(this, mPreference)) {
      mySensorManager = MySensorManager.getInstance(this);
      mySensorManager.startSensing(
              TerminalInfo.getMandownSetAngle(this, mPreference),
              TerminalInfo.getMandownSetTime(this, mPreference),
              mSensingListener);
    }
    SoundMeter.getInstance(this)
            .beginRecoding(
                    level -> {
                      String sendTo = TerminalInfo.getRtmSetCallto(this, mPreference);
                      // 2019/02/25 速度改善のため、構成見直し
                      handleCallAction(
                              CallType.REMOTE_MONITOR, new String[] {StateManager.getMyTerminalId()}, null);
                    });
    if (HeadsetManager.isInstantiated()) {
      HeadsetManager.getInstance().unmuteMicrophone();
    }
  }

  private MySensorManager.OnSensingListener mSensingListener =
          new MySensorManager.OnSensingListener() {
            @Override
            public void onSensingAccelerometer(float angle, boolean isNotified) {
              boolean available = TerminalInfo.getMandownSetUse(CallService.this, mPreference);
              String sendTo = TerminalInfo.getMandownSetMesgto(CallService.this, mPreference);
              if (!available || sendTo == null) {
                return;
              }

              if (!isNotified) {
                if (!MqttManager.getInstance()
                        .notifyMessageWithGps(
                                sendTo,
                                MqttMessageType.MAN_ST.toString(),
                                MyMqttMessage.getMessageString(
                                        CallService.this.getString(R.string.message_mandown_start),
                                        MqttManager.getInstance().getMyTerminalId(),
                                        getLocation()),
                                RemoteNotificationInfo.getLatitudeLongitude(getLocation()))) {
                }
              }
              MyNotificationManager.notifyEmergencyAlarm(CallService.this);
            }

            @Override
            public void onReleaseAccelerometer() {
              boolean available = TerminalInfo.getMandownSetUse(CallService.this, mPreference);
              String sendTo = TerminalInfo.getMandownSetMesgto(CallService.this, mPreference);
              if (!available || sendTo == null) {
                return;
              }

              if (!MqttManager.getInstance()
                      .notifyMessageWithGps(
                              sendTo,
                              MqttMessageType.MAN_ST.toString(),
                              MyMqttMessage.getMessageString(
                                      CallService.this.getString(R.string.message_mandown_end),
                                      MqttManager.getInstance().getMyTerminalId(),
                                      getLocation()),
                              RemoteNotificationInfo.getLatitudeLongitude(getLocation()))) {
              }
            }
          };

  public void logoutRequest() {
    stopCallProcess();
    stopSelf();
  }

  public void startSystemStatusTracking() {
  }

  public void publishSos() {
    boolean available = TerminalInfo.getEmcSetUse(this, mPreference) != EMCSET_USE_DISABLE;
    if (!available) {
      return;
    }

    String sendTo = TerminalInfo.getEmcSetMesgto(this, mPreference);
    String emcSetNotice = TerminalInfo.getEmcSetNotice(this, mPreference);
    String[] valueList = getResources().getStringArray(R.array.list_emcset_notice_value);
    if (sendTo == null || emcSetNotice == null || valueList.length < 3) {
      return;
    }

    boolean doCall = false;
    boolean doMessage = false;
    if (valueList[1].equals(emcSetNotice)) {
      doCall = true;
    } else if (valueList[2].equals(emcSetNotice)) {
      doMessage = true;
      doCall = true;
    } else {
      doMessage = true;
    }

    if (doMessage) {
      if (!MqttManager.getInstance()
              .notifyMessageWithGps(
                      sendTo,
                      MqttMessageType.ETC.toString(),
                      MyMqttMessage.getMessageString(
                              getString(R.string.message_emergency_occur),
                              MqttManager.getInstance().getMyTerminalId(),
                              getLocation()),
                      RemoteNotificationInfo.getLatitudeLongitude(getLocation()))) {
      }
    }

    if (doCall) {
      //requestOutgoingCall(CallType.EMERGENCY_CALL);
      handleCallAction(CallType.EMERGENCY_CALL, null, null);
    }
  }

  public void onPressPttEvent(boolean actionDown) {
    if (actionDown) {
      if (!StateManager.isCalling()) {
        CallInfo callInfo = StateManager.getDefaultCallInfo();
        if(!isNetworkConnected()){
          //Toast.makeText(this, R.string.err_network_connection, Toast.LENGTH_LONG).show();
          return;
        }
        if (callInfo != null && callInfo.getCallType() != null && callInfo.getCallList() != null) {
          if (callInfo.getCallType() == CallType.SITE_ALL_CALL) {
            Site site = Site.getSite(Realm.getDefaultInstance(), callInfo.getCallList());
            // 2019/02/25 速度改善のため、構成見直し
            handleCallAction(callInfo.getCallType(), callInfo.getCallList(), Site.getArrayList(site));
          } else {
            // 2019/02/25 速度改善のため、構成見直し
            handleCallAction(callInfo.getCallType(), callInfo.getCallList(), null);
          }
        } else {
          startDefaultCall();
        }
      }
      startTalking(true);
    } else {
      stopTalking();
    }
  }

  private boolean isNetworkConnected() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
  }

  private void startDefaultCall() {
    CallType callType = StateManager.getDefaultCallType(this, mPreference);
    switch (callType) {
      case REMOTE_MONITOR:
      case CALL:
        UserInfo user = UserInfo.getFirstUser(Realm.getDefaultInstance());
        if (user != null) {
          // 2019/02/25 速度改善のため、構成見直し
          handleCallAction(callType, new String[]{user.getUserId()}, null);
        }
        return;
      case GROUP_CALL:
      case GROUP_CALL_ENFORCEMENT:
        GroupInfo group = GroupInfo.getFirstGroup(Realm.getDefaultInstance());
        if (group != null) {
          // 2019/02/25 速度改善のため、構成見直し
          handleCallAction(callType, new String[]{group.getGroupId()}, null);
        }
        return;
      case MULTI_GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        List<GroupInfo> groups = GroupInfo.getDefaultGroups(this, Realm.getDefaultInstance(), mPreference);
        if (groups != null && !groups.isEmpty()) {
          String[] ids = new String[groups.size()];
          for (int i=0; i<groups.size(); i++) {
            ids[i] = groups.get(i).getGroupId();
          }
          // 2019/02/25 速度改善のため、構成見直し
          handleCallAction(callType, ids, null);
        }
        return;
      case SITE_ALL_CALL:
        Site site = Site.getFirstSite(Realm.getDefaultInstance());
        if (site != null) {
          // 2019/02/25 速度改善のため、構成見直し
          handleCallAction(callType, new String[]{site.getArea_name()}, Site.getArrayList(site));
        }
        return;
    }
    // 2019/02/25 速度改善のため、構成見直し
    handleCallAction(callType, null, null);
  }

  public synchronized void startTalking(boolean update) {
    if (update) {
      this.isPressedPtt = true;
    }
    if (!StateManager.isCalling()) {
      return;
    }
    CallInfo callInfo = StateManager.getCallInfo();
    if (callInfo == null) {
      return;
    }
    // 音声モニタ 緊急通話でモニタする端末は除く.
    if ((StateManager.isMonitorMode() || StateManager.isEmergencyMode())
            && !StateManager.isMonitored()) {
      return;
    }
    MqttManager.getInstance()
            .requestPushNotification(
                    callInfo.getCallType().toString(), callInfo.getCallId(), this);
  }

  public synchronized void stopTalking() {
    // 2019/02/25 速度改善のため、構成見直し
    this.isPressedPtt = false;
    if (!HeadsetManager.getInstance().isMicrophoneMuted()) {
      HeadsetManager.getInstance().muteMicrophone();
      HeadsetManager.getInstance().unmuteSpeaker();
    }
    if (!StateManager.isCalling()) {
      return;
    }
    CallInfo callInfo = StateManager.getCallInfo();
    if (callInfo == null) {
      return;
    }
    if (StateManager.isMonitored()) {
      return;
    }
    MqttManager.getInstance()
            .requestChangeStatusWhenReleased(
                    callInfo.getCallType().toString(), callInfo.getCallId());
  }

  private boolean isRequestingCallList;

  public synchronized boolean stopCalling(Constants.MQTTSTATUS status, boolean requestCallList) {
    if (HeadsetManager.isInstantiated()) {
      //HeadsetManager.getInstance().unmuteMicrophone();
      //HeadsetManager.getInstance().unmuteSpeaker();
      if (StateManager.userOperationAvailable()) {
        //SoundPlayer.start(this, R.raw.talk_end, null);
        SoundPlayer.start(
                this,
                R.raw.talk_end,
                (mp) -> {
                    if (HeadsetManager.isInstantiated()) {
                      HeadsetManager.getInstance().unmuteMicrophone();
                      HeadsetManager.getInstance().unmuteSpeaker();
                    }
                });
      } else {
        HeadsetManager.getInstance().unmuteMicrophone();
        HeadsetManager.getInstance().unmuteSpeaker();
      }
    }
    if (CallManager.isInstantiated()) {
      CallManager.getInstance().terminateCall();
    }

    //終話時間をセットする
    Realm realm = Realm.getDefaultInstance();
    RecentInfo.setEndDate(realm);

    if (mDisposableCall != null) {
      if (!isRequestingCallList) {
        isRequestingCallList = requestCallList;
      }

      notifyBye(status, requestCallList);

      mDisposableCall.dispose();
      mDisposableCall = null;
      return true;
    }
    return false;
  }

  private void notifyBye(Constants.MQTTSTATUS status, boolean requestCallList) {
    // 2019/02/25 速度改善のため、構成見直し
    this.isPressedPtt = false;
    MqttManager.getInstance().stopPushNotification();
    MqttManager.getInstance().stopCallKeepAlive();

    CallInfo callInfo = StateManager.getCallInfo();
    if (callInfo == null) {
      return;
    }
    CallType callType = callInfo.getCallType();
    String callId = callInfo.getCallId();
    String callFrom = callInfo.getCallFrom();
    if (callType == null || callId == null || callFrom == null) {
      return;
    }

    if (StateManager.isMonitored()) {
      return;
    } else if ((StateManager.isMonitorMode()
            || StateManager.isEmergencyMode()
            || callType == CallType.CALL)
            && status != Constants.MQTTSTATUS.PARK) {
      status = Constants.MQTTSTATUS.BYE;
    } else if (!callFrom.equals(StateManager.getMyTerminalId())
            && status == Constants.MQTTSTATUS.BYE) {
      status = Constants.MQTTSTATUS.OUT;
      MqttManager.getInstance().requestChangeStatusWhenReleased(callType.toString(), callId);
    }

    MqttManager.getInstance().notifyMqttStatus(callType, status, callId, requestCallList);
  }

  private void handleCallAction(CallType callType, String[] callIds, ArrayList<String> areaList) {
    if (callType == null) {
      logger.e(TAG, "Call Action:CallInfo is null.");
      return;
    }
    switch (callType) {
      case GROUP_CALL_ENFORCEMENT:
      case GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
      case MULTI_GROUP_CALL:
      case CALL:
      case SITE_ALL_CALL:
      case REMOTE_MONITOR:
        if (callIds == null || callIds.length <= 0 || callIds[0].isEmpty()) {
          logger.e(TAG, "Not found call ids.");
          return;
        }
        break;
    }

    isRequestingCallList = false;
    switch (callType) {
      case NEAR_ALL_CALL:
        Double latitude = null;
        Double longitude = null;
        Location location = getLocation();
        if (location == null) {
          MyNotificationManager.notifiedBusy(this, StateManager.getMyTerminalId(), callType);
          return;
        }
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        MqttManager.getInstance().requestMqttCall(callType, latitude, longitude);
        break;
      case SITE_ALL_CALL:
        MqttManager.getInstance().requestMqttSiteAllCall(callType, callIds[0], areaList);
        break;
      case OPERATING_STATION:
      case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
      case ALL_CALL_SYSTEM_WIDE:
      case EMERGENCY_CALL:
        MqttManager.getInstance().requestMqttCall(callType);
        break;
      case GROUP_CALL_ENFORCEMENT:
      case GROUP_CALL:
      case CALL:
        MqttManager.getInstance().requestMqttCall(callType, callIds[0]);
        break;
      case REMOTE_MONITOR:
        if (callIds[0].equals(StateManager.getMyTerminalId())) {
          // 自端末をモニタさせる.
          String callTo = TerminalInfo.getRtmSetCallto(this, mPreference);
          if (MQTT_TOPIC_PTTALL.equals(callTo)) {
            callTo = CALL_PTTALL_ID;
          }
          MqttManager.getInstance().requestMqttCall(callType, new String[]{callTo});
        } else {
          // 相手をモニタする.
          MqttManager.getInstance().requestMqttCall(callType, callIds[0]);
        }
        break;
      case MULTI_GROUP_CALL_ENFORCEMENT:
      case MULTI_GROUP_CALL:
        MqttManager.getInstance().requestMqttCall(callType, callIds);
        break;
    }
  }

  private void handleIncomingCallAction(@NonNull CallInfo callInfo, boolean display) {
    StateManager.setCallInfo(callInfo);

    mDisposableCall =
            Observable.interval(REFRESH_RATE_MSEC_ON_CALL, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnDispose(getIncomingCallActionOnDispose(callInfo, display))
                    .subscribeWith(getIncomingCallObserver(callInfo, display));
  }

  private DisposableObserver<Long> getIncomingCallObserver(
          @NonNull CallInfo callInfo, boolean display) {
    CallType callType = callInfo.getCallType();
    String callId = callInfo.getCallId();
    return new MyDisposableObserver(callInfo, display) {

      @Override
      protected void onStart() {
        // 2019/01/22 SEC
        // 通話競合不具合対応
        if(telephonyState){
          return;
        }

        // 2018/5/31 V0.4対応 通話応答が遅い件の対処
        inviteCallManager(callId);
        //StateManager.setCallStatus(CALLSTATUS.INCOMING);
        //if (display) {
        //  localBroadcastCallStatus(callInfo, false);
        //}
        // 2019/02/25 速度改善のため、構成見直し
        MqttManager.getInstance().notifyMqttStatus(callType, Constants.MQTTSTATUS.START, callId,false);
        StateManager.setCallStatus(CALLSTATUS.INCOMING_CALL);
        if (display) {
          localBroadcastCallStatus(callInfo, false);
        }else{
          MqttManager.getInstance()
                  .requestPushNotification(callType.toString(), callId, CallService.this);
        }
        // 2019/02/25 速度改善のため、構成見直し
        if (isPressedPtt || StateManager.isMonitored()) {
          startTalking(false);
        }
      }
    };
  }

  private void inviteCallManager(String callId) {
    try {
      // 2018/07/20 Asteriskサーバへの録音有無選択を実装
      // "0" + CallID : 録音無、　"1" + CallID : 録音有
      String callNumber;
      callNumber = TerminalUtils.getRecordFlag() + callId; // 一時的に録音無しで設定
      CallManager.getInstance().invite(callNumber);
      if (StateManager.isMonitored()) {
        HeadsetManager.getInstance().unmuteMicrophone();
        HeadsetManager.getInstance().muteSpeaker();
      } else {
        HeadsetManager.getInstance().muteMicrophone();
        HeadsetManager.getInstance().unmuteSpeaker();
      }
    } catch (PttException e) {
      logger.e(TAG, "invite.", e);
    }
  }

  private Action getIncomingCallActionOnDispose(CallInfo callInfo, boolean display) {
    return () -> {
      StateManager.setCallStatus(CALLSTATUS.IDLE);
      if (display) {
        localBroadcastCallStatus(callInfo, false);
      }
      StateManager.deleteCallInfo();
    };
  }

  private long getCurrentMilliSec() {
    return Double.valueOf((double) System.nanoTime() / 1000000d).longValue();
  }

  private class MyDisposableObserver extends DisposableObserver<Long> {

    private CallInfo callInfo;
    private CallType callType;
    private String callId;
    private boolean display;
    private long lastCallTime;
    private long timerCalling;
    private int talkLimitMillis;
    private int noCallMillis;
    private int levelControl;

    public MyDisposableObserver(@NonNull CallInfo callInfo, boolean display) {
      super();
      this.callInfo = callInfo;
      this.callType = callInfo.getCallType();
      this.callId = callInfo.getCallId();
      this.display = display;
      this.noCallMillis = TerminalInfo.getNocall(CallService.this, mPreference) * 1000;
      // 2018/5/28 V0.4対応　連続通話時間制限機能追加
      String talkLimit = TerminalInfo.getTalklimit(CallService.this, mPreference);
      this.talkLimitMillis = Integer.parseInt(talkLimit)  * 1000 * 60;
      this.levelControl = 0;

    }

    @Override
    public void onNext(Long counter) {
      long currentTime = getCurrentMilliSec();
      if (counter == 0) {
        timerCalling = 0;
        lastCallTime = 0;
        timerNocall = 0;
        levelControl = 0;
      } else if (!StateManager.isParked()) {
        timerCalling += currentTime - lastCallTime;
        CallType callType = StateManager.getCallType();
        if (callType != null) {
          switch (callType) {
            case REMOTE_MONITOR:
            case EMERGENCY_CALL:
              break;
            default:
              timerNocall += currentTime - lastCallTime;
              break;
          }
        }
      }
      lastCallTime = currentTime;

      // 2018/06/08 V0.4対応　音声レベル調整
      if ((levelControl == 0) && (timerNocall > 1000)) {
        levelControl = 1;
        TerminalUtils.setNormalAudioSetting();
      }

      // 2018/5/28 V0.4対応　連続通話時間制限機能追加
      if ((timerNocall > noCallMillis) || (timerCalling > talkLimitMillis)) {
        stopCalling(Constants.MQTTSTATUS.BYE, true);
        return;
      }

      if (display && HeadsetManager.isInstantiated()) {
        localBroadcastCallProgress(
                timerCalling, StateManager.isParked(), HeadsetManager.getInstance().isMicrophoneMuted());
      }
      if (StateManager.isMonitored()) {
        HeadsetManager.getInstance().unmuteMicrophone();
        HeadsetManager.getInstance().muteSpeaker();
      }
    }

    @Override
    public void onError(Throwable e) {
      logger.e(TAG, "ERROR: Call request is failed:" + e);
      e.printStackTrace();
      callEnd(callInfo, display);
    }

    @Override
    public void onComplete() {
      callEnd(callInfo, display);
    }
  }

  private void callEnd(CallInfo callInfo, boolean display) {
    StateManager.setCallStatus(CALLSTATUS.IDLE);
    if (display) {
      localBroadcastCallStatus(callInfo, false);
    }
    StateManager.deleteCallInfo();
  }

  private void localBroadcastCallStatus(@NonNull CallInfo callInfo, boolean isParked) {
    Intent intent = new Intent(Constants.BROADCAST_CALL_STATUS);
    intent.putExtra(
            Constants.INTENT_CALL_STATUS,
            isParked ? CALLSTATUS.PARKING : StateManager.getCallStatus());
    intent.putExtra(Constants.INTENT_CALL_TYPE_ID, callInfo.getCallType().getId());
    intent.putExtra(Constants.INTENT_CALL_IDS, callInfo.getCallList());
    callInfo.putIntent(intent);
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  private void localBroadcastCallProgress(long time, boolean isParked, boolean microphoneMuted) {
    if (!StateManager.isCalling()) {
      return;
    }
    CallInfo callInfo = StateManager.getCallInfo();
    if (callInfo == null) {
      return;
    }
    Intent intent = new Intent(Constants.BROADCAST_CALL_IN_PROGRESS);
    intent.putExtra(
            Constants.INTENT_CALL_STATUS,
            isParked ? CALLSTATUS.PARKING : StateManager.getCallStatus());
    intent.putExtra(Constants.INTENT_CALL_TYPE_ID, callInfo.getCallType().getId());
    intent.putExtra(Constants.INTENT_CALL_IDS, StateManager.getCallList());
    intent.putExtra(INTENT_CALL_PERIOD, time);
    intent.putExtra(INTENT_CALL_MIC_MUTED, microphoneMuted);
    callInfo.putIntent(intent);
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }

  @Override
  public void onIncomingCall(
          CallType calltype, String sender, String receiver, MyMqttMessage mqttMessage) { ;
    String callId = mqttMessage.getCallId();
    String[] callList = CallInfo.convertCallList(calltype, callId, mqttMessage.getCallList());
    if (callId == null || callList == null || TextUtils.isEmpty(callList[0])) {
      return;
    }
    int priority = CallInfo.convertPriority(mqttMessage.getPriority());
    CallInfo callInfo = new CallInfo(calltype, callId, mqttMessage.getFrom(), callList, priority);
    boolean available = checkIncomingCallAvailable(callInfo, sender, mqttMessage);
    if (calltype == CallType.REMOTE_MONITOR || calltype == CallType.EMERGENCY_CALL) {
      return; // コールログを保存せず終了.
    }
    if (calltype == CallType.CALL && StateManager.isMyTerminalID(callInfo.getCallFrom())) {
      callId = callList[0];
    }
    if (calltype == CallType.GROUP_CALL_ENFORCEMENT
            || calltype == CallType.GROUP_CALL
            || calltype == CallType.MULTI_GROUP_CALL_ENFORCEMENT
            || calltype == CallType.MULTI_GROUP_CALL
            || calltype == CallType.SITE_ALL_CALL) {
      callId = receiver;
    }
    try (Realm realm = Realm.getDefaultInstance()) {
      if (available) {
        if (StateManager.isMyTerminalID(callInfo.getCallFrom())) {
          RecentInfo.call(realm, calltype, callId, getCallName(realm, callId), callList);
        } else {
          RecentInfo.received(realm, calltype, callId, getCallName(realm, callId), callList);
        }
      } else {
        RecentInfo.missed(realm, calltype, callId, getCallName(realm, callId), callList);
      }
    }
  }

  /**
   * @param callInfo
   * @param sender
   * @param mqttMessage
   * @return true:accept, false:decline or quit
   */
  private synchronized boolean checkIncomingCallAvailable(
          @NonNull CallInfo callInfo, String sender, MyMqttMessage mqttMessage) {
    CallType incomingCallType = callInfo.getCallType();
    String incomingCallId = callInfo.getCallId();
    String[] incomingCallList = callInfo.getCallList();
    try (Realm realm = Realm.getDefaultInstance()) {
      int interruptType = TerminalInfo.getInterruptSetUse(this, mPreference);
      CallType currentCallType = StateManager.getCallType();
      if (StateManager.getCallStatus() != CALLSTATUS.IDLE) {
        if (isEqualCurrentCallId(incomingCallId)) {
          return false;
        }
        if (interruptType == ENABLE_PRIORITY_CALL_MANUAL
                && (StateManager.getCallType() == CallType.REMOTE_MONITOR
                || StateManager.getCallType() == CallType.EMERGENCY_CALL)) {
          interruptType = ENABLE_PRIORITY_CALL;
        }
        if (StateManager.isParked()
                && incomingCallType != CallType.REMOTE_MONITOR
                && incomingCallType != CallType.EMERGENCY_CALL) {
          if (incomingCallType == CallType.CALL || incomingCallType == CallType.EMERGENCY_CALL || incomingCallType == CallType.REMOTE_MONITOR) {
            // 2019/02/25 速度改善のため、構成見直し
            MqttManager.getInstance().notifyMqttStatus(incomingCallType, Constants.MQTTSTATUS.BUSY, incomingCallId,false);
          }
          return false;
        }
      }
      switch (incomingCallType) {
        case CALL:
          int incomingPriority = 0;
          String incomingUserName;
          if (StateManager.isMyTerminalID(incomingCallId)) {
            incomingPriority = StateManager.getMyPriority();
            incomingUserName = StateManager.getMyUserName();
          } else {
            UserInfo user = UserInfo.getUserInfo(realm, incomingCallId);
            if (user == null) {
              // 2019/02/25 速度改善のため、構成見直し
              MqttManager.getInstance().notifyMqttStatus(incomingCallType,Constants.MQTTSTATUS.BUSY, incomingCallId,false);
              return false;
            }
            incomingPriority = user.getPriority();
            incomingUserName = user.getUserName();
          }
          if (StateManager.getCallStatus() != CALLSTATUS.IDLE) {
            switch (interruptType) {
              case ENABLE_PRIORITY_CALL_MANUAL:
                // Ask user to accept or decline new call
                MyNotificationManager.showIncomingPersonalNotification(
                        this, callInfo, incomingUserName);
                return false;
              case ENABLE_PRIORITY_CALL:
                switch (currentCallType) {
                  case CALL:
                  case GROUP_CALL:
                    // Depends on priority
                    if (incomingPriority > StateManager.getMyPriority()) {
                      if (TerminalInfo.getInterruptSetParking(this, mPreference)) {
                        // 2019/02/25 速度改善のため、構成見直し
                        stopCalling(Constants.MQTTSTATUS.PARK, false);
                        StateManager.setIsParking(true);
                      } else if (TerminalInfo.getInterruptSetNotice(this, mPreference)) {
                        String sendTo = "";
                        String[] id = StateManager.getCallList();
                        if (currentCallType == CallType.CALL && id != null) {
                          sendTo = id[0];
                        } else if (currentCallType == CallType.GROUP_CALL) {
                          sendTo = StateManager.getCallId();
                        }
                        MqttManager.getInstance()
                                .notifyMessage(
                                        sendTo,
                                        MqttMessageType.MSG.toString(),
                                        getString(R.string.message_interruptset_notice));
                        // 2019/02/25 速度改善のため、構成見直し
                        stopCalling(Constants.MQTTSTATUS.BYE, false);
                      } else {
                        // 2019/02/25 速度改善のため、構成見直し
                        stopCalling(Constants.MQTTSTATUS.BYE, false);
                      }
                    } else {
                      MqttManager.getInstance().notifyMqttStatus(incomingCallType,
                              Constants.MQTTSTATUS.BUSY, incomingCallId);
                      return false;
                    }
                    break;
                  default:
                    // 2019/02/25 速度改善のため、構成見直し
                    MqttManager.getInstance().notifyMqttStatus(incomingCallType, Constants.MQTTSTATUS.BUSY, incomingCallId,false);
                    return false;
                }
                break;
              default:
                MqttManager.getInstance().notifyMqttStatus(incomingCallType,
                        Constants.MQTTSTATUS.BUSY, incomingCallId);
                return false;
            }
          }
          handleIncomingCallAction(callInfo, true);
          break;
        case ALL_CALL_SYSTEM_WIDE:
        case SITE_ALL_CALL:
        case NEAR_ALL_CALL:
        case GROUP_CALL:
        case MULTI_GROUP_CALL:
          if (StateManager.getCallStatus() != CALLSTATUS.IDLE) {
            switch (interruptType) {
              case ENABLE_PRIORITY_CALL_MANUAL:
                // Ask user to accept or decline new call
                MyNotificationManager.showIncomingGroupNotification(
                        this, callInfo, incomingCallType, incomingCallId, incomingCallList, incomingCallType.toString(), sender);
                return false;
              default:
                return false;
            }
          }
          handleIncomingCallAction(callInfo, true);
          break;
        case GROUP_CALL_ENFORCEMENT:
        case MULTI_GROUP_CALL_ENFORCEMENT:
        case OPERATING_STATION:
        case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
          if (StateManager.getCallStatus() != CALLSTATUS.IDLE) {
            switch (interruptType) {
              case ENABLE_PRIORITY_CALL_MANUAL:
                // Ask user to accept or decline new call
                MyNotificationManager.showIncomingGroupNotification(
                        this, callInfo, incomingCallType, incomingCallId, incomingCallList, incomingCallType.toString(), sender);
                return false;
              case ENABLE_PRIORITY_CALL:
                if (!switchIncomingGroupCallAuto(incomingCallType, sender, mqttMessage)) {
                  return false;
                }
                break;
              default:
                logger.w(TAG, "Interrupt Type[Group]:" + interruptType);
                return false;
            }
          }
          handleIncomingCallAction(callInfo, true);
          break;
        case EMERGENCY_CALL:
        case REMOTE_MONITOR:
          if (StateManager.getCallStatus() != CALLSTATUS.IDLE) {
            switch (interruptType) {
              case ENABLE_PRIORITY_CALL_MANUAL:
              case ENABLE_PRIORITY_CALL:
                if (!switchIncomingGroupCallAuto(incomingCallType, sender, mqttMessage)) {
                  return false;
                }
                break;
              default:
                logger.w(TAG, "Interrupt Type[Group]:" + interruptType);
                return false;
            }
          }
          boolean display =
                  !(mqttMessage.getFrom() != null
                          && StateManager.isMyTerminalID(mqttMessage.getFrom()));
          if (incomingCallType == CallType.REMOTE_MONITOR) {
            display = !StateManager.isMyTerminalID(mqttMessage.getCallId());
          }
          handleIncomingCallAction(callInfo, display);
          break;
      }
      if ((incomingCallType == CallType.OPERATING_STATION
              || incomingCallType == CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT
              || incomingCallType == CallType.ALL_CALL_SYSTEM_WIDE
              || incomingCallType == CallType.NEAR_ALL_CALL)
              // 2019/02/25 速度改善のため、構成見直し
              && isPressedPtt) {
        MqttManager.getInstance()
                .requestPushNotification(
                        StateManager.getCallType().toString(), StateManager.getCallId(), this);
      }
      return true;
    }
  }

  /**
   * @param incomingCallType
   * @param sender
   * @return
   */
  private boolean switchIncomingGroupCallAuto(
          CallType incomingCallType, String sender, MyMqttMessage mqttMessage) {
    CallType currentCallType = StateManager.getCallType();

    switch (incomingCallType) {
      case ALL_CALL_SYSTEM_WIDE:
      case SITE_ALL_CALL:
      case NEAR_ALL_CALL:
      case GROUP_CALL:
      case MULTI_GROUP_CALL:
        return false;
      case GROUP_CALL_ENFORCEMENT:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        switch (currentCallType) {
          case CALL:
          case GROUP_CALL:
          case MULTI_GROUP_CALL:
          case NEAR_ALL_CALL:
          case SITE_ALL_CALL:
            stopCalling(Constants.MQTTSTATUS.BYE, false);
            break;
          default:
            return false;
        }
        return true;
      case EMERGENCY_CALL:
        switch (currentCallType) {
          case EMERGENCY_CALL:
            if (StateManager.getMyTerminalId().equals(mqttMessage.getFrom())) {
              if (StateManager.isMonitored()) {
                // 本来あり得ない
                // 2019/02/25 速度改善のため、構成見直し
                MqttManager.getInstance().notifyMqttStatus(incomingCallType, Constants.MQTTSTATUS.BUSY, mqttMessage.getCallId(),false);
                return false;
              } else {
                // 2019/02/25 速度改善のため、構成見直し
                stopCalling(Constants.MQTTSTATUS.BYE, false);
              }
            } else {
              // 2019/02/25 速度改善のため、構成見直し
              MqttManager.getInstance().notifyMqttStatus(incomingCallType, Constants.MQTTSTATUS.OUT, mqttMessage.getCallId(),false);
              return false;
            }
            break;
          default:
            // 2019/02/25 速度改善のため、構成見直し
            stopCalling(Constants.MQTTSTATUS.BYE, false);
            break;
        }
        return true;
      case REMOTE_MONITOR:
        switch (currentCallType) {
          case EMERGENCY_CALL:
            Constants.MQTTSTATUS status = Constants.MQTTSTATUS.BUSY;
            if (!StateManager.getMyTerminalId().equals(mqttMessage.getCallId())) {
              status = Constants.MQTTSTATUS.OUT;
            }
            // 2019/02/25 速度改善のため、構成見直し
            MqttManager.getInstance().notifyMqttStatus(incomingCallType, status, mqttMessage.getCallId(),false);

            break;
          case REMOTE_MONITOR:
            // モニタされる && 現在モニタされていない
            if (StateManager.getMyTerminalId().equals(mqttMessage.getCallId())) {
              if (!StateManager.isMonitored()) {
                // 2019/02/25 速度改善のため、構成見直し
                stopCalling(Constants.MQTTSTATUS.BYE, false);
              } else {
                // 2019/02/25 速度改善のため、構成見直し
                MqttManager.getInstance().notifyMqttStatus(incomingCallType,Constants.MQTTSTATUS.BUSY, mqttMessage.getCallId(),false);
                return false;
              }
            } else {
              // 2019/02/25 速度改善のため、構成見直し
              MqttManager.getInstance().notifyMqttStatus(incomingCallType, Constants.MQTTSTATUS.OUT, mqttMessage.getCallId(),false);
              return false;
            }
            break;
          default:
            // 2019/02/25 速度改善のため、構成見直し
            stopCalling(Constants.MQTTSTATUS.BYE, false);
            break;
        }
        return true;
      case OPERATING_STATION:
      case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
        switch (currentCallType) {
          case OPERATING_STATION:
          case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
          case ALL_CALL_SYSTEM_WIDE:
          case EMERGENCY_CALL:
          case REMOTE_MONITOR:
            return false;
          default:
            // 2019/02/25 速度改善のため、構成見直し
            stopCalling(Constants.MQTTSTATUS.BYE, false);
            break;
        }
        return true;
    }
    return true;
  }

  private boolean isEqualCurrentCallId(@NonNull String callId) {
    String currentCallId = StateManager.getCallId();
    if (currentCallId != null) {
      if (!TextUtils.isEmpty(currentCallId) && currentCallId.equals(callId)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void onClosingCall(MyMqttMessage mqttMessage) {
    CallType currentCallType = StateManager.getCallType();
    String currentCallId = StateManager.getCallId();

    if (currentCallType == null || currentCallId == null) {
    } else if (!currentCallId.equals(mqttMessage.getCallId())) {
    } else {
      stopCalling(Constants.MQTTSTATUS.BYE, true);
    }
    if (Constants.MQTTSTATUS.BUSY.toString().equals(mqttMessage.getStatus()) && !StateManager.isMonitored()) {
      MyNotificationManager.notifiedBusy(this, mqttMessage.getFrom(), currentCallType);
    }
  }

  @Override
  public void onUpdateServerInfo() {
    getServerInfo();
  }

  public static String getCallName(Realm realm, String callId) {
    UserInfo userInfo = UserInfo.getUserInfo(realm, callId);
    if (userInfo != null) {
      return userInfo.getUserName();
    }
    GroupInfo groupInfo = GroupInfo.getGroupInfo(realm, callId);
    if (groupInfo != null) {
      return groupInfo.getGroupName();
    }
    return callId;
  }

  @Override
  public void onReceiveMessage(String receiver, MyMqttMessage mqttMessage) {
    String sender = mqttMessage.getFrom();
    String body = Base64Decoder.decode(mqttMessage.getBody());
    String attFileId = mqttMessage.getAttFileId();
    if (MQTT_TOPIC_PTTALL.equals(receiver)) {
      receiveMessage(receiver, body, attFileId);
      return;
    }
    if (MqttManager.getInstance().getMyTerminalId().equals(receiver)) {
      receiveMessage(sender, body, attFileId);
      return;
    }
    try (Realm realm = Realm.getDefaultInstance()) {
      realm.executeTransactionAsync(
              transaction -> {
                GroupInfoEntity group =
                        transaction
                                .where(GroupInfoEntity.class)
                                .equalTo(DB_KEY_CALL_ID_GROUP, receiver)
                                .findFirst();
                if (group != null) {
                  receiveMessage(receiver, body, attFileId);
                }
              });
    }
  }

  private void receiveMessage(String userid, String body, String attFileId) {
    String callAlert = getString(R.string.mqtt_call_alert);
    if (TerminalInfo.getMessageset(this, mPreference) != TerminalInfo.getDefaultMessageset(this)
            && !callAlert.equals(body)) {
      MqttManager.getInstance().notifyMessage(userid, MqttMessageType.MSG.toString(), callAlert);
    }

    String senderName;
    try (Realm realm = Realm.getDefaultInstance()) {
      if (MQTT_TOPIC_PTTALL.equals(userid)) {
        senderName = getString(R.string.label_send_all);
      } else {
        senderName = getCallName(realm, userid);
      }
      MessageInfo.receive(realm, userid, senderName, body, attFileId);
    }
    MyNotificationManager.notifyReceiveMessage(this, userid, senderName, body);
  }

  @Override
  public synchronized void onReceiveCallStatus(MyMqttMessage mqttMessage) {
    // logger.e(TAG, "---- NO CALL TIMER[" + StateManager.getCallType() + "] HOLD:" + isParked() + ",
    // TIMER:" + timerNocall);
    String talk = mqttMessage.getTalk();

    if (StateManager.getCallType() == null
            || mqttMessage.getCallId() == null
            || !mqttMessage.getCallId().equals(StateManager.getCallId())
            || StateManager.getCallType() == CallType.REMOTE_MONITOR
            || StateManager.getCallType() == CallType.EMERGENCY_CALL
            || StateManager.getCallStatus() == CALLSTATUS.IDLE) {
      return;
    }

    // 保留判定
    if (MQTT_STATUS_PARKING.equals(talk)) {
      if (!StateManager.isParked()
              && StateManager.getCallType() != CallType.EMERGENCY_CALL
              && StateManager.getCallType() != CallType.REMOTE_MONITOR) {
        CallInfo callInfo = StateManager.getCallInfo();
        if (callInfo != null) {
          localBroadcastCallStatus(callInfo, true);
        }
        StateManager.setIsParked(true);
        // 画面を保留状態に変更.
      }
    } else if (StateManager.isParked()) {
      CallInfo callInfo = StateManager.getCallInfo();
      if (callInfo != null) {
        localBroadcastCallStatus(callInfo, false);
      }
      StateManager.setIsParked(false);
      // 保留終了 画面を保留状態から戻す.
    }

    // No-call 判定
    if (MQTT_STATUS_NOCALL.equals(talk)) {
      // logger.i(TAG, "---- NO CALL TIMER[" + StateManager.getCallType() + "] HOLD:" + isParked() + ",
      // TIMER:" + timerNocall + "->NO-CALL");
    } else {
      // logger.i(TAG, "---- NO CALL TIMER[" + StateManager.getCallType() + "] HOLD:" + isParked() + ",
      // TIMER:" + timerNocall + "->TALKING");
      timerNocall = 0; // reset nocall timer(diff milli-sec from the last CallStatus received)
    }

    HeadsetManager headsetManager = HeadsetManager.getInstance();
    if (talk == null || !talk.equals(StateManager.getMyTerminalId())) {
      // 2019/02/25 速度改善のため、構成見直し
      if (!isPressedPtt && !headsetManager.isMicrophoneMuted()) {
        headsetManager.muteMicrophone();
        headsetManager.unmuteSpeaker();
      }
      return;
    }
    // 2019/02/25 速度改善のため、構成見直し
    if (!isPressedPtt && talk.equals(StateManager.getMyTerminalId())) {
      MqttManager.getInstance()
              .requestChangeStatusWhenReleased(
                      StateManager.getCallType().toString(), StateManager.getCallId());
      return;
    }
    if (!headsetManager.isMicrophoneMuted()) {
      return;
    }
    headsetManager.unmuteMicrophone();
    SoundPlayer.start(
            this,
            R.raw.talk_start,
            (mp) -> {
              if (!headsetManager.isMicrophoneMuted()) {
                headsetManager.muteSpeaker();
              }
            });
    // 2018/06/08 V0.4対応　音声レベル調整
    TerminalUtils.setMuteAudioSetting();
  }

  @Override
  public void onReceiveCallList(CallType callType, String callId, String callFrom, String[] callList, int priority, String status) {
    CallInfo callInfo = new CallInfo(callType, callId, callFrom, callList, priority);
    isRequestingCallList = false;
    // 2019/01/22 SEC
    // 通話競合不具合対応
    if(telephonyState){
      return;
    }


    if (callType == null || callId == null) {
      if (StateManager.isCalling()) {
        // 2019/02/25 速度改善のため、構成見直し
        stopCalling(Constants.MQTTSTATUS.BYE, false);
      }
      return;
    }

    if (StateManager.isCalling()) {
      if (StateManager.isSameCall(callType, callId)) {
        inviteCallManager(callId);
        return;
      }
      // 2019/02/25 速度改善のため、構成見直し
      stopCalling(Constants.MQTTSTATUS.BYE, false);
    }

    handleIncomingCallAction(callInfo, !callInfo.isMonitored(StateManager.getMyTerminalId()));
  }

  private void getServerInfo() {

    String userId = mPreference.getString(getString(R.string.key_pref_login_id), null);
    String password = mPreference.getString(getString(R.string.key_pref_login_password), null);
    long lastUpdate = mPreference.getLong(getString(R.string.key_pref_server_settings_update), 0);

    if (userId == null || password == null) {
      return;
    }

    new WebApiRequest<Login>(Login.class)
            .setServerInfoUrl(this, mPreference, userId, password, lastUpdate)
            .setOnResponse(
                    (Login response) -> {
                      saveLoginData(userId, response);
                    })
            .enqueue();
  }

  /**
   * Saves data into database synchronous operation so that must be proceeded in background thread.
   *
   * @param myUserId
   * @param resLogin
   */
  private void saveDataIntoDatabase(String myUserId, Login resLogin) {
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(
            () -> {
              try (Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransactionAsync(
                        transaction -> {
                          if (resLogin.getUserInfo() != null) {
                            UserInfo.saveUserInfo(transaction, myUserId, resLogin.getUserInfo());
                          }
                          if (resLogin.getGroupInfo() != null) {
                            GroupInfo.saveGroupInfo(transaction, resLogin.getGroupInfo());
                          }
                          if (resLogin.getMqttInfo() != null) {
                            MqttInfo.saveMqttInfo(transaction, resLogin.getMqttInfo());
                          }
                          if (resLogin.getTerminalInfo() != null) {
                            Site.saveSites(transaction, resLogin.getTerminalInfo());
                          }
                          // 2018/07/24 録音有無
                          if (resLogin.getRecordInfo() != null) {
                            TerminalUtils.setRecordFlag(resLogin.getRecordInfo());
                          }
                        });
              }
            });
  }

  private void saveTerminalInfo(TerminalInfo terminalInfo) {
    if (terminalInfo == null) {
      return;
    }
    TerminalInfo.saveInterruptSet(this, mPreference, terminalInfo);
    TerminalInfo.saveKeepAlive(this, mPreference, terminalInfo);
    TerminalInfo.saveMessageset(this, mPreference, terminalInfo);
    TerminalInfo.saveEmcSet(this, mPreference, terminalInfo);
    TerminalInfo.saveCallDefault(this, mPreference, terminalInfo);
    TerminalInfo.saveCallType(this, mPreference, terminalInfo);
    TerminalInfo.saveTalklimit(this, mPreference, terminalInfo);
    TerminalInfo.saveMandownset(this, mPreference, terminalInfo);
    TerminalInfo.saveGpsInfo(this, mPreference, terminalInfo);
    TerminalInfo.saveNocall(this, mPreference, terminalInfo);
    TerminalInfo.saveRtmSet(this, mPreference, terminalInfo);
    TerminalInfo.saveActiveSet(this, mPreference, terminalInfo);
    TerminalInfo.saveTerminalStatusSet(this, mPreference, terminalInfo);
    //2018/7/24 近隣通話距離設定追加
    TerminalUtils.setRadius(terminalInfo.getNeighborSet());

    // re-enable/disable lone timer
    safetySettings = terminalInfo.getSafetySettings();
  }

  private void saveLoginData(String myUserId, Login resLogin) {
    // save server settings update datetime
    mPreference.edit()
            .putLong(getString(R.string.key_pref_server_settings_update),
                    Calendar.getInstance().getTimeInMillis()).apply();

    // save terminal info
    saveTerminalInfo(resLogin.getTerminalInfo());

    // save user list, group list
    saveDataIntoDatabase(myUserId, resLogin);

    restartLoneTimer();
  }

  public static void broadcastTouchEvent(Context context, MotionEvent event) {
    if (event == null || event.getAction() == MotionEvent.ACTION_DOWN) {
      Intent intent = new Intent();
      intent.setAction(ACTION_TOUCH);
      LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
  }

  @Override
  public void onCallStateError(@NonNull String message) {
    // 2019/02/25 速度改善のため、構成見直し
    stopCalling(Constants.MQTTSTATUS.BYE, false);
    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
  }
}
