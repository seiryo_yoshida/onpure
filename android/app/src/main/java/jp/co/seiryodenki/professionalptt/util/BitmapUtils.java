package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class BitmapUtils {
  private static final String TAG = BitmapUtils.class.getSimpleName();

  /**
   * ファイルを開かず画像のwidthとheightを比較
   * @param path 画像ファイルのパス
   * @param width
   * @param height
   * @return 幅と高さの両方がオーバーしていなければfalse。それ以外はtrue
   */
  public static boolean isLarger(String path, int width, int height) {
    boolean result = true;
    try {
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true;
      Bitmap bitmap = null;
      InputStream stream = new FileInputStream(path);
      bitmap = BitmapFactory.decodeStream(stream, null, options);
      stream.close();
      if (options.outWidth <= width && options.outHeight <= height) {
        result = false;
      }
    } catch (Exception e) {
      logger.e(TAG, "isLarger", e);
    }
    return result;
  }

  /**
   * 画像をVGAサイズ(640x480)に変換して保存する
   * @param c
   * @param srcPath 変換前の画像ファイルのパス
   * @param dstPath 変換後の画像ファイルの保存先
   * @throws Exception
   */
  public static void convertToVGASize(Context c, String srcPath, String dstPath) throws Exception {
    File srcFile = new File(srcPath);
    FutureTarget<Bitmap> target = Glide.with(c).asBitmap().load(srcFile).submit(640, 480);
    try {
      Bitmap bitmap = target.get();
      File dstFile = new File(dstPath);
      dstFile.createNewFile();
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.JPEG, 10, bos);
      byte[] bitmapdata = bos.toByteArray();

      FileOutputStream fos = new FileOutputStream(dstFile);
      fos.write(bitmapdata);
      fos.flush();
      fos.close();
    } catch (Exception e) {
      logger.e(TAG, "convertToVGASize", e);
      throw e;
    }
  }
}
