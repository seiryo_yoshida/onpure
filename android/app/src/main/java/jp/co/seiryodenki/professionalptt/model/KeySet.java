package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class KeySet {

  @SerializedName("ptt")
  private int ptt;

  @SerializedName("key")
  private int key;

  public KeySet(int ptt, int key) {
    this.ptt = ptt;
    this.key = key;
  }

  public int getPtt() {
    return ptt;
  }

  public void setPtt(int ptt) {
    this.ptt = ptt;
  }

  public int getKey() {
    return key;
  }

  public void setKey(int key) {
    this.key = key;
  }
}
