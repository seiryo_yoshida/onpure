package jp.co.seiryodenki.professionalptt.model;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import jp.co.seiryodenki.professionalptt.core.StateManager;

public class CallInfo implements Parcelable {
  private static final String INTENT_CALL_INFO = "intent_call_info";

  private CallType callType;
  private String callId; // 通話先
  private String callFrom; // 通話元ID
  private String[] callList; // for labels
  private int priority;

  public CallInfo() {
  }

  public CallInfo(CallType callType, String callId, String callFrom, String[] callList, int priority) {
    this.callType = callType;
    this.callId = callId;
    this.callFrom = callFrom;
    this.callList = callList;
    if (callList == null) {
      this.callList = new String[]{callId};
    }
    this.priority = priority;
  }

  protected CallInfo(Parcel in) {
    callType = in.readParcelable(CallType.class.getClassLoader());
    callId = in.readString();
    callFrom = in.readString();
    callList = in.createStringArray();
    priority = in.readInt();
  }

  public static final Creator<CallInfo> CREATOR = new Creator<CallInfo>() {
    @Override
    public CallInfo createFromParcel(Parcel in) {
      return new CallInfo(in);
    }

    @Override
    public CallInfo[] newArray(int size) {
      return new CallInfo[size];
    }
  };

  public CallType getCallType() {
    return callType;
  }

  public void setCallType(CallType callType) {
    this.callType = callType;
  }

  public String getCallId() {
    return callId;
  }

  public void setCallId(String callId) {
    this.callId = callId;
  }

  public String getCallFrom() {
    return callFrom;
  }

  public void setCallFrom(String callFrom) {
    this.callFrom = callFrom;
  }

  public String[] getCallList() {
    return callList;
  }

  public void setCallList(String[] callList) {
    this.callList = callList;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public boolean isMonitored(@Nullable String myTerminalId) {
    if (myTerminalId == null) {
      return false;
    }
    switch (callType) {
      case EMERGENCY_CALL:
        return myTerminalId.equals(callFrom);
      case REMOTE_MONITOR:
        return myTerminalId.equals(callId);
      default:
        return false;
    }
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(callType, flags);
    dest.writeString(callId);
    dest.writeString(callFrom);
    dest.writeStringArray(callList);
    dest.writeInt(priority);
  }

  public Intent getIntent() {
    Intent intent = new Intent();
    intent.putExtra(INTENT_CALL_INFO, this);
    return intent;
  }

  public void putIntent(Intent intent) {
    intent.putExtra(INTENT_CALL_INFO, this);
  }

  public static CallInfo getCallInfo(Intent intent) {
    return (CallInfo) intent.getParcelableExtra(INTENT_CALL_INFO);
  }

  public static String[] convertCallList(CallType callType, String callId, List<String> callList) {
    if (callId == null) {
      return null;
    } else if (callList == null || callList.isEmpty()) {
      return new String[]{callId};
    } else if (callType == CallType.CALL) {
      for (String id : callList) {
        if (!id.isEmpty() && !id.equals(StateManager.getMyTerminalId())) {
          return new String[]{id};
        }
      }
    }
    return callList.toArray(new String[callList.size()]);
  }

  public static int convertPriority(Integer priority) {
    if (priority == null) {
      return 0;
    }
    return priority;
  }
}
