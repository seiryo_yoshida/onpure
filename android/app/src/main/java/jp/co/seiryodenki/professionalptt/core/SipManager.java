package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAddress.TransportType;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCore.AuthMethod;
import org.linphone.core.LinphoneCore.EcCalibratorStatus;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.LogCollectionUploadState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCore.RemoteProvisioningState;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneFriendList;
import org.linphone.core.LinphoneInfoMessage;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.PublishState;
import org.linphone.core.SubscriptionState;
import org.linphone.tools.H264Helper;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.util.FileUtils;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;
import android.util.Log;

import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_JITTER_BUFFER_NORMAL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_MIC_VOLUME_NORMAL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL;

public class SipManager implements LinphoneCoreListener {
  private static final String TAG = SipManager.class.getSimpleName();
  private static SipManager instance;
  private CallStateManager callStateManager;
  private ConnectivityManager connectivityManager;
  private Handler looper;
  private LinphoneCore linphoneCore;
  private OnCallStateErrorListener onCallStateErrorListener;
  private static String sipHost;
  // 2019/4/17 新サーバ移行対応
  private static int sipPort; 
  private String companyId;
  private String identity;
  private String password;
  private String terminalId;
  private Timer timer;
  private static float speakerGain;
  private static float micGain;
  private static int jitterBuffer;

  /** 通話状態エラー処理を提供する。 */
  public interface OnCallStateErrorListener {
    /**
     * 通話状態エラーを通知する。
     *
     * @param message エラーメッセージ
     */
    void onCallStateError(@NonNull String message);
  }

  /**
   * 通話状態エラーリスナーを設定する。
   *
   * @param onCallStateErrorListener 通話状態エラーリスナー
   * @return SipManager
   */
  public SipManager setOnCallStateErrorListener(
      @Nullable OnCallStateErrorListener onCallStateErrorListener) {
    this.onCallStateErrorListener = onCallStateErrorListener;
    return this;
  }

  /**
   * SipManagerシングルトンを作成する。 comnapyIdとterminalIdの文字列連結をSIPユーザーとして使用する。
   *
   * @param c コンテキスト
   * @param companyId 会社ID
   * @param terminalId 端末ID
   * @param password SIPパスワード
   * @return SipManager
   */
  public static synchronized SipManager create(
      @NonNull Context c,
      @NonNull String companyId,
      @NonNull String terminalId,
      @NonNull String password,
      // 2019/04/17 新サーバ移行対応
      @NonNull String sipAddr,
      @NonNull String sipPort
      ) {
    if (instance != null) {
      return instance;
    }
    // 2019/04/17 新サーバ移行対応
    //instance = new SipManager(c, companyId, terminalId, password);
    instance = new SipManager(c, companyId, terminalId, password, sipAddr, sipPort);
    return instance;
  }

  /**
   * SipManagerシングルトンが作成されているか判定する。
   *
   * @return true シングルトン作成済み<br>
   *     false シングルトン未作成
   */
  public static synchronized boolean isInstantiated() {
    return instance != null;
  }

  /** SipManagerシングルトンを破棄する。 */
  public static synchronized void destroy() {
    if (instance == null) {
      return;
    }
    instance.onDestroy();
    instance = null;
  }

  /**
   * SipManagerシングルトンを取得する。
   *
   * @return SipManager
   * @exception RuntimeException 失敗
   */
  public static synchronized SipManager getInstance() throws PttException {
    if (instance == null) {
      throw new PttException("SipManager is not active");
    }
    return instance;
  }

  /**
   * 会社IDを取得する。
   *
   * @return 会社ID
   */
  String getCompanyId() {
    return companyId;
  }

  /**
   * SIP URIを取得する。
   *
   * @return 会社ID
   * @param userId ユーザID
   */
  String getSipUri(String userId) {
    // 2019/04/01 Asteriskサーバの統合作業
    //return "sip:" + companyId + userId + "@" + sipHost;
    return "sip:" + "***" + companyId + userId + "@" + sipHost;
  }

  /**
   * 自身のSIP URIを取得する。
   *
   * @return 自身のSIP URI
   */
  String getIdentity() {
    return identity;
  }

  /**
   * LinphoneCoreを取得する。
   *
   * @return LinphoneCore
   * @exception RuntimeException 失敗
   */
  static synchronized LinphoneCore getLinphoneCore() throws PttException {
    SipManager instance;
    try {
      instance = getInstance();
    } catch (Exception e) {
      logger.e(TAG, "SipManager is not active", e);
      return null;
    }
    return getInstance().linphoneCore;
  }

  private SipManager(
      @NonNull Context c,
      @NonNull String companyId,
      @NonNull String terminalId,
      @NonNull String password,
      // 2019/04/17 新サーバ移行対応
      @NonNull String sipAddr,
      @NonNull String sipPort
      ) {
    try {
      // 2019/04/17 新サーバ移行対応
      this.sipHost = sipAddr;

      // 2019/08/08 オンプレ対応
      // SIPポートを22560から5060に変更
      //this.sipPort = Integer.parseInt(sipPort);
      this.sipPort = 5060;

      this.companyId = companyId;
      this.terminalId = terminalId;
      this.password = password;
      linphoneCore = createLinphoneCore(c);

        // 2019/08/08 オンプレ対応
        // UDPポートの衝突問題に対応するため、ランダムアクセスポート設定を実施する

      LinphoneCore.Transports transports = linphoneCore.getSignalingTransportPorts();
      int port =  -1;
      transports.tls = port;
      transports.udp = port;
      linphoneCore.setSignalingTransportPorts(transports);

      HeadsetManager.create(c, linphoneCore);
      callStateManager = new CallStateManager(c);
      connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
      start();
    } catch (Exception e) {
      logger.e(TAG, "SipManager is not created", e);
    }
  }

  private LinphoneCore createLinphoneCore(@NonNull Context c) throws Exception {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);
    jitterBuffer = pref.getInt(c.getString(R.string.key_pref_jitter), SETTINGS_SOUND_JITTER_BUFFER_NORMAL);
    micGain = pref.getFloat(c.getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_NORMAL);
    speakerGain = pref.getFloat(c.getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL);


    LinphoneCore lc = LinphoneCoreFactory.instance().createLinphoneCore(this, c);
    int availableCores = Runtime.getRuntime().availableProcessors();
    lc.setCpuCount(availableCores);
    setConfigFiles(lc, c);
    lc.enableDownloadOpenH264(false);
    H264Helper.setH264Mode(H264Helper.MODE_AUTO, lc);
    return lc;
  }

  private static void setConfigFiles(@NonNull LinphoneCore lc, @NonNull Context c)
      throws IOException {
    lc.setUserCertificatesPath(c.getFilesDir().getAbsolutePath());
    lc.setRootCA(FileUtils.create(c, R.raw.rootca, "rootca.pem"));
  }

  private synchronized void start() {
    try {
      looper = new Handler(Looper.getMainLooper());
      TimerTask task =
          new TimerTask() {
            @Override
            public void run() {
              looper.post(SipManager::iterate);
            }
          };
      timer = new Timer("PTT scheduler");
      timer.schedule(task, 0, 20);
    } catch (Exception e) {
      logger.e(TAG, "Cannot start PTT", e);
    }
  }

  private synchronized void onDestroy() {
    try {
      timer.cancel();
    } catch (Exception e) {
      logger.e(TAG, "cancel timer", e);
    }
    try {
      HeadsetManager.destroy();
    } catch (Exception e) {
      logger.e(TAG, "destroy HeadsetManager", e);
    }
    try {
      linphoneCore.destroy();
      linphoneCore = null;
    } catch (Exception e) {
      logger.e(TAG, "destroy linphoneCore", e);
    }
  }

  private static void iterate() {
    if (instance == null || instance.linphoneCore == null) {
      return;
    }
    instance.linphoneCore.iterate();
  }

  @Override
  public void authenticationRequested(
      @NonNull LinphoneCore lc, @NonNull LinphoneAuthInfo authInfo, @NonNull AuthMethod method) {
  }

  @Override
  @Deprecated
  public void authInfoRequested(
      @NonNull LinphoneCore lc,
      @NonNull String realm,
      @NonNull String terminalId,
      @NonNull String domain) {
  }

  @Override
  public void callEncryptionChanged(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneCall call,
      boolean encrypted,
      @NonNull String authenticationToken) {
  }

  @Override
  public void callState(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneCall call,
      @NonNull State state,
      @NonNull String message) {
    callStateManager.updateCallState(lc, call, state);
    if (state == State.Error && onCallStateErrorListener != null) {
      onCallStateErrorListener.onCallStateError(message);
    }
  }

  @Override
  public void callStatsUpdated(
      @NonNull LinphoneCore lc, @NonNull LinphoneCall call, @NonNull LinphoneCallStats stats) {
  }

  @Override
  public void configuringStatus(
      @NonNull LinphoneCore lc, @NonNull RemoteProvisioningState state, @NonNull String message) {
  }

  @Override
  @Deprecated
  public void displayMessage(@NonNull LinphoneCore lc, @NonNull String message) {
  }

  @Override
  @Deprecated
  public void displayWarning(@NonNull LinphoneCore lc, @NonNull String message) {
  }

  @Override
  @Deprecated
  public void displayStatus(@NonNull LinphoneCore lc, @NonNull String message) {
  }

  @Override
  public void dtmfReceived(@NonNull LinphoneCore lc, @NonNull LinphoneCall call, int dtmf) {
  }

  @Override
  public void ecCalibrationStatus(
      @NonNull LinphoneCore lc,
      @NonNull EcCalibratorStatus status,
      int delay_ms,
      @NonNull Object data) {
  }

  @Override
  public void fileTransferProgressIndication(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneChatMessage message,
      @NonNull LinphoneContent content,
      int progress) {
  }

  @Override
  public void fileTransferRecv(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneChatMessage message,
      @NonNull LinphoneContent content,
      @NonNull byte[] buffer,
      int size) {
  }

  @Override
  public int fileTransferSend(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneChatMessage message,
      @NonNull LinphoneContent content,
      @NonNull ByteBuffer buffer,
      int size) {
    return 0;
  }

  @Override
  public void friendListCreated(@NonNull LinphoneCore lc, @NonNull LinphoneFriendList list) {
  }

  @Override
  public void friendListRemoved(@NonNull LinphoneCore lc, @NonNull LinphoneFriendList list) {
  }

  @Override
  public void globalState(
      @NonNull LinphoneCore lc, @NonNull GlobalState state, @NonNull String message) {
    if (state == GlobalState.GlobalOn) {
      try {
        lc.setUserAgent("AndroidPtt", "1.0.0");
        initAudio(lc);
        String user = companyId + terminalId;
        initAuthInfo(lc, user, password);
        identity = "sip:" + user + "@" + sipHost;
        initProxyConfig(lc, identity);
        lc.setNetworkReachable(true);
        // 2019/08/08 オンプレ対応
        // 暗号化対応を削除
        //lc.setMediaEncryption(LinphoneCore.MediaEncryption.SRTP);
      } catch (LinphoneCoreException e) {
        logger.e(TAG, "init", e);
      }
    }
  }

  private static void initAudio(@NonNull LinphoneCore lc) {
    TerminalUtils.setAudioInstance(lc);
    TerminalUtils.setAudioSpkGainSetting(speakerGain);
    TerminalUtils.setAudioMicGainSetting(micGain);
    TerminalUtils.setAudioJitterSetting(jitterBuffer);
  }

  private static void initAuthInfo(
      @NonNull LinphoneCore lc, @NonNull String user, @NonNull String password) {
    lc.clearAuthInfos();
    lc.addAuthInfo(
      LinphoneCoreFactory.instance().createAuthInfo(user, password, null, sipHost));
  }

  private static void initProxyConfig(@NonNull LinphoneCore lc, @NonNull String identity)
      throws LinphoneCoreException {
    lc.clearProxyConfigs();
    String proxy = "sip:" + sipHost;
    LinphoneAddress proxyAddress = LinphoneCoreFactory.instance().createLinphoneAddress(proxy);

    // 2019/04/17 新サーバ移行対応
    //// 2019/04/01 Asteriskサーバの統合作業
    //int port = 22560;
    //proxyAddress.setPort(port);
    proxyAddress.setPort(sipPort);

    // 2019/08/08 オンプレ対応
    // TLSからUDPに変更
    //proxyAddress.setTransport(TransportType.LinphoneTransportTls);
    proxyAddress.setTransport(TransportType.LinphoneTransportUdp);
    LinphoneProxyConfig proxyConfig =
        lc.createProxyConfig(
            identity, proxyAddress.asStringUriOnly(), proxyAddress.asStringUriOnly(), true);
    if (proxyConfig == null) {
      logger.e(TAG, "create proxy failed");
      return;
    }
    // 2019/04/01 Asteriskサーバの統合作業
    //proxyConfig.setExpires(5);
    proxyConfig.enableAvpf(false);
    proxyConfig.setAvpfRRInterval(0);
    proxyConfig.enableQualityReporting(false);
    proxyConfig.setQualityReportingCollector(null);
    proxyConfig.setQualityReportingInterval(0);
    //proxyConfig.enableRegister(false);
    proxyConfig.enableRegister(true);
    // 2019/08/08 オンプレ対応
    // 証明書の利用を削除
    //lc.setVerifyServerCertificates(false);
    //lc.setVerifyServerCN(false);
    //lc.enableKeepAlive(false);
    //lc.enableKeepAlive(true);

    lc.addProxyConfig(proxyConfig);
    lc.setDefaultProxyConfig(proxyConfig);
  }

  @Override
  public void infoReceived(
      @NonNull LinphoneCore lc, @NonNull LinphoneCall call, @NonNull LinphoneInfoMessage info) {
  }

  @Override
  public void isComposingReceived(@NonNull LinphoneCore lc, @NonNull LinphoneChatRoom cr) {
  }

  @Override
  public void messageReceived(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneChatRoom cr,
      @NonNull LinphoneChatMessage message) {
  }

  @Override
  public void messageReceivedUnableToDecrypted(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneChatRoom cr,
      @NonNull LinphoneChatMessage message) {
  }

  @Override
  public void networkReachableChanged(@NonNull LinphoneCore lc, boolean enable) {
    if (connectivityManager == null) {
      return;
    }
    Network activeNetwork = null;
    for (Network network : connectivityManager.getAllNetworks()) {
      if (connectivityManager.getNetworkInfo(network).isConnected()) {
        activeNetwork = network;
        break;
      }
    }
    if (activeNetwork == null) {
      return;
    }
    int i = 0;
    List<InetAddress> inetServers =
        connectivityManager.getLinkProperties(activeNetwork).getDnsServers();
    String[] servers = new String[inetServers.size()];
    for (InetAddress address : inetServers) {
      servers[i++] = address.getHostAddress();
    }
    lc.setDnsServers(servers);
  }

  public void notifyPresenceReceived(@NonNull LinphoneCore lc, @NonNull LinphoneFriend lf) {
  }

  @Override
  public void notifyReceived(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneCall call,
      @NonNull LinphoneAddress from,
      @NonNull byte[] event) {
  }

  @Override
  public void notifyReceived(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneEvent ev,
      @NonNull String eventName,
      @NonNull LinphoneContent content) {
  }

  @Override
  public void newSubscriptionRequest(
      @NonNull LinphoneCore lc, @NonNull LinphoneFriend lf, @NonNull String url) {
  }

  @Override
  public void publishStateChanged(
      @NonNull LinphoneCore lc, @NonNull LinphoneEvent ev, @NonNull PublishState state) {
  }

  @Override
  public void registrationState(
      @NonNull LinphoneCore lc,
      @NonNull LinphoneProxyConfig proxy,
      @NonNull RegistrationState state,
      @NonNull String message) {

      // 2019/04/02 IPアドレス変更時にAsteriskサーバとの接続が切断されるため、本処理で再接続を実施する
      Log.e(TAG, "registrationState = " + message);

    if((message.equals("Service unavailable, retrying")) || (message.equals("Refresh registration")) || (message.equals("io error"))){
      if (connectivityManager == null) {
        return;
      }
      Network activeNetwork = null;
      for (Network network : connectivityManager.getAllNetworks()) {
        if (connectivityManager.getNetworkInfo(network).isConnected()) {
          activeNetwork = network;
          break;
        }
      }
      if (activeNetwork == null) {
        try {
          LinphoneCore linphoneCore = SipManager.getLinphoneCore();
          linphoneCore.refreshRegisters();
        } catch (PttException e) {
          Log.e(TAG, "SipManager.getLinphoneCore() error = ", e);
        }
        return;
      }
      int i = 0;
      List<InetAddress> inetServers =
              connectivityManager.getLinkProperties(activeNetwork).getDnsServers();
      String[] servers = new String[inetServers.size()];
      for (InetAddress address : inetServers) {
        servers[i++] = address.getHostAddress();
      }
      lc.setDnsServers(servers);
    }
  }

  @Override
  @Deprecated
  public void show(@NonNull LinphoneCore lc) {
  }

  @Override
  public void subscriptionStateChanged(
      @NonNull LinphoneCore lc, @NonNull LinphoneEvent ev, @NonNull SubscriptionState state) {
  }

  @Override
  public void transferState(
      @NonNull LinphoneCore lc, @NonNull LinphoneCall call, @NonNull State new_call_state) {
  }

  @Override
  public void uploadStateChanged(
      @NonNull LinphoneCore linphoneCore,
      @NonNull LogCollectionUploadState state,
      @NonNull String info) {
  }

  @Override
  public void uploadProgressIndication(@NonNull LinphoneCore lc, int offset, int total) {
  }
}
