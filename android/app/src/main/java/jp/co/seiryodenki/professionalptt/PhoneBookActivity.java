package jp.co.seiryodenki.professionalptt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;

import jp.co.seiryodenki.professionalptt.ui.AddressListFragment;


/** 電話帳クラス */
public class PhoneBookActivity extends BaseActivity {

  private static final String TAG = PhoneBookActivity.class.getSimpleName();

  /** 電話帳表示種別 */
  public enum ADDRESS_TYPE {
    USER,
    GROUP;

    public static ADDRESS_TYPE getType(int orginal) {
      if (orginal == 1) {
        return GROUP;
      }
      return USER;
    }
  }

  // Intent Key：ユーザPriority
  private static final String INTENT_USER_PRIORITY = "intent_user_priority";

  // ユーザPriority
  private int userPriority = 0;

  /**
   * Activity起動Intent作成処理
   *
   * @param context Context
   * @param userPriority ユーザPriority
   * @return Activity起動Intent
   */
  public static Intent getIntent(Context context, int userPriority) {
    Intent intent = new Intent(context, PhoneBookActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_USER_PRIORITY, userPriority);
    return intent;
  }

  /**
   * ページアダプタ
   *
   * <p>The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
   * sections. We use a {@link FragmentPagerAdapter} derivative, which will keep every loaded
   * fragment in memory. If this becomes too memory intensive, it may be best to switch to a {@link
   * android.support.v4.app.FragmentStatePagerAdapter}.
   */
  private SectionsPagerAdapter mSectionsPagerAdapter;

  /**
   * ページャ
   *
   * <p>The {@link ViewPager} that will host the section contents.
   */
  private ViewPager mViewPager;

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_phonebook);

    if (getIntent() != null) {
      Intent intent = getIntent();
      userPriority = intent.getIntExtra(INTENT_USER_PRIORITY, 0);
    }

    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    mViewPager = findViewById(R.id.container);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    TabLayout tabLayout = findViewById(R.id.tabs);
    tabLayout.setupWithViewPager(mViewPager);
  }

  /**
   * キータッチのUP時処理
   *
   * @param keyCode キーコード
   * @param event 処理イベント
   * @return 処理結果
   */
  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if (mSectionsPagerAdapter != null && mViewPager != null) {
      mSectionsPagerAdapter.notifyKeyEvent(mViewPager.getCurrentItem(), keyCode);
    }
    return super.onKeyUp(keyCode, event);
  }

  /**
   * アドレス帳画面アダプタ
   *
   * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the
   * sections/tabs/pages.
   */
  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    // タイトル保持
    private final String[] titles;

    // ユーザアドレス帳画面保持
    private AddressListFragment mUserFragment;

    // グループアドレス帳画面保持
    private AddressListFragment mGroupFragment;

    /**
     * コンストラクタ
     *
     * @param fm フラグメントマネージャ
     */
    public SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
      titles = getResources().getStringArray(R.array.tabs_home_values);
    }

    /**
     * 表示画面取得
     *
     * @param position 画面位置
     * @return 表示画面
     */
    @Override
    public Fragment getItem(int position) {
      ADDRESS_TYPE addressType = ADDRESS_TYPE.getType(position);
      switch (addressType) {
        case USER:
          if (mUserFragment == null) {
            mUserFragment = AddressListFragment.newInstance(ADDRESS_TYPE.USER, userPriority);
          }
          return mUserFragment;
        case GROUP:
          if (mGroupFragment == null) {
            mGroupFragment = AddressListFragment.newInstance(ADDRESS_TYPE.GROUP, userPriority);
          }
          return mGroupFragment;
      }
      return null;
    }

    /**
     * リストサイズ取得
     *
     * @return リストサイズ
     */
    @Override
    public int getCount() {
      // Show 2 total pages.
      return ADDRESS_TYPE.values().length;
    }

    /**
     * 画面タイトル取得
     *
     * @param position 画面位置
     * @return 画面タイトル
     */
    @Override
    public CharSequence getPageTitle(int position) {
      ADDRESS_TYPE addressType = ADDRESS_TYPE.getType(position);
      switch (addressType) {
        case USER:
          return titles[0];
        case GROUP:
          return titles[1];
      }
      return null;
    }

    public void notifyKeyEvent(int position, int keyCode) {
      ADDRESS_TYPE addressType = ADDRESS_TYPE.getType(position);
      switch (addressType) {
        case USER:
          if (mUserFragment != null) {
            mUserFragment.notifyKeyEvent(ADDRESS_TYPE.USER, keyCode);
          }
          break;
        case GROUP:
          if (mGroupFragment != null) {
            mGroupFragment.notifyKeyEvent(ADDRESS_TYPE.GROUP, keyCode);
          }
          break;
      }
    }
  }

  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    onBackPressed();
  }
}
