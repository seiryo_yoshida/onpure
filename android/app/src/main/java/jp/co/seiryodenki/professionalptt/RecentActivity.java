package jp.co.seiryodenki.professionalptt;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import jp.co.seiryodenki.professionalptt.ui.RecentFragment;

/** 通話履歴クラス */
public class RecentActivity extends BaseActivity {

  private static final String TAG = RecentActivity.class.getSimpleName();

  /** 通話結果種別 */
  public enum CALL_TYPE {
    CALL,
    RECEIVED,
    MISSED;

    public static RecentActivity.CALL_TYPE getType(int orginal) {
      if (orginal == 2) {
        return MISSED;
      } else if (orginal == 1) {
        return RECEIVED;
      }
      return CALL;
    }
  }

  /**
   * ページアダプタ
   *
   * <p>The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
   * sections. We use a {@link FragmentPagerAdapter} derivative, which will keep every loaded
   * fragment in memory. If this becomes too memory intensive, it may be best to switch to a {@link
   * android.support.v4.app.FragmentStatePagerAdapter}.
   */
  private SectionsPagerAdapter mSectionsPagerAdapter;

  /**
   * ページャ
   *
   * <p>The {@link ViewPager} that will host the section contents.
   */
  private ViewPager mViewPager;

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recent);

    // Create the adapter that will return a fragment for each of the three
    // primary sections of the activity.
    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    // Set up the ViewPager with the sections adapter.
    mViewPager = (ViewPager) findViewById(R.id.recent_container);
    mViewPager.setAdapter(mSectionsPagerAdapter);

    TabLayout tabLayout = (TabLayout) findViewById(R.id.recent_tabs);
    tabLayout.setupWithViewPager(mViewPager);

  }

  /**
   * 履歴画面アダプタ
   *
   * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the
   * sections/tabs/pages.
   */
  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    // タイトル保持
    private final String[] titles;

    /**
     * コンストラクタ
     *
     * @param fm フラグメントマネージャ
     */
    public SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
      titles = getResources().getStringArray(R.array.tabs_recent_values);
    }

    /**
     * 表示画面取得
     *
     * @param position 画面位置
     * @return 表示画面
     */
    @Override
    public Fragment getItem(int position) {
      if (position == 0) {
        return RecentFragment.newInstance(RecentActivity.CALL_TYPE.CALL, position + 1);
      } else if (position == 1) {
        return RecentFragment.newInstance(RecentActivity.CALL_TYPE.RECEIVED, position + 1);
      }
      return RecentFragment.newInstance(RecentActivity.CALL_TYPE.MISSED, position + 1);
    }

    /**
     * リストサイズ取得
     *
     * @return リストサイズ
     */
    @Override
    public int getCount() {
      // Show 2 total pages.
      return titles.length;
    }

    /**
     * 画面タイトル取得
     *
     * @param position 画面位置
     * @return 画面タイトル
     */
    @Override
    public CharSequence getPageTitle(int position) {
      if (position >= 0 && position < titles.length) {
        return titles[position];
      }
      return null;
    }
  }

  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    startActivity(HomeActivity.getCallIntent(this,true));
    onBackPressed();
  }
}
