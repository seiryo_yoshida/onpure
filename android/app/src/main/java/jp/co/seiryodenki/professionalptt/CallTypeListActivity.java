package jp.co.seiryodenki.professionalptt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.util.Util;
import jp.co.seiryodenki.professionalptt.view.CallTypeItemRecyclerViewAdapter;
import jp.co.seiryodenki.professionalptt.model.CallType;

public class CallTypeListActivity extends BaseActivity {

  public static final String TAG = CallTypeListActivity.class.getSimpleName();

  private static List<CallTypeItem> mCallTypeList = new ArrayList<>();

  private static void addItem(CallTypeItem item) {
    mCallTypeList.add(item);
  }

  public static Intent getIntent(Context context) {
    Intent intent = new Intent(context, CallTypeListActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    return intent;
  }

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_call_type_list);

    mCallTypeList.clear();
    Set<String> typeSet = TerminalInfo.getCallType(this,
            PreferenceManager.getDefaultSharedPreferences(this));
    for (String type : typeSet) {
      CallType callType = CallType.getType(type);
      String name = Util.getCallName(getResources(), type);
      addItem(new CallTypeItem(callType.getId(), callType, name, false));
    }

    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_list);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(
        new CallTypeItemRecyclerViewAdapter(
            R.layout.item_call_type,
            mCallTypeList,
            item -> {
              startActivity(
                  HomeActivity.getCallIntent(
                      CallTypeListActivity.this, item.callType, null, false));
              finish();
            }));
  }

  /**
   * ツールバーの×ボタンを押下で処理を閉じる
   * @param v
   */
  public void onClickBack(View v)
  {
    finish();
  }

  public interface OnClickCallTypeListener {
    void onClickCallType(CallTypeItem item);
  }

  public static class CallTypeItem {
    private final int id;
    private final CallType callType;
    private final String title;
    private final boolean isSelected;

    public CallTypeItem(int id, CallType callType, String title, boolean isSelected) {
      this.id = id;
      this.callType = callType;
      this.title = title;
      this.isSelected = isSelected;
    }

    public int getId() {
      return id;
    }

    public CallType getCallType() {
      return callType;
    }

    public String getTitle() {
      return title;
    }

    public boolean isSelected() {
      return isSelected;
    }

    @Override
    public String toString() {
      return title;
    }
  }
}
