package jp.co.seiryodenki.professionalptt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import android.webkit.MimeTypeMap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Math;

import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.VoiceInfo;
import jp.co.seiryodenki.professionalptt.util.logger;
import jp.co.seiryodenki.professionalptt.view.VoiceRecodingListAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


/**
 * 通話録音画面制御クラス
 */
public class VoiceRecodingActivity extends BaseActivity
    implements VoiceRecodingListAdapter.OnSelectVoiceFileListener {

  public static final String TAG = VoiceRecodingActivity.class.getSimpleName();

  // Preference
  private SharedPreferences preference;

  // リスト表示用アダプタ
  private VoiceRecodingListAdapter adapter;

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_voice_recoding);
    preference = PreferenceManager.getDefaultSharedPreferences(this);
    adapter = new VoiceRecodingListAdapter(this);
    RecyclerView recyclerView = findViewById(R.id.recycler_list);
    recyclerView.setAdapter(adapter);
    requestVoiceList();
  }

  /**
   * 通話履歴取得処理
   */
  private void requestVoiceList() {
    String companyID = preference.getString(getString(R.string.key_pref_company_id), null);
    new WebApiRequest<>(VoiceInfo.class)
        .setVoiceFileListUrl(this, preference, companyID)
        .setOnResponse((VoiceInfo response) -> {
          new Handler(Looper.getMainLooper()).post(() -> {
            adapter.notifyDataSetChanged(response.getFiles());
          });
        })
        .setOnFailure((Exception e) -> {
          logger.e(TAG, "requestVoiceList", e);
        })
        .enqueue();
  }

  /**
   * リスト選択時処理
   *
   * @param voiceFile 選択ファイル
   */
  @Override
  public void onSelectVoiceFile(VoiceInfo.VoiceFile voiceFile) {
    if (voiceFile != null && voiceFile.getId() != null) {
      downloadVoiceFile(voiceFile.getId());
    }
  }

  /**
   * ファイル再生処理
   *
   * @param file 選択ファイル
   */
  private void openFile(@NonNull File file) {
    // 2019/01/22 SEC
    // Android SDKバージョンアップ対応でオーディオ再生方法を変更!!
    Uri apkURI = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", file);
    String fileExtension = MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath());
    String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_VIEW);
    intent.setDataAndType(apkURI, mimeType);
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    startActivity(intent);
  }

  /**
   * 選択ファイルダウンロード処理
   *
   * @param id 選択ファイルID
   */
  private void downloadVoiceFile(@NonNull String id) {

    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().url(setVoiceFileUrl(id)).build();
    client.newCall(request).enqueue(new Callback() {

      public void onFailure(@NonNull Call call,@NonNull IOException e) {
        e.printStackTrace();
      }

      public void onResponse(@NonNull Call call,@NonNull Response response) throws IOException {
        ResponseBody responseBody = response.body();
        if (!response.isSuccessful() || responseBody == null) {
          throw new IOException("Failed to download file: " + response);
        }

        File dir = getExternalCacheDir();
        if (dir == null || !dir.canWrite()) {
          dir = getCacheDir();
          if (!dir.canWrite()) {
            Toast.makeText(VoiceRecodingActivity.this,
                    "Output directory is not ready.", Toast.LENGTH_SHORT).show();
            return;
          }
        }
        File file = new File(dir, id + ".wav");
        file.deleteOnExit();

        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
        InputStream input = responseBody.byteStream();

        int readByte;
        byte[] buffer = new byte[8048];

        // WAVヘッダーを読み飛ばす
        if((readByte = input.read(buffer, 0, 125)) < 0) {
          return;
        } else {
          output.write(buffer, 0, readByte);
        }

        while ((readByte = input.read(buffer)) != -1) {
          for (int i = 0; i < readByte; ++i) {
            buffer[i] = (byte)(buffer[i] << 2);
          }
          output.write(buffer, 0, readByte);
        }
        output.flush();
        output.close();
        input.close();

        openFile(file);
      }
    });
  }

  /**
   * 選択ファイルダウンロードURL生成
   *
   * @param id 選択ファイルID
   * @return 選択ファイルダウンロードURL
   */
  public String setVoiceFileUrl(String id) {
    return getString(R.string.web_api_format_secure,
            preference.getString(getString(R.string.key_pref_web_server),
                    getString(R.string.web_api_host)),
            getString(R.string.web_api_prefix))
            + "get_meeting_file?id=" + id;
  }

  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    onBackPressed();
  }
}
