package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import java.util.List;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.UserInfoEntity;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment.OnClickUserListener;

public class UserItemRecyclerViewAdapter
    extends RecyclerView.Adapter<UserItemRecyclerViewAdapter.ViewHolder> {

  private static final String TAG = UserItemRecyclerViewAdapter.class.getSimpleName();

  private int focusPosition = 0;

  private List<UserInfoEntity> userList;

  private final OnClickUserListener onClickUserListener;

  public UserItemRecyclerViewAdapter(OnClickUserListener onClickUserListener) {
    this.onClickUserListener = onClickUserListener;
  }

  public void setUserList(List<UserInfoEntity> userList) {
    this.userList = userList;
    notifyDataSetChanged();
  }

  private ViewGroup parent;

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    Context context = parent.getContext();
    return new ViewHolder(
        context, LayoutInflater.from(context).inflate(R.layout.item_user, parent, false));
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bindView(userList.get(position), position == focusPosition);
  }

  @Override
  public int getItemCount() {
    if (userList != null) {
      return userList.size();
    }
    return 0;
  }

  public int getSelectedPosition() {
    return focusPosition;
  }

  public void setSelectedPosition(int position) {
    int oldPosition = focusPosition;
    focusPosition = position;
    notifyItemChanged(oldPosition);
    notifyItemChanged(focusPosition);
  }

  public UserInfoEntity getCurrentInfo() {
    if (userList == null) {
      return null;
    }
    return userList.get(focusPosition);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final Context context;
    //private final TextView textId;
    private final TextView textName;
    private final Button btnMessage;
    //private final TextView textPriority;
    //private final TextView textTerminalId;
    private final View view;
    private boolean isMessage;

    ViewHolder(Context context, View view) {
      super(view);
      this.context = context;
      this.view = view;
      //textId = this.view.findViewById(R.id.user_id);
      textName = this.view.findViewById(R.id.user_name);
      btnMessage = this.view.findViewById(R.id.user_message);
      //textPriority = this.view.findViewById(R.id.user_priority);
      //textTerminalId = this.view.findViewById(R.id.terminal_id);
    }

    void bindView(UserInfoEntity user, boolean isSelected) {
      //textId.setText(user.getUserId());
      textName.setText(user.getUserName());
      //textPriority.setText(String.valueOf(user.getPriority()));
      //textTerminalId.setText(user.getTerminalId());
      /*
      view.setOnTouchListener(
          (view, event) -> {
            isMessage = event.getX() > 0.8 * view.getWidth();
            CallService.broadcastTouchEvent(context, event);
            return false;
          });
      view.setOnClickListener(
          v -> {
            if (onClickUserListener != null) {
              onClickUserListener.onClickUser(user, isMessage);
            }
          });
      */
      textName.setOnClickListener(
        v -> {
          onClickUserListener.onClickUser(user, false);
        });
      btnMessage.setOnClickListener(
        v -> {
          onClickUserListener.onClickUser(user, true);
        });
      view.setSelected(isSelected);
    }
  }
}
