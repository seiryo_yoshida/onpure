package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class EndCall {

  @SerializedName("endkey")
  private int endkey;

  @SerializedName("outtalk")
  private int outtalk;

  public EndCall(int endkey, int outtalk) {
    this.endkey = endkey;
    this.outtalk = outtalk;
  }

  public int getEndKey() {
    return endkey;
  }

  public void setEndKey(int endKey) {
    this.endkey = endKey;
  }

  public int getOutTalk() {
    return outtalk;
  }

  public void setOutTalk(int outTalk) {
    this.outtalk = outTalk;
  }
}
