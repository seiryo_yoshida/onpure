package jp.co.seiryodenki.professionalptt.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

public class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
  public static final String TAG = MyActivityLifecycleCallbacks.class.getSimpleName();

  private static String currentActivityName = "";

  public static synchronized String getCurrentActivityName() {
    return currentActivityName;
  }

  private synchronized void setCurrentActivityName(String currentActivityName) {
    this.currentActivityName = currentActivityName;
  }
  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
  }

  @Override
  public void onActivityStarted(Activity activity) {

  }

  @Override
  public void onActivityResumed(Activity activity) {
    setCurrentActivityName(activity.getClass().getSimpleName());
  }

  @Override
  public void onActivityPaused(Activity activity) {

  }

  @Override
  public void onActivityStopped(Activity activity) {

  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
  }

  @Override
  public void onActivityDestroyed(Activity activity) {

  }
}
