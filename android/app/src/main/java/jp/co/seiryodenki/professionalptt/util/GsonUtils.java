package jp.co.seiryodenki.professionalptt.util;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class GsonUtils {
  private static Gson instance = null;

  public static void create() {
    instance = new Gson();
  }

  public static Gson getInstance() {
    if (instance == null) {
      create();
    }
    return instance;
  }

  public static String toJson(Object src) {
    if (instance == null) {
      create();
    }
    return instance.toJson(src);
  }

  public static <T> T fromJson(String json, Class<T> classOfT) {
    if (instance == null) {
      create();
    }
    return instance.fromJson(json, classOfT);
  }

  public static <T> T fromJson(String json, Type typeOfT) {
    if (instance == null) {
      create();
    }
    return instance.fromJson(json, typeOfT);
  }
}
