package jp.co.seiryodenki.professionalptt.util;

import android.util.Base64;

public class Base64Encoder {
  public static String encode(String src) {
    if (src == null) {
      return null;
    }
    return Base64.encodeToString(src.getBytes(), Base64.DEFAULT);
  }
}
