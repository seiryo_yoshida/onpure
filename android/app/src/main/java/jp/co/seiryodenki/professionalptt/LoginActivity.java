package jp.co.seiryodenki.professionalptt;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.CheckBox;
import android.widget.Toast;
import android.support.v4.app.DialogFragment;
import java.util.ArrayList;
import java.io.IOException;
import java.util.TimeZone;
import jp.co.seiryodenki.professionalptt.core.AuthException;
import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.AuthDevice;
import jp.co.seiryodenki.professionalptt.model.Login;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.util.IntervalJobManager;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;
import jp.co.seiryodenki.professionalptt.view.SplashDialog;
import jp.co.seiryodenki.professionalptt.ui.ErrorUseridDialogFragment;
import jp.co.seiryodenki.professionalptt.ui.ErrorUuidDialogFragment;
import jp.co.seiryodenki.professionalptt.ui.ErrorNetworkDialogFragment;


/** A login screen that offers login via email/password. */
public class LoginActivity extends BaseActivity {

  private static final String TAG = LoginActivity.class.getSimpleName();

  private static final String INTENT_COMPANY_ID = "intent_company_id";
  private static final String INTENT_TERMINAL_ID = "intent_terminal_id";
  private static final int MAX_RETRY_COUNTER = 5;

  //エラーダイアログ表示
  private static final int ERROR_NETWORK = 1;
  private static final int ERROR_UUID = 2;
  private static final int ERROR_USERID = 3;

  // 画面表示要素
  private Button mBtnLogin;
  private EditText mIdView;
  private EditText mPasswordView;
  //private TextView mErrLogin;
  //private TextView mErrUuid;
  private View mLoginFormView;
  private View mProgressView;
  private CheckBox mAutoLoginView;

  // エラーダイアログ
  private DialogFragment dialogFragment = null;

  // スプラッシュ画面表示ダイアログ
  private SplashDialog splashDialog;

  // 自端末情報保持
  private String mCompanyId;
  private String mTerminalId;

  // Preference
  private SharedPreferences mPreference;

  // CallService
  private CallService mCallService;
  // CallService接続状態フラグ
  private boolean mBound = false;
  // CallService接続クラス
  private ServiceConnection mConnection =
          new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
              CallService.CallServiceBinder binder = (CallService.CallServiceBinder) service;
              mCallService = binder.getService();
              mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
              mBound = false;
            }
          };

  /**
   * Activity起動Intent作成処理
   *
   * @param context Context
   * @param companyId 所属会社ＩＤ
   * @param terminalId 端末ID
   * @return Activity起動Intent
   */
  public static Intent getIntent(Context context, String companyId, String terminalId) {
    Intent intent = new Intent(context, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_COMPANY_ID, companyId);
    intent.putExtra(INTENT_TERMINAL_ID, terminalId);
    return intent;
  }

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    if (getIntent() != null) {
      mCompanyId = getIntent().getStringExtra(INTENT_COMPANY_ID);
      mTerminalId = getIntent().getStringExtra(INTENT_TERMINAL_ID);
    }

    splashDialog = new SplashDialog(this);
    if (mCompanyId == null || mTerminalId == null) {
      splashDialog.showDialog();
    }

    mPreference = PreferenceManager.getDefaultSharedPreferences(this);

    // start call service
    Intent iCall = new Intent(this, CallService.class);
    startService(iCall);

    InputFilter inputFilter =
            (source, start, end, dest, dstart, dend) -> {
              if (source.toString().matches("^[0-9a-zA-Z_]+$")) {
                return source;
              } else {
                return "";
              }
            };

    //mErrUuid = findViewById(R.id.text_err_authorize_uuid);
    ((TextView) findViewById(R.id.text_sign_in_uuid)).setText(getIMEI());

    // 配列をセットする
    //mErrLogin = findViewById(R.id.text_err_login);
    mIdView = findViewById(R.id.edit_sign_in_id);
    mIdView.setFilters(new InputFilter[] {inputFilter});

    mPasswordView = findViewById(R.id.edit_sign_in_password);
    mPasswordView.setOnEditorActionListener(
            (textView, id, keyEvent) -> {
              return (id == R.id.edit_sign_in_id || id == EditorInfo.IME_NULL);
            });

    mAutoLoginView = findViewById(R.id.auto_sign_in_checkbox);

    mBtnLogin = findViewById(R.id.email_sign_in_button);
    mLoginFormView = findViewById(R.id.login_form);
    mProgressView = findViewById(R.id.login_progress);

    String userId = mPreference.getString(getString(R.string.key_pref_login_id), null);
    if (userId != null) {
      mIdView.setText(userId);
    }
    String password = mPreference.getString(getString(R.string.key_pref_login_password), null);
    if (password != null) {
      mPasswordView.setText(password);
    }
    boolean autologin = mPreference.getBoolean(getString(R.string.key_pref_login_autologin), false);
    mAutoLoginView.setChecked(autologin);

    // 2019/01/22 SEC
    // 独自対応、ログインボタン押下時に圏外の場合には、エラーアラートを表示する。
    if (!isNetworkConnected()) {
      mBtnLogin.setOnClickListener(
        view -> {
          if (isNetworkConnected()) {
            if (mCompanyId == null || mTerminalId == null) {
              authorizePhone(MAX_RETRY_COUNTER);
            } else {
              //mErrLogin.setVisibility(View.GONE);
              attemptLogin(mCompanyId, mTerminalId);
            }
          }
          else{
            showFragmentDialog(ERROR_NETWORK);
          }
        });
      runOnUiThread(() -> {
        setLoginErrorView(mCompanyId, mTerminalId, userId, password, getString(R.string.AppCertNetworkErr), ERROR_NETWORK);
        showFragmentDialog(ERROR_NETWORK);
      });
    } else {
      if (mCompanyId == null || mTerminalId == null) {
        authorizePhone(MAX_RETRY_COUNTER);
      } else {
        mBtnLogin.setOnClickListener(
          view -> {
            attemptLogin(mCompanyId, mTerminalId);
          });
      }
      setLoginView(mCompanyId, mTerminalId);
    }
  }

  /** Activity開始時処理 */
  @Override
  public void onStart() {
    super.onStart();
    TerminalUtils.setLoginFlag(true);
    requestLocationPermission();
    IntervalJobManager.setLocationSettings(this);
    // Bind to LocalService
    bindService(new Intent(this, CallService.class), mConnection, Context.BIND_AUTO_CREATE);
  }

  /** Activity終了時処理 */
  @Override
  public void onStop() {
    super.onStop();
  }

  @Override
  public void onDestroy(){
    super.onDestroy();
    // Unbind from the service
    if (mBound) {
      unbindService(mConnection);
      mBound = false;
    }
  }

  /** 戻るキー押下時処理 */
  @Override
  public void onBackPressed() {
  }

  /** ネットワーク接続状態受信レシーバ設定 */
  private void registerNetworkStateReceiver() {
    registerReceiver(mBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
  }

  /** ネットワーク接続状態受信レシーバ */
  private BroadcastReceiver mBroadcastReceiver =
          new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
              if (intent == null) {
                return;
              }
              if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                if (isNetworkConnected()) {
                  unregisterReceiver(mBroadcastReceiver);
                  authorizePhone(MAX_RETRY_COUNTER);
                } else {
                  logger.e(TAG, "Network is disconnected.");
                }
              }
            }
          };

  /**
   * ネットワーク接続状態判定処理
   *
   * @return ネットワーク接続状態判定結果
   */
  private boolean isNetworkConnected() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
  }

  /**
   * Authorization of a cellular phone
   *
   * <p>A function to authorize a cellular phone using IMEI on application begins. ref:network
   * interface 1
   *
   * @param counter 試行回数
   */
  private void authorizePhone(final int counter) {
    showProgress(true);

    new WebApiRequest<AuthDevice>(AuthDevice.class)
            .setBootonUrl(this, mPreference)
            .setOnResponse(
                    response -> {
                      String companyId = response.getCompany_id();
                      String terminalId = response.getTerminal_id();
                      mPreference
                              .edit()
                              .putString(getString(R.string.key_pref_company_id), companyId)
                              .apply();
                      mPreference
                              .edit()
                              .putString(getString(R.string.key_pref_terminal_id), terminalId)
                              .apply();
                      mBtnLogin.setOnClickListener(
                              view -> {
                                attemptLogin(companyId, terminalId);
                              });
                      runOnUiThread(
                              () -> {
                                String strId =
                                        mPreference.getString(getString(R.string.key_pref_login_id), null);
                                String strPassword =
                                        mPreference.getString(getString(R.string.key_pref_login_password), null);
                                boolean autologin = mPreference.getBoolean(getString(R.string.key_pref_login_autologin), false);
                                //if (strId != null && strPassword != null) {
                                if (strId != null && strPassword != null && autologin) {
                                  signIn(companyId, terminalId, strId, strPassword, autologin);
                                } else {
                                  setLoginView(companyId, terminalId);
                                }
                                TerminalUtils.setLoginFlag(false);
                              });
                    })
            .setOnFailure(
                    (Exception e) -> {
                      logger.e(TAG, "auth device", e);
                      int message;
                      int errorflg;
                      if (e instanceof IOException) {
                        if (!isNetworkConnected()) {
                          registerNetworkStateReceiver();
                          return;
                        }
                        if (counter > 0) {
                          authorizePhone(counter - 1);
                          return;
                        }
                        /* サーバ接続失敗 */
                        message = R.string.AppCertServerConnectionError;
                        errorflg = ERROR_USERID;
                      } else if (e instanceof AuthException) {

                        /* クライアントID拒否 */
                        message = R.string.AppCertClientIDReject;
                        errorflg = ERROR_UUID;
                      } else {
                        /* クライアント認証エラー */
                        message = R.string.AppCertClientCertificationError;
                        errorflg = ERROR_USERID;
                      }
                        mBtnLogin.setOnClickListener(
                              view -> {
                                //mErrUuid.setVisibility(View.GONE);
                                authorizePhone(MAX_RETRY_COUNTER);
                              });
                      runOnUiThread(
                              () -> {
                                //mErrUuid.setText(getString(message));
                                //mErrUuid.setVisibility(View.VISIBLE);
                                showFragmentDialog(errorflg);
                                showProgress(false);
                                if (splashDialog != null) {
                                  splashDialog.closeDialog();
                                }
                              });
                    })
            .enqueue();
  }

  /**
   * ユーザ認証処理
   *
   * @param companyId 所属会社ID
   * @param terminalId 端末ID
   * @param userId ユーザID
   * @param password パスワード
   */
  private void signIn(
          final String companyId, final String terminalId, String userId, String password, Boolean autologin) {
    showProgress(true);
    // 2019/04/17 新サーバ対応
    TimeZone zone  = TimeZone.getDefault();
    int timeOffset = zone.getRawOffset() / 3600 / 1000;

    new WebApiRequest<Login>(Login.class)
            .setSignInUrl(this, mPreference, companyId, terminalId, userId, password, String.valueOf(timeOffset))
            .setOnResponse(
                    (Login response) -> {
                      TerminalUtils.setLoginFlag(false);
                      mPreference.edit().putString(getString(R.string.key_pref_login_id), userId).apply();
                      mPreference
                              .edit()
                              .putString(getString(R.string.key_pref_login_password), password)
                              .apply();
                      mPreference.edit().putBoolean(getString(R.string.key_pref_login_autologin), autologin).apply();
                      processSipRequest(companyId, terminalId, userId, password, response);
                    })
            .setOnFailure(
                    (Exception e) -> {
                      logger.e(TAG, "signIn", e);
                      String message;
                      int errorflg;
                      if (e instanceof AuthException) {
                        message = getString(R.string.AppCertClientIDReject);
                        errorflg = ERROR_USERID;
                      } else if (e instanceof IOException) {
                        message = getString(R.string.AppCertServerConnectionError);
                        errorflg = ERROR_NETWORK;
                      } else {
                        message = getString(R.string.AppCertClientCertificationError);
                        errorflg = ERROR_NETWORK;
                      }
                      mBtnLogin.setOnClickListener(
                              view -> {
                                //mErrLogin.setVisibility(View.GONE);
                                attemptLogin(companyId, terminalId);
                              });
                      runOnUiThread(
                              () -> {
                                setLoginErrorView(companyId, terminalId, userId, password, message, errorflg);
                              });
                    })
            .enqueue();
  }

  /**
   * SIP開始処理
   *
   * @param companyId 所属会社ID
   * @param terminalId 端末ID
   * @param userId ユーザID
   * @param password パスワード
   * @param response レスポンスデータ
   */
  private void processSipRequest(
      String companyId, String terminalId, String userId, String password, Login response) {

    if (mBound) {
      mCallService.startBootRequest(companyId, terminalId, userId, password, response);
      if (splashDialog != null) {
        splashDialog.closeDialog(() -> {
          startActivity(new Intent(LoginActivity.this, HomeActivity.class));
          finish();
        });
      } else {
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
      }
    } else {
      Toast.makeText(this, "Failed to bind call service.", Toast.LENGTH_LONG).show();
      runOnUiThread(() -> {
        showProgress(false);
        if (splashDialog != null) {
          splashDialog.closeDialog();
        }
      });
    }
  }

  /**
   * ログイン画面設定処理
   *
   * @param companyId 所属会社ID
   * @param terminalId 端末ID
   */
  private void setLoginView(String companyId, String terminalId) {
    String userId = mPreference.getString(getString(R.string.key_pref_login_id), null);
    String password = mPreference.getString(getString(R.string.key_pref_login_password), null);
    boolean autologin = mPreference.getBoolean(getString(R.string.key_pref_login_autologin), false);

    mIdView.setText(userId);
    mPasswordView.setText(password);
    mAutoLoginView.setChecked(autologin);

    // Reset errors.
    mIdView.setError(null);
    mPasswordView.setError(null);

    View focusView = null;
    if (TextUtils.isEmpty(userId)) {
      focusView = mIdView;
    } else if (TextUtils.isEmpty(password)) {
      focusView = mPasswordView;
    }

    showProgress(false);
    if (focusView != null) {
      focusView.requestFocus();
    }
    if (splashDialog != null) {
      splashDialog.closeDialog();
    }
  }

  /**
   * ログインエラー時画面設定
   *
   * @param companyId 所属会社ID
   * @param terminalId 端末ID
   * @param userId ユーザID
   * @param password パスワード
   * @param message エラーメッセージ
   */
  private void setLoginErrorView(
      String companyId, String terminalId, String userId, String password, String message, int errorflg) {
    mIdView.setError(null);
    mIdView.setText(userId);
    mPasswordView.setError(null);
    mPasswordView.setText(password);

    if (message != null) {
      //mErrLogin.setText(message);
      //mErrLogin.setVisibility(View.VISIBLE);
      showFragmentDialog(errorflg);
    //} else {
      //mErrLogin.setVisibility(View.GONE);
    }

    View focusView = null;
    if (TextUtils.isEmpty(userId)) {
      focusView = mIdView;
    } else if (TextUtils.isEmpty(password)) {
      focusView = mPasswordView;
    }

    showProgress(false);
    if (focusView != null) {
      focusView.requestFocus();
    }
    if (splashDialog != null) {
      splashDialog.closeDialog();
    }
  }

  /**
   * ログイン試行処理
   *
   * Attempts to sign in or register the account specified by the login form. If there are form
   * errors (invalid email, missing fields, etc.), the errors are presented and no actual login
   * attempt is made.
   *
   * @param companyId 所属会社ID
   * @param terminalId 端末ID
   */
  private void attemptLogin(String companyId, String terminalId) {
    // Store values at the time of the login attempt.
    String userId = mIdView.getText().toString();
    String password = mPasswordView.getText().toString();
    boolean autologin = mAutoLoginView.isChecked();
    boolean cancel = false;
    View focusView = null;
    if (TextUtils.isEmpty(userId)) {
      mIdView.setError(getString(R.string.error_field_required));
      focusView = mIdView;
      cancel = true;
    } else if (!isEmailValid(userId)) {
      mIdView.setError(getString(R.string.error_invalid_email));
      focusView = mIdView;
      cancel = true;
    } else if (TextUtils.isEmpty(password)) {
      mPasswordView.setError(getString(R.string.error_field_required));
      focusView = mPasswordView;
      cancel = true;
    } else if (!isPasswordValid(password)) {
      mPasswordView.setError(getString(R.string.error_invalid_password));
      focusView = mPasswordView;
      cancel = true;
    }

    // Check for a valid password, if the user entered one.
    if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
      mPasswordView.setError(getString(R.string.error_invalid_password));
      focusView = mPasswordView;
      cancel = true;
    }

    if (cancel) {
      showProgress(false);
      if (focusView != null) {
        focusView.requestFocus();
      }
      if (splashDialog != null) {
        splashDialog.closeDialog();
      }
    } else {
      signIn(companyId, terminalId, userId, password, autologin);
    }
  }

  /**
   * Emailバリデーション
   *
   * <pre>常にＯＫ</pre>
   * @param email 入力Email
   * @return true
   */
  private boolean isEmailValid(String email) {
    return true;
  }

  /**
   * パスワードバリデーション
   *
   * <pre>常にＯＫ</pre>
   * @param password 入力パスワード
   * @return true
   */
  private boolean isPasswordValid(String password) {
    return true;
  }

  /** Shows the progress UI and hides the login form. */
  private void showProgress(final boolean show) {
    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
  }

  /** 位置情報要求コード */
  private final int REQUEST_CODE_PERMISSION = 1;

  /** 位置情報Permission要求処理 */
  // 2019/01/22 SEC
  // 不具合修正、アプリ実行権限の判定にミスがあるため、アプリ起動時に権限確認を実行しない。
  private void requestLocationPermission() {
    int REQUEST_CODE = 0;
    String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    ArrayList<String> reqPermissions = new ArrayList<>();

    int permissionReadContacts = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
    int permissionFineLoation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
    int permissionCallPhone = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
    int permissionReadExternalStorage = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
    int permissionRecordAudio = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

    // permission が許可されているか確認
    if (permissionReadContacts == PackageManager.PERMISSION_GRANTED) {
    } else {
      reqPermissions.add(Manifest.permission.READ_CONTACTS);
    }
    if (permissionFineLoation == PackageManager.PERMISSION_GRANTED) {
    } else {
      reqPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
    }
    if (permissionCallPhone == PackageManager.PERMISSION_GRANTED) {
    } else {
      reqPermissions.add(Manifest.permission.READ_PHONE_STATE);
    }
    if (permissionReadExternalStorage == PackageManager.PERMISSION_GRANTED) {
    } else {
      reqPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
    }
    if (permissionRecordAudio == PackageManager.PERMISSION_GRANTED) {
    } else {
      reqPermissions.add(Manifest.permission.RECORD_AUDIO);
    }
    if (!reqPermissions.isEmpty()) {
      ActivityCompat.requestPermissions(this,
              reqPermissions.toArray(new String[reqPermissions.size()]),
              REQUEST_CODE);
    }
  }

  /**
   * Permission許可要求処理
   *
   * @param requestCode 要求コード
   * @param permissions 要求Permission
   * @param grantResults 要求結果
   */
  @Override
  public void onRequestPermissionsResult(
      int requestCode, String[] permissions, int[] grantResults) {
    if (requestCode == REQUEST_CODE_PERMISSION) {
      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        if (mBound) {
          mCallService.startSystemStatusTracking();
        }
      }
    }
  }

  /**
   * 端末IMEI取得
   *
   * @return 端末IMEI
   */
  private String getIMEI() {
    return WebApiRequest.getIMEI(this, mPreference);
  }

  /**
   * ダイアログ表示
   */
  public void showFragmentDialog(int error)
  {
    closeDialog();
    switch (error) {
      case ERROR_NETWORK:
        dialogFragment = ErrorNetworkDialogFragment.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog_error_network");
        break;
      case ERROR_UUID:
        dialogFragment = ErrorUuidDialogFragment.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog_error_uuid");
        break;
      case ERROR_USERID:
        dialogFragment = ErrorUseridDialogFragment.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog_error_userid");
        break;
    }
  }

  /**
   * エラーダイアログを閉じる
   */
  private void closeDialog() {
    if (dialogFragment != null) {
      dialogFragment.dismiss();
      dialogFragment = null;
    }
  }

  /**
   * ネットワークエラーダイアログを閉じる
   * @param view
   */
  public void setDialogNetworkOKClick(View view) {
    closeDialog();
  }

  /**
   * UUIDエラーダイアログを閉じる
   * @param view
   */
  public void setDialogUuidOKClick(View view) {
    //TextView textCompanyId = view.findViewById(R.id.dialog_uuid_companyid);
    //mCompanyId = textCompanyId.getText().toString();
    closeDialog();
  }

  /**
   * UUIDエラーダイアログを閉じる
   * @param view
   */
  public void setDialogUuidCancelClick(View view) {
    closeDialog();
  }

  /**
   * ユーザIDエラーダイアログを閉じる
   * @param view
   */
  public void setDialogUseridOKClick(View view) {
    closeDialog();
  }
}
