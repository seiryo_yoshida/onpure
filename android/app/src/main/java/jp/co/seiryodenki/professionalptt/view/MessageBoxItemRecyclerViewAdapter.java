package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Outline;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.MessageActivity;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.MessageInfo;
import jp.co.seiryodenki.professionalptt.util.GlideApp;
import jp.co.seiryodenki.professionalptt.util.Util;

public class MessageBoxItemRecyclerViewAdapter
    extends RecyclerView.Adapter<MessageBoxItemRecyclerViewAdapter.ViewHolder> {

  private static final String TAG = MessageBoxItemRecyclerViewAdapter.class.getSimpleName();
  private static final int MAXSIZE = 560;
  private static final int ROUNDSIZE = 20;

  private final RealmResults<MessageInfo> messages;

  private final int myPriority;

  private SharedPreferences preferences;
  private Context context;

  public MessageBoxItemRecyclerViewAdapter(RealmResults<MessageInfo> messages, int myPriority) {
    this.messages = messages;
    this.myPriority = myPriority;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    context = parent.getContext();
    preferences = PreferenceManager.getDefaultSharedPreferences(context);
    return new ViewHolder(
        context, LayoutInflater.from(context).inflate(R.layout.item_message, parent, false));
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    final MessageInfo item = messages.get(position);
    if (item == null) {
      return;
    }
    holder.bindView(item);
  }

  @Override
  public int getItemCount() {
    return messages.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView textDate;
    private final TextView textName;
    private final TextView textMessage;
    private final ImageView imageAttached;

    public ViewHolder(Context context, View view) {
      super(view);
      textDate = view.findViewById(R.id.date);
      textName = view.findViewById(R.id.name);
      textMessage = view.findViewById(R.id.message);
      imageAttached = view.findViewById(R.id.image_attach);
    }

    private void bindView(MessageInfo item) {
      textDate.setText(Util.getSimpleDateStr(item.getDate().getTime()));
      textName.setText(item.getName());
      textMessage.setText(item.getMessage());
      String url = null;
      if (item.getAttFileId() != null && !item.getAttFileId().isEmpty()) {
        String terminalId;
        // 2018/5/28 V0.4対応　添付ファイル画像非表示対応
        //if (item.getInOutBox() == MessageInfo.INBOX) {
        //  terminalId = item.getCallId();
        //} else {
        //  terminalId = StateManager.getMyTerminalId();
        //}
        terminalId = item.getCallId();

        url = WebApiRequest.getAttFileUrl(
                context,
                preferences,
                R.string.key_pref_web_server,
                item.getAttFileId(),
                MqttManager.getInstance().getMyCompanyId(),
                terminalId);
      }
      if (url != null) {
        //Glide.with(context).load(url).into(imageAttached);
        GlideApp.with(context).load(url).override(MAXSIZE).into(imageAttached);
        imageAttached.setOutlineProvider(new CircleOutlineProvider());
        imageAttached.setVisibility(View.VISIBLE);
      } else {
        imageAttached.setVisibility(View.GONE);
      }
      itemView.setOnClickListener(
              v -> {
                Intent intent = MessageActivity.getMessageIntent(
                        context, item.getCallId(), item.getName(), myPriority);
                context.startActivity(intent);
              });
    }
  }

  /**
   * 角丸
   */
  public static class CircleOutlineProvider extends ViewOutlineProvider {
    @Override
    public void getOutline(View view, Outline outline) {
      int view_width = view.getWidth();
      int view_height = view.getHeight();
      outline.setRoundRect(
        0,
        0,
        view_width,
        view_height,
        ROUNDSIZE);
      view.setClipToOutline(true);
    }
  }
}
