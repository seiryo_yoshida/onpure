package jp.co.seiryodenki.professionalptt.model;

import android.content.Context;
import android.content.SharedPreferences;

import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_GROUP;

import com.google.gson.annotations.SerializedName;
import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.util.Base64Decoder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class GroupInfo {

  @SerializedName("grp_group_id")
  private String grp_group_id;

  @SerializedName("grp_group_name")
  private String grp_group_name;

  public GroupInfo(String grp_group_id, String grp_group_name) {
    this.grp_group_id = grp_group_id;
    this.grp_group_name = grp_group_name;
  }

  public String getGroupId() {
    return grp_group_id;
  }

  public void setGroupId(String grp_group_id) {
    this.grp_group_id = grp_group_id;
  }

  public String getGroupName() {
    return grp_group_name;
  }

  public void setGroupName(String grp_group_name) {
    this.grp_group_name = grp_group_name;
  }

  public String toString() {
    return "grp_group_id:" + grp_group_id + ", grp_group_name:" + grp_group_name;
  }

  public static void saveGroupInfo(Realm realm, List<GroupInfo> groupInfo) {
    realm.delete(GroupInfoEntity.class);
    Iterator<GroupInfo> gi = groupInfo.iterator();
    while (gi.hasNext()) {
      GroupInfo group = gi.next();
      realm.copyToRealmOrUpdate(
              new GroupInfoEntity(group.getGroupId(), Base64Decoder.decode(group.getGroupName())));
    }
  }

  public static GroupInfo getGroupInfo(Realm realm, String callId) {
    GroupInfoEntity group =
        realm.where(GroupInfoEntity.class).equalTo(DB_KEY_CALL_ID_GROUP, callId).findFirst();
    if (group != null) {
      return new GroupInfo(group.getGroupId(), group.getGroupName());
    }
    return null;
  }

  public static List<GroupInfo> getDefaultGroups(Context ctx, Realm realm, SharedPreferences pref) {

    Set<String> groupIdSet =
            pref.getStringSet(ctx.getString(R.string.key_pref_default_multi_group_ids), null);

    String[] groupIds = null;
    if (groupIdSet != null) {
      groupIds = groupIdSet.toArray(new String[] {});
    }

    if (groupIds != null) {
      return getGroupsInfo(realm, groupIds);
    }

    return null;
  }

  public static List<GroupInfo> getGroupsInfo(Realm realm, String[] callIds) {

    RealmResults<GroupInfoEntity> groups =
        realm.where(GroupInfoEntity.class).in(DB_KEY_CALL_ID_GROUP, callIds).findAll();
    if (groups != null && groups.size() > 0) {
      List<GroupInfo> res = new ArrayList<>();
      for (GroupInfoEntity group : groups) {
        res.add(new GroupInfo(group.getGroupId(), group.getGroupName()));
      }
      return res;
    }
    return null;
  }

  public static GroupInfo getFirstGroup(Realm realm) {
    GroupInfoEntity group =
            realm.where(GroupInfoEntity.class).sort(DB_KEY_CALL_ID_GROUP).findFirst();
    if (group != null) {
      return new GroupInfo(group.getGroupId(), group.getGroupName());
    }
    return null;
  }
}
