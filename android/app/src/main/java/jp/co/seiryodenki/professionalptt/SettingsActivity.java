package jp.co.seiryodenki.professionalptt;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.ui.SetConditionFragment;
import jp.co.seiryodenki.professionalptt.ui.VoiceFragment;

import static jp.co.seiryodenki.professionalptt.service.CallService.ACTION_TOUCH;

/** 設定画面制御クラス */
public class SettingsActivity extends BaseActivity {

  private static final String TAG = SettingsActivity.class.getSimpleName();

  // 接続状態設定画面開始要求コード
  private static final int REQ_CODE_PREF_CONNECT = 101;

  // Preference
  private SharedPreferences mPreference;

  // 端末情報保持
  private static String companyId;
  private static String terminalId;

  // ユーザ操作状態保持フラグ
  private static boolean isDefaultChanged = false;

  //ツールバーのタイトル
  private TextView titleToolbar;

  // CallService接続状態フラグ
  private boolean mBound = false;
  // CallService
  private CallService mCallService;

  //戻るボタンを押されたかどうか判断するフラグ
  private boolean isBackButton = true;

  // CallService接続クラス
  private ServiceConnection mConnection =
          new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
              CallService.CallServiceBinder binder = (CallService.CallServiceBinder) service;
              mCallService = binder.getService();
              mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
              mBound = false;
            }
          };

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);

    isDefaultChanged = false;
    mPreference = PreferenceManager.getDefaultSharedPreferences(this);

    companyId = mPreference.getString(getString(R.string.key_pref_company_id), null);
    terminalId = mPreference.getString(getString(R.string.key_pref_terminal_id), null);

    //ツールバータイトルを取得
    titleToolbar = findViewById(R.id.toolbartxt);

    replaceFragment(new HeaderFragment(), "HeaderFragment");
  }

  /**
   * 画面切替処理
   * @param fragment 切替画面
   * @param tag 画面タグ
   */
  private void replaceFragment(Fragment fragment, String tag) {
    //ツールバーのタイトルを設定
    setToolbarTitle(tag);

    getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.main_container, fragment, tag)
            .addToBackStack(tag)
            .commit();
  }

  /** Activity開始時処理 */
  @Override
  public void onStart() {
    super.onStart();
    // Bind to LocalService
    bindService(new Intent(this, CallService.class), mConnection, Context.BIND_AUTO_CREATE);
  }

  /** Activity停止時処理 */
  @Override
  public void onStop() {
    super.onStop();
    // Unbind from the service
    if (mBound) {
      unbindService(mConnection);
      mBound = false;
    }
  }

  /** 戻るキー押下時処理 */
  @Override
  public void onBackPressed() {
    //if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
    if (isBackButton && getSupportFragmentManager().getBackStackEntryCount() > 1) {
      getSupportFragmentManager().popBackStack();
      setToolbarTitle("");
      return;
    }
    isBackButton = true;
    if (isDefaultChanged) {
      setResult(RESULT_OK);
    }
    finish();
  }

  /**
   * Activity処理結果受信処理
   *
   * @param requestCode Activity起動コード
   * @param resultCode 処理結果コード
   * @param data 処理結果データ
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case REQ_CODE_PREF_CONNECT:
        if (resultCode == RESULT_OK) {
          isDefaultChanged = true;
        }
        //デフォルト呼び出し設定画面から、メニューボタンでの戻りの場合は、メニュー画面まで戻る
        if (data != null && data.getBooleanExtra("isMenuButton", false)) {
          isBackButton = false;
          onBackPressed();
        }
        break;
    }
  }

  /** 設定項目一覧画面表示クラス */
  public static class HeaderFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_settings, container, false);
    }
  }

  /** About面表示クラス */
  public static class AboutFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragment_about, container, false);
      ((TextView) view.findViewById(R.id.text_about_version_name)).setText(BuildConfig.VERSION_NAME);
      return view;
    }
  }

  /**
   * 設定項目選択時処理
   *
   * @param view 設定項目View
   */
  public void onClickItem(View view) {
    switch (view.getId()) {
      case R.id.fragment_pref_connect:
        startActivityForResult(new Intent(this, ConnectPreferenceActivity.class), REQ_CODE_PREF_CONNECT);
        break;
      case R.id.fragment_pref_sound:
        replaceFragment(new VoiceFragment(), "VoiceFragment");
        break;
      case R.id.fragment_pref_condition:
        replaceFragment(new SetConditionFragment(), "SetConditionFragment");
        break;
      case R.id.fragment_pref_about:
        replaceFragment(new AboutFragment(), "AboutFragment");
        break;
    }
  }

  /**
   * 画面タッチイベント通知処理
   *
   * @param event タッチイベント
   * @return 画面タッチイベント
   */
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      Intent intent = new Intent();
      intent.setAction(ACTION_TOUCH);
      LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    return super.onTouchEvent(event);
  }

  /**
   * ツールバーのタイトルを変更
   * @param tag
   */
  public void setToolbarTitle(String tag) {
    switch (tag) {
      case "HeaderFragment":
        titleToolbar.setText(getString(R.string.toolbar_config));
        break;
      case "VoiceFragment":
        titleToolbar.setText(getString(R.string.toolbar_sound_config));
        break;
      case "SetConditionFragment":
        titleToolbar.setText(getString(R.string.toolbar_condition_config));
        break;
      case "AboutFragment":
        titleToolbar.setText(getString(R.string.toolbar_prefabout));
        break;
      default:
        titleToolbar.setText(getString(R.string.toolbar_config));
        break;
    }
    return;
  }

  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    isBackButton = true;
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    isBackButton = false;
    onBackPressed();
  }
}
