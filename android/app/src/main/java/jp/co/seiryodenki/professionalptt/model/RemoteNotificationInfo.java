package jp.co.seiryodenki.professionalptt.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Location;
import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import java.util.Calendar;
import jp.co.seiryodenki.professionalptt.util.BatteryUtils;
import jp.co.seiryodenki.professionalptt.util.TelephonyUtils;
import jp.co.seiryodenki.professionalptt.util.logger;
import java.util.Date;
import java.text.SimpleDateFormat;

public class RemoteNotificationInfo {

  @SerializedName("message")
  private DeviceStatusMessage message;

  @SerializedName("location")
  private Location location;

  @SerializedName("last_location")
  private Location last_location;

  @SerializedName("last_notification")
  private long last_notification;

  public DeviceStatusMessage getMessage() {
    return message;
  }

  public void setMessage(DeviceStatusMessage message) {
    this.message = message;
  }

  public Location getLocation() {
    return location;
  }

  public Location getLast_location() {
    return last_location;
  }

  public void setLast_location(Location last_location) {
    this.last_location = last_location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public long getLast_notification() {
    return last_notification;
  }

  public void setLast_notification(long last_notification) {
    this.last_notification = last_notification;
  }

  public static String getLocationString(Location location) {
    if (location == null) {
      return "";
    }
    return "Latitude="
        + String.valueOf(location.getLatitude())
        + "\n"
        + "Longitude="
        + String.valueOf(location.getLongitude())
        + "\n"
        + "Accuracy="
        + String.valueOf(location.getAccuracy())
        + "\n"
        + "Altitude="
        + String.valueOf(location.getAltitude())
        + "\n"
        + "Speed="
        + String.valueOf(location.getSpeed())
        + "\n"
        + "Bearing="
        + String.valueOf(location.getBearing())
        + "\n";
  }

  public static String getLocationStringLite(Location location) {
    if (location == null) {
      return "";
    }
    return "Latitude = "
      + String.valueOf(location.getLatitude())
      + " "
      + "Longitude = "
      + String.valueOf(location.getLongitude())
      + ",";
  }

  public static String getLatitudeLongitude(Location location) {
    if (location == null) {
      return "";
    }
    return String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude());
  }


  public static void updateLocationUI(Location location) {
    if (location == null) {
      return;
    }
  }

  public void setCellIdentity(TelephonyUtils.CellIdentity cellIdentity) {
    if (message == null) {
      message = new DeviceStatusMessage();
    }
    message = DeviceStatusMessage.setCellIdentity(message, cellIdentity);
  }

  public void setBatteryInfo(BatteryUtils.BatteryInfo batteryInfo) {
    if (message == null) {
      message = new DeviceStatusMessage();
    }
    message = DeviceStatusMessage.setBatteryInfo(message, batteryInfo);
  }

  public void setLocation(boolean enable, Location location) {
    if (message == null) {
      message = new DeviceStatusMessage();
    }
    message = DeviceStatusMessage.setLocation(message, enable, location);
    this.location = location;
  }

  public void setAddress(Address address) {
    if (message == null) {
      message = new DeviceStatusMessage();
    }
    message = DeviceStatusMessage.setAddress(message, address);
  }

  public void setReport(String report) {
    if (message == null) {
      message = new DeviceStatusMessage();
    }
    message.setReport(report);
  }

  public void setLastInformation() {
    this.last_notification = Calendar.getInstance().getTimeInMillis();
    this.last_location = location;
  }

  public boolean isSatisfiedInterval(@NonNull final Context ctx, SharedPreferences pref) {
    if (message == null) {
      logger.e("RemoteNotificationInfo", "[IntervalJob]Message is null.");
      return false;
    }
    // 2019/01/22 SEC
    // サーバで設定した日時のみGPS情報を通知する
    // 2019/01/22 SEC
    if(TerminalInfo.getGpsInfoLimit(ctx, pref))
    {// 位置情報の測位制限を有効とする!!
      Calendar now = Calendar.getInstance();
      if(TerminalInfo.getGpsInfoType(ctx, pref))
      {// 曜日による制限を有効とする。
        int dayOfWeek = (now.get(Calendar.DAY_OF_WEEK) - 1);

        switch(dayOfWeek){
          case 0: // 日曜日
            if(!TerminalInfo.getGpsInfoSun(ctx, pref))
              return false;
            break;
          case 1: // 月曜日
            if(!TerminalInfo.getGpsInfoMon(ctx, pref))
              return false;
            break;
          case 2: // 火曜日
            if(!TerminalInfo.getGpsInfoTue(ctx, pref))
              return false;
            break;
          case 3: // 水曜日
            if(!TerminalInfo.getGpsInfoWed(ctx, pref))
              return false;
            break;
          case 4: // 木曜日
            if(!TerminalInfo.getGpsInfoThu(ctx, pref))
              return false;
            break;
          case 5: // 金曜日
            if(!TerminalInfo.getGpsInfoFri(ctx, pref))
              return false;
            break;
          case 6: // 土曜日
            if(!TerminalInfo.getGpsInfoSat(ctx, pref))
              return false;
            break;
            default:
              return false; // 曜日設定が範囲外の場合にはエラーとする
        }
      }
      // 時間による制限を行う。
      try {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        Date currentDate = sdf.parse(hour + ":" + minute);
        Date startDate = sdf.parse(TerminalInfo.getGpsInfoStart(ctx, pref));
        Date endtDate = sdf.parse(TerminalInfo.getGpsInfoEnd(ctx, pref));
        // 開始時間チェック
        if(startDate.after(currentDate)){
          return false;
        }
        // 終了時間チェック
        if(endtDate.before(currentDate)){
          return false;
        }
      } catch (Exception e) {
        logger.e("RemoteNotificationInfo","ERROR! - comparing DATES");
      }
    }

    int dist = TerminalInfo.getGpsInfoDist(ctx, pref);
    boolean isRemoteEnable = TerminalInfo.getGpsInfoUse(ctx, pref);
    int jobInterval = TerminalInfo.getGpsInfoTime(ctx, pref);
    if (isRemoteEnable && jobInterval > 0) {
      Calendar cal = Calendar.getInstance();
      long currentDatetime = cal.getTimeInMillis();
      cal.setTimeInMillis(last_notification);
      cal.add(Calendar.MINUTE, jobInterval);
      if (cal.getTimeInMillis() <= currentDatetime) {
        return true;
      }
    }
    if (isRemoteEnable && dist > 0) {
      if (last_location == null) {
        return true;
      }
      if (location.distanceTo(last_location) >= dist) {
        return true;
      }
    }
    return false;
  }

}
