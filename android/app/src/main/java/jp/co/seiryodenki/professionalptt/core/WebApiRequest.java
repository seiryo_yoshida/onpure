package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.util.BitmapUtils;
import jp.co.seiryodenki.professionalptt.util.GsonUtils;
import jp.co.seiryodenki.professionalptt.util.logger;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static jp.co.seiryodenki.professionalptt.Constants.NETWORK_TIMEOUT_SEC;

public class WebApiRequest<T> implements Callback {
  public static final String TAG = WebApiRequest.class.getSimpleName();

  private final Class<T> responseType;
  private OnResponseListener<T> onResponseListener;
  private OnFailureListener onFailureListener;
  private String url;

  public WebApiRequest(Class<T> responseType) {
    this.responseType = responseType;
  }

  public interface OnResponseListener<T> {
    void onResponse(@Nonnull T response);
  }

  public interface OnFailureListener {
    void onFailure(@Nonnull Exception e);
  }

  public WebApiRequest<T> setOnResponse(@Nullable OnResponseListener<T> listener) {
    onResponseListener = listener;
    return this;
  }

  public WebApiRequest<T> setOnFailure(@Nullable OnFailureListener listener) {
    onFailureListener = listener;
    return this;
  }

  private Request request() {
    return new Request.Builder().url(this.url).build();
  }

  private static OkHttpClient client;

  private synchronized OkHttpClient create() {
    if (client != null) {
      return client;
    }
    client =
        new OkHttpClient()
            .newBuilder()
            .connectTimeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build();
    return client;
  }

  @Override
  public void onFailure(@Nonnull Call call, @Nonnull IOException e) {
    onFailure(e);
  }

  private void onFailure(@Nonnull Exception e) {
    if (onFailureListener != null) {
      onFailureListener.onFailure(e);
    }
  }

  @Override
  public void onResponse(@Nonnull Call call, @Nonnull Response response) {
    onResponse(response);
  }

  private void onResponse(@Nonnull Response response) {
    if (!response.isSuccessful()) {
      onFailure(new PttException(response.toString()));
      return;
    }
    try {
      if (response.body() == null) {
        onFailure(new PttException("no body"));
      }
      String json = response.body().string();
      T result = GsonUtils.fromJson(json, responseType);
      WebApiResponse status = (WebApiResponse) result;
      if (status == null || status.getResult() == null) {
        onFailure(new PttException("no result: " + json));
        return;
      }
      if (!status.getResult().equalsIgnoreCase("OK")) {
        onFailure(new AuthException(status.getMessage()));
        return;
      }
      if (onResponseListener != null) {
        onResponseListener.onResponse(result);
      }
    } catch (Exception e) {
      onFailure(e);
    }
  }

  public void execute() {
    try {
      onResponse(create().newCall(request()).execute());
    } catch (IOException e) {
      onFailure(e);
    }
  }

  public void enqueue() {
    create().newCall(request()).enqueue(this);
  }

  private static boolean isEmulator() {
    return Build.PRODUCT.contains("sdk_google");
  }

  /**
   * Returns Device ID
   *
   * @return Device ID
   */
  @SuppressWarnings("deprecation")
  public static String getIMEI(Context c, SharedPreferences preference) {
    // 2018/5/28 V0.4対応　IMEI->UUID変更
    final String DUMMY_TERMINAL_ID = "262bf5f7afb62817";
    String imei = preference.getString(c.getString(R.string.key_pref_terminal_imei), null);
    if (imei == null) {
      imei = Settings.Secure.getString(c.getContentResolver(),android.provider.Settings.Secure.ANDROID_ID);
      preference.edit().putString(c.getString(R.string.key_pref_terminal_imei), imei).apply();
    }
    //imei = DUMMY_TERMINAL_ID;
    return imei;
//    final String DUMMY_TERMINAL_ID = "351544081925309";
//    String imei = preference.getString(c.getString(R.string.key_pref_terminal_imei), null);
//    if (imei == null) {
//      TelephonyManager telephonyManager =
//              (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
//      if (isEmulator() || telephonyManager == null) {
//        imei = DUMMY_TERMINAL_ID;
//      } else {
//        imei = telephonyManager.getDeviceId();
//      }
//      preference.edit().putString(c.getString(R.string.key_pref_terminal_imei), imei).apply();
//    }
//    // PTT01とPTT02切り替えのために変更
//    //imei = DUMMY_TERMINAL_ID;
//    return imei;
  }

  public static String getServerUrl(Context c, SharedPreferences preference, int resource) {
    switch (resource) {
      case R.string.key_pref_web_server:
        return c.getString(R.string.web_api_format_secure,
            preference.getString(c.getString(resource), c.getString(R.string.web_api_host)),
            c.getString(R.string.web_api_prefix));
    }
    return null;
  }

  /**
   * URLを設定する。
   *
   * @return WebApiRequest
   */
  public WebApiRequest<T> setUrl(@Nonnull String url) {
    this.url = url;
    return this;
  }

  /**
   * URLを取得する。
   *
   * @return URL
   */
  public String getUrl() {
    return url;
  }

  /**
   * Booton URLを設定する。
   *
   * @param c
   * @param preference
   * @return WebApiRequest
   */
  public WebApiRequest<T> setBootonUrl(Context c, SharedPreferences preference) {
    this.url =
        getServerUrl(c, preference, R.string.key_pref_web_server)
            + "booton?uuid="
            + getIMEI(c, preference);
    return this;
  }

  /**
   * SignIn URLを設定する。
   *
   * @param c
   * @param preference
   * @param userId
   * @param password
   * @param lastUpdate
   * @return WebApiRequest
   */
  public WebApiRequest<T> setServerInfoUrl(
      Context c,
      SharedPreferences preference,
      String userId,
      String password,
      long lastUpdate) {
    this.url =
        getServerUrl(c, preference, R.string.key_pref_web_server)
            + "get_info?login_id=" + userId
            + "&login_password=" + password
            + "&uuid="+ getIMEI(c, preference);
    if (lastUpdate > 0) {
      this.url += "&timestamp=" + lastUpdate;
    }
    return this;
  }

  /**
   * SignIn URLを設定する。
   *
   * @param c
   * @param preference
   * @param companyId
   * @param terminalId
   * @param userId
   * @param password
   * @return WebApiRequest
   */
  public WebApiRequest<T> setSignInUrl(
      Context c,
      SharedPreferences preference,
      String companyId,
      String terminalId,
      String userId,
      String password,
      // 2019/04/17 新サーバ対応
      String timeZone
      ) {
    this.url =
        getServerUrl(c, preference, R.string.key_pref_web_server)
            + "login?company_id="
            + companyId
            + "&login_id="
            + userId
            + "&login_password="
            + password
            + "&terminal_id="
            + terminalId
            // 2019/04/17 新サーバ対応
            + "&time_zone="
            + timeZone;
    return this;
  }

  /**
   * BootoutのURLを設定する。
   *
   * @param c
   * @param preference
   * @return URL
   */
  public WebApiRequest<T> setBootoutUrl(Context c, SharedPreferences preference) {
    this.url =
        getServerUrl(c, preference, R.string.key_pref_web_server)
            + "bootout?uuid="
            + getIMEI(c, preference);
    return this;
  }

  Handler handler = new Handler();
  File file;

  /**
   * 画像ファイルをアップロードする
   * 2MPix(1600x1200)より大きい場合はVGA(640x480)に変換してアップロード
   * 変換したファイルはgetExternalCacheDir()へ一時保存
   * @param c
   * @param preference
   * @param companyId
   * @param userId
   * @param filePath アップロードする画像のパス
   */
  public void uploadAttachFile(
      Context c, SharedPreferences preference, String companyId, String userId, String filePath) {
    if (filePath == null || filePath.isEmpty()) {
      onFailure(new PttException("file path is empty."));
      return;
    }
    this.url = getServerUrl(c, preference, R.string.key_pref_web_server) + "set_attach_file_send";
    MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    new Thread(
            () -> {
              try {
                file = null;
                final int IMG_WIDTH_LIMIT = 1600;
                final int IMG_HEIGHT_LIMIT = 1200;
                String tempFilePath = String.format("%s/temp.jpg", c.getExternalCacheDir());
                if (BitmapUtils.isLarger(filePath, IMG_WIDTH_LIMIT, IMG_HEIGHT_LIMIT)) {
                  try {
                    tempFilePath = String.format("%s/temp.jpg", c.getExternalCacheDir());
                    BitmapUtils.convertToVGASize(c, filePath, tempFilePath);
                    file = new File(tempFilePath);
                  } catch (Exception e) {
                    file = new File(filePath);
                  }
                } else {
                  file = new File(filePath);
                }
              } catch (Exception e) {
                logger.e(TAG, "VGA conversion fails", e);
              }
              handler.post(
                  () -> {
                    RequestBody requestBody =
                        new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("term_id", companyId + userId)
                            .addFormDataPart(
                                "upload_file", "temp.png", RequestBody.create(MEDIA_TYPE_PNG, file))
                            .build();

                    Request request = new Request.Builder().url(url).post(requestBody).build();

                    create().newCall(request).enqueue(WebApiRequest.this);
                  });
            })
        .start();
  }

  public static String getAttFileUrl(Context c, SharedPreferences preference, int resource,
                                     String attFileId, String companyId, String userId) {
    switch (resource) {
      case R.string.key_pref_web_server:
        return String.format("%s%s%s%s%s%s",
          getServerUrl(c, preference, R.string.key_pref_web_server),
          "get_attach_file_download?att_file_id=",
          attFileId,
          "&term_id=",
          companyId,
          userId);
    }
    return null;
  }

  public String downloadAttFileId(
      Context c, SharedPreferences preference, String attFileId, String companyId, String userId) {
    String filePath = "/sdcard/DCIM/Camera/" + attFileId + ".png";
    // String att_file_id = "de56c29a-5886-4d75-8179-9581370706d4";
    this.url =
        String.format(
            "%s%s%s%s%s%s",
            getServerUrl(c, preference, R.string.key_pref_web_server),
            "get_attach_file_download?att_file_id=",
            attFileId,
            "&term_id=",
            companyId,
            userId);

    try {
      Response response = create().newCall(request()).execute();
      if (!response.isSuccessful()) {
        throw new IOException("Unexpected code " + response);
      }
      FileOutputStream fos = new FileOutputStream(filePath);
      fos.write(response.body().bytes());
      fos.close();
    } catch (Throwable e) {
      logger.e(TAG, "getAttFile", e);
      filePath = null;
    }
    return filePath;
  }

  /**
   * 通話録音リスト URLを設定する。
   *
   * @param c
   * @param preference
   * @param companyId
   * @return WebApiRequest
   */
  public WebApiRequest<T> setVoiceFileListUrl(
          Context c, SharedPreferences preference, String companyId) {

    this.url =
            getServerUrl(c, preference, R.string.key_pref_web_server)
                    + "list_meeting_file?company_id="
                    + companyId;

    //録音一覧確認のため
    //this.url = getServerUrl(c, preference, R.string.key_pref_web_server) + "list_meeting_file";
    return this;
  }

}
