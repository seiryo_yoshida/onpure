package jp.co.seiryodenki.professionalptt.core;

import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_PTT_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_PTT_STATUS;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAssignedNumbers;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import java.util.List;
import org.linphone.core.LinphoneCore;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;

public class HeadsetManager extends BroadcastReceiver {
  private static final String TAG = HeadsetManager.class.getSimpleName();

  private static HeadsetManager instance;

  private boolean isAudioFocus;
  private AudioManager audioManager;
  private BluetoothAdapter bluetoothAdapter;
  private BluetoothDevice bluetoothDevice;
  private BluetoothHeadset bluetoothHeadset;
  private BluetoothProfile.ServiceListener profileListener;
  private Context context;
  private LinphoneCore linphoneCore;
  private boolean isBluetoothConnected;
  private boolean isScoConnected;
  private boolean isFirstConnected = false;

  protected HeadsetManager(@NonNull final Context c, @NonNull LinphoneCore lc) {
    context = c;
    linphoneCore = lc;
    audioManager = ((AudioManager) c.getSystemService(Context.AUDIO_SERVICE));
    if (audioManager != null) {
      audioManager.setStreamVolume(
          AudioManager.STREAM_MUSIC,
          audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / 2,
          0);
    }
    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    initBluetooth();
    startBluetooth();
    setModeCommunication();
    //    muteMicrophone();
  }

  public static synchronized HeadsetManager create(
      @NonNull Context c, @NonNull LinphoneCore linphoneCore) {
    if (instance != null) {
      // TODO create
      return instance;
    }
    instance = new HeadsetManager(c, linphoneCore);
    return instance;
  }

  public static synchronized void destroy() {
    if (instance == null) {
      return;
    }
    instance.onDestroy();
    instance = null;
  }

  public static HeadsetManager getInstance() {
    if (instance == null) {
      throw new RuntimeException("HeadsetManager is not active");
    }
    return instance;
  }

  public static synchronized boolean isInstantiated() {
    return instance != null;
  }

  public void initBluetooth() {
    IntentFilter filter = new IntentFilter();
    filter.addCategory(
        BluetoothHeadset.VENDOR_SPECIFIC_HEADSET_EVENT_COMPANY_ID_CATEGORY
            + "."
            + BluetoothAssignedNumbers.PLANTRONICS);
    filter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED);
    filter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
    filter.addAction(BluetoothHeadset.ACTION_VENDOR_SPECIFIC_HEADSET_EVENT);
    context.registerReceiver(this, filter);
  }

  private void startBluetooth() {
    if (isBluetoothConnected) {
      return;
    }
    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
      return;
    }
    if (profileListener != null) {
      bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
    }
    profileListener =
        new BluetoothProfile.ServiceListener() {
          public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile != BluetoothProfile.HEADSET) {
              return;
            }
            bluetoothHeadset = (BluetoothHeadset) proxy;
            isBluetoothConnected = true;
          }

          public void onServiceDisconnected(int profile) {
            if (profile != BluetoothProfile.HEADSET) {
              return;
            }
            bluetoothHeadset = null;
            isBluetoothConnected = false;
            routeAudioToReceiver();
          }
        };
    if (!bluetoothAdapter.getProfileProxy(context, profileListener, BluetoothProfile.HEADSET)) {
      logger.e(TAG, "getProfileProxy failed !");
    }
  }

  private boolean routeAudioToBluetooth() {
    if (bluetoothAdapter.isEnabled() && audioManager.isBluetoothScoAvailableOffCall()) {
      if (isBluetoothHeadsetAvailable()) {
        if (!audioManager.isBluetoothScoOn()) {
          audioManager.setBluetoothScoOn(true);
          audioManager.startBluetoothSco();
        }
      } else {
        return false;
      }

      // Hack to ensure bluetooth sco is really running
      boolean ok = isUsingBluetoothAudioRoute();
      int retries = 0;
      while (!ok && retries < 5) {
        retries++;

        try {
          Thread.sleep(200);
        } catch (InterruptedException e) {
          // Do nothing.
        }

        if (audioManager != null) {
          audioManager.setBluetoothScoOn(true);
          audioManager.startBluetoothSco();
        }

        ok = isUsingBluetoothAudioRoute();
      }
      return ok;
    }
    return false;
  }

  public boolean isUsingBluetoothAudioRoute() {
    return bluetoothHeadset != null
        && bluetoothHeadset.isAudioConnected(bluetoothDevice)
        && isScoConnected;
  }

  public boolean isBluetoothHeadsetAvailable() {
    if (bluetoothAdapter != null
        && bluetoothAdapter.isEnabled()
        && audioManager != null
        && audioManager.isBluetoothScoAvailableOffCall()) {
      boolean isHeadsetConnected = false;
      if (bluetoothHeadset != null) {
        List<BluetoothDevice> devices = bluetoothHeadset.getConnectedDevices();
        bluetoothDevice = null;
        for (final BluetoothDevice dev : devices) {
          if (bluetoothHeadset.getConnectionState(dev) == BluetoothHeadset.STATE_CONNECTED) {
            bluetoothDevice = dev;
            isHeadsetConnected = true;
            break;
          }
        }
      }
      return isHeadsetConnected;
    }
    return false;
  }

  public void disableBluetoothSCO() {
    if (audioManager != null && audioManager.isBluetoothScoOn()) {
      audioManager.stopBluetoothSco();
      audioManager.setBluetoothScoOn(false);

      // Hack to ensure bluetooth sco is really stopped
      int retries = 0;
      while (isScoConnected && retries < 10) {
        retries++;

        try {
          Thread.sleep(200);
        } catch (InterruptedException e) {
          // Do nothing
        }

        audioManager.stopBluetoothSco();
        audioManager.setBluetoothScoOn(false);
      }
    }
  }

  public void stopBluetooth() {
    isBluetoothConnected = false;

    disableBluetoothSCO();

    if (bluetoothAdapter != null && profileListener != null && bluetoothHeadset != null) {
      bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
      profileListener = null;
    }
    bluetoothDevice = null;
  }

  public void onDestroy() {
    try {
      stopBluetooth();
    } catch (Exception e) {
      logger.e(TAG, "stopBluetooth", e);
      return;
    }
    try {
      context.unregisterReceiver(this);
    } catch (Exception e) {
      logger.e(TAG, "unregisterReceiver", e);
    }
  }

  private void onScoAudioStateUpdated(@NonNull Intent intent) {
    int state = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, 0);
    if (state == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
      isScoConnected = true;
    } else if (state == AudioManager.SCO_AUDIO_STATE_DISCONNECTED) {
      isScoConnected = false;
    }
  }

  private void onConnectionStateChanged(@NonNull Intent intent) {
    int state =
        intent.getIntExtra(
            BluetoothAdapter.EXTRA_CONNECTION_STATE, BluetoothAdapter.STATE_DISCONNECTED);
    if (state == BluetoothAdapter.STATE_DISCONNECTED) {
      stopBluetooth();
    } else if (state == BluetoothAdapter.STATE_CONNECTED) {
      isFirstConnected = true;
      startBluetooth();
    }
  }

  private void onVendorSpecificHeadsetEvent(@NonNull Context context, @NonNull Intent intent) {
    if (isFirstConnected) {
      isFirstConnected = false;
      return;
    }
    Object[] args = null;
    if (intent.getExtras() != null) {
      args =
          (Object[])
              intent.getExtras().get(BluetoothHeadset.EXTRA_VENDOR_SPECIFIC_HEADSET_EVENT_ARGS);
    }
    if (args != null && args.length >= 2 && "TALK".equals(args[0])) {
      Intent pttIntent = new Intent(BROADCAST_PTT_STATUS);
      pttIntent.putExtra(INTENT_PTT_STATUS, (Integer) args[1]);
      LocalBroadcastManager.getInstance(context).sendBroadcast(pttIntent);
    }
  }

  public void onReceive(Context context, @NonNull Intent intent) {
    String action = intent.getAction();
    if (action == null) {
      logger.e(TAG, "Action is null.");
      return;
    }
    if (AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED.equals(action)) {
      onScoAudioStateUpdated(intent);
    } else if (BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
      onConnectionStateChanged(intent);
    } else if (action.equals(BluetoothHeadset.ACTION_VENDOR_SPECIFIC_HEADSET_EVENT)) {
      onVendorSpecificHeadsetEvent(context, intent);
    }
  }

  public void routeAudio() {
    if (!speakerEnabled) {
      routeAudioToReceiver();
      return;
    }
    if (instance.isBluetoothHeadsetAvailable()) {
      routeAudioToBluetooth();
      return;
    }
    routeAudioToSpeaker(true);
  }

  @Deprecated
  private void routeAudioToSpeaker(boolean speakerOn) {
    if (audioManager.isWiredHeadsetOn()) {
      speakerOn = false;
    }
    linphoneCore.enableSpeaker(speakerOn);
  }

  public void routeAudioToReceiver() {
    routeAudioToSpeaker(false);
  }

  public void setModeCommunication() {
    if (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {
      return;
    }
    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
  }

  public void setModeNormal() {
    if (audioManager.getMode() == AudioManager.MODE_NORMAL) {
      return;
    }
    audioManager.setMode(AudioManager.MODE_NORMAL);
  }

  @SuppressWarnings("deprecation")
  public void abandonAudioFocus() {
    if (isAudioFocus) {
      isAudioFocus = false;
      audioManager.abandonAudioFocus(null);
    }
  }

  @SuppressWarnings("deprecation")
  private void requestAudioFocus(int stream) {
    if (isAudioFocus) {
      return;
    }
    isAudioFocus = true;
    int rc = audioManager.requestAudioFocus(null, stream, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
  }

  public void requestFocusStreamVoiceCall() {
    requestAudioFocus(AudioManager.STREAM_VOICE_CALL);
  }

  private boolean microphoneMuted = true;

  private void setMicrophoneMute(boolean mute) {
    microphoneMuted = mute;
    linphoneCore.muteMic(mute);
    audioManager.setMicrophoneMute(mute);
  }

  public void muteMicrophone() {
    setMicrophoneMute(true);
  }

  public void unmuteMicrophone() { setMicrophoneMute(false); }

  public boolean isMicrophoneMuted() {
    return linphoneCore.isMicMuted();
  }

  private boolean speakerEnabled = true;

  private void setSpeakerEnabled(boolean on) {
    speakerEnabled = on;
    routeAudio();
  }

  public void muteSpeaker() {
    // 2018/06/08 V0.4対応　音声レベル調整
    TerminalUtils.setMuteAudioSetting();
    setSpeakerEnabled(false);
  }

  public void unmuteSpeaker() {
    // 2018/06/08 V0.4対応　音声レベル調整
    TerminalUtils.setNormalAudioSetting();
    setSpeakerEnabled(true);
  }
}
