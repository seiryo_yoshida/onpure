package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class RadioSet {

  @SerializedName("zero")
  private int zero;

  @SerializedName("no")
  private int no;

  @SerializedName("one")
  private int one;

  public RadioSet(int zero, int no, int one) {
    this.zero = zero;
    this.no = no;
    this.one = one;
  }

  public int getZero() {
    return zero;
  }

  public void setZero(int zero) {
    this.zero = zero;
  }

  public int getNo() {
    return no;
  }

  public void setNo(int no) {
    this.no = no;
  }

  public int getOne() {
    return one;
  }

  public void setOne(int one) {
    this.one = one;
  }
}
