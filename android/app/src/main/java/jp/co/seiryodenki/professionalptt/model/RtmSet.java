package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class RtmSet {

  @SerializedName("use")
  private int use;

  @SerializedName("callto")
  private String callto;

  @SerializedName("notice")
  private int notice;

  public RtmSet() {}

  public RtmSet(int use, int notice) {
    this.use = use;
    this.notice = notice;
  }

  public int getUse() {
    return use;
  }

  public void setUse(int use) {
    this.use = use;
  }

  public int getNotice() {
    return notice;
  }

  public void setNotice(int notice) {
    this.notice = notice;
  }

  public String getCallto() {
    return callto;
  }

  public void setCallto(String callto) {
    this.callto = callto;
  }
}
