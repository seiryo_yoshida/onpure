package jp.co.seiryodenki.professionalptt.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.stream.Collectors;

public enum CallType implements Parcelable {
  OPERATING_STATION("CMD"),
  ALL_CALL_SYSTEM_WIDE_ENFORCEMENT("HHH"),
  ALL_CALL_SYSTEM_WIDE("ACS"),
  GROUP_CALL_ENFORCEMENT("FGC"),
  GROUP_CALL("GPC"),
  MULTI_GROUP_CALL_ENFORCEMENT("FMG"),
  MULTI_GROUP_CALL("MGP"),
  CALL("CLL"),
  SITE_ALL_CALL("SAL"),
  NEAR_ALL_CALL("NAL"),
  EMERGENCY_CALL("EMC"),
  REMOTE_MONITOR("RTM");

  public static final Parcelable.Creator<CallType> CREATOR = new Parcelable.Creator<CallType>() {
    public CallType createFromParcel(Parcel in) {
      return values()[in.readInt()];
    }

    public CallType[] newArray(int size) {
      return new CallType[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel out, int flags) {
    out.writeInt(ordinal());
  }

  private static final Map<String,CallType> nameMap = new HashMap<String,CallType>();
  static {
    for (CallType s : CallType.values()) {
      nameMap.put(s.toString(), s);
    }
  }

  public static final CallType DEFAULT_CALL_TYPE = CallType.CALL;
  public static final int DEFAULT_CALL_TYPE_ID = DEFAULT_CALL_TYPE.getId();

  public static CallType getType(int callId) {
    if (callId <= 0 || callId > REMOTE_MONITOR.getId()) {
      return DEFAULT_CALL_TYPE;
    }
    return CallType.values()[callId-1];
  }

  public static CallType getType(String str) {
    try {
      return nameMap.get(str);
    } catch (Throwable ignored) {
      return null;
    }
  }

  private String text;

  CallType(final String text) {
    this.text = text;
  }

  public int getId() {
    return this.ordinal() + 1;
  }

  @Override
  public String toString() {
    return text;
  }
}
