package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import com.google.gson.Gson;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.Nonnull;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.DeviceStatusMessage;
import jp.co.seiryodenki.professionalptt.model.MqttInfoEntity;
import jp.co.seiryodenki.professionalptt.model.MyMqttMessage;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.util.Base64Encoder;
import jp.co.seiryodenki.professionalptt.util.GsonUtils;
import jp.co.seiryodenki.professionalptt.util.logger;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;

import static jp.co.seiryodenki.professionalptt.Constants.CALL_PTTALL_ID;
import static jp.co.seiryodenki.professionalptt.Constants.MQTTSTATUS;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_PUSH_NOTIFICATION_PERIOD;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TIMEOUT_MILLIS;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_CALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_CALLLIST;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_DUMMY;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_INFO;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_PUSH;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_RELEASE;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_SEND_MSG;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_DUMMY_HEADER;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_FORMAT_PTTALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_HEADER;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_MSG;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_PTTALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_SL_CONTROL;

public class MqttManager implements MqttCallbackExtended {
  private static final String TAG = MqttManager.class.getSimpleName();
  private static final int QOS_EXACTLY_ONCE = 2;
  private static MqttManager instance;
  private Context context;
  private MqttAsyncClient mqttClient = null;

  private MqttNotifier mqttNotifier = null;
  private MqttSubscribeManager mqttSubscribeManager = null;
  private MqttDummyPublisher mqttDummyPublisher = null;
  private String companyId;
  private String myTerminalId;

  private Handler pushLooper;
  private Timer pushTimer;
  private String pushCalltype;
  private String pushCallId;

  private String topicToPublishToControl;

  private OnReceiveMqttMessageListener onReceiveMqttMessageListener;

  private MqttManager(@NonNull Context context, @NonNull String companyId, @NonNull String myTerminalId) {
    this.context = context;
    this.companyId = companyId;
    this.myTerminalId = myTerminalId;
    this.topicToPublishToControl = MQTT_TOPIC_HEADER + companyId + MQTT_TOPIC_CMD
      + myTerminalId + MQTT_TOPIC_SL_CONTROL;
    mqttNotifier = new MqttNotifier(companyId, myTerminalId);
    mqttSubscribeManager = new MqttSubscribeManager(companyId);
    mqttDummyPublisher = new MqttDummyPublisher(context);
  }

  public static synchronized MqttManager create(
          @NonNull Context context, @NonNull String companyId, @NonNull String myTerminalId) {
    if (instance == null) {
      instance = new MqttManager(context, companyId, myTerminalId);
    }
    return instance;
  }

  public MqttManager setOnReceiveMqttMessageListener(OnReceiveMqttMessageListener listener) {
    mqttNotifier.setOnReceiveMqttMessageListener(listener);
    onReceiveMqttMessageListener = listener;
    return this;
  }

  public static synchronized MqttManager getInstance() {
    if (instance == null) {
      throw new RuntimeException("MqttManager is not active");
    }
    return instance;
  }

  public static synchronized boolean isInstantiated() {
    return instance != null;
  }

  private synchronized void startPushNotification(String callType, String callId, long periodMilli, CallService callService) {
    if (callType == null || callId == null) {
      return;
    }
    if (pushTimer != null) {
      stopPushNotification();
    }
    pushCalltype = callType;
    pushCallId = callId;
    try {
      pushLooper = new Handler(Looper.getMainLooper());
      TimerTask task =
        new TimerTask() {
          @Override
          public void run() {
            pushLooper.post(
              () -> {
                if (instance != null) {
                  if ((callService != null && callService.isPressedPtt) ||
                    callService.isMonitored()) {
                    requestChangeStatusWhenPushed(pushCalltype, pushCallId, false);
                  }
                }
              });
          }
        };
      pushTimer = new Timer("MQTT PushNotification scheduler");
      pushTimer.schedule(task, 0, periodMilli);
    } catch (Exception e) {
      logger.e(TAG, "Cannot start PushNotification", e);
    }
  }

  public synchronized void stopPushNotification() {
    try {
      if (pushTimer != null) {
        pushTimer.cancel();
        pushTimer = null;
      }
    } catch (Exception e) {
      logger.e(TAG, "cancel PushNotification timer", e);
    }
  }

  /**
   * Connect MQTT server
   *
   * <pre>
   *     following value is default for debug.
   *     clientId = "MQIsdp";
   *     userId = "admin";
   *     password = "secpttpassword";
   * </pre>
   *
   * @param clientId mqtt client id (better to be unique)
   * @param userId login id for connection
   * @param password login password for connection
   */
  public void connect(@NonNull String clientId, String userId, String password) {
    userId = "admin";
    // 2019/08/08 オンプレ対応
    // ローカル接続用のパスワードに変更
    password = "password";
    try {
      String serverUri =
              context.getString(R.string.mqtt_api_format_secure,
                      context.getString(R.string.mqtt_host),
                      context.getString(R.string.mqtt_port_secure));
      mqttClient = new MqttAsyncClient(serverUri, clientId, new MemoryPersistence());
      mqttClient.setCallback(this);
      MqttConnectOptions connOpts = new MqttConnectOptions();
      connOpts.setAutomaticReconnect(true);
      connOpts.setCleanSession(true);
      connOpts.setUserName(userId);
      connOpts.setPassword(password.toCharArray());
      // 2019/5/16 大阪空港交通対応
      // MQTT切断が頻発するため、KeepAliveの期間を短くする。60->5秒に変更
      connOpts.setKeepAliveInterval(5);

      // 2019/08/08 オンプレ対応
      // MQTT接続をTLSからTCPに変更に際して、証明書を削除する
      /*
      TrustManager[] tm = {
              new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                  return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {}

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {}
              }
      };

      SSLContext sslContext = SSLContext.getInstance("TLS");
      sslContext.init(null, tm, null);
      connOpts.setSocketFactory(sslContext.getSocketFactory());
      */

      IMqttToken token = mqttClient.connect(connOpts);
      if (token.getException() != null) {
        token.getException().printStackTrace();
      }
    //} catch (MqttException | NoSuchAlgorithmException | KeyManagementException e) {
    } catch (MqttException e) {
      logger.e(TAG, "connect", e);
    }
  }

  public boolean reconnect() {
    if (mqttClient == null) {
      return false;
    }
    if (!mqttClient.isConnected()) {
      return true;
    }
    try {
      mqttClient.reconnect();
    } catch (Exception e) {
      logger.e(TAG, "reconnect", e);
      return false;
    }
    return true;
  }

  public void disconnect() {
    if (mqttClient == null) {
      return;
    }
    try {
      if (mqttClient.isConnected()) {
        mqttClient.disconnect();
      }
    } catch (Exception e) {
      logger.e(TAG, "disconnect", e);
    }
  }

  public void close() {
    if (mqttClient == null) {
      return;
    }
    try {
      mqttClient.close();
    } catch (Exception e) {
      logger.e(TAG, "close", e);
    }
  }

  private void publish(@NonNull String topic, @NonNull byte[] payload) throws MqttException {
    // ネットワーク未接続の場合には通知しない
    if(!isNetworkConnected(context)) {
      return;
    }
    // MQTTの再接続を行う。再接続時には処理を失敗とする。
    if (!mqttClient.isConnected()) {
      try {
        mqttClient.reconnect();
      } catch (Exception e) {
        logger.e(TAG, "reconnect", e);
        return;
      } finally {
        return;
      }
    }
    MqttMessage msg = new MqttMessage(payload);
    msg.setQos(QOS_EXACTLY_ONCE);
    msg.setRetained(false);
    mqttClient.publish(topic, msg).waitForCompletion(MQTT_TIMEOUT_MILLIS);
  }

  public boolean publish(@NonNull String topic, @NonNull String message) {
    if (mqttClient == null) {
      return false;
    }
    try {
      publish(topic, message.getBytes());
    } catch (Exception e) {
      logger.e(TAG, "publish", e);
      return false;
    }
    return true;
  }

  static public  boolean isNetworkConnected(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
  }

  public String getMyCompanyId() {
    return companyId;
  }

  public String getMyTerminalId() {
    return myTerminalId;
  }

  @Override
  public void connectionLost(Throwable cause) {
  }

  @Override
  public void messageArrived(@NonNull String topic, @NonNull MqttMessage message) throws Exception {
    String msg = new String(message.getPayload());
    MyMqttMessage mqttMessage = GsonUtils.fromJson(msg, MyMqttMessage.class);
    mqttNotifier.onReceive(topic, mqttMessage);
  }

  @Override
  public void deliveryComplete(@NonNull IMqttDeliveryToken token) {
  }

  private boolean subscribe(@NonNull String topic) {
    try {
      mqttClient.subscribe(topic, QOS_EXACTLY_ONCE).waitForCompletion(MQTT_TIMEOUT_MILLIS);
    } catch (Exception e) {
      logger.e(TAG, "subscribe", e);
      return false;
    }
    return true;
  }

  private boolean subscribeTopics() {
    try (Realm realm = Realm.getDefaultInstance()) {
      RealmResults<MqttInfoEntity> resMqtt = realm.where(MqttInfoEntity.class).findAll();
      Iterator<MqttInfoEntity> it = resMqtt.iterator();
      while (it.hasNext()) {
        MqttInfoEntity mqttInfo = it.next();
        String topic = mqttInfo.getMqttTopic();
        if (topic.indexOf("/") == 0) {
          topic = topic.substring(1);
        }
        if (!subscribe(topic)) {
          return false;
        }
        mqttSubscribeManager.addDefaultSubscribeInfo(topic);
      }
      // subscribe secptt/company_id/CMD/+/10000
      // subscribe secptt/company_id/CMD/+/PTTALL
      // subscribe secptt/company_id/MSG/+/PTTALL
      String[] defaultTopics = new String[]{
        MqttSubscribeManager.getTopicFromCallId(companyId, CALL_PTTALL_ID),
        MqttSubscribeManager.getTopicFromCallId(companyId, MQTT_TOPIC_PTTALL),
        String.format(MQTT_TOPIC_FORMAT_PTTALL, companyId)
      };
      for (String defaultTopic : defaultTopics) {
        if (!subscribe(defaultTopic)) {
          return false;
        }
        mqttSubscribeManager.addDefaultSubscribeInfo(defaultTopic);
      }
    }
    return true;
  }

  public void unsubscribe(@Nonnull String topic) {
    try {
      mqttClient.unsubscribe(topic);
    } catch (Exception e) {
      logger.e(TAG, "unsubscribe", e);
    }
  }

  @Override
  public void connectComplete(boolean reconnect, String serverURI) {
    subscribeTopics();
    // 2019/5/16 大阪空港交通対応
    // MQTTの再接続処理のため、ここで対応は不要と考える。
    requestCallList();
    //mqttDummyPublisher.startKeepAlive(() -> {
    //  if (onReceiveMqttMessageListener != null) {
    //    onReceiveMqttMessageListener.onUpdateServerInfo();
    //  }
    //});
  }

  /* 4.1.個別通話呼出要求 publish */
  public boolean requestIndivisualCall(@NonNull String terminalId) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.CALL.toString());
    message.setCallId(myTerminalId);
    message.setFrom(myTerminalId);
    List<String> callList = Arrays.asList(terminalId);
    message.setCallList(callList);
    message.setPriority(StateManager.getMyPriority());
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.2.強制一斉通話呼出要求：指令局 publish */
  public boolean requestForcedSimultaneousPagingFromOperateStation() {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.OPERATING_STATION.toString());
    message.setCallId(CALL_PTTALL_ID);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.3.強制一斉通話呼出要求 publish */
  public boolean requestForcedSimultaneousPaging() {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT.toString());
    message.setCallId(CALL_PTTALL_ID);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.4.一斉通話呼出呼出要求 publish */
  public boolean requestPaging() {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.ALL_CALL_SYSTEM_WIDE.toString());
    message.setCallId(CALL_PTTALL_ID);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.5.強制グループ通話呼出要求 publish */
  public boolean requestForcedGroupCall(@NonNull String groupId) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.GROUP_CALL_ENFORCEMENT.toString());
    message.setCallId(groupId);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.6.グループ通話呼出要求 publish */
  public boolean requestGroupCall(@NonNull String groupId) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.GROUP_CALL.toString());
    message.setCallId(groupId);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.7.強制マルチグループ通話呼出要求 publish */
  public boolean requestForcedMultiGroupCall(@NonNull List<String> callList) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.MULTI_GROUP_CALL_ENFORCEMENT.toString());
    message.setCallList(callList);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.9.マルチグループ通話呼出要求 publish */
  public boolean requestMultiGroupCall(@NonNull List<String> callList) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.MULTI_GROUP_CALL.toString());
    message.setCallList(callList);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.11.地域通話呼出要求 */
  public boolean requestRegionalCall(@NonNull String siteName, @NonNull ArrayList<String> areaList) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.SITE_ALL_CALL.toString());
    message.setCall_name(siteName);
    message.setArea(areaList);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.13.近隣通話呼出要求 publish */
  public boolean requestNearCall(Double latitude, Double longitude) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.NEAR_ALL_CALL.toString());
    message.setFrom(myTerminalId);
    message.setLatitude(latitude);
    message.setLongitude(longitude);
    // 2018/5/28 V0.4対応　近隣通話時の半径設定
    message.setRadius(TerminalUtils.getRadius());
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.15.緊急通話呼出要求 publish */
  public boolean requestEmergencyCall() {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.EMERGENCY_CALL.toString());
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4.17.音声モニタ要求 publish */
  public boolean requestRemoteMonitor(@NonNull String callId, @NonNull List<String> callList) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.REMOTE_MONITOR.toString());
    message.setFrom(myTerminalId);
    message.setCallId(callId);
    message.setCallList(callList);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 4 */
  public void requestMqttCall(@NonNull CallType callType, @NonNull String callId,
                              @NonNull String[] callList) {
    switch (callType) {
      case REMOTE_MONITOR:
        requestRemoteMonitor(callId, Arrays.asList(callList));
        break;
    }
  }

  /* 4 */
  public void requestMqttCall(@NonNull CallType callType, @NonNull String callId) {
    switch (callType) {
      case GROUP_CALL_ENFORCEMENT:
        requestForcedGroupCall(callId);
        break;
      case GROUP_CALL:
        requestGroupCall(callId);
        break;
      case CALL:
        requestIndivisualCall(callId);
        break;
      case REMOTE_MONITOR:
        requestRemoteMonitor(callId, Arrays.asList(callId, myTerminalId));
        break;
    }
  }

  /* 4 */
  public void requestMqttSiteAllCall(
          @NonNull CallType callType, @NonNull String siteName, @NonNull ArrayList<String> areaList) {
    switch (callType) {
      case SITE_ALL_CALL:
        requestRegionalCall(siteName, areaList);
        break;
    }
  }

  /* 4 */
  public void requestMqttCall(@NonNull CallType callType) {
    switch (callType) {
      case OPERATING_STATION:
        requestForcedSimultaneousPagingFromOperateStation();
        break;
      case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
        requestForcedSimultaneousPaging();
        break;
      case ALL_CALL_SYSTEM_WIDE:
        requestPaging();
        break;
      case EMERGENCY_CALL:
        requestEmergencyCall();
        break;
    }
  }

  /* 4 */
  public void requestMqttCall(@NonNull CallType callType, Double latitude, Double longitude) {
    switch (callType) {
      case NEAR_ALL_CALL:
        requestNearCall(latitude, longitude);
        break;
    }
  }

  /* 4 */
  public void requestMqttCall(@NonNull CallType callType, @NonNull String[] callList) {
    List<String> listCallList = Arrays.asList(callList);
    switch (callType) {
      case MULTI_GROUP_CALL_ENFORCEMENT:
        requestForcedMultiGroupCall(listCallList);
        break;
      case MULTI_GROUP_CALL:
        requestMultiGroupCall(listCallList);
        break;
      case REMOTE_MONITOR:
        requestRemoteMonitor(myTerminalId, listCallList);
        break;
    }
  }

  /* 6.通話通知 publish */
  public boolean notifyMqttStatus(@Nonnull CallType callType, @NonNull MQTTSTATUS status,
                                   @NonNull String callId, boolean requestCallList) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_STATUS);
    message.setCallType(callType.toString());
    message.setStatus(status.toString());
    message.setCallId(callId);
    message.setFrom(myTerminalId);
    message.setFlag((requestCallList ? "1" : "0"));
    String messageStr = GsonUtils.toJson(message);

    if (status == MQTTSTATUS.START) {
      mqttDummyPublisher.startCallKeepAlive();
      if (mqttSubscribeManager.addSubscribeInfo(callId)) {
        subscribe(MqttSubscribeManager.getTopicFromCallId(companyId, callId));
      }
    } else if (status == MQTTSTATUS.OUT || status == MQTTSTATUS.BYE || status == MQTTSTATUS.PARK) {
      mqttDummyPublisher.stopCallKeepAlive();
      if (mqttSubscribeManager.needsToUnsubscribe(callId)) {
        unsubscribe(MqttSubscribeManager.getTopicFromCallId(companyId, callId));
      }
    }

    return publish(topicToPublishToControl, messageStr);
  }

  /* 6.通話通知 publish */
  public void notifyMqttStatus(@NonNull CallType callType, @NonNull MQTTSTATUS status,
                               @NonNull String callId) {
    notifyMqttStatus(callType, status, callId, false);
  }

  public void stopCallKeepAlive() {
    mqttDummyPublisher.stopCallKeepAlive();
  }

  /* 7.1.メッセージ通知
   * destinationId: terminalId or groupId or PTTALL
   */
  public boolean notifyMessage(@NonNull String destinationId, @NonNull String type,
                               @NonNull String body, String att_file_id) {
    String pubTopic_7_1 =
        MQTT_TOPIC_HEADER + companyId + MQTT_TOPIC_MSG + myTerminalId + "/" + destinationId;
    String bodyBase64 = Base64Encoder.encode(body);
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_SEND_MSG);
    message.setType(type);
    message.setBody(bodyBase64);
    message.setAttFileId(att_file_id);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(pubTopic_7_1, messageStr);
  }

  public boolean notifyMessage(@NonNull String destinationId, @NonNull String type,
                               @NonNull String body) {
    return notifyMessage(destinationId, type, body, null);
  }

  public boolean notifyMessageWithGps(@NonNull String destinationId, @NonNull String type,
                               @NonNull String body, String gps) {
    String pubTopic_7_1 =
      MQTT_TOPIC_HEADER + companyId + MQTT_TOPIC_MSG + myTerminalId + "/" + destinationId;
    String bodyBase64 = Base64Encoder.encode(body);
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_SEND_MSG);
    message.setType(type);
    message.setBody(bodyBase64);
    message.setGps(gps);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(pubTopic_7_1, messageStr);
  }

  /* 8.1.ダミーパケット publish */
  public boolean publishDummyPacket(boolean isTalking) {
    MyMqttMessage message = new MyMqttMessage();
    String dummy;
    if (isTalking) {
      dummy = String.format("%0300d", 0);
    } else {
      dummy = String.format("%064d", 0);
    }
    message.setCmd(MQTT_TOPIC_CMD_DUMMY);
    message.setDummy(dummy);
    String messageStr = GsonUtils.toJson(message);
    return publish(MQTT_TOPIC_DUMMY_HEADER, messageStr);
  }

  /** 8.3.ステータス変更要求 publish push.ver send when you push PTT button */
  public boolean requestChangeStatusWhenPushed(@NonNull String type,
                                               @NonNull String callId, boolean isFirst) {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_PUSH);
    message.setCallType(type);
    message.setCallId(callId);
    message.setFlag(isFirst ? "0" : "1");
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /** 8.3.ステータス変更要求を定期的に送信する */
  public void requestPushNotification(@NonNull String type, @NonNull String number,
                                      CallService callService) {
    requestChangeStatusWhenPushed(type, number, true);
    startPushNotification(type, number, MQTT_PUSH_NOTIFICATION_PERIOD, callService);
  }

  /** 8.3.ステータス変更要求 publish release.ver send when you release PTT button */
  public boolean requestChangeStatusWhenReleased(@NonNull String type, @NonNull String callId) {
    stopPushNotification();
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_RELEASE);
    message.setCallType(type);
    message.setCallId(callId);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 8.5.端末情報通知 */
  public boolean notifyTerminalInfo(@NonNull DeviceStatusMessage message) {
    message.setCmd(MQTT_TOPIC_CMD_INFO);
    message.setFrom(myTerminalId);
    String messageStr = new Gson().toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  /* 8.10.通話中リスト要求 publish */
  public boolean requestCallList() {
    MyMqttMessage message = new MyMqttMessage();
    message.setCmd(MQTT_TOPIC_CMD_CALLLIST);
    message.setFrom(myTerminalId);
    String messageStr = GsonUtils.toJson(message);
    return publish(topicToPublishToControl, messageStr);
  }

  public interface MyMqttMessageListener {
    void onReceive(String topic, MyMqttMessage message);
  }

  public interface OnReceiveMqttMessageListener {
    void onIncomingCall(
        CallType callType, String sender, String receiver, MyMqttMessage mqttMessage);

    void onClosingCall(MyMqttMessage mqttMessage);

    void onUpdateServerInfo();

    void onReceiveMessage(String receiver, MyMqttMessage mqttMessage);

    void onReceiveCallStatus(MyMqttMessage mqttMessage);

    void onReceiveCallList(CallType callType, String callId, String callFrom, String[] callList, int priority, String status);
  }

  public static synchronized void destroy() {
    if (instance == null) {
      return;
    }
    instance.mqttDummyPublisher.destroy();
    instance.stopPushNotification();
    instance.disconnect();
    instance.close();
    instance = null;
  }
}
