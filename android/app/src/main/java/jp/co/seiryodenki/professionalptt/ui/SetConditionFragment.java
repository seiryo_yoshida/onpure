package jp.co.seiryodenki.professionalptt.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import java.util.Set;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;

public class SetConditionFragment extends Fragment {

  @Nullable
  @Override
  public View onCreateView(
      LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    final Context ctx = inflater.getContext();

    View view = inflater.inflate(R.layout.fragment_condition_settings, container, false);
    RadioGroup group = view.findViewById(R.id.radio_status);

    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
    Set<String> set = TerminalInfo.getTerminalStatusSet(ctx, pref);
    String current = TerminalInfo.getTerminalStatus(ctx, pref);

    for (String item : set) {
      RadioButton r = (RadioButton) inflater.inflate(R.layout.item_radio, null);
      r.setText(item);
      //r.setButtonDrawable(R.drawable.btn_radiobt);
      //r.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.textsize_32));
      group.addView(r);
      if (item.equals(current)) {
        group.check(r.getId());
      }
    }

    //ラジオボタンのチェックが変更されたら、値をセットする
    group.setOnCheckedChangeListener(new OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        for (int i=0; i<group.getChildCount(); i++) {
          RadioButton r = (RadioButton) group.getChildAt(i);
          if (r.isChecked()) {
            TerminalInfo.setTerminalStatus(ctx, pref, r.getText().toString());
            getFragmentManager().popBackStack();
            break;
          }
        }
      }
    });

    /*
    Button btnOk = view.findViewById(R.id.btn_submit_ok);
    btnOk.setOnClickListener(v -> {
      for (int i=0; i<group.getChildCount(); i++) {
        RadioButton r = (RadioButton) group.getChildAt(i);
        if (r.isChecked()) {
          TerminalInfo.setTerminalStatus(ctx, pref, r.getText().toString());
          break;
        }
      }
      getFragmentManager().popBackStack();
    });

    Button btnCancel = view.findViewById(R.id.btn_submit_cancel);
    btnCancel.setOnClickListener(v -> {
      getFragmentManager().popBackStack();
    });
    */

    return view;
  }

}
