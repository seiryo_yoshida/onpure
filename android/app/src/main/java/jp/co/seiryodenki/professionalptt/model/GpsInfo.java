package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class GpsInfo {

  @SerializedName("use")
  private int use;

  // 2019/01/22 SEC
  // トランシーバー位置情報通知制限対応
  @SerializedName("limit")
  private int limit;

  @SerializedName("type")
  private int type;

  @SerializedName("mon")
  private int mon;

  @SerializedName("tue")
  private int tue;

  @SerializedName("wed")
  private int wed;

  @SerializedName("thu")
  private int thu;

  @SerializedName("fri")
  private int fri;

  @SerializedName("sat")
  private int sat;

  @SerializedName("sun")
  private int sun;

  @SerializedName("start")
  private String start;

  @SerializedName("end")
  private String end;
  // トランシーバー位置情報通知制限対応

  @SerializedName("dist")
  private int dist;

  @SerializedName("time")
  private int time;

  public GpsInfo(int use, int limit, int type, int mon, int tue, int wed, int thu, int fri, int sat, int sun, String start, String end, int dist, int time) {
    this.use   = use;
    this.limit = limit;
    this.type  = type;
    this.mon   = mon;
    this.tue   = tue;
    this.wed   = wed;
    this.thu   = thu;
    this.fri   = fri;
    this.sat   = sat;
    this.sun   = sun;
    this.start = start;
    this.end   = end;
    this.dist  = dist;
    this.time  = time;
  }

  public int getUse() {
    return use;
  }

  public void setUse(int use) {
    this.use = use;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public int getMon() {
    return mon;
  }

  public void setMon(int mon) {
    this.mon = mon;
  }

  public int getTue() {
    return tue;
  }

  public void setTue(int tue) {
    this.tue = tue;
  }

  public int getWed() {
    return wed;
  }

  public void setWed(int wed) {
    this.wed = wed;
  }

  public int getThu() {
    return thu;
  }

  public void setThu(int thu) {
    this.thu = thu;
  }

  public int getFri() {
    return thu;
  }

  public void setFri(int fri) {
    this.fri = fri;
  }

  public int getSat() {
    return sat;
  }

  public void setSat(int sat) {
    this.sat = sat;
  }

  public int getSun() {
    return sun;
  }

  public void setSun(int sun) {
    this.sat = sun;
  }

  public String getStart() {
    return start;
  }

  public void setStart(String start) {
    this.start = start;
  }

  public String getEnd() {
    return end;
  }

  public void setEnd(String end) {
    this.end = end;
  }

  public int getDist() {
    return dist;
  }

  public void setDist(int dist) {
    this.dist = dist;
  }

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }
}
