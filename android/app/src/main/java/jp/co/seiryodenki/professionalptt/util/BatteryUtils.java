package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.annotation.NonNull;

public class BatteryUtils {

  private static BatteryUtils instance;
  private BatteryInfo batteryInfo;

  public static class BatteryInfo {

    private int rest; // BatteryManager.EXTRA_LEVEL
    private float temp; // BatteryManager.EXTRA_TEMPERATURE
    private String status; // BatteryManager.EXTRA_STATUS

    public void setBatteryStatus(@NonNull Intent batteryStatus) {
      status = "FAILED";

      // level
      rest = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

      // temperature
      temp = batteryStatus.getIntExtra(
              BatteryManager.EXTRA_TEMPERATURE, 0) / 10f;

      // battery status
      int bst = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
      if (bst == BatteryManager.BATTERY_STATUS_CHARGING) {
        status = "CHARGING";
      } else if (bst == BatteryManager.BATTERY_STATUS_DISCHARGING) {
        status = "DISCHARGING";
      } else if (bst == BatteryManager.BATTERY_STATUS_FULL) {
        status = "FULL";
      } else if (bst == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
        status = "NOT CHARGING";
      } else if (bst == BatteryManager.BATTERY_STATUS_UNKNOWN) {
        status = "UNKNOWN";
      }
    }

    public int getRest() {
      return rest;
    }

    public float getTemp() {
      return temp;
    }

    public String getStatus() {
      return status;
    }
  }

  public static BatteryInfo getBatteryInfo(@NonNull final Context ctx) {
    if (instance == null) {
      instance = new BatteryUtils();
    }
    Intent intent = ctx.registerReceiver(null,
            new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    if (intent != null) {
      instance.batteryInfo = new BatteryInfo();
      instance.batteryInfo.setBatteryStatus(intent);
    }
    return instance.batteryInfo;
  }

  public static void destroy() {
    if (instance == null) {
      return;
    }
    instance = null;
  }
}
