package jp.co.seiryodenki.professionalptt.core;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_FORMAT;

/**
 * MqttManager内でのみ使われることを想定
 */
public class MqttSubscribeManager {
  private static final String TAG = MqttSubscribeManager.class.getSimpleName();
  private List<String> defaultSubscribeList;
  private List<String> subscribeList;
  private String companyId;

  MqttSubscribeManager(String companyId) {
    this.companyId = companyId;
    defaultSubscribeList = new ArrayList<String>();
    subscribeList = new ArrayList<String>();
  }

  public synchronized void clear() {
    defaultSubscribeList.clear();
    subscribeList.clear();
  }

  /**
   * ログイン時のmqtt_infoでsubscribeしたときはこちらで追加
   * @param topic
   */
  public synchronized void addDefaultSubscribeInfo(@Nonnull String topic) {
    defaultSubscribeList.add(topic);
  }

  /**
   * 通話開始したときにsubscribeしたときはこちらを追加する
   * @param callId
   */
  public synchronized boolean addSubscribeInfo(@Nonnull String callId) {
    if (isSubscribed(callId)) {
      return false;
    }
    subscribeList.add(getTopicFromCallId(companyId, callId));
    return true;
  }

  /**
   * 通話終了時(status:"BEY"や"OUT"のとき)に実行
   * subscribe不要になったトピックがあればunsubscribeする
   * @param callId
   * @return subscribe不要判定結果
   */
  public synchronized boolean needsToUnsubscribe(@Nonnull String callId) {
    boolean needsToUnsubscribe = false;
    String topic = getTopicFromCallId(companyId, callId);
    if (defaultSubscribeList.contains(topic)) {
      return false;
    }
    if (subscribeList.contains(topic)) {
      needsToUnsubscribe = true;
      subscribeList.remove(topic);
    }
    return needsToUnsubscribe;
  }

  private synchronized boolean isSubscribed(@Nonnull String callId) {
    if (defaultSubscribeList.contains(getTopicFromCallId(companyId, callId))) {
      return true;
    }
    if (subscribeList.contains(getTopicFromCallId(companyId, callId))) {
      return true;
    }
    return false;
  }

  public static String getCallIdFromTopic(@Nonnull String topic) {
    String[] topicLevel = topic.split("/");
    if (topicLevel == null || topicLevel.length != MqttNotifier.TOPIC_LENGTH) {
      return null;
    }
    return topicLevel[MqttNotifier.IDX_RECEIVER];
  }

  public static String getTopicFromCallId(@Nonnull String companyId, @Nonnull String callId) {
    return String.format(MQTT_TOPIC_FORMAT, companyId, callId);
  }
}
