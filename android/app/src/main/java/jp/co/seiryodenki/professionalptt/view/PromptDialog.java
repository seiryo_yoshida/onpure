package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;

import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.ui.AddressListFragment.OnSelectPromptListener;
import jp.co.seiryodenki.professionalptt.model.CallType;

public class PromptDialog extends AppCompatDialog {

  private final OnSelectPromptListener onSelectPromptListener;

  /**
   * コンストラクタ.
   *
   * @param context コンテキスト
   */
  public PromptDialog(final Context context, final OnSelectPromptListener onSelectPromptListener) {
    super(context);
    this.onSelectPromptListener = onSelectPromptListener;
  }

  /**
   * ダイアログ表示.
   */
  public final void showDialog(
    final CallType callType, final CallType enforcementCallType) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder
      .setNegativeButton(
        R.string.dialog_call_enforcement_mode_cancel,
        (dialog, which) -> {
          dismiss();
        })
      .setNeutralButton(
        R.string.dialog_call_enforcement_mode_normal,
        (dialog, which) -> {
          if (onSelectPromptListener != null) {
            onSelectPromptListener.onSelectPrompt(callType);
          }
          dismiss();
        })
      .setPositiveButton(
        R.string.dialog_call_enforcement_mode_enforcement,
        (dialog, which) -> {

          if (onSelectPromptListener != null) {
            onSelectPromptListener.onSelectPrompt(enforcementCallType);
          }
          dismiss();
        })
      .setTitle(R.string.dialog_call_enforcement_mode_title)
      .setMessage(R.string.dialog_call_enforcement_mode_message)
      .create();
    builder.show();
  }
}
