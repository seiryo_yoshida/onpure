package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import jp.co.seiryodenki.professionalptt.util.logger;

class CallStateManager implements SensorEventListener {
  private static final String TAG = CallStateManager.class.getSimpleName();

  private PowerManager powerManager;
  private Sensor proximitySensor;
  private SensorManager sensorManager;
  private TelephonyManager telephonyManager;
  private WakeLock incallWakeLock;
  private WakeLock proximityWakelock;
  private boolean proximitySensingEnabled;

  /**
   * 通信状態を管理する。
   *
   * @param c コンテキスト
   */
  CallStateManager(@NonNull Context c) {
    telephonyManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
    powerManager = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
    sensorManager = (SensorManager) c.getSystemService(Context.SENSOR_SERVICE);
    if (sensorManager != null) {
      proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }
    proximityWakelock =
        powerManager.newWakeLock(
            PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "manager_proximity_sensor");
  }

  void updateCallState(@NonNull LinphoneCore lc, @NonNull LinphoneCall call, @NonNull State state) {
    if (state == State.IncomingReceived) {
      onIncomingReceived(lc, call);
    } else if (state == State.Connected) {
      onConnected(lc, call);
    } else if (state == State.CallEnd) {
      onCallEnd(lc);
    } else if (state == State.Error) {
      onError(lc);
    } else if (state == State.CallUpdatedByRemote) {
      onCallUpdatedByRemote();
    } else if (state == State.OutgoingInit) {
      onOutgoingInit();
    } else if (state == State.StreamsRunning) {
      onStreamsRunning();
    }
  }

  boolean getTelephonyState(){
    boolean result = false;
    if(telephonyManager.getCallState() == telephonyManager.CALL_STATE_OFFHOOK){
      result = true;
    }
    return result;
  }

  private void onIncomingReceived(@NonNull LinphoneCore lc, @NonNull LinphoneCall call) {
    if (!call.equals(lc.getCurrentCall()) && call.getReplacedCall() != null) {
      return;
    }
    try {
      lc.acceptCall(call);
    } catch (LinphoneCoreException e) {
      logger.e(TAG, "accept call", e);
    }
  }

  private void onConnected(@NonNull LinphoneCore lc, @NonNull LinphoneCall call) {
    if (lc.getCallsNb() == 1) {
      enableProximitySensing(true);
      if (call.getDirection() == CallDirection.Incoming) {
        HeadsetManager headsetManager = HeadsetManager.getInstance();
        headsetManager.setModeCommunication();
        headsetManager.requestFocusStreamVoiceCall();
      }
    }
  }

  private void onCallEnd(@NonNull LinphoneCore lc) {
    if (lc.getCallsNb() != 0) {
      return;
    }
    enableProximitySensing(false);
    HeadsetManager headsetManager = HeadsetManager.getInstance();
    headsetManager.abandonAudioFocus();
    if (telephonyManager != null
        && telephonyManager.getCallState() == TelephonyManager.CALL_STATE_IDLE) {
      // headsetManager.setModeNormal();
      headsetManager.disableBluetoothSCO();
      headsetManager.routeAudioToReceiver();
    }
    if (incallWakeLock != null && incallWakeLock.isHeld()) {
      incallWakeLock.release();
    }
  }

  private void onError(@NonNull LinphoneCore lc) {
    onCallEnd(lc);
  }

  private void onCallUpdatedByRemote() {
  }

  private void onOutgoingInit() {
    HeadsetManager headsetManager = HeadsetManager.getInstance();
    headsetManager.setModeCommunication();
    headsetManager.requestFocusStreamVoiceCall();
    headsetManager.routeAudio();
    enableProximitySensing(true);
  }

  private void onStreamsRunning() {
    HeadsetManager headsetManager = HeadsetManager.getInstance();
    headsetManager.setModeCommunication();
    headsetManager.routeAudio();
    if (incallWakeLock == null) {
      incallWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "incall");
    }
    if (!incallWakeLock.isHeld()) {
      incallWakeLock.acquire();
    } 
  }

  private void enableProximitySensing(boolean enable) {
    if (sensorManager == null || proximitySensor == null) {
      logger.e(TAG, "SensorManager/ProximitySensor is null.");
      return;
    }
    if (enable && !proximitySensingEnabled) {
      sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
      proximitySensingEnabled = true;
    } else if (!enable && proximitySensingEnabled) {
      sensorManager.unregisterListener(this);
      proximitySensingEnabled = false;
      if (proximityWakelock.isHeld()) {
        proximityWakelock.release();
      }
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

  @Override
  public void onSensorChanged(SensorEvent event) {
  }
}
