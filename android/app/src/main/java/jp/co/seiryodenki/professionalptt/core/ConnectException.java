package jp.co.seiryodenki.professionalptt.core;

public class ConnectException extends PttException {

  public ConnectException() {}

  public ConnectException(String message) {
    super(message);
  }

  public ConnectException(Throwable throwable) {
    super(throwable);
  }

  public ConnectException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
