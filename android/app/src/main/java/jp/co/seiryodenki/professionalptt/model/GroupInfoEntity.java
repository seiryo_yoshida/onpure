package jp.co.seiryodenki.professionalptt.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class GroupInfoEntity extends RealmObject {

  @PrimaryKey private String grp_group_id;

  @Index private String grp_group_name;

  private boolean isSelected = false;

  public GroupInfoEntity(String grp_group_id, String grp_group_name) {
    this.grp_group_id = grp_group_id;
    this.grp_group_name = grp_group_name;
  }

  public GroupInfoEntity() {}

  public String getGroupId() {
    return grp_group_id;
  }

  public void setGroupId(String grp_group_id) {
    this.grp_group_id = grp_group_id;
  }

  public String getGroupName() {
    return grp_group_name;
  }

  public void setGroupName(String grp_group_name) {
    this.grp_group_name = grp_group_name;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
