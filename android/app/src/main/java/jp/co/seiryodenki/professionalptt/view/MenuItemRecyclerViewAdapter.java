package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
//import android.graphics.Color;

import java.util.List;

import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.ui.MenuFragment.MenuItem;
import jp.co.seiryodenki.professionalptt.ui.MenuFragment.OnClickMenuListener;

public class MenuItemRecyclerViewAdapter
    extends RecyclerView.Adapter<MenuItemRecyclerViewAdapter.ViewHolder> {

  private Context context;
  private final List<MenuItem> values;
  private final OnClickMenuListener listener;

  public MenuItemRecyclerViewAdapter(List<MenuItem> items, OnClickMenuListener listener) {
    values = items;
    this.listener = listener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    context = parent.getContext();
    View view = LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bindView(values.get(position));
  }

  @Override
  public int getItemCount() {
    return values.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final View view;
    public final LinearLayout layoutView;
    public final ImageView iconView;
    public final TextView contentView;
    //public final TextView subtitle;
    public MenuItem item;

    public ViewHolder(View view) {
      super(view);
      this.view = view;
      this.layoutView = view.findViewById(R.id.menulayout);
      this.iconView = view.findViewById(R.id.menuimage);
      this.contentView = view.findViewById(R.id.menutext);
      //iconView = view.findViewById(R.id.image_icon);
      //contentView = view.findViewById(R.id.content);
      //subtitle = view.findViewById(R.id.menu_subtitle);
    }

    public void bindView(MenuItem menuItem) {
      item = menuItem;
      //if (menuItem.isTitle) {
        //iconView.setVisibility(View.GONE);
        //contentView.setVisibility(View.GONE);
        //subtitle.setText(menuItem.title);
        //subtitle.setVisibility(View.VISIBLE);
        //subtitle.setBackgroundColor(Color.rgb(52, 152, 219));
        //return;
      //} else {
        //iconView.setVisibility(View.VISIBLE);
        //contentView.setVisibility(View.VISIBLE);
        //subtitle.setVisibility(View.GONE);
        contentView.setText(menuItem.title);
        view.setOnTouchListener(
                (view, event) -> {
                  CallService.broadcastTouchEvent(context, event);
                  return false;
                });
        //if (menuItem.packageName != null) {
        if (menuItem.isPackage) {
          //iconView.setImageDrawable(menuItem.icon);
          //contentView.setBackground(menuItem.icon);
          layoutView.setBackgroundResource(R.drawable.bk_selector_backcolor);
          iconView.setBackground(menuItem.icon);
          view.setOnClickListener(v -> {
            if (context != null) {
              try {
                context.startActivity(
                        context.getPackageManager().getLaunchIntentForPackage(menuItem.packageName));
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          });
        } else {
          //iconView.setImageResource(menuItem.idRes);
          //contentView.setBackgroundResource(menuItem.idRes);
          layoutView.setBackgroundResource(menuItem.idRes);
          iconView.setBackground(null);
          view.setOnClickListener(v -> {
            if (null != listener) {
              listener.onClickMenu(menuItem);
            }
          });
        }
      //}
    }

    @Override
    public String toString() {
      return super.toString() + " '" + contentView.getText() + "'";
    }
  }
}
