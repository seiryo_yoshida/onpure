package jp.co.seiryodenki.professionalptt;

public class Constants {
  /* 位置情報で許容する最大精度(m) */
  public static final float MAX_ACCURACY = 500.0f;

  public static final int REALM_SCHEMA_VERSION = 1;
  public static final String DB_KEY_CALL_ID_USER = "trm_terminal_id";
  public static final String DB_KEY_CALL_ID_GROUP = "grp_group_id";
  public static final String DB_KEY_CALL_ID_AREA = "area_name";

  /* 通話中画面の更新レート（ミリ秒） */
  public static final int REFRESH_RATE_MSEC_ON_CALL = 100;

  /* 位置情報を含む端末情報更新レート（分） */
  public static final int DEVICE_STATUS_INTERVAL_RATE_MIN = 1;

  /* リモートモニター：サーバ通知レベル×ベース値 */
  public static final int NOISE_LEVEL_BASE = 100;
  /* リモートモニター：通知最低間隔（秒） */
  public static final int NOISE_EXCEED_MIN_PERIOD = 60;

  public static final int NETWORK_TIMEOUT_SEC = 10;

  public static final String CALL_PTTALL_ID = "10000";
  public static final String CALL_EMERGENCY_ID = "30000";
  public static final String CALL_REMOTE_MONITOR_ID = "40000";

  public static final int MQTT_KEEP_ALIVE_PERIOD = 120000;
  public static final int MQTT_TIMEOUT_MILLIS = 1500;
  public static final int MQTT_PUSH_NOTIFICATION_PERIOD = 1000;
  public static final String MQTT_TOPIC_FORMAT = "secptt/%s/CMD/+/%s";
  public static final String MQTT_TOPIC_FORMAT_PTTALL = "secptt/%s/MSG/+/PTTALL";
  public static final String MQTT_TOPIC_HEADER = "secptt/";
  public static final String MQTT_TOPIC_DUMMY_HEADER = "secpttdummy/";
  public static final String MQTT_TOPIC_CMD = "/CMD/";
  public static final String MQTT_TOPIC_MSG = "/MSG/";
  public static final String MQTT_TOPIC_PTTALL = "PTTALL";
  public static final String MQTT_TOPIC_SL_PTTALL = "/PTTALL";
  public static final String MQTT_TOPIC_CONTROL = "CONTROL";
  public static final String MQTT_TOPIC_SL_CONTROL = "/CONTROL";
  public static final String MQTT_TOPIC_CMD_SETTING = "SETTING";
  public static final String MQTT_TOPIC_CMD_DUMMY = "DUMMY";
  public static final String MQTT_TOPIC_CMD_PUSH = "PUSH";
  public static final String MQTT_TOPIC_CMD_RELEASE = "RELEASE";
  public static final String MQTT_TOPIC_CMD_APPEND_BYE = "-BYE";
  public static final String MQTT_TOPIC_CMD_PARK = "PARK";
  public static final String MQTT_TOPIC_CMD_SEND_MSG = "SEND_MSG";
  public static final String MQTT_TOPIC_CMD_RESTRICT = "RESTRICT";
  public static final String MQTT_TOPIC_CMD_STATUS = "STATUS";
  public static final String MQTT_TOPIC_CMD_INFO = "INFO";
  public static final String MQTT_TOPIC_CMD_CALLLIST = "CALLLIST";
  public static final String MQTT_TOPIC_CMD_CALL = "CALL";

  public static final String MQTT_STATUS_PARKING = "F";
  public static final String MQTT_STATUS_NOCALL = "0";

  public enum MqttMessageType {
    MSG,
    MAN_ST,
    MAN_ED,
    SAF_ST,
    SAF_ED,
    ETC,
    ACT_ST,
    ACT_ED
  }

  public enum MQTTSTATUS {
    START,
    BUSY,
    OUT,
    BYE,
    PARK
  }

  public enum CALLSTATUS {
    IDLE,
    INCOMING,
    OUTGOING,
    INCOMING_CALL,
    OUTGOING_CALL,
    TALKING,
    PARKING,
    BUSY
  }

  public static final String BROADCAST_PTT_DOWN = "android.intent.action.PTT.down";
  public static final String BROADCAST_CALL_STATUS =
          BuildConfig.APPLICATION_ID + ".service.action.BROADCAST_CALL_STATUS";
  public static final String BROADCAST_CALL_IN_PROGRESS =
          BuildConfig.APPLICATION_ID + ".service.action.BROADCAST_CALL_IN_PROGRESS";
  public static final String BROADCAST_PTT_STATUS =
          BuildConfig.APPLICATION_ID + ".service.action.BROADCAST_PTT_STATUS";
  public static final String BROADCAST_NETWORK_ONOFF =
          BuildConfig.APPLICATION_ID + ".service.action.BROADCAST_NETWORK_ONOFF";
  public static final String INTENT_CALL_ID = "intent_call_id";
  public static final String INTENT_CALL_IDS = "intent_call_ids";
  public static final String INTENT_CALL_MIC_MUTED = "intent_call_mic_muted";
  public static final String INTENT_CALL_NAME = "intent_call_name";
  public static final String INTENT_CALL_PERIOD = "intent_call_period";
  public static final String INTENT_CALL_STATUS = "intent_call_status";
  public static final String INTENT_CALL_TYPE_ID = "intent_call_type_id";
  public static final String INTENT_PTT_STATUS = "intent_ptt_status";

  public static final String ACTION_LOGOUT = BuildConfig.APPLICATION_ID + ".ACTION_LOGOUT";

  public static final int WAKELOCK_RELEASE = 5000;

  public static final int KEY_ARROW_UP = 19;
  public static final int KEY_ARROW_DOWN = 20;
  public static final int KEY_ARROW_LEFT = 21;
  public static final int KEY_ARROW_RIGHT = 22;
  public static final int KEY_PTT_CAT = 27;
  public static final int KEY_HEADSETHOOK = 79;
  public static final int KEY_PTT = 266;
  public static final int KEY_PTT_KYOCERA = 285;
  public static final int KEY_PTT_ATOM = 286;
  public static final int KEY_SOS = 267;
  public static final int KEY_FUNCTION = 269;

  public static final int DISABLE_PRIORITY_CALL = 0;
  public static final int ENABLE_PRIORITY_CALL_MANUAL = 1;
  public static final int ENABLE_PRIORITY_CALL = 2;

  public static final int SETTINGS_ENABLE = 1;
  public static final int SETTINGS_DISABLE = 0;
  public static final int EMCSET_USE_ENABLE = 1;
  public static final int EMCSET_USE_DISABLE = 0;

  public static final float SETTINGS_SOUND_SPEAKER_VOLUME_MUTE = -8.0f;
  public static final float SETTINGS_SOUND_SPEAKER_VOLUME_SMALL = 4.0f;
  public static final float SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL = 8.0f;
  public static final float SETTINGS_SOUND_SPEAKER_VOLUME_LARGE = 16.0f;
  public static final float SETTINGS_SOUND_MIC_VOLUME_SMALL = -6.0f;
  public static final float SETTINGS_SOUND_MIC_VOLUME_NORMAL = -2.0f;
  public static final float SETTINGS_SOUND_MIC_VOLUME_LARGE = 4.0f;
  public static final int SETTINGS_SOUND_JITTER_BUFFER_SMALL = 400;
  public static final int SETTINGS_SOUND_JITTER_BUFFER_NORMAL = 700;
  public static final int SETTINGS_SOUND_JITTER_BUFFER_LARGE = 1000;
}
