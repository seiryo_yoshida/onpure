package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class MandownSet {

  @SerializedName("use")
  private int use;

  @SerializedName("angle")
  private String angle;

  @SerializedName("time")
  private String time;

  @SerializedName("mesgto")
  private String mesgto;

  public MandownSet(int use, String angle, String time, String mesgto) {
    this.use = use;
    this.angle = angle;
    this.time = time;
    this.mesgto = mesgto;
  }

  public int getUse() {
    return use;
  }

  public void setUse(int use) {
    this.use = use;
  }

  public String getAngle() {
    return angle;
  }

  public void setAngle(String angle) {
    this.angle = angle;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getMesgto() {
    return mesgto;
  }

  public void setMesgto(String mesgto) {
    this.mesgto = mesgto;
  }
}
