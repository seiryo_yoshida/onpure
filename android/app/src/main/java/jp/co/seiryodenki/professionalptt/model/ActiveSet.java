package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class ActiveSet {
  @SerializedName("use")
  private int use;

  @SerializedName("mesgto")
  private String mesgto;

  @SerializedName("size")
  private String size;

  @SerializedName("lat")
  private String latitude;

  @SerializedName("long")
  private String longitude;

  public ActiveSet(int use, String mesgto, String size, String latitude, String longitude) {
    this.use = use;
    this.mesgto = mesgto;
    this.size = size;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public int getUse() {
    return use;
  }

  public void setUse(int use) {
    this.use = use;
  }

  public String getMesgto() {
    return mesgto;
  }

  public void setMesgto(String mesgto) {
    this.mesgto = mesgto;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
}
