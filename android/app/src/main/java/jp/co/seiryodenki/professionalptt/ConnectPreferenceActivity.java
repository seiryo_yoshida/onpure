package jp.co.seiryodenki.professionalptt;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.util.Util;

/** 接続設定保存制御クラス */
public class ConnectPreferenceActivity extends PreferenceActivity {

    private static final String TAG = ConnectPreferenceActivity.class.getSimpleName();

    // Realm DBヘルパ
    private Realm mRealm;

    // グループ情報保持
    private static String[] mGroupIds;
    private static String[] mGroupNames;

    // ユーザ操作状態保持フラグ
    private static boolean isCreateView = true;
    private static boolean isDefaultChanged = false;

    //バックボタンで終了
    private static boolean isMenuButton = true;

    //メニュー（Bold文字に変更する）
    private TextView mToolbarMenu;

    /**
     * A preference value change listener that updates the preference's summary to reflect its new
     * value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            (preference, value) -> {
                Context ctx = preference.getContext();
                if (value == null) {
                    return false;
                }
                if (preference instanceof EditTextPreference) {
                    String stringValue = value.toString();
                    preference.setSummary(stringValue);
                } else if (preference instanceof ListPreference) {
                    String stringValue = value.toString();
                    CharSequence[] labels = ((ListPreference) preference).getEntries();
                    CharSequence[] values = ((ListPreference) preference).getEntryValues();
                    int index = 0;
                    for (; index < values.length; index++) {
                        if (stringValue.equals(values[index])) {
                            break;
                        }
                    }
                    if (index >= 0 && index < labels.length) {
                        preference.setSummary(labels[index]);
                    }
                } else if (preference instanceof MultiSelectListPreference) {
                    Set<String> stringValues = getStringArrayValue(value);
                    String key = preference.getKey();
                    if (key.equals(ctx.getString(R.string.key_pref_shortcut_call_types))) {
                        String summary = "";
                        Resources resources = ctx.getResources();
                        if (stringValues != null) {
                            String[] multiValueArray = stringValues.toArray(new String[] {});
                            for (int i = 0; i < multiValueArray.length; i++) {
                                if (summary.length() > 0) {
                                    summary = summary.concat(",");
                                }
                                String name =
                                        (resources == null)
                                                ? multiValueArray[i]
                                                : Util.getCallName(resources, multiValueArray[i]);
                                if (name != null) {
                                    summary = summary.concat(name);
                                }
                            }
                        }
                        preference.setSummary(summary);
                    }
                } else if (preference instanceof RingtonePreference) {
                    String stringValue = value.toString();
                    if (TextUtils.isEmpty(stringValue)) {
                        preference.setSummary(null);
                    } else {
                        Ringtone ringtone =
                                RingtoneManager.getRingtone(preference.getContext(), Uri.parse(stringValue));
                        String summary = null;
                        if (ringtone != null) {
                            summary = ringtone.getTitle(preference.getContext());
                        }
                        preference.setSummary(summary);
                    }
                } else {
                    preference.setSummary(value.toString());
                }
                if (!isCreateView) {
                    isDefaultChanged = true;
                }
                return true;
            };

    @SuppressWarnings("unchecked")
    private static Set<String> getStringArrayValue(Object value) {
        try {
            if (value != null) {
                return (Set<String>) value;
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 設定情報画面表示処理
     *
     * Binds a preference's summary to its value. More specifically, when the preference's value is
     * changed, its summary (line of text below the preference title) is updated to reflect the value.
     * The summary is also immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        if (preference == null) {
            return;
        }
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        if (preference instanceof EditTextPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        } else if (preference instanceof ListPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), null));
        } else if (preference instanceof MultiSelectListPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                            .getStringSet(preference.getKey(), null));
        } else if (preference instanceof RingtonePreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), null));
        } else if (preference instanceof SwitchPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                            .getBoolean(preference.getKey(), true));
        }
    }

    /**
     * Activity起動時処理
     *
     * @param savedInstanceState 状態保存データ
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isDefaultChanged = false;
        mRealm = Realm.getDefaultInstance();

        RealmResults<GroupInfoEntity> groups = mRealm.where(GroupInfoEntity.class).findAll();
        if (groups != null) {
            mGroupIds = new String[groups.size()];
            mGroupNames = new String[groups.size()];
            int i = 0;
            for (GroupInfoEntity group : groups) {
                mGroupIds[i] = group.getGroupId();
                mGroupNames[i++] = group.getGroupName();
            }
        }
        setContentView(R.layout.activity_connect_preference);

        //ツールバーのタイトル部のフォント変更
        mToolbarMenu = findViewById(R.id.toolbartxt);
        mToolbarMenu.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));

      //getFragmentManager().beginTransaction().replace(
      //          android.R.id.content,
      //          new ConnectPreferenceFragment()).commit();
    }

    /** Activity終了時処理 */
    @Override
    protected void onDestroy() {
        if (mRealm != null) {
            mRealm.close();
        }
        super.onDestroy();
    }

    /** 戻るキー押下時処理 */
    @Override
    public void onBackPressed() {
        int result = RESULT_CANCELED;
        Intent intent = new Intent();
        //変更があった場合
        if (isDefaultChanged) {
            //setResult(RESULT_OK);
            result = RESULT_OK;
        }
        //戻る処理
        intent.putExtra("isMenuButton", isMenuButton);
        setResult(result, intent);
        super.onBackPressed();
    }

    /**
     * This method stops fragment injection in malicious applications. Make sure to deny any unknown
     * fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || ConnectPreferenceFragment.class.getName().equals(fragmentName);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ConnectPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_default_connect);

            //ショートカット呼び出し設定
            /*
            MultiSelectListPreference shortcutCallTypes =
                    (MultiSelectListPreference)findPreference(getString(R.string.key_pref_shortcut_call_types));
            //MultiSelectListPreference shortcutCallTypes = new MultiSelectListPreference(getActivity());
            //shortcutCallTypes.setKey(getString(R.string.key_pref_shortcut_call_types));
            //shortcutCallTypes.setTitle(R.string.pref_shortcut_connect);

            Set<String> typeSet =
                    PreferenceManager.getDefaultSharedPreferences(getActivity())
                            .getStringSet(getString(R.string.key_pref_calltype), null);
            String[] entryValues = typeSet.toArray(new String[]{});
            String[] entries = new String[entryValues.length];
            for (int i = 0; i < entryValues.length; i++) {
                entries[i] = Util.getCallName(getResources(), entryValues[i]);
            }
            shortcutCallTypes.setEntryValues(entryValues);
            shortcutCallTypes.setEntries(entries);

            //getPreferenceScreen().addPreference(shortcutCallTypes);
            bindPreferenceSummaryToValue(shortcutCallTypes);
            */


            //デフォルト呼び出し設定
            MultiSelectListPreference multiSelectListPreference =
                    (MultiSelectListPreference)findPreference(getString(R.string.key_pref_default_multi_group_ids));
            //MultiSelectListPreference multiSelectListPreference =
            //        new MultiSelectListPreference(getActivity());
            //multiSelectListPreference.setKey(getString(R.string.key_pref_default_multi_group_ids));
            //multiSelectListPreference.setTitle(R.string.pref_default_multi_group);
            if (mGroupNames != null && mGroupIds != null) {
                multiSelectListPreference.setEntries(mGroupNames);
                multiSelectListPreference.setEntryValues(mGroupIds);
            }
            //getPreferenceScreen().addPreference(multiSelectListPreference);
            bindPreferenceSummaryToValue(multiSelectListPreference);

            isCreateView = false;
        }

    }


  /**
   * ツールバーのバックボタンクリックで終了
   * @param v
   */
  public void onClickBack(View v) {
    isMenuButton = false;
    onBackPressed();
  }

  /**
   * ツールバーのメニューボタンでメニュー表示
   * @param v
   */
  public void onClickMenu(View v) {
    isMenuButton = true;
    onBackPressed();
  }

}
