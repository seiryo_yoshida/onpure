package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import jp.co.seiryodenki.professionalptt.Constants.CALLSTATUS;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfo;
import jp.co.seiryodenki.professionalptt.util.Base64Encoder;

public class StateManager {
  private static final String TAG = StateManager.class.getSimpleName();
  private static StateManager instance;

  private CALLSTATUS callStatus = CALLSTATUS.IDLE;
  private CallInfo callInfo;
  private CallInfo defaultCallInfo;

  @NonNull private final String myCompanyId;
  @NonNull private final String myTerminalId;
  private String myUserName;
  private int myPriority = 0;

  private boolean isMonitored = false;
  private boolean isParked = false; // 保留状態 待ち
  private boolean isParking = false; // 保留した 待たせている

  private StateManager(@NonNull String myCompanyId, @NonNull String myTerminalId) {
    this.myCompanyId = myCompanyId;
    this.myTerminalId = myTerminalId;
  }

  public static synchronized void create(String myCompanyId, @NonNull String myTerminalId) {
    instance = new StateManager(myCompanyId, myTerminalId);
  }

  public static void setMyUserInfo(UserInfo me) {
    if (instance != null) {
      instance.myUserName = Base64Encoder.encode(me.getUserName());
      instance.myPriority = me.getPriority();
    }
  }

  public static void setDefaultCallInfo(@NonNull CallType callType, String[] callIDs) {
    if (instance != null) {
      instance.defaultCallInfo = new CallInfo();
      instance.defaultCallInfo.setCallType(callType);
      if (callIDs != null) {
        instance.defaultCallInfo.setCallId(callIDs[0]);
        instance.defaultCallInfo.setCallList(callIDs);
      }
    }
  }

  public static CallType getDefaultCallType(Context ctx, SharedPreferences pref) {
    CallType res = null;
    if (instance != null && instance.defaultCallInfo != null) {
      res = instance.defaultCallInfo.getCallType();
    }
    if (res == null) {
      res = CallType.getType(TerminalInfo.getCallDefault(ctx, pref));
    }
    return res;
  }

  public static CallInfo getDefaultCallInfo() {
    if (instance != null) {
      if (instance.defaultCallInfo != null) {
        CallType callType = instance.defaultCallInfo.getCallType();
        String callID = instance.defaultCallInfo.getCallId();
      }
      return instance.defaultCallInfo;
    }
    return null;
  }

  public static boolean userOperationAvailable() {
    if (isMonitorMode()) {
      String id = getCallId();
      return false;
    }
    if (isEmergencyMode()) {
      return false;
    }
    return true;
  }

  public static boolean isMonitored() {
    if (instance != null) {
      return instance.isMonitored;
    }
    return false;
  }

  public boolean isMonitoring() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallType() == CallType.REMOTE_MONITOR && !isMonitored;
    }
    return false;
  }

  public static boolean isMonitorMode() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallType() == CallType.REMOTE_MONITOR;
    }
    return false;
  }

  public static boolean isEmergencyMode() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallType() == CallType.EMERGENCY_CALL;
    }
    return false;
  }

  public static synchronized boolean isParked() {
    if (instance != null) {
      return instance.isParked;
    }
    return false;
  }

  public static synchronized void setIsParking(boolean isParking) {
    if (instance != null) {
      instance.isParking = isParking;
    }
  }

  public static synchronized void setIsParked(boolean isParked) {
    if (instance != null) {
      instance.isParked = isParked;
    }
  }

  public static boolean isParkMode() {
    if (instance != null) {
      return instance.isParked || instance.isParking;
    }
    return false;
  }

  public static synchronized void deleteCallInfo() {
    if (instance != null) {
      instance.callInfo = null;
      instance.isMonitored = false;
      instance.isParked = false;
      instance.isParking = false;
    }
  }

  public static synchronized void setCallStatus(CALLSTATUS status) {
    if (instance != null) {
      instance.callStatus = status;
    }
  }

  public static CALLSTATUS getCallStatus() {
    if (instance != null) {
      return instance.callStatus;
    }
    return CALLSTATUS.BUSY;
  }

  public static synchronized void setCallInfo(CallInfo callInfo) {
    if (instance != null) {
      if (instance.myPriority > callInfo.getPriority()) {
        callInfo.setPriority(instance.myPriority);
      }
      instance.callInfo = callInfo;
      instance.isMonitored = callInfo.isMonitored(instance.myTerminalId);
    }
  }

  public static CallInfo getCallInfo() {
    if (instance != null) {
      return instance.callInfo;
    }
    return null;
  }

  public static boolean isCalling() {
    return instance != null && instance.callStatus != CALLSTATUS.IDLE;
  }

  public static CallType getCallType() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallType();
    }
    return null;
  }

  public static synchronized String getCallId() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallId();
    }
    return null;
  }

  public static synchronized String[] getCallList() {
    if (instance != null && instance.callInfo != null) {
      return instance.callInfo.getCallList();
    }
    return null;
  }

  public static synchronized String getMyCompanyId() {
    if (instance != null) {
      return instance.myCompanyId;
    }
    return null;
  }

  public static synchronized String getMyTerminalId() {
    if (instance != null) {
      return instance.myTerminalId;
    }
    return null;
  }

  public static synchronized String getMyUserName() {
    if (instance != null) {
      return instance.myUserName;
    }
    return null;
  }

  public void setMyUserName(String myUserName) {
    if (instance != null) {
      instance.myUserName = myUserName;
    }
  }

  public static synchronized int getMyPriority() {
    if (instance != null) {
      return instance.myPriority;
    }
    return 0;
  }

  public static boolean isSameCall(@NonNull CallType callType, @NonNull String callId) {
    return getCallType() != null && getCallId() != null &&
            callType == getCallType() && getCallId().equals(callId);
  }

  public static boolean isMyTerminalID(@NonNull String callFrom) {
    return callFrom.equals(getMyTerminalId());
  }
}
