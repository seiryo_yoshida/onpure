package jp.co.seiryodenki.professionalptt;

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import jp.co.seiryodenki.professionalptt.util.MyActivityLifecycleCallbacks;

/** Applicationクラス */
public class MyApplication extends Application {
  /** Activity起動時処理 */
  @Override
  public void onCreate() {
    super.onCreate();
    Realm.init(this);
    RealmConfiguration config =
        new RealmConfiguration.Builder()
            .name("seiryodenki.ptt.realm")
            .schemaVersion(Constants.REALM_SCHEMA_VERSION)
            .build();
    Realm.setDefaultConfiguration(config);
    registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
  }
}
