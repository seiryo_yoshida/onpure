package jp.co.seiryodenki.professionalptt.view;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import io.realm.RealmResults;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.RecentInfo;
import jp.co.seiryodenki.professionalptt.ui.RecentFragment;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.dbUtil;

public class RecentItemRecyclerViewAdapter
    extends RecyclerView.Adapter<RecentItemRecyclerViewAdapter.ViewHolder> {

  private static final String TAG = RecentItemRecyclerViewAdapter.class.getSimpleName();

  private final List<RecentInfo> recentList;
  private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm", Locale.US);

  private final RecentFragment.OnClickListener onClickListener;
  private Resources resources;

  public RecentItemRecyclerViewAdapter(
      RealmResults<RecentInfo> items, RecentFragment.OnClickListener onClickListener) {
    this.recentList = items;
    this.onClickListener = onClickListener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    resources = parent.getResources();
    return new ViewHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent, parent, false));
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    final RecentInfo item = recentList.get(position);
    if (item == null) return;
    holder.date.setText(formatter.format(item.getDate()));
    switch (item.getCallType()) {
    case GROUP_CALL_ENFORCEMENT:
    case GROUP_CALL:
    case MULTI_GROUP_CALL_ENFORCEMENT:
    case MULTI_GROUP_CALL:
      holder.textCallId.setText(dbUtil.getGroupName(item.getCallId()));
      break;
    case CALL:
      holder.textCallId.setText(dbUtil.getUserName(item.getCallId()));
      break;
    default:
      String[] titles = resources.getStringArray(R.array.list_call_type);
      holder.textName.setText(titles[item.getCallType().ordinal()]);
      holder.textCallId.setText("");
      break;
    }


    // 通話時間をセット
    //String strTime = "(" + item.getTimeStr() + ")";
    holder.textTime.setText("(");
    holder.textTime.append(item.getTimeStr());
    holder.textTime.append(")");

    // CallIDのアイコンを表示
    switch (item.getCallType()) {
      case CALL:
        holder.textCallType.setBackgroundResource(R.drawable.ic_personalicon);
        break;
      case GROUP_CALL:
        holder.textCallType.setBackgroundResource(R.drawable.ic_groupicon);
        break;
      case GROUP_CALL_ENFORCEMENT:
        holder.textCallType.setBackgroundResource(R.drawable.ic_forcegroupicon);
        break;
      case MULTI_GROUP_CALL:
        holder.textCallType.setBackgroundResource(R.drawable.ic_multigroupicon);
        break;
      case MULTI_GROUP_CALL_ENFORCEMENT:
        holder.textCallType.setBackgroundResource(R.drawable.ic_forcemultigroupicon);
        break;
      case SITE_ALL_CALL:
        holder.textCallType.setBackgroundResource(R.drawable.ic_regionicon);
        break;
      case NEAR_ALL_CALL:
        holder.textCallType.setBackgroundResource(R.drawable.ic_neighborhoodicon);
        break;
      case OPERATING_STATION:
        holder.textCallType.setBackgroundResource(R.drawable.ic_commandstationicon);
        break;
      case ALL_CALL_SYSTEM_WIDE:
        holder.textCallType.setBackgroundResource(R.drawable.ic_allatonceicon);
        break;
      case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
        holder.textCallType.setBackgroundResource(R.drawable.ic_forceallicon);
        break;
      case REMOTE_MONITOR:
        holder.textCallType.setBackgroundResource(R.drawable.ic_monitoricon);
        break;
      default:
        holder.textCallType.setBackgroundResource(R.drawable.ic_personalicon);
        break;
    }
    holder.view.setOnClickListener(
        view -> onClickListener.onClick(item));
  }

  @Override
  public int getItemCount() {
    return recentList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView date;
    private final TextView textCallId;
    private final TextView textName;
    private final ImageView textCallType;
    private final TextView textTime;
    // private TextView mTextPriority;
    private final View view;

    public ViewHolder(View view) {
      super(view);
      this.view = view;
      date = view.findViewById(R.id.recent_date);
      textName = view.findViewById(R.id.recent_user_name);
      textCallId = view.findViewById(R.id.recent_call_id);
      textCallType = view.findViewById(R.id.recent_call_image);
      textTime = view.findViewById(R.id.recent_time);
      // mTextPriority = view.findViewById(R.id.recent_priority);
    }
  }
}
