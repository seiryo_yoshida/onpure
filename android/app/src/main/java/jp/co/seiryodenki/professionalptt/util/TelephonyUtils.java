package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import java.util.List;

import static android.content.Context.TELEPHONY_SERVICE;

public class TelephonyUtils {
  private static final String TAG = TelephonyUtils.class.getSimpleName();

  private static TelephonyManager telephonyManager;
  private static CellIdentity cellIdentity;

  public static class CellIdentity {

    private int ci;
    private int mcc;
    private int mnc;
    private int areaCode;

    public boolean setCellIdentity(@NonNull CellInfo cellInfo) {
      if (cellInfo instanceof CellInfoLte) {
        CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
        CellIdentityLte cellIdentityLte = cellInfoLte.getCellIdentity();
        this.ci = cellIdentityLte.getCi(); // 28-bit Cell Identity, Integer.MAX_VALUE if unknown
        this.mcc =
            cellIdentityLte
                .getMcc(); // 3-digit Mobile Country Code, 0..999, Integer.MAX_VALUE if unknown
        this.mnc =
            cellIdentityLte
                .getMnc(); // 2 or 3-digit Mobile Network Code, 0..999, Integer.MAX_VALUE if unknown
        this.areaCode =
            cellIdentityLte.getTac(); // 16-bit Tracking Area Code, Integer.MAX_VALUE if unknown
        return true;
      } else if (cellInfo instanceof CellInfoGsm) {
        CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
        CellIdentityGsm cellIdentityGsm = cellInfoGsm.getCellIdentity();
        this.ci =
            cellIdentityGsm
                .getCid(); // CID Either 16-bit GSM Cell Identity described in TS 27.007, 0..65535,
                           // Integer.MAX_VALUE if unknown
        this.mcc =
            cellIdentityGsm
                .getMcc(); // 3-digit Mobile Country Code, 0..999, Integer.MAX_VALUE if unknown
        this.mnc =
            cellIdentityGsm
                .getMnc(); // 2 or 3-digit Mobile Network Code, 0..999, Integer.MAX_VALUE if unknown
        this.areaCode =
            cellIdentityGsm
                .getLac(); // 16-bit Location Area Code, 0..65535, Integer.MAX_VALUE if unknown
        return true;
      }
      return false;
    }

    public int getCi() {
      return ci;
    }

    public int getMcc() {
      return mcc;
    }

    public int getMnc() {
      return mnc;
    }

    public int getAreaCode() {
      return areaCode;
    }
  }

  private static PhoneStateListener phoneStateListener =
      new PhoneStateListener() {
        public void onCellInfoChanged(List<CellInfo> cellInfoList) {
          if (cellInfoList == null) {
            logger.e(TAG, "[CELLINFO] CellInfo list is null.");
            return;
          }
          for (CellInfo cellInfo : cellInfoList) {
            setCellIdentity(cellInfo);
          }
        }
      };

  private static synchronized void setCellIdentity(@NonNull CellInfo cellInfo) {
    if (!cellInfo.isRegistered()) {
      return;
    }
    cellIdentity = new CellIdentity();
    if (!cellIdentity.setCellIdentity(cellInfo)) {
      cellIdentity = null;
    }
  }

  private static void init(@NonNull final Context ctx) {
    telephonyManager = (TelephonyManager) ctx.getSystemService(TELEPHONY_SERVICE);
    //落ちる対策
    /*
    if (telephonyManager != null) {
      telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CELL_INFO);
      List<CellInfo> cells = telephonyManager.getAllCellInfo();
      if (cells != null) {
        for (CellInfo cellInfo : cells) {
          setCellIdentity(cellInfo);
        }
      }
    }*/
    if (telephonyManager == null) {
      return;
    }
    try {
      telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CELL_INFO);
    } catch (SecurityException e) {
      logger.e(TAG, "TelephonyUtils.java init telephonyManager.listen");
      return;
    }
    List<CellInfo> cells;
    try {
      cells = telephonyManager.getAllCellInfo();
    } catch (SecurityException e) {
      return;
    }
    if (cells == null) {
      return;
    }
    for (CellInfo cellInfo : cells) {
      setCellIdentity(cellInfo);
    }
  }

  public static CellIdentity getCellIdentity(@NonNull final Context ctx) {
    if (telephonyManager == null) {
      init(ctx);
    }
    return cellIdentity;
  }

  public static void destroy() {
    if (telephonyManager != null) {
      telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
    }
    cellIdentity = null;
  }
}
