package jp.co.seiryodenki.professionalptt.util;

import android.media.AudioRecord;
import org.linphone.core.LinphoneCore;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_MUTE;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL;


public class TerminalUtils {
    /* ---------------------------- */
    /* ログイン状態管理フラグ */
    /* ---------------------------- */
    private static boolean loginFlag = false;

    public static void setLoginFlag(boolean flag){
        loginFlag = flag;
    }
    public static boolean getLoginFlag(){
        return loginFlag;
    }

    /* ---------------------------- */
    /* 録音有無フラグ */
    /* ---------------------------- */
    private static String recordFlag = "0";

    public static void setRecordFlag(String flag) {
      recordFlag = flag;
    }
    public static String getRecordFlag() {
      return recordFlag;
    }

    /* ---------------------------- */
    /* 近隣通話距離設定 */
    /* ---------------------------- */
    private static Double radius;

    public static void setRadius(int neighborSet) {
        if(neighborSet != 0) {
            radius = (double) (neighborSet / 1000);
        } else {
            // デフォルト　4km設定
            radius = 4.0;
        }
    }
    public static Double getRadius() { return radius; }

    /* ---------------------------- */
    /* SIPオーディオ管理 */
    /* ---------------------------- */
    private static LinphoneCore audioInstance = null;
    private static float speakerVolume = SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL;

    // SIPオーディオ管理インスタンス設定
    public static void setAudioInstance(LinphoneCore base){
        audioInstance = base;
    }
    // スピーカボリューム設定
    public static void setAudioSpkGainSetting(float gain){
        if(audioInstance != null) {
            audioInstance.setPlaybackGain(gain);
            if(gain != SETTINGS_SOUND_SPEAKER_VOLUME_MUTE) {
                speakerVolume = gain;
            }
        }
    }
    // マイクボリューム設定
    public static void setAudioMicGainSetting(float gain){
        if(audioInstance != null) {
            audioInstance.setMicrophoneGain(gain);
        }
    }
    // ジッターバッファボリューム設定
    public static void setAudioJitterSetting(int jitter){
        if(audioInstance != null) {
            audioInstance.setAudioJittcomp(jitter);
        }
    }
    // 音声通話開始時のゲインコントロール
    public  static void setMuteAudioSetting(){
        setAudioSpkGainSetting(SETTINGS_SOUND_SPEAKER_VOLUME_MUTE);
    }
    public  static void setNormalAudioSetting(){
        setAudioSpkGainSetting(speakerVolume);
    }
    /* ---------------------------- */
    /* オーディオレコード管理 */
    /* ---------------------------- */
    private static AudioRecord audioRecordInstance = null;
    // オーディオレコード管理インスタンス設定
    public static void setAudioRecordInstance(AudioRecord instance){
        audioRecordInstance = instance;
    }
    // オーディオレコード開始設定
    public static void startRecord(){
        if(audioRecordInstance != null){
            audioRecordInstance.startRecording();
        }
    }
    // オーディオレコード停止設定
    public static void stopRecord(){
        if(audioRecordInstance != null){
            audioRecordInstance.stop();
        }
    }
    // オーディオレコード管理インスタンスの解放
    public static void releaseRecord(){
        if(audioRecordInstance != null){
            audioRecordInstance.stop();
            audioRecordInstance.release();
            audioRecordInstance = null;
        }
    }

}
