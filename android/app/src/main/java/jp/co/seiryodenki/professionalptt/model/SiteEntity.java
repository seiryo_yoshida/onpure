package jp.co.seiryodenki.professionalptt.model;

import io.realm.RealmObject;

public class SiteEntity extends RealmObject {

  private String area_name;

  private String postal_code;

  private String admin;

  private String subadmin;

  private String locality;

  public SiteEntity() {
  }

  public SiteEntity(String area_name, String postal_code, String admin, String subadmin, String locality) {
    this.area_name = area_name;
    this.postal_code = postal_code;
    this.admin = admin;
    this.subadmin = subadmin;
    this.locality = locality;
  }

  public String getArea_name() {
    return area_name;
  }

  public void setArea_name(String area_name) {
    this.area_name = area_name;
  }

  public String getPostal_code() {
    return postal_code;
  }

  public void setPostal_code(String postal_code) {
    this.postal_code = postal_code;
  }

  public String getAdmin() {
    return admin;
  }

  public void setAdmin(String admin) {
    this.admin = admin;
  }

  public String getSubadmin() {
    return subadmin;
  }

  public void setSubadmin(String subadmin) {
    this.subadmin = subadmin;
  }

  public String getLocality() {
    return locality;
  }

  public void setLocality(String locality) {
    this.locality = locality;
  }
}
