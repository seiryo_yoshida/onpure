package jp.co.seiryodenki.professionalptt.util;

import android.util.Log;

public class logger {
    /* ログ出力レベル設定 */
    public static final int LOG_LEVEL = Log.ERROR;
    /* DEBUGログ */
    public  static final int DEBUG = Log.DEBUG;
    /* INFOログ */
    public  static final int INFO = Log.INFO;
    /* WARNログ */
    public  static final int WARN = Log.WARN;
    /* ERRORログ */
    public  static final int ERROR = Log.ERROR;

    /* コンストラクタ */
    protected  logger(){
    }

    /* DEBUGログ出力 */
    public  static void d(final String tag, final String msg){
        if(isLoggable(DEBUG)){
            Log.d(tag, msg);
        }
    }

    /* INFOログ出力 */
    public  static void i(final String tag, final String msg){
        if(isLoggable(INFO)){
            Log.i(tag, msg);
        }
    }

    public  static void i(final String tag, final String msg, final Throwable tr){
        if(isLoggable(INFO)){
            Log.i(tag, msg, tr);
        }
    }

    /* WARNログ出力 */
    public  static void w(final String tag, final String msg){
        if(isLoggable(WARN)){
            Log.w(tag, msg);
        }
    }

    public  static void w(final String tag, final String msg, final Throwable tr){
        if(isLoggable(WARN)){
            Log.w(tag, msg, tr);
        }
    }

    /* ERRORログ出力 */
    public  static void e(final String tag, final String msg){
        if(isLoggable(ERROR)){
            Log.e(tag, msg);
        }
    }

    public  static void e(final String tag, final String msg, final Throwable tr){
        if(isLoggable(ERROR)){
            Log.e(tag, msg, tr);
        }
    }

    /* ログ出力チェック */
    public static  boolean isLoggable(final int level){
        return level >= LOG_LEVEL;
    }
}
