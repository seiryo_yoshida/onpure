package jp.co.seiryodenki.professionalptt.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class UserInfoEntity extends RealmObject {

  @PrimaryKey private String trm_terminal_id;

  private String usr_login_id;

  @Index private String usr_name;

  private int usr_priority;

  private boolean isSelected = false;

  public UserInfoEntity(
      String trm_terminal_id, String usr_login_id, String usr_name, int usr_priority) {
    this.trm_terminal_id = trm_terminal_id;
    this.usr_login_id = usr_login_id;
    this.usr_name = usr_name;
    this.usr_priority = usr_priority;
  }

  public UserInfoEntity() {}

  public String getTerminalId() {
    return trm_terminal_id;
  }

  public void setTerminalId(String trm_terminal_id) {
    this.trm_terminal_id = trm_terminal_id;
  }

  public String getUserId() {
    return usr_login_id;
  }

  public void setUserId(String usr_login_id) {
    this.usr_login_id = usr_login_id;
  }

  public String getUserName() {
    return usr_name;
  }

  public void setUserName(String usr_name) {
    this.usr_name = usr_name;
  }

  public int getPriority() {
    return usr_priority;
  }

  public void setPriority(int usr_priority) {
    this.usr_priority = usr_priority;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
