package jp.co.seiryodenki.professionalptt.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import java.util.Calendar;

import jp.co.seiryodenki.professionalptt.R;

public class SplashDialog extends AppCompatDialog {

  private static final String TAG = "SplashDialog";

  private static final int DISMISS_SEC = 5;
  private long splashEndTime = -1;

  private LayoutInflater inflater;

  public interface OnDismissDialogListener {
    void onDismissDialog();
  }

  /**
   * コンストラクタ.
   *
   * @param context コンテキスト
   */
  public SplashDialog(final Context context) {
    super(context, R.style.FullscreenTheme);
    inflater = LayoutInflater.from(context);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    getWindow().setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT);
    getWindow().getDecorView()
            .setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
  }

  /**
   * ダイアログ表示.
   */
  public final void showDialog() {
    if (findViewById(R.id.container) == null) {
      setContentView(inflater.inflate(R.layout.dialog_splash, null));
    }

    splashEndTime = getTime(DISMISS_SEC);
    show();
  }

  private long getTime(int sec) {
    Calendar cal = Calendar.getInstance();
    if (sec > 0) {
      cal.add(Calendar.SECOND, sec);
    }
    return cal.getTimeInMillis();
  }

  public void closeDialog() {
    closeDialog(null);
  }

  public void closeDialog(OnDismissDialogListener onDismissDialogListener) {
    long delay = splashEndTime - getTime(0);
    if (delay > 0) {
      new Handler(Looper.getMainLooper()).postDelayed(() -> {
        if (onDismissDialogListener != null) {
          onDismissDialogListener.onDismissDialog();
        }
        dismiss();
      }, delay);
    } else {
      if (onDismissDialogListener != null) {
        onDismissDialogListener.onDismissDialog();
      }
      dismiss();
    }
  }
}
