package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

public class InterruptSet {

  @SerializedName("parking")
  private int parking;

  @SerializedName("use")
  private String use;

  @SerializedName("notice")
  private int notice;

  public InterruptSet(int parking, String use, int notice) {
    this.parking = parking;
    this.use = use;
    this.notice = notice;
  }

  public int getParking() {
    return parking;
  }

  public void setParking(int parking) {
    this.parking = parking;
  }

  public String getUse() {
    return use;
  }

  public void setUse(String use) {
    this.use = use;
  }

  public int getNotice() {
    return notice;
  }

  public void setNotice(int notice) {
    this.notice = notice;
  }
}
