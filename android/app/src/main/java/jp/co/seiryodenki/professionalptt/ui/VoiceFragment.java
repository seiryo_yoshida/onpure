package jp.co.seiryodenki.professionalptt.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_LARGE;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_SPEAKER_VOLUME_SMALL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_JITTER_BUFFER_LARGE;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_JITTER_BUFFER_NORMAL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_JITTER_BUFFER_SMALL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_MIC_VOLUME_LARGE;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_MIC_VOLUME_NORMAL;
import static jp.co.seiryodenki.professionalptt.Constants.SETTINGS_SOUND_MIC_VOLUME_SMALL;

public class VoiceFragment extends Fragment {

  private SharedPreferences mPref;
  private static AudioManager mAudioManager;
  private SeekBar mSeekSpeaker;

  @Nullable
  @Override
  public View onCreateView(
      LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    final Context ctx = inflater.getContext();

    mPref = PreferenceManager.getDefaultSharedPreferences(ctx);
    mAudioManager = (AudioManager) ctx.getSystemService(Context.AUDIO_SERVICE);

    View view = inflater.inflate(R.layout.fragment_sound_settings, container, false);

//    final int volMax = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
//    int vol = mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
//    mSeekSpeaker = view.findViewById(R.id.seek_speaker_volume);
//    mSeekSpeaker.setMax(volMax);
//    mSeekSpeaker.setProgress(vol);
//    mSeekSpeaker.setOnSeekBarChangeListener(
//        new SeekBar.OnSeekBarChangeListener() {
//          @Override
//          public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//            mAudioManager.setStreamVolume(
//                AudioManager.STREAM_VOICE_CALL,
//                progress,
//                AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
//          }
//
//          @Override
//          public void onStartTrackingTouch(SeekBar seekBar) {}
//
//          @Override
//          public void onStopTrackingTouch(SeekBar seekBar) {
//            mAudioManager.setStreamVolume(
//                AudioManager.STREAM_VOICE_CALL,
//                seekBar.getProgress(),
//                AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
//          }
//        });

    // スピーカーボリュームをシークバーで変更
    int speakervolume = chgSpeakerVolumeFtoI(
            mPref.getFloat(getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL));
    SeekBar seekBarSpeakerVolume = view.findViewById(R.id.seekbar_speaker);
    seekBarSpeakerVolume.setProgress(speakervolume);
    //つまみの表示をセット
    seekBarSpeakerVolume.setThumb(ctx.getDrawable(chgVolumeThumb(seekBarSpeakerVolume.getProgress())));
    //シークバー移動時の処理
    seekBarSpeakerVolume.setOnSeekBarChangeListener(
      new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        public void onStartTrackingTouch(SeekBar seekBar) {
          //つまみの表示を変更
          final Context ctx = inflater.getContext();
          seekBar.setThumb(ctx.getDrawable(chgVolumeThumb(R.drawable.btn_volumehandle)));
        }
        public void onStopTrackingTouch(SeekBar seekBar) {
          //つまみの表示を変更
          final Context ctx = inflater.getContext();
          seekBar.setThumb(ctx.getDrawable(chgVolumeThumb(seekBar.getProgress())));
          //ボリュームを変更
          float volume = chgSpeakerVolumeItoF(seekBar.getProgress());
          mPref
            .edit()
            .putFloat(getString(R.string.key_pref_speaker), volume)
            .apply();
          TerminalUtils.setAudioSpkGainSetting(volume);
        }});

    //マイクボリュームをシークバーで変更
    int micvolume = chgMicVolumeFtoI(
      mPref.getFloat(getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_NORMAL));
    SeekBar seekBarMicVolume = view.findViewById(R.id.seekbar_mic);
    seekBarMicVolume.setProgress(micvolume);
    //つまみの表示をセット
    seekBarMicVolume.setThumb(ctx.getDrawable(chgVolumeThumb(seekBarMicVolume.getProgress())));
    //シークバー移動時の処理
    seekBarMicVolume.setOnSeekBarChangeListener(
      new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
        public void onStartTrackingTouch(SeekBar seekBar) {
          //つまみの表示を変更
          final Context ctx = inflater.getContext();
          seekBar.setThumb(ctx.getDrawable(chgVolumeThumb(R.drawable.btn_volumehandle)));
        }
        public void onStopTrackingTouch(SeekBar seekBar) {
          //つまみの表示を変更
          final Context ctx = inflater.getContext();
          seekBar.setThumb(ctx.getDrawable(chgVolumeThumb(seekBar.getProgress())));
          //ボリュームを変更
          float volume = chgMicVolumeItoF(seekBar.getProgress());
          mPref
            .edit()
            .putFloat(getString(R.string.key_pref_mic_volume), volume)
            .apply();
          TerminalUtils.setAudioMicGainSetting(volume);
        }});
/*
    float speakerVolume =
            mPref.getFloat(getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL);
    RadioGroup rgSpeaker= view.findViewById(R.id.radio_speaker);
    if(speakerVolume == SETTINGS_SOUND_SPEAKER_VOLUME_SMALL){
      rgSpeaker.check(R.id.radio_speaker_small);
    } else if(speakerVolume == SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL){
      rgSpeaker.check(R.id.radio_speaker_normal);
    } else if(speakerVolume == SETTINGS_SOUND_SPEAKER_VOLUME_LARGE) {
      rgSpeaker.check(R.id.radio_speaker_large);
    }
    rgSpeaker.setOnCheckedChangeListener(this::onClickRadio);
*/

    int jitter =
        mPref.getInt(getString(R.string.key_pref_jitter), SETTINGS_SOUND_JITTER_BUFFER_NORMAL);
    RadioGroup rgJitter = view.findViewById(R.id.radio_jitter);
    switch (jitter) {
      case SETTINGS_SOUND_JITTER_BUFFER_SMALL:
        rgJitter.check(R.id.radio_jitter_small);
        break;
      case SETTINGS_SOUND_JITTER_BUFFER_NORMAL:
        rgJitter.check(R.id.radio_jitter_normal);
        break;
      case SETTINGS_SOUND_JITTER_BUFFER_LARGE:
        rgJitter.check(R.id.radio_jitter_large);
        break;
    }
    rgJitter.setOnCheckedChangeListener(this::onClickRadio);
/*
    float micVoluem =
        mPref.getFloat(getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_NORMAL);
    RadioGroup rgMic = view.findViewById(R.id.radio_mic);
    if (micVoluem == SETTINGS_SOUND_MIC_VOLUME_SMALL) {
      rgMic.check(R.id.radio_mic_small);
    } else if (micVoluem == SETTINGS_SOUND_MIC_VOLUME_NORMAL) {
      rgMic.check(R.id.radio_mic_normal);
    } else if (micVoluem == SETTINGS_SOUND_MIC_VOLUME_LARGE) {
      rgMic.check(R.id.radio_mic_large);
    }
    rgMic.setOnCheckedChangeListener(this::onClickRadio);
*/
    return view;
  }

  private void onClickRadio(View viewGroup, int checkedId) {
    switch (checkedId) {
      /*
      case R.id.radio_speaker_small:
        mPref
                .edit()
                .putFloat(getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_SMALL)
                .apply();
        TerminalUtils.setAudioSpkGainSetting(SETTINGS_SOUND_SPEAKER_VOLUME_SMALL);
        break;
      case R.id.radio_speaker_normal:
        mPref
                .edit()
                .putFloat(getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL)
                .apply();
        TerminalUtils.setAudioSpkGainSetting(SETTINGS_SOUND_SPEAKER_VOLUME_NORMAL);
        break;
      case R.id.radio_speaker_large:
        mPref
                .edit()
                .putFloat(getString(R.string.key_pref_speaker), SETTINGS_SOUND_SPEAKER_VOLUME_LARGE)
                .apply();
        TerminalUtils.setAudioSpkGainSetting(SETTINGS_SOUND_SPEAKER_VOLUME_LARGE);
        break;
      case R.id.radio_mic_small:
        mPref
            .edit()
            .putFloat(getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_SMALL)
            .apply();
        TerminalUtils.setAudioMicGainSetting(SETTINGS_SOUND_MIC_VOLUME_SMALL);
        break;
      case R.id.radio_mic_normal:
        mPref
            .edit()
            .putFloat(getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_NORMAL)
            .apply();
        TerminalUtils.setAudioMicGainSetting(SETTINGS_SOUND_MIC_VOLUME_NORMAL);
        break;
      case R.id.radio_mic_large:
        mPref
            .edit()
            .putFloat(getString(R.string.key_pref_mic_volume), SETTINGS_SOUND_MIC_VOLUME_LARGE)
            .apply();
        TerminalUtils.setAudioMicGainSetting(SETTINGS_SOUND_MIC_VOLUME_LARGE);
        break;
      */
      case R.id.radio_jitter_small:
        mPref
            .edit()
            .putInt(getString(R.string.key_pref_jitter), SETTINGS_SOUND_JITTER_BUFFER_SMALL)
            .apply();
        TerminalUtils.setAudioJitterSetting(SETTINGS_SOUND_JITTER_BUFFER_SMALL);
        break;
      case R.id.radio_jitter_normal:
        mPref
            .edit()
            .putInt(getString(R.string.key_pref_jitter), SETTINGS_SOUND_JITTER_BUFFER_NORMAL)
            .apply();
        TerminalUtils.setAudioJitterSetting(SETTINGS_SOUND_JITTER_BUFFER_NORMAL);

        break;
      case R.id.radio_jitter_large:
        mPref
            .edit()
            .putInt(getString(R.string.key_pref_jitter), SETTINGS_SOUND_JITTER_BUFFER_LARGE)
            .apply();
        TerminalUtils.setAudioJitterSetting(SETTINGS_SOUND_JITTER_BUFFER_LARGE);
        break;
    }
  }

  /**
   * スピーカーボリュームを実際の値からシークバーの値に変換
   * @param volume
   * @return
   */
  private int chgSpeakerVolumeFtoI(float volume) {
    return (int)((volume - SETTINGS_SOUND_SPEAKER_VOLUME_SMALL) / (SETTINGS_SOUND_SPEAKER_VOLUME_LARGE - SETTINGS_SOUND_SPEAKER_VOLUME_SMALL) * 100);
  }

  /**
   * スピーカーボリュームをシークバーの値からを実際の値に変換
   * @param volume
   * @return
   */
  private float chgSpeakerVolumeItoF(int volume) {
    return ((SETTINGS_SOUND_SPEAKER_VOLUME_LARGE - SETTINGS_SOUND_SPEAKER_VOLUME_SMALL) * (float)volume / 100.0f) + SETTINGS_SOUND_SPEAKER_VOLUME_SMALL;
  }

  /**
   * マイクボリュームを実際の値からシークバーの値に変換
   * @param volume
   * @return
   */
  private int chgMicVolumeFtoI(float volume) {
    return (int)((volume - SETTINGS_SOUND_MIC_VOLUME_SMALL) / (SETTINGS_SOUND_MIC_VOLUME_LARGE - SETTINGS_SOUND_MIC_VOLUME_SMALL) * 100);
  }

  /**
   * マイクボリュームをシークバーの値からを実際の値に変換
   * @param volume
   * @return
   */
  private float chgMicVolumeItoF(int volume) {
    return ((SETTINGS_SOUND_MIC_VOLUME_LARGE - SETTINGS_SOUND_MIC_VOLUME_SMALL) * (float)volume / 100.0f) + SETTINGS_SOUND_MIC_VOLUME_SMALL;
  }

  private int chgVolumeThumb(float volume) {
    if (volume <= 10.0) {
      return R.drawable.btn_volumehandle_10;
    } else if (volume <= 20.0) {
      return R.drawable.btn_volumehandle_20;
    } else if (volume <= 30.0) {
      return R.drawable.btn_volumehandle_30;
    } else if (volume <= 40.0) {
      return R.drawable.btn_volumehandle_40;
    } else if (volume <= 50.0) {
      return R.drawable.btn_volumehandle_50;
    } else if (volume <= 60.0) {
      return R.drawable.btn_volumehandle_60;
    } else if (volume <= 70.0) {
      return R.drawable.btn_volumehandle_70;
    } else if (volume <= 80.0) {
      return R.drawable.btn_volumehandle_80;
    } else if (volume <= 90.0) {
      return R.drawable.btn_volumehandle_90;
    } else {
      return R.drawable.btn_volumehandle_100;
    }
  }
}
