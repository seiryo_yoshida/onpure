package jp.co.seiryodenki.professionalptt.core;

public class PttException extends Exception {

  public PttException() {}

  public PttException(String message) {
    super(message);
  }

  public PttException(Throwable throwable) {
    super(throwable);
  }

  public PttException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
