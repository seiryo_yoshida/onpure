package jp.co.seiryodenki.professionalptt.model;


import com.google.gson.annotations.SerializedName;

import jp.co.seiryodenki.professionalptt.core.WebApiResponse;

public class AttFile extends WebApiResponse {

  @SerializedName("att_file_id")
  private String attFileId;

  public String getAttFileId() {
    return attFileId;
  }

  public void setAttFileId(String attFileId) {
    this.attFileId = attFileId;
  }
}
