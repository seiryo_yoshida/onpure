package jp.co.seiryodenki.professionalptt.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import jp.co.seiryodenki.professionalptt.BuildConfig;
import jp.co.seiryodenki.professionalptt.HomeActivity;
import jp.co.seiryodenki.professionalptt.MessageBoxActivity;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;

public class MyNotificationManager {
  private static final String TAG = MyNotificationManager.class.getSimpleName();

  public static final String ACTION_PHONE_CALL = "android.intent.action.PHONE_STATE";
  public static final String ACTION_CALL_CONFIRMATION =
          BuildConfig.APPLICATION_ID + ".ACTION_CALL_CONFIRMATION";
  private static final String INTENT_CONFIRM_CALL_ACCEPT = "intent_confirm_call_accept";
  private static final String INTENT_CONFIRM_CALL_TYPE_ID = "intent_confirm_call_type_id";
  private static final String INTENT_CONFIRM_CALL_ID = "intent_confirm_call_id";
  private static final String INTENT_CONFIRM_CALL_SENDER_ID = "intent_confirm_call_sender_id";
  private static final String INTENT_CONFIRM_CALL_PRIORITY = "intent_confirm_call_priority";

  private static final int NOTIFICATION_INCOMING = 2;
  private static final int NOTIFICATION_BUSY = 3;
  private static final int NOTIFICATION_MESSAGE = 4;
  private static final int NOTIFICATION_EMERGENCY_ALARM = 5;
  private static final int NOTIFICATION_ACTIVITY_ZONE_WARNING = 6;

  public static void showIncomingPersonalNotification(
    Context ctx, @NonNull CallInfo callInfo, String userName) {
    showNotification(
        ctx, callInfo, CallType.CALL, callInfo.getCallId(), callInfo.getCallList(), callInfo.getCallFrom(), userName, callInfo.getPriority());
  }

  public static void showIncomingGroupNotification(
      Context ctx,
      CallInfo callInfo,
      CallType callType,
      String callId,
      String[] callList,
      String callFrom,
      String groupName) {
    showNotification(ctx, callInfo, callType, callId, callList, callFrom, groupName, 0);
  }

  public static PendingIntent getPendingIntentAccept(
          Context context, CallType callType, String callId, String[] callList, int priority, String sender) {
    Intent intent = new Intent();
    intent.setAction(ACTION_CALL_CONFIRMATION);
    intent.putExtra(INTENT_CONFIRM_CALL_ACCEPT, true);
    intent.putExtra(INTENT_CONFIRM_CALL_TYPE_ID, callType.getId());
    intent.putExtra(INTENT_CONFIRM_CALL_ID, callList);
    intent.putExtra(INTENT_CONFIRM_CALL_SENDER_ID, sender);
    intent.putExtra(INTENT_CONFIRM_CALL_PRIORITY, priority);
    CallInfo callInfo = new CallInfo(callType, callId, sender, callList, priority);
    callInfo.putIntent(intent);
    return PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }

  public static PendingIntent getPendingIntentDecline(Context context, String sender) {
    Intent intent = new Intent();
    intent.setAction(ACTION_CALL_CONFIRMATION);
    intent.putExtra(INTENT_CONFIRM_CALL_ACCEPT, false);
    intent.putExtra(INTENT_CONFIRM_CALL_SENDER_ID, sender);
    return PendingIntent.getBroadcast(context, 2, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }

  private static void showNotification(
      Context ctx,
      @NonNull CallInfo callInfo,
      CallType callType,
      @NonNull String callId,
      @NonNull String[] callList,
      String callFrom,
      String userName,
      int priority) {
    String message = ctx.getString(R.string.notification_incoming_text, userName, callFrom);
    String submessage = ctx.getString(R.string.notification_incoming_subtext);

    // clear other notifications
    removeBusyNotification(ctx);

    Intent intent = HomeActivity.getCallIntent(ctx, callInfo, false);
    PendingIntent piBar =
        PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    // 2018/5/28 V0.4対応　Android8.xで通知が表示されない
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      String channelId = "notify-01";
      String channelName = "notify name";
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_INCOMING))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(message)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .addAction(
                              R.drawable.ic_phone,
                              ctx.getString(R.string.notification_incoming_yes),
                              getPendingIntentAccept(
                                      ctx, callType, callId, callList, priority, callFrom)) // #1
                      .addAction(
                              R.drawable.ic_clear,
                              ctx.getString(R.string.notification_incoming_no),
                              getPendingIntentDecline(ctx, callFrom)) // #2
                      .setContentIntent(piBar)
                      .setAutoCancel(false)
                      .setDefaults(Notification.DEFAULT_LIGHTS)
                      .setChannelId(channelId)
                      .setStyle(new NotificationCompat.InboxStyle().addLine(message).addLine(submessage));

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getRingtoneSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.createNotificationChannel(mChannel);
      notificationManager.notify(NOTIFICATION_INCOMING, builder.build());
    }
    else {
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_INCOMING))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(message)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .addAction(
                              R.drawable.ic_phone,
                              ctx.getString(R.string.notification_incoming_yes),
                              getPendingIntentAccept(
                                      ctx, callType, callId, callList, priority, callFrom)) // #1
                      .addAction(
                              R.drawable.ic_clear,
                              ctx.getString(R.string.notification_incoming_no),
                              getPendingIntentDecline(ctx, callFrom)) // #2
                      .setContentIntent(piBar)
                      .setAutoCancel(false)
                      .setDefaults(Notification.DEFAULT_LIGHTS)
                      .setStyle(new NotificationCompat.InboxStyle().addLine(message).addLine(submessage));

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getRingtoneSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(NOTIFICATION_INCOMING, builder.build());
    }
  }

  public static void notifiedBusy(Context ctx, String sender, CallType callType) {
    String errMessage = ctx.getString(R.string.notification_call_declined, sender);
    if((callType == CallType.NEAR_ALL_CALL) || (callType == null)){
      errMessage = ctx.getString(R.string.notification_call_nobady);
    }
    // 2018/5/28 V0.4対応　Android8.xで通知が表示されない
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      String channelId = "busy-01";
      String channelName = "busy name";
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_BUSY))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(errMessage)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setChannelId(channelId)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.createNotificationChannel(mChannel);
      notificationManager.notify(NOTIFICATION_BUSY, builder.build());
    }
    else {
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_BUSY))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(errMessage)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(NOTIFICATION_BUSY, builder.build());
    }
  }

  public static void notifyEmergencyAlarm(Context ctx) {

    // 2018/5/28 V0.4対応　Android8.xで通知が表示されない
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      String channelId = "emergency-01";
      String channelName = "emergency name";
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

      // 2019/02/06 SEC
      // 一人安全およびマンダウン発生時に強制終了が発生する問題に対応 -->
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_EMERGENCY_ALARM))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                      .setAutoCancel(true)
                      .setChannelId(channelId)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.createNotificationChannel(mChannel);
      notificationManager.notify(NOTIFICATION_EMERGENCY_ALARM, builder.build());

    }
    else{
      // 2019/02/06 SEC
      // 一人安全およびマンダウン発生時に強制終了が発生する問題に対応 -->
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_EMERGENCY_ALARM))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                      .setAutoCancel(true)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(NOTIFICATION_EMERGENCY_ALARM, builder.build());
    }
  }

  public static void notifiedActivityZoneWarning(Context ctx, String text) {
    // 2018/5/28 V0.4対応　Android8.xで通知が表示されない
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      String channelId = "activity-01";
      String channelName = "activity name";
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_ACTIVITY_ZONE_WARNING))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(text)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setChannelId(channelId)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }
      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.createNotificationChannel(mChannel);
      notificationManager.notify(NOTIFICATION_ACTIVITY_ZONE_WARNING, builder.build());
    } else {
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_ACTIVITY_ZONE_WARNING))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setContentText(text)
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getAlarmSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(NOTIFICATION_ACTIVITY_ZONE_WARNING, builder.build());
    }
  }

  public static void notifyReceiveMessage(Context ctx, String sender, String senderName, String message) {

    Intent intent = MessageBoxActivity.getIntent(ctx, StateManager.getMyPriority());
    PendingIntent piBar =
            PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    // 2018/5/28 V0.4対応　Android8.xで通知が表示されない
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      String channelId = "message-01";
      String channelName = "message name";
      int importance = NotificationManager.IMPORTANCE_HIGH;
      NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);

      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_MESSAGE))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setStyle(
                              new NotificationCompat.InboxStyle()
                                      .addLine(ctx.getString(R.string.notification_message, sender))
                                      .addLine(message))
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setContentIntent(piBar)
                      .setChannelId(channelId)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getRingtoneSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.createNotificationChannel(mChannel);

      notificationManager.notify(NOTIFICATION_MESSAGE, builder.build());
    } else {
      NotificationCompat.Builder builder =
              new NotificationCompat.Builder(ctx, String.valueOf(NOTIFICATION_MESSAGE))
                      .setSmallIcon(R.drawable.ic_ptt_notification)
                      .setContentTitle(ctx.getString(R.string.notification_title))
                      .setStyle(
                              new NotificationCompat.InboxStyle()
                                      .addLine(ctx.getString(R.string.notification_message, sender))
                                      .addLine(message))
                      .setPriority(NotificationCompat.PRIORITY_MAX)
                      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                      .setAutoCancel(true)
                      .setContentIntent(piBar)
                      .setDefaults(Notification.DEFAULT_LIGHTS);

      boolean isVib =
              PreferenceManager.getDefaultSharedPreferences(ctx)
                      .getBoolean(ctx.getString(R.string.key_pref_vibrate), true);
      if (isVib) {
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
      }

      Uri ringtone = getRingtoneSound(ctx);
      if (ringtone != null) {
        builder.setSound(ringtone);
      }

      NotificationManager notificationManager =
              (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.notify(NOTIFICATION_MESSAGE, builder.build());
    }
  }

  private static Uri getRingtoneSound(Context ctx) {
    String uriStr =
        PreferenceManager.getDefaultSharedPreferences(ctx)
            .getString(ctx.getString(R.string.key_pref_ringtone), null);
    Uri soundUri = null;
    Ringtone ringtone = null;
    if (uriStr != null) {
      soundUri = Uri.parse(uriStr);
      ringtone = RingtoneManager.getRingtone(ctx, soundUri);
    }
    if (ringtone != null) {
      return soundUri;
    }
    return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
  }

  private static Uri getAlarmSound(Context ctx) {
    String uriStr =
        PreferenceManager.getDefaultSharedPreferences(ctx)
            .getString(ctx.getString(R.string.key_pref_notifications), null);
    Uri soundUri = null;
    Ringtone ringtone = null;
    if (uriStr != null) {
      soundUri = Uri.parse(uriStr);
      ringtone = RingtoneManager.getRingtone(ctx, soundUri);
    }
    if (ringtone != null) {
      return soundUri;
    }
    return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
  }

  public static void removeNotification(Context ctx) {
    NotificationManager notificationManager =
        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancel(NOTIFICATION_INCOMING);
  }

  public static void removeBusyNotification(Context ctx) {
    NotificationManager notificationManager =
        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancel(NOTIFICATION_BUSY);
  }

  public static void removeWarningNotification(Context ctx) {
    NotificationManager notificationManager =
      (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancel(NOTIFICATION_ACTIVITY_ZONE_WARNING);
  }

  public static void removeMessageNotification(Context ctx) {
    NotificationManager notificationManager =
        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancel(NOTIFICATION_MESSAGE);
  }

  public static CallType getCallType(@NonNull Intent intent) {
    int callTypeId = intent.getIntExtra(INTENT_CONFIRM_CALL_TYPE_ID, -1);
    if (callTypeId < 0) {
      logger.e(TAG, "Unknown call type.");
    }
    return CallType.getType(callTypeId);
  }

  public static boolean isAccept(@NonNull Intent intent) {
    return intent.getBooleanExtra(INTENT_CONFIRM_CALL_ACCEPT, true);
  }

  public static String getSenderID(@NonNull Intent intent) {
    return intent.getStringExtra(INTENT_CONFIRM_CALL_SENDER_ID);
  }

  public static int getPriority(@NonNull Intent intent) {
    return intent.getIntExtra(INTENT_CONFIRM_CALL_PRIORITY, 0);
  }
}
