package jp.co.seiryodenki.professionalptt.core;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;

public class CallManager {
  private static final String TAG = CallManager.class.getSimpleName();
  private static CallManager instance;

  private String callId;

  private CallManager() {}

  /**
   * CallManagerシングルトンを取得する。
   * 未生成であればシングルトンを生成する。
   * @return CallManager
   */
  public static final synchronized CallManager getInstance() {
    if (instance == null) {
      instance = new CallManager();
    }
    return instance;
  }

  /**
   * CallManagerシングルトンを生成されているか判定する。
   * @return true 生成済み<br>
   * false 未生成
   */
  public static synchronized boolean isInstantiated() {
    return instance != null;
  }

  /**
   * CallManagerシングルトンを破棄する。
   */
  public static synchronized void destroy() {
    instance = null;
  }

  /**
   * 自身のSIP URIにSIPのINVITE（招待）メッセージを送信する。
   * @exception PttException 失敗
   */
  public void inviteSelf() throws PttException {
    inviteAddress(SipManager.getInstance().getIdentity());
  }

  /**
   * SIPのINVITE（招待）メッセージを送信する。
   * ユーザーIDからSIP URIを生成し、INVITEメッセージを送信する。
   * @param callId ユーザーIDのリスト
   * @exception PttException 失敗
   */
  public void invite(@Nullable String callId) throws PttException {
    // 2018/5/31 V0.4対応 画面消灯時の着信が受けれない問題に対処
    LinphoneCore linphoneCore = SipManager.getLinphoneCore();

    //219/05/16 大阪空港交通対応
    // 呼び出しを早くするために対応する
    MqttManager.getInstance().publishDummyPacket(true);
    MqttManager.getInstance().publishDummyPacket(true);
    MqttManager.getInstance().publishDummyPacket(true);

    linphoneCore.refreshRegisters();
    inviteAddress(SipManager.getInstance().getSipUri(callId));
  }

  /**
   * SIPのINVITE（招待）メッセージを送信する。
   * @param sipUri SIP URI
   * @exception PttException 失敗
   */
  public void inviteAddress(@NonNull String sipUri) throws PttException {
    try {
      TerminalUtils.stopRecord();
      LinphoneCore linphoneCore = SipManager.getLinphoneCore();
      LinphoneAddress address = LinphoneCoreFactory.instance().createLinphoneAddress(sipUri);
      LinphoneCallParams params = linphoneCore.createCallParams(null);
      params.setVideoEnabled(false);
      //      params.setAudioBandwidth(40);
      //      params.enableLowBandwidth(true);
      linphoneCore.inviteAddressWithParams(address, params);
    } catch (LinphoneCoreException e) {
      logger.e("CallManager", "inviteAddress" + sipUri, e);
      throw new PttException(e);
    }
  }

  /**
   * SIPコールを終了する。
   */
  public void terminateCall() {
    try {
      LinphoneCore linphoneCore = SipManager.getLinphoneCore();
      //if (linphoneCore.isIncall()) {
      if ((linphoneCore != null) && (linphoneCore.isIncall())) {
        linphoneCore.terminateAllCalls();
      }
      TerminalUtils.stopRecord();
    }catch (PttException e) {
      logger.e(TAG, "terminalCall", e);
    }
  }

  /**
   * SIPコール中か判定する。
   * @return true 通話中<br>
   * false 無通話
   */
  public boolean isIncall() {
    try {
      return SipManager.getLinphoneCore().isIncall();
    } catch (PttException e) {
      return false;
    }
  }
}
