package jp.co.seiryodenki.professionalptt.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import io.realm.Realm;
import io.realm.RealmResults;
import jp.co.seiryodenki.professionalptt.CallTypeListActivity;
import jp.co.seiryodenki.professionalptt.Constants.CALLSTATUS;
import jp.co.seiryodenki.professionalptt.R;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfoEntity;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.util.Util;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_CALL_IN_PROGRESS;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_GROUP;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_USER;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_IDS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_MIC_MUTED;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_PERIOD;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_TYPE_ID;
import static jp.co.seiryodenki.professionalptt.model.CallType.DEFAULT_CALL_TYPE_ID;

public class HomeFragment extends Fragment {

  private static final String TAG = HomeFragment.class.getSimpleName();

  private static final String KEY_CALL_TYPE = "key_call_type";
  private static final String KEY_CALL_LIST = "key_call_list";

  private static final int GRID_COLUMN_NUM = 3;
  //private RecyclerView recyclerView;
  private TextView viewTimer;
  private TextView viewParkingMsg;
  private TextView textTitle;
  private TextView textCallName;
  private Spinner spinnerCallList;

  private ImageButton btnPtt;
  private ImageView imgTalking;
  private ImageView imgNetwork;
  private ImageView imgCircle;
  private ImageView imgShadow;
  private ImageView btnArrowLeft;
  private ImageView btnArrowRight;
  private FrameLayout layoutArrowLeft;
  private FrameLayout layoutArrowRight;
  private Button btnMenu;

  private SharedPreferences preference;

  private CallType currentCallType;
  private String[] currentCallIDs;

  private OnClickArrowListener onClickArrowListener;
  private OnClickPttListener onClickPttListener;
  private OnClickShortcutListener onClickShortcutListener;

  //業態設定
  private String srcCondition = "";

  public interface OnClickPttListener {
    void onClickPtt(View v, MotionEvent event);
  }

  public interface OnClickShortcutListener {
    void onClickShortcut(CallType callType);
  }

  public interface OnClickArrowListener {
    void onClickArrow(int diff);
  }

  private BroadcastReceiver mBroadcastReceiver =
      new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          if (intent == null || intent.getAction() == null) {
            return;
          }
          if (BROADCAST_CALL_IN_PROGRESS.equals(intent.getAction())) {
            onReceiveCallInProgress(intent);
          }
        }

        private void onReceiveCallInProgress(Intent intent) {
          CALLSTATUS callStatus = (CALLSTATUS) intent.getSerializableExtra(INTENT_CALL_STATUS);
          int callTypeId = intent.getIntExtra(INTENT_CALL_TYPE_ID, DEFAULT_CALL_TYPE_ID);
          String[] callIDs = intent.getStringArrayExtra(INTENT_CALL_IDS);
          long timer = intent.getLongExtra(INTENT_CALL_PERIOD, 0);
          boolean micMuted = intent.getBooleanExtra(INTENT_CALL_MIC_MUTED, true);
          changeCallTimer(callStatus, CallType.getType(callTypeId), callIDs, timer, micMuted);
        }
      };

  public HomeFragment() {}

  public static HomeFragment newInstance(CallType callType, String[] callList) {
    HomeFragment fragment = new HomeFragment();
    Bundle args = new Bundle();
    args.putInt(KEY_CALL_TYPE, callType.getId());
    args.putStringArray(KEY_CALL_LIST, callList);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_home, container, false);

    preference = PreferenceManager.getDefaultSharedPreferences(getContext());

    //textCallName = view.findViewById(R.id.text_call_callname);
    viewTimer = view.findViewById(R.id.text_call_time);
    viewParkingMsg = view.findViewById(R.id.text_park_calling);
    //spinnerCallList = view.findViewById(R.id.spinner_call);
    //recyclerView = view.findViewById(R.id.recycler_list);
    imgTalking = view.findViewById(R.id.image_talking);
    imgNetwork = view.findViewById(R.id.image_network);
    imgCircle = view.findViewById(R.id.image_volume_circle);
    //imgShadow = view.findViewById(R.id.image_volume_shadow);
    btnPtt = view.findViewById(R.id.image_volume);
    btnPtt.setOnTouchListener((v, event) -> {
      if (onClickPttListener != null) {
        onClickPttListener.onClickPtt(v, event);
      }
      CallService.broadcastTouchEvent(getContext(), event);
      return true;
    });

    //親Activityのview
    textTitle = getActivity().findViewById(R.id.text_call_title);
    textCallName = getActivity().findViewById(R.id.text_call_callname);
    spinnerCallList = getActivity().findViewById(R.id.spinner_call);
    btnArrowLeft = getActivity().findViewById(R.id.btn_arrow_down);
    btnArrowLeft.setOnClickListener(v -> {
      if (onClickArrowListener != null) {
        onClickArrowListener.onClickArrow(-1);
      }
    });
    btnArrowRight = getActivity().findViewById(R.id.btn_arrow_up);
    btnArrowRight.setOnClickListener(v -> {
      if (onClickArrowListener != null) {
        onClickArrowListener.onClickArrow(1);
      }
    });
    layoutArrowLeft = getActivity().findViewById(R.id.btnframe_left);
    layoutArrowRight = getActivity().findViewById(R.id.btnframe_right);
    btnMenu = getActivity().findViewById(R.id.btn_spinner_menu);

    //業態設定を取得
    srcCondition = TerminalInfo.getTerminalStatus(getContext(), preference);
    if (srcCondition == null) {
      srcCondition = "";
    }

    //setShortCutIcons();

    if (getArguments() != null) {
      CallType callType = CallType.getType(getArguments().getInt(KEY_CALL_TYPE));
      String[] callIDs = getArguments().getStringArray(KEY_CALL_LIST);
      refreshView(callType, callIDs);
      setBtnVisibility(callType, false);
      setBtnCallStatus(CALLSTATUS.IDLE, true, true);
    }

    return view;
  }

  public void changeCallInfo(CALLSTATUS callStatus, CallType callType, String[] callIDs) {
    if (!isAdded() || isDetached() || isStateSaved()) {
      return;
    }

    saveData(callType, callIDs);

    setBtnVisibility(callType, callStatus != CALLSTATUS.IDLE);
    refreshView(callType, callIDs);
    setBtnCallStatus(callStatus, true, true);
  }

  public void changeCallTimer(
          CALLSTATUS callStatus, CallType callType, String[] callIDs, long time, boolean micMuted) {
    if (!isAdded() || isDetached() || isStateSaved()) {
      return;
    }

    saveData(callType, callIDs);

    setBtnVisibility(callType, callStatus != CALLSTATUS.IDLE);
    refreshView(callType, callIDs);
    setBtnCallStatus(callStatus, micMuted, true);
    viewTimer.setText(Util.getTimerFormat(time));
  }

  /**
   * PTTボタンの表示のみ変更
   */
  public void changeCallNetwork() {
    if (!isAdded() || isDetached() || isStateSaved()) {
      return;
    }
    if (MqttManager.isNetworkConnected(getContext())) {
      setBtnCallStatus(CALLSTATUS.IDLE, false, true);
    } else {
      setBtnCallStatus(CALLSTATUS.IDLE, false, false);
    }
  }

  private void saveData(CallType callType, String[] callIDs) {
    Bundle args = new Bundle();
    args.putInt(KEY_CALL_TYPE, callType.getId());
    args.putStringArray(KEY_CALL_LIST, callIDs);
    setArguments(args);
  }

  /*
  private void setShortCutIcons() {
    List<CallTypeListActivity.CallTypeItem> list = getListCallType();
    if (list.isEmpty()) {
      recyclerView.setVisibility(View.INVISIBLE);
    }
    recyclerView.setAdapter(
        new CallTypeItemRecyclerViewAdapter(
            R.layout.item_shortcut_call_type,
            list,
            item -> {
              if (onClickShortcutListener != null) {
                onClickShortcutListener.onClickShortcut(item.getCallType());
              }
            }));
    recyclerView.setLayoutManager(new GridLayoutManager(getContext(), GRID_COLUMN_NUM));
    //recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
  }
  */

  private List<CallTypeListActivity.CallTypeItem> getListCallType() {
    List<CallTypeListActivity.CallTypeItem> list = new ArrayList<>();
    try {
      Set<String> stringValues =
          preference.getStringSet(getString(R.string.key_pref_shortcut_call_types), null);
      if (stringValues != null) {
        String[] multiValueArray = stringValues.toArray(new String[] {});
        for (String multiValue : multiValueArray) {
          if (isInList(multiValue)) {
            CallType callType = CallType.getType(multiValue);
            if (callType != null) {
              String name = Util.getCallNameShort(getResources(), multiValue);
              list.add(
                      new CallTypeListActivity.CallTypeItem(callType.getId(), callType, name, false));
            }
          }
        }
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
    return list;
  }

  private boolean isInList(String callTypeStr) {
    Set<String> typeSet = TerminalInfo.getCallType(getContext(), preference);
    for (String type : typeSet) {
      if (type.equals(callTypeStr)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    if (context instanceof OnClickArrowListener) {
      onClickArrowListener = (OnClickArrowListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnClickArrowListener");
    }

    if (context instanceof OnClickPttListener) {
      onClickPttListener = (OnClickPttListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnClickPttListener");
    }

    if (context instanceof OnClickShortcutListener) {
      onClickShortcutListener = (OnClickShortcutListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnClickShortcutListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    onClickPttListener = null;
    onClickShortcutListener = null;
  }

  @Override
  public void onStart() {
    super.onStart();
    final IntentFilter filter = new IntentFilter();
    filter.addAction(BROADCAST_CALL_IN_PROGRESS);
    LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, filter);
  }

  @Override
  public void onStop() {
    super.onStop();
    currentCallIDs = null;
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
  }

  private boolean isSameCall(CallType callType, String[] callIDs) {
    return currentCallType != null && currentCallType == callType &&
            currentCallIDs != null && callIDs != null &&
            Arrays.deepEquals(currentCallIDs, callIDs);
  }

  private void refreshView(CallType callType, String[] callIDs) {
    if (isSameCall(callType, callIDs)) {
      return;
    }

    try (Realm realm = Realm.getDefaultInstance()) {
      switch (callType) {
        case MULTI_GROUP_CALL:
        case MULTI_GROUP_CALL_ENFORCEMENT:
          realm.executeTransaction(
              transaction -> {
                String[] names = null;
                if (callIDs != null) {
                  RealmResults<GroupInfoEntity> groups =
                          transaction.where(GroupInfoEntity.class)
                                  .in(DB_KEY_CALL_ID_GROUP, callIDs).findAll();
                  if (groups != null) {
                    names = new String[groups.size()];
                    int i = 0;
                    for (GroupInfoEntity group : groups) {
                      names[i++] = group.getGroupName();
                    }
                  }
                }
                currentCallType = callType;
                currentCallIDs = callIDs;
                String[] finalNames = names;
                new Handler(Looper.getMainLooper())
                        .post(() -> refreshMultiGroupView(callType, finalNames));
              });
          break;
        case GROUP_CALL:
        case GROUP_CALL_ENFORCEMENT:
          realm.executeTransaction(
              transaction -> {
                String name = null;
                if (callIDs != null) {
                  GroupInfoEntity group =
                          transaction
                                  .where(GroupInfoEntity.class)
                                  .equalTo(DB_KEY_CALL_ID_GROUP, callIDs[0])
                                  .findFirst();
                  if (group != null) {
                    name = group.getGroupName();
                  }
                }
                currentCallType = callType;
                currentCallIDs = callIDs;
                String finalName = name;
                new Handler(Looper.getMainLooper())
                        .post(() -> refreshCallView(callType, finalName));
              });
          break;
        case OPERATING_STATION:
        case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
        case ALL_CALL_SYSTEM_WIDE:
        case NEAR_ALL_CALL:
        case EMERGENCY_CALL:
          currentCallType = callType;
          currentCallIDs = null;
          refreshCallView(callType, null);
          break;
        case SITE_ALL_CALL:
          currentCallType = callType;
          currentCallIDs = callIDs;
          if (callIDs != null) {
            refreshCallView(callType, callIDs[0]);
          }
          break;
        default:
          realm.executeTransaction(
                  transaction -> {
                    String name = null;
                    if (callIDs != null) {
                      UserInfoEntity user =
                              transaction
                                      .where(UserInfoEntity.class)
                                      .equalTo(DB_KEY_CALL_ID_USER, callIDs[0])
                                      .findFirst();
                      if (user != null) {
                        name = user.getUserName();
                      }
                    }
                    currentCallType = callType;
                    currentCallIDs = callIDs;
                    String finalName = name;
                    new Handler(Looper.getMainLooper())
                            .post(() -> refreshCallView(callType, finalName));
                  });
          break;
      }
    }
  }

  private void refreshMultiGroupView(CallType callType,  String[] callNames) {

    if (!isAdded() || isDetached() || isStateSaved()) {
      return;
    }

    switch (callType) {
      case MULTI_GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        Context ctx = getContext();
        if (spinnerCallList != null && ctx != null) {
          ArrayList<String> spinnerItems = new ArrayList<>();

          if (callNames != null) {
            spinnerItems.addAll(Arrays.asList(callNames).subList(0, callNames.length));
          } else {
            spinnerItems.add(getString(R.string.error_no_multi_group));
          }

          ArrayAdapter adapter = new ArrayAdapter<String>(ctx,
                  R.layout.item_spinner_call, spinnerItems);
          adapter.setDropDownViewResource(R.layout.item_spinner_call);
          spinnerCallList.setAdapter(adapter);
          spinnerCallList.setVisibility(View.VISIBLE);

          btnMenu.setOnClickListener(v -> {
            spinnerCallList.performClick();
          });
        }
        break;
    }

    if (textTitle != null) {
      String[] titles = getResources().getStringArray(R.array.list_call_type);
      textTitle.setText(titles[callType.ordinal()]);
    }
  }

  private void refreshCallView(CallType callType, String callName) {

    if (!isAdded() || isDetached() || isStateSaved()) {
      return;
    }

    switch (callType) {
      case OPERATING_STATION:
      case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
      case ALL_CALL_SYSTEM_WIDE:
      case NEAR_ALL_CALL:
      case EMERGENCY_CALL:
      case MULTI_GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        textCallName.setText(null);
        break;
      case CALL:
      case GROUP_CALL:
      case GROUP_CALL_ENFORCEMENT:
      case SITE_ALL_CALL:
      case REMOTE_MONITOR:
        textCallName.setText(callName);
        break;
    }

    if (textTitle != null) {
      String[] titles = getResources().getStringArray(R.array.list_call_type);
      textTitle.setText(titles[callType.ordinal()]);
    }
  }

  private void setBtnVisibility(CallType callType, boolean isCalling) {

    switch (callType) {
      case MULTI_GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        spinnerCallList.setVisibility(View.VISIBLE);
        spinnerCallList.setEnabled(!isCalling);
        btnMenu.setVisibility(View.VISIBLE);
        textCallName.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);
        layoutArrowLeft.setVisibility(View.INVISIBLE);
        layoutArrowRight.setVisibility(View.VISIBLE);
        break;
      case CALL:
      case GROUP_CALL:
      case GROUP_CALL_ENFORCEMENT:
      case SITE_ALL_CALL:
      case REMOTE_MONITOR:
        spinnerCallList.setVisibility(View.GONE);
        textCallName.setVisibility(View.VISIBLE);
        btnMenu.setVisibility(View.GONE);
        btnArrowRight.setVisibility((isCalling) ? View.INVISIBLE : View.VISIBLE);
        btnArrowLeft.setVisibility((isCalling) ? View.INVISIBLE : View.VISIBLE);
        layoutArrowLeft.setVisibility(View.VISIBLE);
        layoutArrowRight.setVisibility(View.VISIBLE);
        break;
      default:
        spinnerCallList.setVisibility(View.GONE);
        textCallName.setVisibility(View.VISIBLE);
        btnMenu.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);
        layoutArrowLeft.setVisibility(View.GONE);
        layoutArrowRight.setVisibility(View.GONE);
        break;
    }

    /*
    if (isCalling) {
      recyclerView.setVisibility(View.INVISIBLE);
    } else {
      List<CallTypeListActivity.CallTypeItem> list = getListCallType();
      if (list != null && list.size() > 0) {
        recyclerView.setVisibility(View.VISIBLE);
      } else {
        recyclerView.setVisibility(View.INVISIBLE);
      }
    }
    */
  }

  /**
   * PUSH画像表示
   * @param callStatus 通話状態
   * @param micMuted マイク／スピーカー
   * @param networkOn ネットワーク接続
   */
  int i = 0;
  private void setBtnCallStatus(CALLSTATUS callStatus, boolean micMuted, boolean networkOn) {

    if (!networkOn || micMuted) {
      i = 0;
    } else {
      i += 10;
    }

    if (networkOn) {
      switch (callStatus) {
        case INCOMING_CALL:
        case OUTGOING_CALL:
          //btnPtt.setImageResource(
          //        micMuted ? R.drawable.ic_voice_black_24dp : R.drawable.ic_volume_up_black_24dp);
          //btnPtt.setImageResource(R.drawable.ic_pressbtactive);
          //btnPtt.setColorFilter(Color.WHITE);
          //btnPtt.setBackgroundResource(
          //  micMuted ? R.drawable.bk_ptt_button_incoming : R.drawable.bk_ptt_button_outgoing);
          //btnPtt.setBackgroundResource(R.drawable.ic_pressbtactive);
          btnPtt.setBackgroundResource(R.drawable.btn_pressbtactive);
          //imgCircle.setBackgroundResource(R.mipmap.ic_networkcircleon);
          imgCircle.setImageResource(R.mipmap.ic_networkcircleon);
          imgCircle.setRotation(i);
          //imgShadow.setBackgroundResource(R.drawable.ic_pressbtactiveshadow);
          imgTalking.setBackgroundResource(R.drawable.ic_talkingtexton);
          imgNetwork.setBackgroundResource(R.drawable.ic_networktexton);
          viewTimer.setVisibility(View.VISIBLE);
          viewParkingMsg.setVisibility(View.GONE);
          break;
        case INCOMING:
        case OUTGOING:
          //btnPtt.setImageResource(R.drawable.ic_sync_black_24dp);
          //btnPtt.setImageResource(R.drawable.ic_pressbtactive);
          //btnPtt.setColorFilter(Color.WHITE);
          //btnPtt.setBackgroundResource(
          //  micMuted ? R.drawable.bk_ptt_button_incoming : R.drawable.bk_ptt_button_outgoing);
          //btnPtt.setBackgroundResource(R.drawable.btn_pressbtactive);
          btnPtt.setBackgroundResource(R.drawable.btn_pressbtactive);
          //imgCircle.setBackgroundResource(R.mipmap.ic_networkcircleon);
          imgCircle.setImageResource(R.mipmap.ic_networkcircleon);
          //imgShadow.setBackgroundResource(R.drawable.ic_pressbtactiveshadow);
          imgTalking.setBackgroundResource(R.drawable.ic_talkingtexton);
          imgNetwork.setBackgroundResource(R.drawable.ic_networktexton);
          viewTimer.setText(null);
          viewTimer.setVisibility(View.INVISIBLE);
          viewParkingMsg.setVisibility(View.GONE);
          break;
        case PARKING:
          //btnPtt.setImageResource(R.drawable.ic_query);
          //btnPtt.setImageResource(R.drawable.ic_pressbtonhold);
          //btnPtt.setColorFilter(Color.WHITE);
          //btnPtt.setBackgroundResource(R.drawable.bk_ptt_button_on_hold);
          //btnPtt.setBackgroundResource(R.drawable.btn_pressbtonhold);
          btnPtt.setBackgroundResource(R.drawable.btn_pressbtonhold);
          //imgCircle.setBackgroundResource(R.mipmap.ic_networkcircleon);
          imgCircle.setImageResource(R.mipmap.ic_networkcircleon);
          //imgShadow.setBackgroundResource(R.drawable.ic_pressbtnormalshadow);
          imgTalking.setBackgroundResource(R.drawable.ic_talkingtexton);
          imgNetwork.setBackgroundResource(R.drawable.ic_networktexton);
          viewTimer.setVisibility(View.GONE);
          String parking = viewTimer.getText().toString() + " " + getString(R.string.home_msg_parking_call);
          viewParkingMsg.setText(parking);
          viewParkingMsg.setVisibility(View.VISIBLE);
          break;
        default:
          //btnPtt.setImageResource(R.drawable.ic_record_voice_over_black_24px);
          //btnPtt.setImageResource(R.drawable.ic_pressbtnormal);
          //btnPtt.setColorFilter(Color.WHITE);
          //btnPtt.setBackgroundResource(R.drawable.bk_ptt_button_normal);
          //btnPtt.setBackgroundResource(R.drawable.btn_pressbtnormal);
          btnPtt.setBackgroundResource(R.drawable.btn_pressbtnormal);
          //imgCircle.setBackgroundResource(R.mipmap.ic_networkcircleon);
          imgCircle.setImageResource(R.mipmap.ic_networkcircleon);
          //imgShadow.setBackgroundResource(R.drawable.ic_pressbtnormalshadow);
          imgTalking.setBackgroundResource(R.drawable.ic_talkingtextoff);
          imgNetwork.setBackgroundResource(R.drawable.ic_networktexton);
          //viewTimer.setText(null);
          //viewTimer.setVisibility(View.INVISIBLE);
          viewTimer.setText(srcCondition);
          viewTimer.setVisibility(View.VISIBLE);
          viewParkingMsg.setVisibility(View.GONE);
          break;
      }
    } else {
      //btnPtt.setImageResource(R.drawable.ic_pressbtnetworkoff);
      //btnPtt.setColorFilter(Color.WHITE);
      //btnPtt.setBackgroundResource(R.drawable.bk_ptt_button_normal);
      //btnPtt.setBackgroundResource(R.drawable.btn_pressbtnetworkoff);
      btnPtt.setBackgroundResource(R.drawable.btn_pressbtnetworkoff);
      //imgCircle.setBackgroundResource(R.mipmap.ic_networkcircleoff);
      imgCircle.setImageResource(R.mipmap.ic_networkcircleoff);
      //imgShadow.setBackgroundResource(R.drawable.ic_pressbtoffshadow);
      imgTalking.setBackgroundResource(R.drawable.ic_talkingtextnetworkoff);
      imgNetwork.setBackgroundResource(R.drawable.ic_networktextoff);
      viewTimer.setText(null);
      viewTimer.setVisibility(View.INVISIBLE);
      viewParkingMsg.setVisibility(View.GONE);
    }
  }

}
