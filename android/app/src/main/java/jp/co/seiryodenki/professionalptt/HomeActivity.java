package jp.co.seiryodenki.professionalptt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;

import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
// 2019/04/02 IPアドレス変更時に動作しない問題に対応
//import jp.co.seiryodenki.professionalptt.core.SipManager;
import jp.co.seiryodenki.professionalptt.core.StateManager;
import jp.co.seiryodenki.professionalptt.core.MqttManager;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.GroupInfoEntity;
import jp.co.seiryodenki.professionalptt.model.SiteEntity;
import jp.co.seiryodenki.professionalptt.model.TerminalInfo;
import jp.co.seiryodenki.professionalptt.model.UserInfoEntity;
import jp.co.seiryodenki.professionalptt.service.CallService;
import jp.co.seiryodenki.professionalptt.service.LogoutService;
import jp.co.seiryodenki.professionalptt.service.ServiceConnectionHelper;
import jp.co.seiryodenki.professionalptt.ui.ErrorNetworkDialogFragment;
import jp.co.seiryodenki.professionalptt.ui.HomeFragment;
import jp.co.seiryodenki.professionalptt.ui.HomeFragment.OnClickArrowListener;
import jp.co.seiryodenki.professionalptt.ui.HomeFragment.OnClickPttListener;
import jp.co.seiryodenki.professionalptt.ui.HomeFragment.OnClickShortcutListener;
import jp.co.seiryodenki.professionalptt.ui.MenuFragment;
import jp.co.seiryodenki.professionalptt.ui.MenuFragment.OnClickMenuListener;
import jp.co.seiryodenki.professionalptt.util.MyActivityLifecycleCallbacks;
import jp.co.seiryodenki.professionalptt.util.TerminalUtils;
import jp.co.seiryodenki.professionalptt.util.logger;

import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_CALL_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_PTT_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.BROADCAST_NETWORK_ONOFF;
import static jp.co.seiryodenki.professionalptt.Constants.CALLSTATUS;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_AREA;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_GROUP;
import static jp.co.seiryodenki.professionalptt.Constants.DB_KEY_CALL_ID_USER;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_CALL_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.INTENT_PTT_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_ARROW_DOWN;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_ARROW_LEFT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_ARROW_RIGHT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_ARROW_UP;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_FUNCTION;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_KYOCERA;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_CAT;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_PTT_ATOM;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_SOS;
import static jp.co.seiryodenki.professionalptt.Constants.KEY_HEADSETHOOK;

/**
 * 通話制御画面クラス
 */
public class HomeActivity extends BaseActivity
    implements OnClickPttListener,
        OnClickShortcutListener,
        OnClickArrowListener,
        OnClickMenuListener {
  public static final String TAG = HomeActivity.class.getSimpleName();

  // メニュー画面要求コード
  private static final int REQUEST_CODE_MENU = 1;

  // Intent key
  private static final String INTENT_CALL_TYPE = "intent_call_type";
  private static final String INTENT_CALL_LIST = "intent_call_list";
  private static final String INTENT_IMMEDIATE_CALL = "intent_immediate_call";
  private static final String TAG_FRAGMENT_HOME = "tag_fragment_home";
  private static final String TAG_FRAGMENT_MENU = "tag_fragment_menu";
  //メニュー画面への遷移
  private static final String INTENT_MENU_FLG = "intent_menu_flg";

  // Preference
  private SharedPreferences mPreference;

  // Realm DBハンドラ
  private Realm mRealm;

  // CallService接続ヘルパ
  private ServiceConnectionHelper mHelper;

  // 画面表示要素
  private Button btnDisconnect;
  //private View mNavigationView;
  private HomeFragment mHomeFragment;
  private int mMenuId = R.id.fragment_home;
  private LinearLayout layoutCallType;
  //ツールバーのタイトル
  private TextView titleToolbar;
  //private ImageView imageToolbarRight;
  //private ImageView imageToolbarLeft;
  private ImageView imageToolbarBlank;
  private ImageView imageToolbarMenu;
  private ImageView imageToolbarHistory;
  private ImageView imageToolbarBack;

  // 通話情報保持
  private CallType displayCallType;
  private String[] callIDs;
  private boolean isImmediateCall = false;
  private boolean isCalling = false;

  // PTTボタン押下状態保持
  private boolean mPttKeyPushed = false;

  // エラーダイアログ
  private DialogFragment dialogFragmentNetwork = null;

  // 端末情報保持
  private static String companyId;
  private static String terminalId;

  /**
   * Activity起動Intent作成処理
   *
   * @param context Context
   * @param callType 通話種別
   * @param callIds 通話先ID
   * @param isImmediateCall 通話開始フラグ
   * @return Activity起動Intent
   */
  public static Intent getCallIntent(
      Context context, CallType callType, String[] callIds, boolean isImmediateCall) {
    Intent intent = new Intent(context, HomeActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_CALL_TYPE, callType.getId());
    intent.putExtra(INTENT_CALL_LIST, callIds);
    intent.putExtra(INTENT_IMMEDIATE_CALL, isImmediateCall);
    //メニュー画面への遷移でないことをセット
    intent.putExtra(INTENT_MENU_FLG, false);
    return intent;
  }

  /**
   * Activity起動Intent作成処理
   *
   * @param context Context
   * @param callInfo 通話情報
   * @param isImmediateCall 通話開始フラグ
   * @return Activity起動Intent
   */
  public static Intent getCallIntent(
      Context context, @NonNull CallInfo callInfo, boolean isImmediateCall) {
    Intent intent = new Intent(context, HomeActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_CALL_TYPE, (callInfo.getCallType() != null ? callInfo.getCallType().getId() : null));
    intent.putExtra(INTENT_CALL_LIST, callInfo.getCallList());
    intent.putExtra(INTENT_IMMEDIATE_CALL, isImmediateCall);
    //メニュー画面への遷移でないことをセット
    intent.putExtra(INTENT_MENU_FLG, false);
    callInfo.putIntent(intent);
    return intent;
  }

  /**
   * ホーム画面に戻るためのIntent作成処理
   */
  public static Intent getCallIntent( Context context, boolean isMenuFragment ) {
    Intent intent = new Intent(context, HomeActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra(INTENT_MENU_FLG, isMenuFragment);
    return intent;
  }

  /**
   * Activity呼び出し処理
   *
   * @param intent 開始授受情報
   */
  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    setIntent(intent);

    //メニュー画面へ遷移
    if (intent != null && intent.getBooleanExtra(INTENT_MENU_FLG, false)) {
      mMenuId = R.id.fragment_menu;
      switchFragment(mMenuId);
      return;
    }

    if (intent != null) {
      if (mHelper == null) {
        mHelper = new ServiceConnectionHelper(this);
      }
      //mMenuId = R.id.btn_navi_home;
      mMenuId = R.id.fragment_home;
      isImmediateCall = intent.getBooleanExtra(INTENT_IMMEDIATE_CALL, false);
      CallType type = CallType.getType(
              intent.getIntExtra(INTENT_CALL_TYPE, CallType.DEFAULT_CALL_TYPE_ID));
      // 2019/01/22　SEC
      // サーバで無効設定した通話種別が呼び出される不具合に対応
      if(!enableCallType(type)) {
        type = getNextCallType(type);
      }

      String[] ids = intent.getStringArrayExtra(INTENT_CALL_LIST);
      // 2018/7/24 地域通話時の強制終了対応
      switch (type) {
        case SITE_ALL_CALL:
          setCallInfo(type, 0);
          break;
        default:
          if (ids == null) {
            setCallInfo(type, 0);
          }
          else{
            notifyFragment(type,ids);
          }
          break;
      }
    }
  }

  /**
   * 個別通話選択処理
   *
   * @param callType 通話種別
   * @param diff 選択移動値
   */
  private synchronized void personalCall(CallType callType, int diff) {

    RealmResults<UserInfoEntity> users =
            mRealm.where(UserInfoEntity.class).sort(DB_KEY_CALL_ID_USER).findAll();
    if (users != null && users.size() > 0) {
      mRealm.executeTransaction(realm -> {
        UserInfoEntity user = null;
        if (callIDs != null && callIDs.length > 0) {
          user = realm.where(UserInfoEntity.class)
                  .equalTo(DB_KEY_CALL_ID_USER, callIDs[0]).findFirst();
        }

        UserInfoEntity nextUser;
        if (user != null) {
          int index = users.indexOf(user) + diff;
          if (index >= users.size()) {
            nextUser = users.get(0);
          } else if (index < 0) {
            nextUser = users.last();
          } else {
            nextUser = users.get(index);
          }
        } else {
          nextUser = users.first();
        }

        if (nextUser != null) {
          callIDs = new String[]{nextUser.getTerminalId()};
        }
      });
    }

    notifyFragment(callType, callIDs);
  }

  /**
   * グループ通話選択処理
   *
   * @param callType 通話種別
   * @param diff 選択移動値
   */
  private synchronized void groupCall(CallType callType, int diff) {
    RealmResults<GroupInfoEntity> groups =
            mRealm.where(GroupInfoEntity.class).sort(DB_KEY_CALL_ID_GROUP).findAll();
    if (groups != null && groups.size() > 0) {
      mRealm.executeTransaction(realm -> {
        GroupInfoEntity group = null;
        if (callIDs != null && callIDs.length > 0) {
          group = realm.where(GroupInfoEntity.class)
                  .equalTo(DB_KEY_CALL_ID_GROUP, callIDs[0]).findFirst();
        }

        GroupInfoEntity nextGroup;
        if (group != null) {
          int index = groups.indexOf(group) + diff;
          if (index >= groups.size()) {
            nextGroup = groups.get(0);
          } else if (index < 0) {
            nextGroup = groups.last();
          } else {
            nextGroup = groups.get(index);
          }
        } else {
          nextGroup = groups.first();
        }

        if (nextGroup != null) {
          callIDs = new String[]{nextGroup.getGroupId()};
        }
      });
    }

    notifyFragment(callType, callIDs);
  }

  /**
   * マルチグループ通話選択処理
   *
   * @param callType 通話種別
   * @param diff 選択移動値
   */
  private synchronized void multiGroupCall(CallType callType, int diff) {
    Set<String> groupIdSet =
            mPreference.getStringSet(getString(R.string.key_pref_default_multi_group_ids), null);

    String[] groupIds = null;
    if (groupIdSet != null) {
      groupIds = groupIdSet.toArray(new String[] {});
    }

    if (groupIds == null) {
      groupCall(callType, diff);
    } else {
      RealmResults<GroupInfoEntity> resMultiGroups =
              mRealm.where(GroupInfoEntity.class)
                      .in(DB_KEY_CALL_ID_GROUP, groupIds).sort(DB_KEY_CALL_ID_GROUP).findAll();
      if (resMultiGroups != null && resMultiGroups.size() > 0) {
        String[] ids = new String[resMultiGroups.size()];
        int i = 0;
        for (GroupInfoEntity group : resMultiGroups) {
          ids[i++] = group.getGroupId();
        }
        callIDs = ids;
      }

      notifyFragment(callType, callIDs);
    }
  }

  /**
   * 一斉通話選択処理
   *
   * @param callType 通話種別
   * @param diff 選択移動値
   */
  private synchronized void siteAllCall(CallType callType, int diff) {

    RealmResults<SiteEntity> sites = mRealm.where(SiteEntity.class).findAll();
    if (sites != null && sites.size() > 0) {
      mRealm.executeTransaction(realm -> {
        SiteEntity site = null;
        if (callIDs != null && callIDs.length > 0) {
          site = realm.where(SiteEntity.class)
                  .equalTo(DB_KEY_CALL_ID_AREA, callIDs[0]).findFirst();
        }

        SiteEntity nextSite;
        if (site != null) {
          int index = sites.indexOf(site) + diff;
          if (index >= sites.size()) {
            nextSite = sites.get(0);
          } else if (index < 0) {
            nextSite = sites.last();
          } else {
            nextSite = sites.get(index);
          }
        } else {
          nextSite = sites.first();
        }

        if (nextSite != null) {
          callIDs = new String[]{nextSite.getArea_name()};
        }
      });
    }

    notifyFragment(callType, callIDs);
  }

  /**
   * 通話情報設定処理
   *
   * @param callType 通話種別
   * @param diff 選択移動値
   */
  private synchronized void setCallInfo(CallType callType, int diff) {
    if (displayCallType != callType) {
      callIDs = null;
    }

    switch (callType) {
      case REMOTE_MONITOR:
      case CALL:
        personalCall(callType, diff);
        return;
      case GROUP_CALL:
      case GROUP_CALL_ENFORCEMENT:
        groupCall(callType, diff);
        return;
      case MULTI_GROUP_CALL:
      case MULTI_GROUP_CALL_ENFORCEMENT:
        multiGroupCall(callType, diff);
        return;
      case SITE_ALL_CALL:
        siteAllCall(callType, diff);
        return;
    }

    notifyFragment(callType, callIDs);
  }

  /**
   * 通話情報画面通知処理
   *
   * @param callType 通話種別
   * @param callIDs 通話ID
   */
  private void notifyFragment(CallType callType, String[] callIDs) {
    this.callIDs = callIDs;
    this.displayCallType = callType;

    mHelper.setDefaultCallInfo(this, callType, callIDs);
    if (mMenuId == R.id.fragment_home && mHomeFragment != null) {
      mHomeFragment.changeCallInfo(CALLSTATUS.IDLE, displayCallType, callIDs);
    }
  }

  /**
   * Activity起動時処理
   *
   * @param savedInstanceState 状態保存データ
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    //mNavigationView = findViewById(R.id.navigation);
    //切断ボタン
    btnDisconnect = findViewById(R.id.btn_disconnect);
    btnDisconnect.setOnClickListener(v -> onClickDisconnect());
    //
    layoutCallType = findViewById(R.id.layout_calltype);
    //ツールバータイトル
    titleToolbar = findViewById(R.id.toolbartxt);
    //imageToolbarLeft = findViewById(R.id.toolbar_navi_left);
    //imageToolbarRight = findViewById(R.id.toolbar_navi_right);
    imageToolbarMenu = findViewById(R.id.toolbar_navi_menu);
    imageToolbarBlank = findViewById(R.id.toolbar_navi_blank);
    imageToolbarHistory = findViewById(R.id.toolbar_navi_history);
    imageToolbarBack = findViewById(R.id.toolbar_navi_back);

    mHelper = new ServiceConnectionHelper(this);
    mPreference = PreferenceManager.getDefaultSharedPreferences(this);
    mRealm = Realm.getDefaultInstance();
    companyId = mPreference.getString(getString(R.string.key_pref_company_id), null);
    terminalId = mPreference.getString(getString(R.string.key_pref_terminal_id), null);

    registerReceiver();

    // 2019/01/22　SEC
    // サーバで無効設定した通話種別が呼び出される不具合に対応
    CallType callType = CallType.getType(TerminalInfo.getCallDefault(this, mPreference));
    if(enableCallType(callType)) {
      setCallInfo(CallType.getType(TerminalInfo.getCallDefault(this, mPreference)), 0);
    } else {
      setCallInfo(getNextCallType(CallType.getType(TerminalInfo.getCallDefault(this, mPreference))), 0);
    }
  }

  /** Activity終了時処理 */
  @Override
  protected void onDestroy() {
    if (mHelper != null) {
      mHelper.destory(this);
    }
    if (mRealm != null) {
      mRealm.close();
    }
    LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    super.onDestroy();
  }

  /** Activity開始時処理 */
  @Override
  protected void onResume() {
    super.onResume();
    try{
      // 2019/04/02 IPアドレス変更時に動作しない問題に対応
      //if (!SipManager.isInstantiated() && !MqttManager.isInstantiated()) {
      if (!MqttManager.isInstantiated()) {
        startActivity(LoginActivity.getIntent(this, null, null));
      }
    }catch (Exception e) {
      logger.e(TAG,"Start Up Sequence Miss", e);
      return;
    }

    if(TerminalUtils.getLoginFlag()){
      startActivity(LoginActivity.getIntent(this, null, null));
      return;
    }

    switchFragment(mMenuId);
    //setButtonStatus(mMenuId);

    // Bind to LocalService
    if (!mHelper.isBound()) {
      mHelper.bindDataService(this, (isSuccess, service) -> {
        if (!isSuccess) {
          logger.e(TAG, "Failed to bind service.");
          return;
        }
        if (isImmediateCall) {
          onPressPttEvent(true);
          isImmediateCall = false;
        }
      });
    } else {
      if (isImmediateCall) {
        onPressPttEvent(true);
        isImmediateCall = false;
      }
    }
  }

  /** Activity停止時処理 */
  @Override
  protected void onPause() {
    super.onPause();
    if (mHelper.isBound()) {
      mHelper.unbindDataService(this);
    }
    mHomeFragment = null;
  }

  /**
   * キータッチのUP時処理
   *
   * @param keyCode キーコード
   * @param event 処理イベント
   * @return 処理結果
   */
  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if (isCalling) {
      if ((keyCode == KEY_PTT) || (keyCode == KEY_PTT_KYOCERA) || (keyCode == KEY_PTT_ATOM) || (keyCode == KEY_PTT_CAT)) {
        mPttKeyPushed = false;
        onPressPttEvent(false);
      }
    } else {
      if ((keyCode == KEY_PTT) || (keyCode == KEY_PTT_KYOCERA) || (keyCode == KEY_PTT_ATOM) || (keyCode == KEY_PTT_CAT))  {
        mPttKeyPushed = false;
        onPressPttEvent(false);
      } else if (keyCode == KEY_ARROW_UP) {
        changeCall(-1);
      } else if (keyCode == KEY_ARROW_DOWN) {
        changeCall(1);
      } else if (keyCode == KEY_ARROW_LEFT) {
        changeCall(-10);
      } else if (keyCode == KEY_ARROW_RIGHT) {
        changeCall(10);
      } else if (keyCode == KEY_FUNCTION) {
        setCallInfo(getNextCallType(displayCallType), 0);
      } else if (keyCode == KEY_SOS) {
        mHelper.publishSos(this);
      }
    }
    return super.onKeyUp(keyCode, event);
  }

  /**
   * キータッチのPUSH時処理
   *
   * @param keyCode キーコード
   * @param event 処理イベント
   * @return 処理結果
   */
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if ((keyCode == KEY_PTT) || (keyCode == KEY_PTT_KYOCERA) || (keyCode == KEY_PTT_ATOM) || (keyCode == KEY_PTT_CAT))  {
      if (!mPttKeyPushed) {
        mPttKeyPushed = true;
        onPressPttEvent(true);
      }
      CallService.broadcastTouchEvent(this, null);
      return true;
    }
    else if(keyCode == KEY_HEADSETHOOK){
      if (!mPttKeyPushed) {
        mPttKeyPushed = true;
        onPressPttEvent(true);
      } else {
        mPttKeyPushed = false;
        onPressPttEvent(false);
      }
      CallService.broadcastTouchEvent(this, null);
      return true;
    }
    return super.onKeyDown(keyCode, event);
  }

  private void changeCall(int diff) {
    setCallInfo(displayCallType, diff);
  }

  /**
   * 通話種別有効判定
   */
  private boolean enableCallType(CallType callType) {
    boolean found = false;
    if (callType == null) {
      return found;
    }
    Set<String> typeSet = TerminalInfo.getCallType(this, mPreference);
    String[] typeArr = typeSet.toArray(new String[] {});
    for (int i = 0; i < typeArr.length; i++) {
      if (!found) {
        if (typeArr[i].equals(callType.toString())) {
          found = true;
        }
      } else {
        break;
      }
    }
    return found;
  }

  /**
   * 次通話種別取得処理
   *
   * @param callType 現通話種別
   * @return 次通話種別
   */
  private CallType getNextCallType(CallType callType) {
    if (callType == null) {
      return CallType.getType(TerminalInfo.getCallDefault(this, mPreference));
    }

    String nextCall = callType.toString();
    boolean found = false;
    Set<String> typeSet = TerminalInfo.getCallType(this, mPreference);
    String[] typeArr = typeSet.toArray(new String[] {});
    for (int i = 0; i < typeArr.length; i++) {
      if (i == 0) {
        nextCall = typeArr[i];
      }
      if (!found) {
        if (typeArr[i].equals(callType.toString())) {
          found = true;
        }
      } else {
        return CallType.getType(typeArr[i]);
      }
    }

    return CallType.getType(nextCall);
  }

  /** Broadcast受信種別設定 */
  private void registerReceiver() {
    final IntentFilter filter = new IntentFilter();
    filter.addAction(BROADCAST_PTT_STATUS);
    filter.addAction(BROADCAST_CALL_STATUS);
    filter.addAction(BROADCAST_NETWORK_ONOFF);
    LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, filter);
  }

  /**
   * Broadcastレシーバ
   */
  private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

    /**
     * Broadcast受信処理
     *
     * @param context Context
     * @param intent 受信データ
     */
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent == null || intent.getAction() == null) {
        return;
      }
      if (BROADCAST_CALL_STATUS.equals(intent.getAction())) {
        onReceiveCallStatus(intent);
      } else if (BROADCAST_PTT_STATUS.equals(intent.getAction())) {
        onReceivePttStatus(intent);
      } else if (BROADCAST_NETWORK_ONOFF.equals(intent.getAction())) {
        onReceiveNetworkOnOff(context);
      }
    }

    /**
     * Broadcast受信データ処理
     *
     * @param intent 受信データ
     */
    private void onReceiveCallStatus(Intent intent) {
      CALLSTATUS callStatus = (CALLSTATUS) intent.getSerializableExtra(INTENT_CALL_STATUS);
      switch (callStatus) {
        case IDLE:
          isCalling = false;
          //mNavigationView.setVisibility(View.VISIBLE);
          btnDisconnect.setVisibility(View.INVISIBLE);
          CallInfo callInfo = mHelper.getDefaultCallInfo();
          if (callInfo != null) {
            notifyFragment(callInfo.getCallType(), callInfo.getCallList());
          }
          break;
        case INCOMING_CALL:
        case OUTGOING_CALL:
          isCalling = true;
          //mNavigationView.setVisibility(View.GONE);
          btnDisconnect.setVisibility(View.VISIBLE);
          break;
        case PARKING:
          btnDisconnect.setVisibility(View.INVISIBLE);
          break;
        default:
          isCalling = true;
          //mNavigationView.setVisibility(View.GONE);
          btnDisconnect.setVisibility(View.VISIBLE);
          break;
      }
      if (!TAG.equals(MyActivityLifecycleCallbacks.getCurrentActivityName())) {
        Intent newIntent = new Intent(HomeActivity.this, HomeActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
      } else if (mMenuId != R.id.fragment_home) {
        mMenuId = R.id.fragment_home;
        switchFragment(mMenuId);
        //setButtonStatus(mMenuId);
      }
    }

    /**
     * PTTボタン押下Broadcast受信データ処理
     *
     * @param intent 受信データ
     */
    private void onReceivePttStatus(Intent intent) {
      Integer isPushed = intent.getIntExtra(INTENT_PTT_STATUS, 0);
      if (isPushed == 0) {
        onKeyUp(KEY_PTT, null);
      } else {
        onKeyDown(KEY_PTT, null);
      }
    }

    /**
     * ネットワークON/OFF Broadcast受信データ処理
     * @param context
     */
    private void onReceiveNetworkOnOff(Context context) {
    if (mHomeFragment != null) {
      mHomeFragment.changeCallNetwork();
    }
    }
  };

  /**
   * ナビゲーションボタン押下時画面処理
   *
   * @param menuId ナビゲーションボタンID
   */
  private void switchFragment(int menuId) {
    switch (menuId) {
      //case R.id.btn_navi_home:
      case R.id.fragment_home:
        if (mHomeFragment == null) {
          mHomeFragment = HomeFragment.newInstance(displayCallType, callIDs);
        }
        replaceFragment(mHomeFragment, TAG_FRAGMENT_HOME);
        layoutCallType.setVisibility(View.VISIBLE);
        titleToolbar.setText(getString(R.string.toolbar_presstalk));
        //imageToolbarLeft.setImageResource(R.drawable.btn_menubt);
        //imageToolbarRight.setImageResource(R.drawable.btn_historybt);
        imageToolbarMenu.setVisibility(View.VISIBLE);
        imageToolbarBlank.setVisibility(View.GONE);
        imageToolbarHistory.setVisibility(View.VISIBLE);
        imageToolbarBack.setVisibility(View.GONE);
        break;
      //case R.id.btn_navi_menu:
      case R.id.fragment_menu:
        replaceFragment(new MenuFragment(), TAG_FRAGMENT_MENU);
        layoutCallType.setVisibility(View.GONE);
        titleToolbar.setText(getString(R.string.toolbar_menu));
        //imageToolbarLeft.setImageDrawable(null);
        //imageToolbarRight.setImageResource(R.drawable.btn_backbt);
        imageToolbarMenu.setVisibility(View.GONE);
        imageToolbarBlank.setVisibility(View.VISIBLE);
        imageToolbarHistory.setVisibility(View.GONE);
        imageToolbarBack.setVisibility(View.VISIBLE);
        break;
      default:
        break;
    }
  }

  /**
   * 画面切替処理
   * @param fragment 切替画面
   * @param tag 画面タグ
   */
  private void replaceFragment(Fragment fragment, String tag) {
    getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.main_container, fragment, tag)
        .commitAllowingStateLoss();
  }

  /** 戻るキー押下時処理 */
  @Override
  public void onBackPressed() {
    if (mMenuId != R.id.fragment_home) {
      mMenuId = R.id.fragment_home;
      switchFragment(R.id.fragment_home);
      //setButtonStatus(R.id.btn_navi_home);
    }
  }

  /**
   * メニュー画面項目選択時処理
   *
   * @param item 選択メニュー
   */
  @Override
  public void onClickMenu(MenuFragment.MenuItem item) {
    switch (item.idRes) {
      //case R.drawable.ic_phonebook_black:
      case R.drawable.btn_menuadressbt:
        startActivity(PhoneBookActivity.getIntent(this, StateManager.getMyPriority()));
        break;
      //case R.drawable.ic_email_black_24dp:
      case R.drawable.btn_menunewmessagebt:
        startActivity(MessageActivity.getIntent(this, StateManager.getMyPriority()));
        break;
      //case R.drawable.ic_inbox_black_24dp:
      case R.drawable.btn_menumessagehistorybt:
        startActivity(MessageBoxActivity.getIntent(this, StateManager.getMyPriority()));
        break;
      //case R.drawable.ic_history_black_24dp:
      case R.drawable.btn_menutransmissionbt:
        startActivity(new Intent(this, RecentActivity.class));
        break;
      //case R.drawable.ic_settings_black_24dp:
      case R.drawable.btn_menuappssettingbt:
        startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_CODE_MENU);
        break;
      //case R.drawable.ic_record_black:
      case R.drawable.btn_menurecordingbt:
        startActivity(new Intent(this, VoiceRecodingActivity.class));
        break;
      case R.drawable.btn_menulogoutbt:
        logout();
        break;
    }
  }

  /**
   * Activity処理結果受信処理
   *
   * @param requestCode Activity起動コード
   * @param resultCode 処理結果コード
   * @param data 処理結果データ
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case (REQUEST_CODE_MENU):
        if (resultCode == RESULT_OK) {
          setCallInfo(displayCallType, 0);
        }
        break;
      default:
        break;
    }
  }

  /**
   * ナビメニュー押下時処理
   *
   * @param v 押下ボタンView
   */
  public void setClickNaviAction(View v) {
    if (isCalling) {
      return;
    }
    int id = v.getId();
    switch (id) {
      //case R.id.btn_navi_menu:
      case R.id.toolbar_navi_menu:
        if (mMenuId == R.id.fragment_home) {
          mMenuId = R.id.fragment_menu;
          switchFragment(mMenuId);
        }
        //setButtonStatus(id);
        break;
      //case R.id.btn_navi_list:
      case R.id.btn_call_title:
        if (mMenuId == R.id.fragment_home) {
          startActivity(CallTypeListActivity.getIntent(this));
        }
        //setButtonStatus(id);
        break;
      case R.id.toolbar_navi_history:
      case R.id.toolbar_navi_back:
        if (mMenuId == R.id.fragment_home) {
          startActivity(new Intent(this, RecentActivity.class));
        } else {
          onBackPressed();
        }
        break;
    }
  }

  /**
   * PTTボタン押下時処理
   *
   * @param v PTTボタンView
   * @param event PTTボタン押下イベント
   */
  @Override
  public void onClickPtt(View v, MotionEvent event) {

    if (event.getAction() == MotionEvent.ACTION_DOWN) {
      onPressPttEvent(true);
    } else if (event.getAction() == MotionEvent.ACTION_UP) {
      onPressPttEvent(false);
    }
  }

  /**
   * PTTボタン押下時処理
   *
   * <pre>PTTボタン押下状態をサービスに通知する</pre>
   *
   * @param actionDown 押下状態
   */
  private void onPressPttEvent(boolean actionDown) {
    if (mMenuId != R.id.fragment_home) {
      mMenuId = R.id.fragment_home;
      switchFragment(mMenuId);
      //setButtonStatus(mMenuId);
    }
    mHelper.onPressPttEvent(this, actionDown);
    //ネットワークエラーダイアログを表示
    if (actionDown && !MqttManager.isNetworkConnected(this)) {
      showNetworkErrorDialog();
    }
  }

  /**
   * 通話ショートカットボタン押下処理
   *
   * @param callType 項目データ
   */
  @Override
  public void onClickShortcut(CallType callType) {
    setCallInfo(callType, 0);

    mMenuId = R.id.fragment_home;
    switchFragment(mMenuId);
    //setButtonStatus(mMenuId);
  }

  private void onClickDisconnect() {
    mHelper.stopCalling(this);
    changeCall(0);
  }

  /**
   * 前後ボタン押下処理
   *
   * @param diff 表示項目移動数
   */
  @Override
  public void onClickArrow(int diff) {
    changeCall(diff);
  }

  /**
   * ログアウト実行
   */
  public void logout() {
    BaseActivity.broadcastLogout(this);
    LogoutService.startLogoutService(this);
    startActivity(LoginActivity.getIntent(this, companyId, terminalId));
    finish();
  }

  /**
   * エラーダイアログを閉じる
   */
  private void closeDialog() {
    if (dialogFragmentNetwork != null) {
      dialogFragmentNetwork.dismiss();
      dialogFragmentNetwork = null;
    }
  }

  /**
   * ネットワークエラーダイアログを表示
   */
  public void showNetworkErrorDialog() {
    closeDialog();
    dialogFragmentNetwork = ErrorNetworkDialogFragment.newInstance();
    dialogFragmentNetwork.show(getSupportFragmentManager(), "dialog_error_network");
  }

  /**
   * ネットワークエラーダイアログを閉じる
   * @param view
   */
  public void setDialogNetworkOKClick(View view) {
    closeDialog();
  }
}
