package jp.co.seiryodenki.professionalptt.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import jp.co.seiryodenki.professionalptt.core.WebApiResponse;

public class VoiceInfo extends WebApiResponse {

  @SerializedName("files")
  private List<VoiceFile> files;

  public VoiceInfo() {
  }


  public List<VoiceFile> getFiles() {
    return files;
  }

  public static class VoiceFile {

    public VoiceFile() {
    }

    @SerializedName("id")
    private String id;

    @SerializedName("meeting")
    private String meeting;

    @SerializedName("begin")
    private String begin;

    @SerializedName("end")
    private String end;

    public String getId() {
      return id;
    }

    public String getMeeting() {
      return meeting;
    }

    public String getBegin() {
      return begin;
    }

    public String getEnd() {
      return end;
    }
  }

}
