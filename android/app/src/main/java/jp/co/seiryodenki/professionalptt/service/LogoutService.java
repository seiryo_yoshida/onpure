package jp.co.seiryodenki.professionalptt.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import jp.co.seiryodenki.professionalptt.core.WebApiRequest;
import jp.co.seiryodenki.professionalptt.model.Logout;

import static jp.co.seiryodenki.professionalptt.Constants.ACTION_LOGOUT;

/** ログアウト処理クラス */
public class LogoutService extends IntentService {
  private static final String TAG = LogoutService.class.getSimpleName();

  // Preference保持
  private SharedPreferences preference;

  /** コンストラクタ */
  public LogoutService() {
    super("LogoutService");
  }

  /**
   * サービス起動処理
   *
   * @param context Context
   */
  public static void startLogoutService(Context context) {
    Intent intent = new Intent(context, LogoutService.class);
    intent.setAction(ACTION_LOGOUT);
    context.startService(intent);
  }

  /**
   * Service起動時処理
   */
  @Override
  public void onCreate() {
    super.onCreate();
    preference = PreferenceManager.getDefaultSharedPreferences(this);
  }

  /** ログアウト処理. */
  private void onLogout() {
    new WebApiRequest<Logout>(Logout.class).setBootoutUrl(this, preference).execute();
  }

  /**
   * インテント処理.
   *
   * @param intent
   */
  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent == null) {
      return;
    }
    if (ACTION_LOGOUT.equals(intent.getAction())) {
      onLogout();
    }
  }
}
