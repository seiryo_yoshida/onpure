package jp.co.seiryodenki.professionalptt.core;

import android.support.annotation.NonNull;

import io.realm.Realm;
import jp.co.seiryodenki.professionalptt.Constants;
import jp.co.seiryodenki.professionalptt.model.CallInfo;
import jp.co.seiryodenki.professionalptt.model.GroupInfo;
import jp.co.seiryodenki.professionalptt.model.MyMqttMessage;
import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.util.logger;

import static jp.co.seiryodenki.professionalptt.Constants.CALL_PTTALL_ID;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_APPEND_BYE;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_CALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_CALLLIST;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_INFO;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_RESTRICT;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_SEND_MSG;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_SETTING;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_STATUS;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CONTROL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_HEADER;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_PTTALL;

public class MqttNotifier implements MqttManager.MyMqttMessageListener {

  private static final String TAG = MqttNotifier.class.getSimpleName();
  public static final int TOPIC_LENGTH = 5;
  public static final int IDX_HEAD = 0;
  public static final int IDX_COMPANY = 1;
  public static final int IDX_SENDER = 3;
  public static final int IDX_RECEIVER = 4;

  private MqttManager.OnReceiveMqttMessageListener onReceiveMqttMessageListener = null;
  private String companyId;
  private String myTerminalId;

  public MqttNotifier(@NonNull String companyId, @NonNull String myTerminalId) {
    this.companyId = companyId;
    this.myTerminalId = myTerminalId;
  }

  public void setOnReceiveMqttMessageListener(MqttManager.OnReceiveMqttMessageListener listener) {
    this.onReceiveMqttMessageListener = listener;
  }

  public void removeOnReceiveMqttMessageListener() {
    this.onReceiveMqttMessageListener = null;
  }

  @Override
  public void onReceive(@NonNull String topic, @NonNull MyMqttMessage message) {
    parseAndNotify(topic, message);
  }

  private boolean isGroupId(@NonNull String topicLevel) {
    boolean result = false;
    try (Realm realm = Realm.getDefaultInstance()) {
      GroupInfo group = GroupInfo.getGroupInfo(realm, topicLevel);
      if (group != null) {
        result = true;
      }
      return result;
    }
  }

  private boolean isMyTerminalId(@NonNull String topicLevel) {
    return topicLevel.equals(myTerminalId);
  }

  private boolean isControl(@NonNull String topicLevel) {
    return MQTT_TOPIC_CONTROL.equals(topicLevel);
  }

  private boolean isPttAll(@NonNull String topicLevel) {
    return MQTT_TOPIC_PTTALL.equals(topicLevel);
  }

  private boolean isPttAllId(@NonNull String topicLevel) {
    return CALL_PTTALL_ID.equals(topicLevel);
  }

  /**
   * compare CALLTYPE.x.toString() + MQTT_TOPIC_CMD_APPEND_BYE ex) CLL-BYE, OPS-BYE
   *
   * @param cmdBye MyMqttMessage.getCmd()
   * @return CALLTYPE
   */
  private CallType getCallTypeAppendBye(String cmdBye) {
    CallType result = null;
    if (cmdBye != null && cmdBye.length() > MQTT_TOPIC_CMD_APPEND_BYE.length()) {
      int cmdLength = cmdBye.length() - MQTT_TOPIC_CMD_APPEND_BYE.length();
      String bye = cmdBye.substring(cmdLength);
      String cmd = cmdBye.substring(0, cmdLength);
      if (MQTT_TOPIC_CMD_APPEND_BYE.equals(bye)) {
        result = CallType.getType(cmd);
      }
    }
    return result;
  }

  private synchronized void parseAndNotify(@NonNull String topic, @NonNull MyMqttMessage message) {
    String[] topicLevel = topic.split("/");
    if (topicLevel.length != TOPIC_LENGTH) {
      logIllegalTopic(topic, message);
      return;
    }
    if (topicLevel[IDX_HEAD].equals(MQTT_TOPIC_HEADER)) {
      logIllegalTopic(topic, message);
      return;
    }
    String companyId = topicLevel[IDX_COMPANY];
    if (!companyId.equals(this.companyId)) {
      logIllegalTopic(topic, message);
      return;
    }
    if (message.getCmd() == null) {
      logIllegalTopic(topic, message);
      return;
    }

    /* 4.通話呼出 */
    CallType calltype = CallType.getType(message.getCallType());
    if (calltype != null && MQTT_TOPIC_CMD_CALL.equals(message.getCmd())) {
      switch (calltype) {
        case OPERATING_STATION:
        case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
        case ALL_CALL_SYSTEM_WIDE:
          if (!isPttAllId(topicLevel[IDX_RECEIVER])) {
            logIllegalTopic(topic, message);
            return;
          }
          break;
        case GROUP_CALL_ENFORCEMENT:
        case GROUP_CALL:
        case MULTI_GROUP_CALL_ENFORCEMENT:
        case MULTI_GROUP_CALL:
          if (!isGroupId(topicLevel[IDX_RECEIVER])) {
            logIllegalTopic(topic, message);
            return;
          }
          break;
        case CALL:
        case SITE_ALL_CALL:
        case NEAR_ALL_CALL:
          if (!isMyTerminalId(topicLevel[IDX_RECEIVER])) {
            logIllegalTopic(topic, message);
            return;
          }
          break;
        case REMOTE_MONITOR:
        case EMERGENCY_CALL:
          if (!isMyTerminalId(topicLevel[IDX_RECEIVER])
              && !isGroupId(topicLevel[IDX_RECEIVER])
              && !isPttAllId(topicLevel[IDX_RECEIVER])) {
            logIllegalTopic(topic, message);
            return;
          }
          break;
      }
      if (onReceiveMqttMessageListener != null) {
        onReceiveMqttMessageListener.onIncomingCall(
            calltype, topicLevel[IDX_SENDER], topicLevel[IDX_RECEIVER], message);
      }
      return;
    }
    /* end of call from server */
    if (message.getCmd().equals(MQTT_TOPIC_CMD_STATUS)) {
      if (message.getStatus() != null) {
        if (message.getStatus().equals(Constants.MQTTSTATUS.BYE.toString())
            || message.getStatus().equals(Constants.MQTTSTATUS.BUSY.toString())) {
          if (onReceiveMqttMessageListener != null) {
            onReceiveMqttMessageListener.onClosingCall(message);
          }
        }
        return;
      }
    }

    /* 7.1.メッセージ通知 */
    if (message.getCmd().equals(MQTT_TOPIC_CMD_SEND_MSG)) {
      if (onReceiveMqttMessageListener != null) {
        onReceiveMqttMessageListener.onReceiveMessage(topicLevel[IDX_RECEIVER], message);
      }
    }
    /* 8.2.回線状態通知 */
    /* 8.4.ステータス変更通知 */
    else if (message.getCmd().equals(MQTT_TOPIC_CMD_STATUS)) {
      if (!isControl(topicLevel[IDX_SENDER])) {
        logIllegalTopic(topic, message);
        return;
      }
      if (onReceiveMqttMessageListener != null) {
        onReceiveMqttMessageListener.onReceiveCallStatus(message);
      }
    }
    /* 8.11.通話中リスト応答 */
    else if (message.getCmd().equals(MQTT_TOPIC_CMD_CALLLIST)) {
      if (!isControl(topicLevel[IDX_SENDER])
        || !isMyTerminalId(topicLevel[IDX_RECEIVER])) {
        logIllegalTopic(topic, message);
        return;
      }
      int priority = CallInfo.convertPriority(message.getPriority());
      if (onReceiveMqttMessageListener != null) {
        String[] callList = CallInfo.convertCallList(calltype, message.getCallId(), message.getCallList());
        onReceiveMqttMessageListener.onReceiveCallList(calltype,
          message.getCallId(), message.getFrom(), callList, priority, message.getStatus());
      }
    }
    /* 2.3.サーバ情報更新通知 */
    else if (message.getCmd().equals(MQTT_TOPIC_CMD_INFO)) {
      if (!isMyTerminalId(topicLevel[IDX_RECEIVER])
        || !isGroupId(topicLevel[IDX_RECEIVER])
        || !isPttAll(topicLevel[IDX_RECEIVER])
        || !isPttAllId(topicLevel[IDX_RECEIVER])) {
        logIllegalTopic(topic, message);
        return;
      }
      if (onReceiveMqttMessageListener != null) {
        onReceiveMqttMessageListener.onUpdateServerInfo();
      }
    }
  }

  private void logIllegalTopic(String topic, MyMqttMessage message) {
    // new Throwable().printStackTrace(System.out);
    logger.e(TAG, "MqttManager Illegal Topic: " + topic + " cmd: " + message.getCmd());
  }
}
