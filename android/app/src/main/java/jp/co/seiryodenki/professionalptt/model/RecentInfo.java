package jp.co.seiryodenki.professionalptt.model;

import android.support.annotation.NonNull;

import com.google.gson.reflect.TypeToken;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import jp.co.seiryodenki.professionalptt.util.GsonUtils;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class RecentInfo extends RealmObject {

  private int callReceivedMissed;
  private int callType;
  private Date date;
  private Date enddate;                  //終話時間
  private String callId;
  private String name;
  private String callListString;

  public Date getDate() {
    return date;
  }

  public String getCallId() {
    return callId;
  }

  public String getName() {
    return name;
  }

  /**
   * 発話時間を求める
   * @return 経過時間（文字列型）
   */
  public String getTimeStr()
  {
    String strTime = "00:00";

    long c_date = date.getTime();
    long c_last = enddate.getTime();

    if (c_date < c_last) {
      int c_s_time = (int)(((c_last - c_date) / 100) % 60);
      int c_m_time = (int)((((c_last - c_date) / 100) / 60) % 60);
      int c_h_time = (int)((((c_last - c_date) / 100) / 60) / 60);
      if (c_h_time >= 1) {
        strTime = String.format("%02d", c_h_time) + ":" + String.format("%02d", c_m_time) + ":" + String.format("%02d", c_s_time);
      } else {
        strTime = String.format("%02d", c_m_time) + ":" + String.format("%02d", c_s_time);
      }
    }

    return strTime;
  }

  public String[] getCallList() {
    Type listType = new TypeToken<List<String>>(){}.getType();
    List<String> list = GsonUtils.fromJson(callListString, listType);
    String[] array = list.toArray(new String[list.size()]);
    return array;
  }

  public static final int CALL = 0;
  public static final int RECEIVED = 1;
  public static final int MISSED = 2;

  public static void received(Realm realm, CallType callType, String callId, String name, String[] callList) {
    insert(realm, RECEIVED, callType, callId, name, callList);
  }

  public static void call(Realm realm, CallType callType, String callId, String name, String[] callList) {
    insert(realm, CALL, callType, callId, name, callList);
  }

  public static void missed(Realm realm, CallType callType, String callId, String name, String[] callList) {
    insert(realm, MISSED, callType, callId, name, callList);
  }

  public static void insert(
      Realm realm, int callReceivedMissed, CallType callType, String callId, String name, String[] callList) {
    realm.executeTransactionAsync(
        new Realm.Transaction() {
          private final int MAX_SIZE = 100;

          private void deleteFrom(Realm realm, int location) {
            RealmResults<RecentInfo> logs =
                realm
                    .where(RecentInfo.class)
                    .equalTo("callReceivedMissed", callReceivedMissed)
                    .sort("date", Sort.DESCENDING)
                    .findAll();
            if (logs.size() < location) {
              return;
            }
            ListIterator<RecentInfo> it = logs.listIterator(location);
            while (it.hasNext()) {
              it.next().deleteFromRealm();
            }
          }

          @Override
          public void execute(@NonNull Realm realm) {
            deleteFrom(realm, MAX_SIZE - 1);
            RecentInfo recentInfo = realm.createObject(RecentInfo.class);
            recentInfo.callReceivedMissed = callReceivedMissed;
            recentInfo.date = new Date();
            recentInfo.enddate = recentInfo.date;
            recentInfo.callType = callType.getId();
            recentInfo.callId = callId;
            recentInfo.name = name;
            List<String> list = Arrays.asList(callList);
            recentInfo.callListString = GsonUtils.toJson(list);
          }
        });
  }

  public static RealmResults<RecentInfo> findAllAsync(Realm realm, int callReceivedMissed) {
    return realm
        .where(RecentInfo.class)
        .equalTo("callReceivedMissed", callReceivedMissed)
        .sort("date", Sort.DESCENDING)
        .findAllAsync();
  }

  public CallType getCallType() {
    return CallType.getType(callType);
  }

  public int getCallReceivedMissed() {
    return callReceivedMissed;
  }

  /**
   * 終話時間をセットする
   */
  public static void setEndDate(Realm realm) {
    realm.executeTransaction(new Realm.Transaction() {
        @Override
        public void execute(Realm realm) {
          RealmResults<RecentInfo> list = realm.where(RecentInfo.class).sort("date", Sort.DESCENDING).findAll();
          if (list.isEmpty()) {
            return;
          }
          RecentInfo recent = list.first();
          if (recent.date.getTime() == recent.enddate.getTime()) {
            recent.enddate = new Date();
          }
        }
      });
  }
}
