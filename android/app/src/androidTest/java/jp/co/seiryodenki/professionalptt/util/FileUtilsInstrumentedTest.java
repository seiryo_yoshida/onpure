package jp.co.seiryodenki.professionalptt.util;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import jp.co.seiryodenki.professionalptt.R;

import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FileUtilsInstrumentedTest {
  @Test
  public void create() throws Exception {
    Context c = InstrumentationRegistry.getTargetContext();
    String filename = FileUtils.create(c, R.raw.rootca, "rootca.pem");
    assertThat(filename, endsWith("/rootca.pem"));
    assertTrue("file is created", new File(filename).exists());
  }
}
