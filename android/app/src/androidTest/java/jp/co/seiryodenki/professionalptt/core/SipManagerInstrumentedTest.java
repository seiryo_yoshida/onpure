package jp.co.seiryodenki.professionalptt.core;

import jp.co.seiryodenki.professionalptt.R;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class SipManagerInstrumentedTest {
  @Test
  public void createAndDestroy() throws Exception {
    Context c = InstrumentationRegistry.getTargetContext();
    if (SipManager.isInstantiated()) {
      SipManager.destroy();
    }
    String sipHost = c.getString(R.string.sip_host);
    assertFalse(SipManager.isInstantiated());
    assertNotNull(SipManager.create(c, "com", "user", "password"));
    SipManager sipManager = SipManager.getInstance();
    assertNotNull(sipManager);
    assertEquals("com", sipManager.getCompanyId());
    assertEquals("sip:comuser@" + sipHost, sipManager.getIdentity());
    assertTrue(SipManager.isInstantiated());
    SipManager.destroy();
    assertFalse(SipManager.isInstantiated());
  }
}
