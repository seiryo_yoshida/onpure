package jp.co.seiryodenki.professionalptt.core;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class CallManagerInstrumentedTest implements SipManager.OnCallStateErrorListener {
  static final String companyId = "480549";
  static final String terminalId = "400001";
  static final String password = "PW400001";

  private String errorMessage;

  @Test
  public void inviteSelf() throws Exception {
    Context c = InstrumentationRegistry.getTargetContext();
    if (SipManager.isInstantiated()) {
      SipManager.destroy();
    }
    SipManager.create(c, companyId, terminalId, password)
      .setOnCallStateErrorListener(this);
    CallManager.getInstance().inviteSelf();
    errorMessage = null;
    Thread.sleep(1000);
    assertNull(errorMessage);
    CallManager.destroy();
    SipManager.destroy();
  }

  @Override
  public void onCallStateError(@NonNull String message) {
    errorMessage = message;
  }
}
