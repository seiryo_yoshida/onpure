package jp.co.seiryodenki.professionalptt.util;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import jp.co.seiryodenki.professionalptt.R;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class UtilInstrumentedTest {
  @Test
  public void getTimerFormat() throws Exception {
    assertEquals("00:00:00", Util.getTimerFormat(0));
    assertEquals("00:00:00", Util.getTimerFormat(999));
    assertEquals("00:00:01", Util.getTimerFormat(1000));
    assertEquals("00:01:00", Util.getTimerFormat(60 * 1000));
    assertEquals("01:00:00", Util.getTimerFormat(60 * 60 * 1000));
    assertEquals("24:00:00", Util.getTimerFormat(24 * 60 * 60 * 1000));
  }
}
