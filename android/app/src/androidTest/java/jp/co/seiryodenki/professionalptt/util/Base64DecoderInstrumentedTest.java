package jp.co.seiryodenki.professionalptt.util;

import static org.junit.Assert.*;

import android.support.test.runner.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class Base64DecoderInstrumentedTest {
  @Test
  public void decode_isCorrect() throws Exception {
    assertEquals("a", Base64Decoder.decode("YQ=="));
    assertEquals("a\n", Base64Decoder.decode("YQo="));
    assertEquals("abc", Base64Decoder.decode("YWJj"));
  }
  public void decode_isCorrupted() throws Exception {
    assertNull(Base64Decoder.decode("YQ"));
  }

  public void decode_isNull() throws Exception {
    assertNull(Base64Decoder.decode(null));
  }

  public void decode_isEmpty() throws Exception {
    assertEquals("", Base64Decoder.decode(""));
  }
}
