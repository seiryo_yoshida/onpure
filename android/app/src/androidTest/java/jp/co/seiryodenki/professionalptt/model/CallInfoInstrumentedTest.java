package jp.co.seiryodenki.professionalptt.model;

import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class CallInfoInstrumentedTest {
  @Test
  public void checkIntent() {
    CallInfo callInfo = new CallInfo(CallType.CALL,
      "400003",
      "400003",
      new String[]{"400001","400003"},
      1);

    Intent intent = new Intent();
    callInfo.putIntent(intent);

    CallInfo intentCallInfo = CallInfo.getCallInfo(intent);
    assertNotNull(intentCallInfo);
    assertEquals(callInfo.getCallType(), intentCallInfo.getCallType());
    assertEquals(callInfo.getCallId(), intentCallInfo.getCallId());
    assertEquals(callInfo.getCallList(), intentCallInfo.getCallList());
    assertEquals(callInfo.getCallFrom(), intentCallInfo.getCallFrom());
    assertEquals(callInfo.getPriority(), intentCallInfo.getPriority());
  }
}
