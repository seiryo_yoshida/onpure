package jp.co.seiryodenki.professionalptt.core;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import jp.co.seiryodenki.professionalptt.model.CallType;
import jp.co.seiryodenki.professionalptt.model.MyMqttMessage;

import static jp.co.seiryodenki.professionalptt.Constants.CALL_PTTALL_ID;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CMD_CALL;
import static jp.co.seiryodenki.professionalptt.Constants.MQTT_TOPIC_CONTROL;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MqttNotifierInstrumentedTest {
  @Test
  public void checkRequestMqttCall() {
    String companyId = "480549";
    String terminalId = "400001";
    String callIdCLL = "400002";
    String callIdFGC = "20001";
    String callIdFMG = "800001";
    String[] callList = {"20001","20002"};
    String callIdSAL = "800002";
    String callIdNAL = "800003";
    String callIdEMC = "800004";
    String callIdRTM = "800005";

    MqttNotifier mqttNotifier = new MqttNotifier(companyId, terminalId);
    MqttManager.OnReceiveMqttMessageListener listener = new MqttManager.OnReceiveMqttMessageListener() {
      @Override
      public void onIncomingCall(CallType calltype, String sender, String receiver, MyMqttMessage mqttMessage) {
        if (MQTT_TOPIC_CMD_CALL.equals(mqttMessage.getCmd())) {
          switch (calltype) {
            case CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdCLL);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, terminalId);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case OPERATING_STATION:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), CALL_PTTALL_ID);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, CALL_PTTALL_ID);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case ALL_CALL_SYSTEM_WIDE_ENFORCEMENT:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), CALL_PTTALL_ID);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, CALL_PTTALL_ID);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case ALL_CALL_SYSTEM_WIDE:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), CALL_PTTALL_ID);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, CALL_PTTALL_ID);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case GROUP_CALL_ENFORCEMENT:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdFGC);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, callIdFGC);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case GROUP_CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdFGC);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, callIdFGC);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case MULTI_GROUP_CALL_ENFORCEMENT:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdFMG);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, callIdFGC);
              assertEquals(mqttMessage.getFrom(), terminalId);
              assertEquals(mqttMessage.getCallList(), Arrays.asList(callList));
              break;
            case MULTI_GROUP_CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdFMG);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, callIdFGC);
              assertEquals(mqttMessage.getFrom(), terminalId);
              assertEquals(mqttMessage.getCallList(), Arrays.asList(callList));
              break;
            case SITE_ALL_CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdSAL);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, terminalId);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case NEAR_ALL_CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdNAL);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, terminalId);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case EMERGENCY_CALL:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdEMC);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, terminalId);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
            case REMOTE_MONITOR:
              System.out.println("incoming " + calltype.toString());
              assertEquals(mqttMessage.getCallId(), callIdRTM);
              assertEquals(sender, MQTT_TOPIC_CONTROL);
              assertEquals(receiver, terminalId);
              assertEquals(mqttMessage.getFrom(), terminalId);
              break;
          }
        }
      }

      @Override
      public void onClosingCall(MyMqttMessage mqttMessage) {

      }

      @Override
      public void onUpdateServerInfo(String companyId, String terminalId) {

      }

      @Override
      public void onReceiveMessage(String receiver, MyMqttMessage mqttMessage) {

      }

      @Override
      public void onReceiveCallStatus(MyMqttMessage mqttMessage) {

      }

      @Override
      public void onReceiveCallList(CallType callType, String callId, String callFrom, String[] callList, int priority, String status) {

      }
    };
    mqttNotifier.setOnReceiveMqttMessageListener(listener);
    String topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, terminalId);
    MyMqttMessage message = new MyMqttMessage();

    // CLL
    message.setCmd(MQTT_TOPIC_CMD_CALL);
    message.setCallType(CallType.CALL.toString());
    message.setFrom(terminalId);
    message.setCallId(callIdCLL);
    message.setPriority(1);
    mqttNotifier.onReceive(topic, message);

    // CMD
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, CALL_PTTALL_ID);
    message.setCallType(CallType.OPERATING_STATION.toString());
    message.setCallId(CALL_PTTALL_ID);
    mqttNotifier.onReceive(topic, message);

    // HHH
    message.setCallType(CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT.toString());
    mqttNotifier.onReceive(topic, message);

    // ACS
    message.setCallType(CallType.ALL_CALL_SYSTEM_WIDE.toString());
    mqttNotifier.onReceive(topic, message);

    // FGC
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, callIdFGC);
    message.setCallType(CallType.GROUP_CALL_ENFORCEMENT.toString());
    message.setCallId(callIdFGC);
    mqttNotifier.onReceive(topic, message);

    // GPC
    message.setCallType(CallType.GROUP_CALL.toString());
    mqttNotifier.onReceive(topic, message);

    // FMG
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, callIdFGC);
    message.setCallType(CallType.MULTI_GROUP_CALL_ENFORCEMENT.toString());
    message.setCallId(callIdFMG);
    message.setCallList(Arrays.asList(callList));
    mqttNotifier.onReceive(topic, message);

    // MGC
    message.setCallType(CallType.MULTI_GROUP_CALL.toString());
    mqttNotifier.onReceive(topic, message);

    // SAL
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, terminalId);
    message.setCallType(CallType.SITE_ALL_CALL.toString());
    message.setCallId(callIdSAL);
    message.setCallList(null);
    mqttNotifier.onReceive(topic, message);

    // NAL
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, terminalId);
    message.setCallType(CallType.NEAR_ALL_CALL.toString());
    message.setCallId(callIdNAL);
    mqttNotifier.onReceive(topic, message);

    // EMC
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, terminalId);
    message.setCallType(CallType.EMERGENCY_CALL.toString());
    message.setCallId(callIdEMC);
    mqttNotifier.onReceive(topic, message);

    // RTM
    topic = String.format("secptt/%s/CMD/CONTROL/%s", companyId, terminalId);
    message.setCallType(CallType.REMOTE_MONITOR.toString());
    message.setCallId(callIdRTM);
    mqttNotifier.onReceive(topic, message);
  }
}
