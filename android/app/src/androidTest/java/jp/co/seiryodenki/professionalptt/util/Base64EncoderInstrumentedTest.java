package jp.co.seiryodenki.professionalptt.util;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(AndroidJUnit4.class)
public class Base64EncoderInstrumentedTest {
  @Test
  public void decode_isCorrect() throws Exception {
    assertEquals("YQ==", Base64Encoder.encode("a"));
    assertEquals("YQo=", Base64Encoder.encode("a\n"));
    assertEquals("YWJj", Base64Encoder.encode("abc"));
  }

  public void decode_isNull() throws Exception {
    assertNull(Base64Encoder.encode(null));
  }

  public void decode_isEmpty() throws Exception {
    assertEquals("", Base64Encoder.encode(""));
  }
}
