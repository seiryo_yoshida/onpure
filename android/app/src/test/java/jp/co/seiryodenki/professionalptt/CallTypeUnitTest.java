package jp.co.seiryodenki.professionalptt;

import jp.co.seiryodenki.professionalptt.model.CallType;

import static org.junit.Assert.*;

import org.junit.Test;

public class CallTypeUnitTest {
  @Test
  public void checkGetTypeInt() {
    assertEquals(CallType.OPERATING_STATION, CallType.getType(1));
    assertEquals(CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT, CallType.getType(2));
    assertEquals(CallType.ALL_CALL_SYSTEM_WIDE, CallType.getType(3));
    assertEquals(CallType.GROUP_CALL_ENFORCEMENT, CallType.getType(4));
    assertEquals(CallType.GROUP_CALL, CallType.getType(5));
    assertEquals(CallType.MULTI_GROUP_CALL_ENFORCEMENT, CallType.getType(6));
    assertEquals(CallType.MULTI_GROUP_CALL, CallType.getType(7));
    assertEquals(CallType.CALL, CallType.getType(8));
    assertEquals(CallType.SITE_ALL_CALL, CallType.getType(9));
    assertEquals(CallType.NEAR_ALL_CALL, CallType.getType(10));
    assertEquals(CallType.EMERGENCY_CALL, CallType.getType(11));
    assertEquals(CallType.REMOTE_MONITOR, CallType.getType(12));

    assertEquals(CallType.DEFAULT_CALL_TYPE, CallType.getType(0));
    assertEquals(CallType.DEFAULT_CALL_TYPE, CallType.getType(13));
  }

  @Test
  public void checkGetTypeString() {
    assertEquals(CallType.OPERATING_STATION, CallType.getType("CMD"));
    assertEquals(CallType.ALL_CALL_SYSTEM_WIDE_ENFORCEMENT, CallType.getType("HHH"));
    assertEquals(CallType.ALL_CALL_SYSTEM_WIDE, CallType.getType("ACS"));
    assertEquals(CallType.GROUP_CALL_ENFORCEMENT, CallType.getType("FGC"));
    assertEquals(CallType.GROUP_CALL, CallType.getType("GPC"));
    assertEquals(CallType.MULTI_GROUP_CALL_ENFORCEMENT, CallType.getType("FMG"));
    assertEquals(CallType.MULTI_GROUP_CALL, CallType.getType("MGP"));
    assertEquals(CallType.CALL, CallType.getType("CLL"));
    assertEquals(CallType.SITE_ALL_CALL, CallType.getType("SAL"));
    assertEquals(CallType.NEAR_ALL_CALL, CallType.getType("NAL"));
    assertEquals(CallType.EMERGENCY_CALL, CallType.getType("EMC"));
    assertEquals(CallType.REMOTE_MONITOR, CallType.getType("RTM"));

    assertEquals(null, CallType.getType(null));
    assertEquals(null, CallType.getType(""));
    assertEquals(null, CallType.getType("NON"));
  }
}
